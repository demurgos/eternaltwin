import {SEARCH_PARAMS_READER} from "kryo-search-params/search-params-reader";
import {SEARCH_PARAMS_ROOT_READER} from "kryo-search-params/search-params-root-reader";
import {SEARCH_PARAMS_WRITER} from "kryo-search-params/search-params-writer";
import {registerErrMochaTests, registerMochaSuites, TestItem} from "kryo-testing";

import {Url} from "../../lib/core/url.mjs";
import {$OauthAuthorizationRequest, OauthAuthorizationRequest} from "../../lib/oauth/oauth-authorization-request.mjs";
import {OauthResponseType} from "../../lib/oauth/oauth-response-type.mjs";
import {registerJsonIoTests} from "../helpers.mjs";

describe("OauthAuthorizationRequest", function () {
  const items: TestItem<OauthAuthorizationRequest>[] = [
    {
      name: "Local Hammerfest user",
      value: {
        responseType: OauthResponseType.Code,
        clientId: "eternalfest",
        redirectUri: new Url("http://localhost:50313/oauth/callback"),
        scope: "",
        state: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemF0aW9uU2VydmVyIjoiZXRlcm5hbC10d2luLm5ldCIsInJlcXVlc3RGb3JnZXJ5UHJvdGVjdGlvbiI6Ijc4Nzg4NWIzNDM0YTAzZTlkMjJjZDcyZWFjZWU1ZjQxIiwiaWF0IjoxNTg4ODcwNjMyLCJleHAiOjE1ODg5NTcwMzJ9.bZe_x0elHyaZsS0HL7AcPIN_27V3iWv-DQKTec85IQc",
      },
      io: [
        {
          writer: SEARCH_PARAMS_WRITER,
          reader: SEARCH_PARAMS_READER,
          raw: "client_id=eternalfest&redirect_uri=http%3A%2F%2Flocalhost%3A50313%2Foauth%2Fcallback&response_type=code&scope=&state=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemF0aW9uU2VydmVyIjoiZXRlcm5hbC10d2luLm5ldCIsInJlcXVlc3RGb3JnZXJ5UHJvdGVjdGlvbiI6Ijc4Nzg4NWIzNDM0YTAzZTlkMjJjZDcyZWFjZWU1ZjQxIiwiaWF0IjoxNTg4ODcwNjMyLCJleHAiOjE1ODg5NTcwMzJ9.bZe_x0elHyaZsS0HL7AcPIN_27V3iWv-DQKTec85IQc",
        },
        {
          reader: SEARCH_PARAMS_READER,
          raw: "response_type=code&client_id=eternalfest&redirect_uri=http%3A%2F%2Flocalhost%3A50313%2Foauth%2Fcallback&scope=&state=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemF0aW9uU2VydmVyIjoiZXRlcm5hbC10d2luLm5ldCIsInJlcXVlc3RGb3JnZXJ5UHJvdGVjdGlvbiI6Ijc4Nzg4NWIzNDM0YTAzZTlkMjJjZDcyZWFjZWU1ZjQxIiwiaWF0IjoxNTg4ODcwNjMyLCJleHAiOjE1ODg5NTcwMzJ9.bZe_x0elHyaZsS0HL7AcPIN_27V3iWv-DQKTec85IQc",
        },
        {
          reader: SEARCH_PARAMS_READER,
          raw: "response_type=code&client_id=eternalfest&redirect_uri=http%3A%2F%2Flocalhost%3A50313%2Foauth%2Fcallback&scope=&state=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemF0aW9uU2VydmVyIjoiZXRlcm5hbC10d2luLm5ldCIsInJlcXVlc3RGb3JnZXJ5UHJvdGVjdGlvbiI6Ijc4Nzg4NWIzNDM0YTAzZTlkMjJjZDcyZWFjZWU1ZjQxIiwiaWF0IjoxNTg4ODcwNjMyLCJleHAiOjE1ODg5NTcwMzJ9.bZe_x0elHyaZsS0HL7AcPIN_27V3iWv-DQKTec85IQc&access_type=offline",
        },
        {
          reader: SEARCH_PARAMS_ROOT_READER,
          raw: new URLSearchParams({
            response_type: "code",
            client_id: "eternalfest",
            redirect_uri: "http://localhost:50313/oauth/callback",
            scope: "",
            state: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemF0aW9uU2VydmVyIjoiZXRlcm5hbC10d2luLm5ldCIsInJlcXVlc3RGb3JnZXJ5UHJvdGVjdGlvbiI6Ijc4Nzg4NWIzNDM0YTAzZTlkMjJjZDcyZWFjZWU1ZjQxIiwiaWF0IjoxNTg4ODcwNjMyLCJleHAiOjE1ODg5NTcwMzJ9.bZe_x0elHyaZsS0HL7AcPIN_27V3iWv-DQKTec85IQc",
            access_type: "offline",
          }),
        },
      ],
    },
  ];

  registerMochaSuites($OauthAuthorizationRequest, items);

  describe("Reader", function () {
    const invalids: string[] = [
      "",
    ];
    registerErrMochaTests(SEARCH_PARAMS_READER, $OauthAuthorizationRequest, invalids);
  });

  registerJsonIoTests(
    $OauthAuthorizationRequest,
    "core/oauth/oauth-authorization-request",
    new Map([
      [
        "eternalfest",
        {
          responseType: OauthResponseType.Code,
          clientId: "eternalfest",
          redirectUri: new Url("http://localhost:50313/oauth/callback"),
          scope: "",
          state: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemF0aW9uU2VydmVyIjoiZXRlcm5hbC10d2luLm5ldCIsInJlcXVlc3RGb3JnZXJ5UHJvdGVjdGlvbiI6Ijc4Nzg4NWIzNDM0YTAzZTlkMjJjZDcyZWFjZWU1ZjQxIiwiaWF0IjoxNTg4ODcwNjMyLCJleHAiOjE1ODg5NTcwMzJ9.bZe_x0elHyaZsS0HL7AcPIN_27V3iWv-DQKTec85IQc",
        },
      ],
    ])
  );
});
