import { LiteralUnionType } from "kryo/literal-union";
import { $Ucs2String } from "kryo/ucs2-string";

/**
 * A Dinoparc dinoz race.
 */
export type DinoparcDinozRace =
  "Cargou"
  | "Castivore"
  | "Gluon"
  | "Gorilloz"
  | "Hippoclamp"
  | "Kabuki"
  | "Korgon"
  | "Kump"
  | "Moueffe"
  | "Ouistiti"
  | "Picori"
  | "Pigmou"
  | "Pteroz"
  | "Rokky"
  | "Santaz"
  | "Serpantin"
  | "Sirain"
  | "Wanwan"
  | "Winks";

export const $DinoparcDinozRace: LiteralUnionType<DinoparcDinozRace> = new LiteralUnionType({
  type: $Ucs2String,
  values: [
    "Cargou",
    "Castivore",
    "Gluon",
    "Gorilloz",
    "Hippoclamp",
    "Kabuki",
    "Korgon",
    "Kump",
    "Moueffe",
    "Ouistiti",
    "Picori",
    "Pigmou",
    "Pteroz",
    "Rokky",
    "Santaz",
    "Serpantin",
    "Sirain",
    "Wanwan",
    "Winks",
  ]
});
