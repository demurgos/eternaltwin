import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import {$OutboundEmailId, OutboundEmailId} from "./outbound-email-id.mjs";

export interface OutboundEmail {
  id: OutboundEmailId;
}

export const $OutboundEmail: RecordIoType<OutboundEmail> = new RecordType<OutboundEmail>({
  properties: {
    id: {type: $OutboundEmailId},
  },
  changeCase: CaseStyle.SnakeCase,
});
