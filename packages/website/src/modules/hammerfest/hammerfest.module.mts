import {NgModule} from "@angular/core";

import {HammerfestService} from "./hammerfest.service.mjs";
import {HammerfestItemNamePipe} from "./hammerfest-item-name.pipe.mjs";

@NgModule({
  providers: [
    HammerfestService
  ],
  imports: [],
  declarations: [HammerfestItemNamePipe],
  exports: [HammerfestItemNamePipe],
})
export class HammerfestModule {
}
