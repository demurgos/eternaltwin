import { Injectable } from "@angular/core";
import { makeStateKey, StateKey } from "@angular/platform-browser";
import { IoType } from "kryo";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";
import { Observable } from "rxjs";

export interface SimpleRequestOptions<Res> {
  queryType?: undefined,
  query?: undefined,
  reqType?: undefined,
  req?: undefined,
  resType: IoType<Res>,
}

export interface QueryRequestOptions<Query, Res> {
  queryType: IoType<Query>,
  query: Query,
  reqType?: undefined,
  req?: undefined,
  resType: IoType<Res>,
}

export interface BodyRequestOptions<Req, Res> {
  queryType?: undefined,
  query?: undefined,
  reqType: IoType<Req>,
  req: Req,
  resType: IoType<Res>,
}

export interface CompleteRequestOptions<Query, Req, Res> {
  queryType: IoType<Query>,
  query: Query,
  reqType: IoType<Req>,
  req: Req,
  resType: IoType<Res>,
}

export type RequestOptions<Query, Req, Res> =
  SimpleRequestOptions<Res>
  | QueryRequestOptions<Query, Res>
  | BodyRequestOptions<Req, Res>
  | CompleteRequestOptions<Query, Req, Res>;

@Injectable()
export abstract class RestService {
  public abstract delete<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res>;

  public abstract get<Query, Res>(route: readonly string[], options: SimpleRequestOptions<Res> | QueryRequestOptions<Query, Res>): Observable<Res>;

  public abstract patch<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res>;

  public abstract post<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res>;

  public abstract put<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res>;
}

export type GetInput<Query> = {
  readonly route: readonly string[],
  readonly queryType?: undefined,
  readonly query?: undefined,
} | {
  readonly route: readonly string[],
  readonly queryType: IoType<Query>,
  readonly query: Query,
};

export function toTransferStateKey<Query>(input: GetInput<Query>): StateKey<unknown> {
  const path = input.route.map(encodeURIComponent).join("/");
  const query: string | undefined = input.queryType !== undefined ? input.queryType.write(JSON_VALUE_WRITER, input.query) : undefined;
  const key = JSON.stringify([path, query]);
  return makeStateKey(key);
}
