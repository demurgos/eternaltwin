import { NgModule } from "@angular/core";

import { JobService } from "./job.service.mjs";

@NgModule({
  imports: [],
  providers: [
    JobService,
  ],
})
export class JobModule {
}
