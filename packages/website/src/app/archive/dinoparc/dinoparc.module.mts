import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { SharedModule } from "../../shared/shared.module.mjs";
import { DinoparcDinozComponent } from "./dinoparc-dinoz.component.mjs";
import { DinoparcDinozView } from "./dinoparc-dinoz.view.mjs";
import { DinoparcHomeView } from "./dinoparc-home.view.mjs";
import { DinoparcRoutingModule } from "./dinoparc-routing.module.mjs";
import { DinoparcUserComponent } from "./dinoparc-user.component.mjs";
import { DinoparcUserView } from "./dinoparc-user.view.mjs";

@NgModule({
  declarations: [DinoparcDinozComponent, DinoparcDinozView, DinoparcHomeView, DinoparcUserComponent, DinoparcUserView],
  imports: [
    CommonModule,
    DinoparcRoutingModule,
    SharedModule,
  ],
})
export class DinoparcModule {
}
