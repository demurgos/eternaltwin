import {Injectable, NgModule} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes} from "@angular/router";
import {$DinoparcDinozId, DinoparcDinozId} from "@eternaltwin/core/dinoparc/dinoparc-dinoz-id";
import {$DinoparcServer, DinoparcServer} from "@eternaltwin/core/dinoparc/dinoparc-server";
import {$DinoparcUserId, DinoparcUserId} from "@eternaltwin/core/dinoparc/dinoparc-user-id";
import {NullableEtwinDinoparcDinoz} from "@eternaltwin/core/dinoparc/etwin-dinoparc-dinoz";
import {NullableEtwinDinoparcUser} from "@eternaltwin/core/dinoparc/etwin-dinoparc-user";
import {CheckId, NOOP_CONTEXT, Result} from "kryo";
import {$Date} from "kryo/date";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";
import {Observable, of as rxOf} from "rxjs";

import {DinoparcService} from "../../../modules/dinoparc/dinoparc.service.mjs";
import {DinoparcDinozView} from "./dinoparc-dinoz.view.mjs";
import {DinoparcHomeView} from "./dinoparc-home.view.mjs";
import {DinoparcUserView} from "./dinoparc-user.view.mjs";

@Injectable()
export class DinoparcUserResolverService implements Resolve<NullableEtwinDinoparcUser> {
  readonly #router: Router;
  readonly #dinoparc: DinoparcService;

  constructor(router: Router, dinoparc: DinoparcService) {
    this.#router = router;
    this.#dinoparc = dinoparc;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<NullableEtwinDinoparcUser> {
    const {
      ok: okServer,
      value: server
    }: Result<DinoparcServer, CheckId> = $DinoparcServer.test(NOOP_CONTEXT, route.paramMap.get("server"));
    const {
      ok: okUserId,
      value: userId
    }: Result<DinoparcUserId, CheckId> = $DinoparcUserId.test(NOOP_CONTEXT, route.paramMap.get("user_id"));
    const {
      ok: okTime,
      value: rawTime
    }: Result<Date, CheckId> = $Date.read(NOOP_CONTEXT, SEARCH_PARAMS_VALUE_READER, route.paramMap.get("time"));
    if (!okServer || !okUserId) {
      return rxOf(null);
    }
    const time: Date | undefined = okTime ? rawTime : undefined;
    return this.#dinoparc.getUser({server, id: userId, time});
  }
}

@Injectable()
export class DinoparcDinozResolverService implements Resolve<NullableEtwinDinoparcDinoz> {
  readonly #router: Router;
  readonly #dinoparc: DinoparcService;

  constructor(router: Router, dinoparc: DinoparcService) {
    this.#router = router;
    this.#dinoparc = dinoparc;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<NullableEtwinDinoparcDinoz> {
    const {
      ok: okServer,
      value: server
    }: Result<DinoparcServer, CheckId> = $DinoparcServer.test(NOOP_CONTEXT, route.paramMap.get("server"));
    const {
      ok: okUserId,
      value: dinozId
    }: Result<DinoparcDinozId, CheckId> = $DinoparcDinozId.test(NOOP_CONTEXT, route.paramMap.get("dinoz_id"));
    const {
      ok: okTime,
      value: rawTime
    }: Result<Date, CheckId> = $Date.read(NOOP_CONTEXT, SEARCH_PARAMS_VALUE_READER, route.paramMap.get("time"));
    if (!okServer || !okUserId) {
      return rxOf(null);
    }
    const time: Date | undefined = okTime ? rawTime : undefined;
    return this.#dinoparc.getDinoz({server, id: dinozId, time});
  }
}

const routes: Routes = [
  {
    path: "",
    component: DinoparcHomeView,
    resolve: {},
  },
  {
    path: ":server/users/:user_id",
    component: DinoparcUserView,
    resolve: {
      user: DinoparcUserResolverService,
    },
  },
  {
    path: ":server/dinoz/:dinoz_id",
    component: DinoparcDinozView,
    resolve: {
      dinoz: DinoparcDinozResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [DinoparcDinozResolverService, DinoparcUserResolverService],
})
export class DinoparcRoutingModule {
}
