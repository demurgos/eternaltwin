import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MarktwinModule } from "../../modules/marktwin/marktwin.module.mjs";
import { SharedModule } from "../shared/shared.module.mjs";
import { AddModeratorComponent } from "./add-moderator.component.mjs";
import { DeleteModeratorComponent } from "./delete-moderator.component.mjs";
import { ForumActorComponent } from "./forum-actor.component.mjs";
import { ForumHomeComponent } from "./forum-home.component.mjs";
import { ForumRoutingModule } from "./forum-routing.module.mjs";
import { ForumSectionComponent } from "./forum-section.component.mjs";
import { ForumThreadComponent } from "./forum-thread.component.mjs";
import { NewForumPostComponent } from "./new-forum-post.component.mjs";
import { NewForumThreadComponent } from "./new-forum-thread.component.mjs";
import { ShortForumPostComponent } from "./short-post/short-forum-post.component.mjs";
import { UpdateForumPostComponent } from "./update-forum-post.component.mjs";
import { UserInputComponent } from "./user-input.component.mjs";

@NgModule({
  declarations: [
    AddModeratorComponent,
    DeleteModeratorComponent,
    ForumActorComponent,
    ForumHomeComponent,
    ForumThreadComponent,
    ForumSectionComponent,
    NewForumPostComponent,
    NewForumThreadComponent,
    ShortForumPostComponent,
    UpdateForumPostComponent,
    UserInputComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ForumRoutingModule,
    MarktwinModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class ForumModule {
}
