import { Component } from "@angular/core";

@Component({
  selector: "et-admin-view",
  template:
`<etwin-main-layout>
  <h1>Admin</h1>
  <a routerLink="/docs" class="button">Documentation</a>
  <ul>
    <li><a routerLink="./jobs" class="button">Jobs</a></li>
    <li><a routerLink="./get-outbound-emails" class="button">Outbound emails</a></li>
    <li><a routerLink="./get-outbound-email-requests" class="button">Outbound email requests</a></li>
    <li><a routerLink="./send-marktwin-email" class="button">Send Marktwin Email</a></li>
  </ul>
</etwin-main-layout>
`,
  styleUrls: [],
})
export class AdminViewComponent {
  constructor() {}
}
