import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {JobModule} from "../../modules/job/job.module.mjs";
import {MailerModule} from "../../modules/mailer/mailer.module.mjs";
import {SharedModule} from "../shared/shared.module.mjs";
import {AdminRoutingModule} from "./admin-routing.module.mjs";
import {AdminViewComponent} from "./admin-view.component.mjs";
import {JobsViewComponent} from "./jobs-view.component.mjs";
import {OutboundEmailRequestsViewComponent} from "./outbound-email-requests-view.component.mjs";
import {OutboundEmailsViewComponent} from "./outbound-emails-view.component.mjs";
import {SendMarktwinEmailViewComponent} from "./send-marktwin-email-view.component.mjs";

@NgModule({
  declarations: [
    AdminViewComponent,
    OutboundEmailsViewComponent,
    OutboundEmailRequestsViewComponent,
    SendMarktwinEmailViewComponent,
    JobsViewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    JobModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    MailerModule,
    SharedModule,
  ],
})
export class AdminModule {
}
