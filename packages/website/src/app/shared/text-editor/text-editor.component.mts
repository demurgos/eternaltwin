import { Component, Input } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { $MarktwinText, MarktwinText } from "@eternaltwin/core/core/marktwin-text";
import { Grammar } from "@eternaltwin/marktwin/grammar";

import { MarktwinService } from "../../../modules/marktwin/marktwin.service.mjs";

const DEFAULT_GRAMMAR: Grammar = {
  admin: false,
  depth: 4,
  emphasis: true,
  icons: [],
  links: ["http", "https"],
  mod: false,
  quote: false,
  spoiler: true,
  strikethrough: true,
  strong: true,
};

@Component({
  selector: "text-editor",
  templateUrl: "./text-editor.component.html",
  styleUrls: ["text-editor.component.scss"],
})
export class TextEditorComponent {

  readonly #marktwin: MarktwinService;

  public extendedEditor: boolean;
  public preview: string | null;

  @Input() public formGroup?: FormGroup;
  @Input() public formControlName: string = "body";

  constructor(
    marktwin: MarktwinService,
  ) {
    this.#marktwin = marktwin;
    this.extendedEditor = false;
    this.preview = null;
  }


  public toggleEmphasis(textArea: HTMLTextAreaElement) {
    const text: string = this.formGroup?.controls[this.formControlName].value;
    const start: number = textArea.selectionStart ?? 0;
    const end: number = textArea.selectionEnd ?? 0;

    const updated = this.#marktwin.toggleEmphasis(DEFAULT_GRAMMAR, text, {start, end});

    this.formGroup?.controls[this.formControlName]?.setValue(updated.text);
    textArea.selectionStart = updated.range.start;
    textArea.selectionEnd = updated.range.end;

    textArea.focus();
  }

  public toggleStrong(textArea: HTMLTextAreaElement) {
    const text: string = this.formGroup?.controls[this.formControlName].value;
    const start: number = textArea.selectionStart ?? 0;
    const end: number = textArea.selectionEnd ?? 0;

    const updated = this.#marktwin.toggleStrong(DEFAULT_GRAMMAR, text, {start, end});

    this.formGroup?.controls[this.formControlName]?.setValue(updated.text);
    textArea.selectionStart = updated.range.start;
    textArea.selectionEnd = updated.range.end;

    textArea.focus();
  }

  public toggleStrikethrough(textArea: HTMLTextAreaElement) {
    const text: string = this.formGroup?.controls[this.formControlName].value;
    const start: number = textArea.selectionStart ?? 0;
    const end: number = textArea.selectionEnd ?? 0;

    const updated = this.#marktwin.toggleStrikethrough(DEFAULT_GRAMMAR, text, {start, end});

    this.formGroup?.controls[this.formControlName]?.setValue(updated.text);
    textArea.selectionStart = updated.range.start;
    textArea.selectionEnd = updated.range.end;

    textArea.focus();
  }

  public toggleSpoiler(textArea: HTMLTextAreaElement) {
    const text: string = this.formGroup?.controls[this.formControlName].value;
    const start: number = textArea.selectionStart ?? 0;
    const end: number = textArea.selectionEnd ?? 0;

    const updated = this.#marktwin.toggleSpoiler(DEFAULT_GRAMMAR, text, {start, end});

    this.formGroup?.controls[this.formControlName]?.setValue(updated.text);
    textArea.selectionStart = updated.range.start;
    textArea.selectionEnd = updated.range.end;

    textArea.focus();
  }

  public togglePreview() {
    if (this.preview === null) {
      this.preview = this.#marktwin.renderMarktwin(DEFAULT_GRAMMAR, this.formGroup?.controls[this.formControlName].value);
    } else {
      this.preview = null;
    }
  }

  public toggleExtendedEditor()
  {
    this.extendedEditor = !this.extendedEditor;
  }
}
