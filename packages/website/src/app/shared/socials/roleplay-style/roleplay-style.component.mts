import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";

@Component({
  selector: "roleplay-style",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./roleplay-style.component.html",
  styleUrls: ["./roleplay-style.component.scss"],
})
export class RoleplayStyleComponent {
  @Input() author: string = "";
}