import {getExecutableUri} from "@eternaltwin/exe";
import chai from "chai";

import {Sdk} from "../lib/index.mjs";
// import {findExe, readLines, readUtf8} from "../lib/index.mjs";

describe("findExe", function () {
  it("finds the local dev exe", async function () {
    const exe = await getExecutableUri();
    const sdk = await Sdk.fromExe(exe);
    const actual = await sdk.getVersion();
    const expected = "0.12.5";
    chai.assert.strictEqual(actual, expected);
  });

  it("starts the server", async function () {
    const exe = await getExecutableUri();
    const sdk = await Sdk.fromExe(exe);
    const server = await sdk.startServer();
    try {
      const actual = server.getPort();
      const expected = "0.12.5";
      chai.assert.strictEqual(actual, expected);
    } finally {
      await server.stop();
    }
  });

  // it("reads lines", async function () {
  //   async function* getChunks() {
  //     yield Buffer.from("abc\n");
  //     yield Buffer.from("d");
  //     yield Buffer.from("e");
  //     yield Buffer.from("f\ngh\nij\nkl");
  //   }
  //   const text = readUtf8(getChunks());
  //   const lines = readLines(text);
  //   const out = [];
  //   for await (const line of lines) {
  //     out.push(line);
  //   }
  //   chai.assert.deepEqual(out, ["abc\n", "def\n", "gh\n", "ij\n", "kl"]);
  // });
});
