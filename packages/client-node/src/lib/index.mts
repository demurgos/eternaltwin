import {Axios, AxiosResponse} from "axios";
import {readOrThrow} from "kryo";
import {JSON_READER} from "kryo-json/json-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {SEARCH_PARAMS_WRITER} from "kryo-search-params/search-params-writer";
import {firstValueFrom as rxFirstValueFrom, from as rxFrom, Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";
import superagent from "superagent";
import urlJoin from "url-join";

import {HttpHandler, HttpMethod, RequestOptions} from "./http-handler.mjs";
import * as getAuthSelf from "./query/get-auth-self.mjs";
import * as getClock from "./query/get-clock.mjs";
import * as getUserById from "./query/get-user-by-id.mjs";
import {RequestType} from "./request-type.mjs";

export type Agent = superagent.SuperAgent<superagent.SuperAgentRequest>;

export interface Request {
  readonly type: RequestType,
}

export interface RequestToResponse {
  readonly [getClock.TYPE]: getClock.Response;
  readonly [getAuthSelf.TYPE]: getAuthSelf.Response;
  readonly [getUserById.TYPE]: getUserById.Response;
}

export class EternaltwinNodeClient {
  readonly #httpHandler: HttpHandler;

  public constructor(apiBase: URL) {
    const agent = new Axios({});
    this.#httpHandler = EternaltwinNodeClient.#makeHttpHandler(agent, apiBase);
  }

  public getAuthSelf(query: Omit<getAuthSelf.Request, "type">): Promise<getAuthSelf.Response> {
    return rxFirstValueFrom(getAuthSelf.handle(this.#httpHandler, {...query, type: RequestType.GetAuthSelf}));
  }

  public getUserById(query: Omit<getUserById.Request, "type">): Promise<getUserById.Response> {
    return rxFirstValueFrom(getUserById.handle(this.#httpHandler, {...query, type: RequestType.GetUserById}));
  }

  static #makeHttpHandler(agent: Axios, apiBase: URL): HttpHandler {
    return <Query, Req, Res>(method: HttpMethod, route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> => {
      const uri = resolveRoute(apiBase, ["api", "v1", ...route]);
      const rawReq: object | undefined = options.reqType !== undefined ? options.reqType.write(JSON_VALUE_WRITER, options.req) : undefined;
      const rawQuery: string | undefined = options.queryType !== undefined ? options.queryType.write(SEARCH_PARAMS_WRITER, options.query) : undefined;
      const resType = options.resType;

      const req = agent.request({
        method,
        url: uri.toString(),
        params: rawQuery,
        headers: {
          "Authorization": (typeof options.auth === "string") ? `Bearer ${options.auth}` : undefined,
        },
        data: rawReq !== undefined ? JSON.stringify(rawReq) : undefined,
        responseType: "json",
      });

      return rxFrom(req).pipe(rxMap((res: AxiosResponse): Res => {
        const resRaw: unknown = res.data;
        let resObj: Res;
        try {
          resObj = readOrThrow(resType, JSON_READER, resRaw);
        } catch (err) {
          throw new Error(`failed to read server reply: ${JSON.stringify(resRaw)}: ${err}`);
        }
        return resObj;
      }));
    };
  }
}

function resolveRoute(base: URL, route: readonly string[]): URL {
  return new URL(urlJoin(base.toString(), route.map(encodeURIComponent).join("/")));
}
