import {RfcOauthAccessTokenKey} from "@eternaltwin/core/oauth/rfc-oauth-access-token-key";
import {IoType} from "kryo";
import {Observable} from "rxjs";

export interface SimpleRequestOptions<Res> {
  auth?: RfcOauthAccessTokenKey;
  queryType?: undefined,
  query?: undefined,
  reqType?: undefined,
  req?: undefined,
  resType: IoType<Res>,
}

export interface QueryRequestOptions<Query, Res> {
  auth?: RfcOauthAccessTokenKey;
  queryType: IoType<Query>,
  query: Query,
  reqType?: undefined,
  req?: undefined,
  resType: IoType<Res>,
}

export interface BodyRequestOptions<Req, Res> {
  auth?: RfcOauthAccessTokenKey;
  queryType?: undefined,
  query?: undefined,
  reqType: IoType<Req>,
  req: Req,
  resType: IoType<Res>,
}

export interface CompleteRequestOptions<Query, Req, Res> {
  auth?: RfcOauthAccessTokenKey;
  queryType: IoType<Query>,
  query: Query,
  reqType: IoType<Req>,
  req: Req,
  resType: IoType<Res>,
}

export type RequestOptions<Query, Req, Res> =
  SimpleRequestOptions<Res>
  | QueryRequestOptions<Query, Res>
  | BodyRequestOptions<Req, Res>
  | CompleteRequestOptions<Query, Req, Res>;

export type HttpMethod = "get" | "post";

export type HttpHandler = <Query, Req, Res>(method: HttpMethod, route: readonly string[], options: RequestOptions<Query, Req, Res>) => Observable<Res>;
