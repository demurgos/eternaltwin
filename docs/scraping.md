# How to write a scraping client for Eternaltwin ?

## Setup a dev environment

Scraping clients must be written in [Rust 1.70+](https://www.rust-lang.org/tools/install).

### Dev Containers on VSCode

If you have [Docker](https://www.docker.com/) and [VSCode](https://code.visualstudio.com/) installed, I highly suggest using the Dev Containers feature to get Rust and extensions needed running in three clicks.

- First install the [Dev Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers).
- `Ctrl+Shift+P` > `Dev Containers: Open Folder in Container...` > Select the `eternaltwin` folder.
- Select `Rust` and `bullseye` when prompted.
- Wait for the container to build and start and you're good to go.

### MacOS and Linux

Run `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh` to install Rust.

### Windows

Follow [this page](tools/rust.md) instructions.

## Upload HTML example files to scrap and define the mapping

We need to upload example HTML files to scrap to the [test-resources/scraping](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/test-resources/scraping) folder to test our client.

For a `Hordes` client, we can create a `hordes` folder in the [test-resources/scraping](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/test-resources/scraping) folder, and add the following structure :

```bash
|-- scraping/
    |-- hordes/
        |-- user/
            |-- 476256.fr.476256/ -- Folder for the french user page of the user with id 476256, viewed as the user 476256.
                |-- input.html -- HTML code of the page.
                |-- value.json -- JSON file containing the expected result of the scraping.
            |-- 476256.en.476256/ -- Folder file for the english user page of the user with id 476256,
```

`input.html` is actual HTML code to scrap and `value.json` is the expected result of the scraping.

For a user `Hordes` cities, it would be something like :

```json
{
  "user": {
    "id": "476256",
    "username": "Evian",
  }
  "cities": [
    {
      "id": "1",
      "name": "Creux infernal",
      "nb_survived_days": "0",
    },
    {
      "id": "2",
      "name": "Trou des cousins déformés",
      "nb_survived_days": "59",
    },
  ]
}
```

## Create our client `crate`

Now that we have something to scrap from, we need to create a `crate` for our client in the `crates` folder to write actual scraping code.

If we want to create a client for `Hordes` for example, we want to run the following commands (if we run an Unix-like OS shell) to get started :

```bash
# Create client folder / crate
cd crates
cargo new hordes_client
cd hordes_client

# Create all needed files
echo "# `etwin_hordes_client`" > README.md
touch src/lib.rs src/mem.rs
mkdir src/http
mv src/main.rs src/http/scraper.rs
touch src/http/errors.rs src/http/mod.rs src/http/url.rs
```

Then, besure to add our client to the `Cargo.toml` file at the root of the project.

We will now go through the different files created in the next section.

## Client files

Here is the structure of a client :
```bash
|-- src/
    |-- http/
        |-- errors.rs -- All errors that can be returned by the client.
        |-- mod.rs   -- High-level functions of the client.
        |-- scraper.rs -- Actual functions (using CSS selectors...) that scrap the pages.
        |-- url.rs      -- Contains builder functions to create URLs to scrap.
    |-- lib.rs         -- Exports crate packages.
    |-- mem.rs        -- Functions for in-memory tests.
    |-- Cargo.toml   -- Client dependencies.
    |-- README.md  -- Client documentation.
```

I highly suggest taking a look at the [dinorpg_client](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/dinorpg_client) client to get the details of how to write a client.

### errors.rs

This file contains an enum that represents all errors that can be returned by the client.

For example, we may want to return an error if a `div` with a specific `id` containing a specific information we needed was not found, or if the information was not in the expected format.

```rust
use thiserror::Error;

#[allow(unused)]
#[derive(Debug, Error)]
pub enum ScraperError {
  #[error("DivWithIdNotFound: {0}")]
    DivWithIdNotFound(String),
  #[error("InvalidHordesUserCityId: {0}")]
    InvalidHordesUserCityId(String),
}
```

### mod.rs

This file contains the high-level functions of our client.
Basically we have to create :
 - a resolver implementation which finds the URLs to scrap. (for them to be found, we have to configure them in [mt_dns](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/mt_dns/src/) crate)
 - a method which opens a session with game or Twinoid credentials to access the page.
 - one or several functions which scraps structures of interest, using the scraper functions we will write in the `scraper.rs` file.

### scraper.rs

This file contains the actual functions that scrap the pages.

We use the [scraper](https://docs.rs/scraper/latest/scraper/) crate to scrap the pages.
It exposes a `Selector` struct which allows us to recover elements in the HTML page using CSS selectors.

An dummy example to recover user cities for the Hordes client would look like this :

```rust
use scraper::{Html, Selector};

pub(crate) fn scrape_user_cities(doc: &Html) -> Result<HordesUserCitiesResponse, ScraperError> {
  let root = doc.root_element(); // Get the root element of the HTML page
  let selector = Selector::parse("div#cities").unwrap(); // Create a selector to get the div with id "cities"
  let cities = root.select(&selector).next().ok_or(ScraperError::DivWithIdNotFound("cities".to_string()))?; // Get the actual div with id "cities".

  let mut user_cities = Vec::new();

  // for each div with class "city" in the div with id "cities", recover the city id, name, level and number of survived days.
  for city in cities.select(&Selector::parse("div.city").unwrap()) {
    let id = city.value().attr("data-city-id").ok_or(ScraperError::DivWithIdNotFound("data-city-id".to_string()))?.parse::<u32>()?;
    let name = city.select(&Selector::parse("div.city-name").unwrap()).next().ok_or(ScraperError::DivWithIdNotFound("city-name".to_string()))?.inner_html();
    let name = HordesUserCityName::from_str(&name)?;
    let nb_survived_days = city.select(&Selector::parse("div.city-main").unwrap()).next().is_some();

    user_cities.push(HordesUserCity { id, name, level, nb_survived_days });
  }

  Ok(HordesUserCitiesResponse { profile, cities: Some(user_cities) })
}
```

As we see, we return the scrapped data in a custom `HordesUserCity` struct, which allows data validation.
We will discuss this in next sections.

### url.rs

This file contains an enum listing all base URLs to scrap, and builder functions to add subpaths and query parameters to them.

### lib.rs

This file exports the packages of our client so they can be used elsewhere. This will probably only contains :
```rust
#[cfg(feature = "http")]
pub mod http;
#[cfg(feature = "mem")]
pub mod mem;
```

### mem.rs

This file may contain functions to scrap pages from a local HTML file, for in-memory tests. We can ignore this file for now.

### Cargo.toml

This file contains the metadata and dependencies of our client.

I highly suggest getting the same dependencies of another client, like `dinorpg_client` for example : [Cargo.toml](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/dinorpg_client/Cargo.toml)

## Steps to write our client

### 1. Defining the data structures we want to recover

We have to define the data structures we want to recover from the pages in the [crates/core/src](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/core/src) folder.

If we keep following the Hordes client example, we will have to create a `hordes.rs` file in the [crates/core/src](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/core/src) folder.

Then we can for example define the `HordesUserCity` struct like this :

```rust
#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct HordesUserCity {
  pub id: u32,
  pub name: HordesUserCityName,
  pub level: u32,
  pub nb_survived_days: u32,
}
```

We can see that we use the `HordesUserCityName` type for the `name` field, to allow some data validation (for example, with a regex) in the same file :

```rust
declare_new_string! {
  pub struct HordesUserCityName(String);
  pub type ParseError = HordesUserCityNameParseError;
  const PATTERN = r"^[a-zA-Z]{1,32}$"; // check that the city name is between 1 and 32 characters long, and only contains letters.
  const SQL_NAME = "hordes_user_city_name";
}
```

We can also define enums, storing expected values for example `HordesUserCityDeathCause`:
```rust
declare_new_enum!(
  pub enum HordesUserCityDeathCause {
    #[str("Disparu dans l'Outre-Monde pendant la nuit !")]
    LostInTheOuterMonde,
    #[str("Lacéré, dévoré... pendant l'attaque de la nuit !")]
    DeadAtTown
    // ...
  }
);
```

This also where we can define the responses of the client, for example the `HordesUserCitiesResponse` struct :

```rust
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct HordesUserCitiesResponse {
  pub profile: HordesUserProfile,
  pub cities: Option<Vec<HordesUserCity>>,
}
```

I highly suggest reading the [DinoRPG example](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/core/src/dinorpg.rs) to get an exhaustive view of the structures we must and can define (especially enums for server URLs, functions of the scrapper, responses etc.).

## 2. Writing the scraper functions

### Testing our scrapper

We already wrote a `scrape_user_cities` function in the [scraper.rs](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/dinorpg_client/src/http/scraper.rs) section.

We also put example HTML files in the [test-resources/scraping/hordes](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/test-resources/scraping) folder.

We can then test our function on the example files by putting the following code at the end of the `scraper.rs` file :

```rust
#[test_resources("./test-resources/scraping/hordes/*/")]
  fn test_scrape_user_cities(path: &str) {
    let path: PathBuf = Path::join(Path::new("../.."), path);
    let value_path = path.join("value.json");
    let html_path = path.join("input.html");
    let actual_path = path.join("rs.actual.json");

    let raw_html = ::std::fs::read_to_string(html_path).expect("Failed to read html file");

    let html = Html::parse_document(&raw_html);

    let actual = scrape_user_cities(&html).unwrap(); // our function to test is here!
    let actual_json = serde_json::to_string_pretty(&actual).unwrap();
    ::std::fs::write(actual_path, format!("{}\n", actual_json)).expect("Failed to write actual file");

    let value_json = ::std::fs::read_to_string(value_path).expect("Failed to read value file");
    let expected = serde_json::from_str::<HordesUserCitiesResponse>(&value_json).expect("Failed to parse value file");

    assert_eq!(actual, expected);
  }
```

And then we can run `cargo test` in the `crates/hordes_client` folder.

### Recovering a single value

TODO

### Recovering a list of values

TODO

### Recovering values as a map

TODO

### Using regexes to select HTML elements

TODO

## 3. Querying the servers

For our scrapper to work in "real conditions", we need to tell them where to find the `Hordes` servers to scrap.

We do this by implementing a DNS resolver [mt_dns](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/mt_dns/src/) crate.

Assuming you have declared the following `HordesServer` enum in `crates/core/src/hordes.rs` :

```rust
declare_new_enum!(
  pub enum HordesServer {
    #[str("hordes.fr")]
    HordesFR,
    #[str("die2nite.com")]
    Die2NiteCom,
    #[str("www.zombinoia.com")]
    ZombinoiaCom,
  }
  pub type ParseError = HordesServerParseError;
  const SQL_NAME = "hordes_server";
);
```

- In `crates/mt_dns/src/lib.rs` :

```rust
impl DnsResolver<HordesServer> for MtDnsResolver {
  fn resolve4(&self, domain: &HordesServer) -> Option<Ipv4Addr> {
    dead::DnsClient
      .resolve4(domain)
      .or_else(|| live::DnsClient.resolve4(domain))
  }

  fn resolve6(&self, domain: &HordesServer) -> Option<Ipv6Addr> {
    dead::DnsClient
      .resolve6(domain)
      .or_else(|| live::DnsClient.resolve6(domain))
  }
}
```

- In `crates/mt_dns/src/live.rs` :

```rust
impl crate::DnsResolver<eternaltwin_core::hordes::HordesServer> for DnsClient {
  fn resolve4(&self, domain: &eternaltwin_core::hordes::HordesServer) -> Option<std::net::Ipv4Addr> {
    #[allow(unreachable_patterns)]
    match domain {
      eternaltwin_core::hordes::HordesServer::HordesFR => Some(std::net::Ipv4Addr::new(178, 32, 123, 64)),
      eternaltwin_core::hordes::HordesServer::Die2NiteCom => Some(std::net::Ipv4Addr::new(178, 32, 123, 64)),
      eternaltwin_core::hordes::HordesServer::ZombinoiaCom => Some(std::net::Ipv4Addr::new(178, 32, 123, 64)),
      _ => None,
    }
  }
  fn resolve6(&self, _domain: &eternaltwin_core::hordes::HordesServer) -> Option<std::net::Ipv6Addr> {
    None
  }
}
```

- In `crates/mt_dns/src/dead.rs` :

```rust
impl crate::DnsResolver<eternaltwin_core::dinorpg::DinorpgServer> for DnsClient {
  fn resolve4(&self, _domain: &eternaltwin_core::dinorpg::DinorpgServer) -> Option<std::net::Ipv4Addr> {
    None
  }
  fn resolve6(&self, _domain: &eternaltwin_core::dinorpg::DinorpgServer) -> Option<std::net::Ipv6Addr> {
    None
  }
}
```

## 4. Creating a CLI demo of our scrapper

TODO : create a CLI demo in the [cli](https://gitlab.com/eternaltwin/eternaltwin/-/tree/main/crates/cli/src) crate. Get inspiration from what is already in the folder and [this commit](https://gitlab.com/eternaltwin/eternaltwin/-/merge_requests/496/diffs?commit_id=06b9a8e1e5aca981e0bd78c4fd6538459034465c#05b1c872fe56182193f1aabdc03de50d68adc2b7)

We can then run Hordes demo CLI with `cargo run --package etwin_cli --bin etwin_cli -- hordes`
