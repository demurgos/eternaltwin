# NeoParc development team

## **Eternaltwin:** Hello, thank you for taking part in this interview exercise. Can you briefly introduce yourself?

**Dau2004:** Hello, I'm Dau2004 / PharoDev, and I was a more or less regular player of few of MotionTwin games back in the day (Dinoparc, Dinorpg, Hammerfest, the occasional Hordes, ...). On a day-to-day basis, I'm a developer in my professional life. On a personal level I also try to devote some time to it, even if it's far too occasional in my opinion at the moment 😅

**Jonathan:** My name is Jonathan, I'm 28 and I'm originally from Quebec City, Canada. I've been working as a developer in the business consulting sector (development and digital transformation) since 2017. From a young age, I've always been fascinated by computers, because for me they represented a medium on which anything was possible, an almost infinite source of content and knowledge. When I was younger, we had a PC at home that my sister and I had to share. She was the one who introduced me to Dinoparc via MonLapin in 2005-2006, and it was so cool to discover this world that Motion Twin was creating at the time!

## **Eternalwin:** Jonathan, NeoParc was released on June 21, 2021. How did you feel when the first players went online?

**Jonathan:** I was a little nervous, as it was the first time one of my personal projects had been put online (thanks to Demurgos for his help with this stage of the process 🙂 ). I was also quite proud of the work accomplished, and surprised to see all these players gradually picking up the game 🙂

## **Eternalwin:** Why did you choose Dinoparc as your project? Was it your game of choice? The only MT game you used to play?

**Dau2004:** So, at the moment, it's all for Jo and nothing for me in terms of development 😁 Overall, without a doubt, everything to do with gameplay and new features goes through Jo 🙂 For my part, I'm making little fixes here and there (performance, security, anomaly detection, urgent fixes if I'm available); but nothing really substantial at the moment ^^.

**Jonathan:** As far as development is concerned, we don't really separate tasks, since the game runs smoothly in production. Updates consist mainly of putting online the various events of the year (Easter, Birthday, Halloween and Christmas.) and the clan wars. I take care of all these aspects, and Dao is there to support me when we have specific needs for technical improvements or in-game calibrations. For his part, he's also in charge of monitoring the game and imposing penalties when certain players step outside the limits allowed by our community. Otherwise, it's not out of the question for me to work on new features stemming purely from the Neoparc era, but these are often epics that I undertake alone. I like developing as much as game-designing, but I'm not very good at calibrating and moderating.

## **Eternalwin:** The second clan war ended this summer. This new war format is already different from the original, can you tell us more about what led you to a tri-faction war rather than every clan for itself?

**Dau2004:** I'll leave this one exclusively to Jo: in terms of gameplay, he gets all the credit ^^.

**Jonathan:** Basically, the aim was to offer a viable competition despite the small number of active players in our community. Other rules, such as a maximum of 5 players per clan, were introduced in order to provide an interesting challenge and keep the number of clans growing. With factions, the various allied clans can also collaborate to take territories together, thus gaining an advantage over those who don't. I felt this was essential to the game's success. I felt it was essential to rethink the Clan Wars model, as the context and gameplay of Neoparc is not the same as Dinoparc.

## **Eternalwin:** It's true that seeing PvP trifaction is a daring gamble because of the balance to be found, but it's refreshing because of the gameplay it brings. Are you planning other types of clan events?

**Dau2004:** Ditto gameplay for Jo 😁

**Jonathan:** For the time being, Clan Wars will remain in this format since there are other new things to bring besides clans in general, but it's not impossible that this format will be modified or changed in the future!

## **Eternalwin:** The Christmas event is already halfway through. In addition to this, there's an Easter event and a Halloween event. Are you planning any other temporary events linked to particular periods?

**Dau2004:** Usually there's also the anniversary event at the end of June, which gives us "non-war" events throughout the year (with a few months' break at the start of the year and over the summer): Easter, anniversary, Halloween, Christmas. As for forecasts of new releases, not to my knowledge, but Jo may have a few tricks up her sleeve 🙂

## **Eternalwin:** Like many of us, you grew up playing Motion Twin games. You loved their games so much that you decided to revive one of them. How did you feel about the closure of Motion Twin's web games?

**Dau2004:** Not too bad, still a bit disappointed not to have been able to recover historical data on the Dinoparc side (database dinoz/players without mail for example), that would have allowed some to come back one day without starting all over again, but well it's understandable on their part 😅 Quite a few games are lost forever, but it's the hard law of computing, some stuff is recast, others disappear :/.

**Jonathan:** For my part, I didn't find it so terrible. It's true that big chunks of history are lost, but at the same time, the games that have been given a new lease of life thanks to our projects are improved versions of their original counterparts.

## **Eternalwin:** 2024 is here, what are NeoParc's next major milestones and objectives?

**Jonathan:** Without going into too much detail, there will be some new features outside the event and technical updates. That's all we can say at the moment 😉

**Dau2004:** Indeed it's Jo who has the plans in mind 😁 Thanks to you for the time spent and the patience on our feedback ^^

## **Eternalwin:** Can you tell us the number of players who participated in the Christmas event and the total number of collections exchanged?

**Dau2004:** 49 players took part in all, 42 made at least 10 collections, and a total of 4030 collections were exchanged.