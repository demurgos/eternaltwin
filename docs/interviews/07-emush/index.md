

# eMush development team

## **Eternaltwin:** Hello, thank you for taking part in this interview exercise. Can you briefly introduce yourself?

**Breut:** Hello, I'm Breut (Sylvain Breton, 31 IRL). I discovered Twinoid games with Hordes (French version of Die2Night) around 2013 and quickly started playing Mush. When the ET project was created, I quickly got involved in the collection of Mush data, and after 6 months, I finally took the plunge and started programming for eMush. I love coding, but I'm not a trained developer. I was using python to do research and didn't think I could be useful on the project. In the end, I'm now one of eMush's three main developers.

**Sami:** Hello, I'm Sami, 22 years old, and I was helping this project because Mush was part of my childhood, I was looking for an existing project and there was already one so I landed on it, I started programming quite early in my life which turned into one of my passions and I wanted to find a use for it even if it took me a while to dare to do it here. In real life, I'm a Java developer in a consulting firm, which takes me quite a long way from PHP, but I'm still in my field because I’m doing the back end, and for the moment I'm taking a back seat because I find it hard not to fall back into my Java reflexes when I start using PHP (and without Webstorm it's even worse haha). My work has taken precedence over my personal projects for training, so I've had to put the project on hold for the time being.

**Zasfree:** Hi, I'm zasfree. I discovered Twinoid games in 2011, and later Mush in 2013. I really started to get attached to this game in 2015.

Ever since I started playing Mush, I've wanted to understand the game mechanics as well as possible in order to set the most impressive survival records. When the Emush project started, even though I'm not a developer, I became one of the first three people in charge of the project. I was mainly in charge of collecting data on the original game, selecting players for Emush's alpha ships, and launching test ships in Mush to gather as much information as possible. I was also in charge of project announcements.

Today, the alpha is open to all, Mush is closed, and Evian is much better at announcements than I am XD. So I'm in charge of making available information about the original game from screenshots or videos I took when the game was still open, to help programmers when they need it or have questions.

**Gowoons:** Yo, I'm gowoons, an engineering student. I discovered mush in 2020 (yes late lol, the emush project already existed I think) because I was quarantined with one of my best friends who has loved the game for a long time (joker). I immediately got hooked and loved it. I'm studying computer science, particularly devops and cyber. I started taking part in the project 1-2 years ago, and even though dev isn't my specialty, I really enjoy it. I pitch in when I have time, there was a long period when I didn't do anything, and I'm trying to pick up a bit now, which is sometimes difficult because unfortunately my courses/training have to take priority over emush :calim:

**Evian:** Hi, I'm Evian. In real life, I'm 25 and working as a data science engineer.
I discovered Motion Twin games with Minitroopers in college, then played pretty much every Twinoid game, until Mush came out in 2013 and I sacrificed my soul to it (useless thus essential anecdote, at the time I was the 2nd top Chun player). 😛

I had joined an eMush alpha in early 2022 and had been impressed by the progress made.
Having learned to program during my apprenticeship at the same time, it made me want to take the plunge. Today, I'm one of eMush's main developers.

**A7:** I'm A7 (or Atar7, it doesn't matter), I seem to have discovered MT games with Hordes, then Twinoid (it's been a while…). Professionally, I come from the graphics industry, with a strong interest in development and technology. I joined the Eternaltwin project at first to work on the main site, then the eMush project, which seemed an interesting challenge.

## **Eternaltwin:** How do you manage eMush and your professional/personal life? Does eMush set the pace for your days, or is it instead what fills your downtime?

**Breut:** In three years, there have been a lot of changes in my professional life, so my answer isn't the same today as it was last year. In particular, two years ago, I was able to work on eMush regularly and efficiently because I was underworked (not enough school hours available for me to do them), today it's more during school vacations. However, I do my best to regularly solve the bugs that are reported to us so that the community, which no longer has access to Mush, can feel the project moving forward despite our reduced availability.

**Evian:** I have monomaniacal tendencies, so eMush takes up a (very) large part of my evenings and weekends at the moment, or even more if you count the time I spend fixing bugs when I'm in class (yes, I know, it's not right 😔).

So it's fair to say that it sets the pace for my days.

I can sometimes be less active when my free time decreases, mostly because I have to revise longer to compensate for the time I spent coding for eMush in class. Hmm... Maybe there's a change of strategy to be made.... 🤔

**A7:** There have been periods of varied activity in my life in recent years. As a matter of principle, I always consider eMush as a second priority, my professional life has to come first. Luckily I sometimes have a lot of free time, which I fill with personal projects such as eMush.

## **Eternaltwin:** How do you divide up your tasks? Do some of you have an affinity for specific functionalities and just do that, or does everyone take what they want?

**Breut:** So, to start with, I'm going to give 2 terms that we're likely to use to answer this question. We can divide the project into two main parts. The front is what you see on your web page, how the different elements are arranged, the Daedalus represented in isometric view and the very tempting button to hit a laid down Chao. The back is all the operations on the eMush server, where Chao's health gets changed because you couldn't resist pressing that button.
I started on the back part of eMush, then ended up doing a huge section of the front, the Daedalus in isometric view. For the latter, you could say it's my specialty. For the back end, we often work together, and every time a dev makes a modification, his code has to be reread by another dev, which means understanding what has been done. But the project is getting bigger and bigger with diseases, hunters, explorations... I have to admit that I'm getting less and less of an overview over the project as a whole.

**Evian:** Breut summed it up very well. You can also think of the back as the engine of the game and the front as its display.

There are a few areas of expertise: at the moment, I'm developing most of the new features on the back end, such as exploration, while Breut is consolidating and cleaning up the engine foundations. 
I’m also thinking very much of A7, without whom the game would just be a blank page with text (come on, it might be blue). 😱 

On the other hand, we need to be versatile if we want to move the project forward: if I want the explorations to really work, I also need to code the button to launch them and the page to visualize the events, even though this isn't my specialty (but fortunately A7 comes after me with corrections).

We've all had to get our hands dirty on every aspect of eMush to produce regular updates, and it's not that unusual for several developers to collaborate on a single feature for a few days at a time.

Finally, there are the more occasional contributors, who are no less important.
When you look closely, they have developed features that we don't prioritize, but which are still important. I'm thinking, for example, of our internal alert system, which helps us enormously to debug and intervene in the event of problems, or the ship's minimap.

**Zasfree:** Currently, my involvement with eMush is flexible. I monitor the project several times a day, but because of the low number of questions, it doesn't take up most of my day. I maintain a balance by devoting specific time to the project while preserving my personal and professional life. Although interactions are rare, I remain available and active to meet the needs of the community.

Breut and Evian explained our modus operandi very well. For my part, after the holidays, I plan to get more involved as a tester by installing the game locally. This will make it easier for me to test the game without disturbing existing players. My aim is to identify potential problems and contribute by proposing solutions. I aspire to offer a better gaming experience to the gaming community. 

**A7:** Breut and Evian have indeed summed up the process very well. Everyone finds their specialties and the project moves forward thanks to a multi-disciplinary team. Personally, I wouldn't be able to touch the back-end, even with the best will in the world.
Evian says that without me, the game would just be a blank page of text; my answer is that without him and Breut, the game would just be a homepage with no follow-up 🙂

_Evian_ And Simpkin above all, don't forget **Simpkin**! Without Simpkin there's no project, eMush on pencil paper.

## **Eternaltwin:** Today, eMush is still in open alpha. What features will be implemented next? Have you already set a milestone to end this alpha and move on to the beta?

**Breut:** As a reminder, the last major update added explorations, and there's still content to be added, such as implementing the effects of all sectors and equipment. The next step is likely to be related to projects-skills-research. Normally, we've worked upstream to make their implementation as easy as possible, but there are so many of them that we're bound to come across some special cases that will give us a hard time. Once this stage has been completed, we'll be very close to having all the features of the basic game. I think we'll be able to go into beta after that. Then there'll be a lot of finishing work to do: bugs, little things not yet implemented, UI improvements, admin tools...

**Gowoons:** Schrödinger also in the finishing touches 😜 this one's eagerly awaited

**A7:** We'll also have to start working on user accounts, and all the other features that aren't part of the game itself. There's still a bit of work to be done before we can expect a really clean beta.

**Evian:** We're still developing the explorations, and indeed, the next step will focus on all of Daedalus "enhancements" (projects, research and skills), depending on what's most requested by players.

Many of the details mentioned by gowoons and A7 remain to be worked out before a beta (and there are others). On the one hand, because I tend to prioritize the speed of release of updates over accuracy (even if it means fixing bugs quickly) in order to keep a steady sense of progress. What's more, the remaining 20% of details tend to take up 80% of the time...

On the other hand, because eMush is a very complex game: I often write more test code than code implementing a feature, which doesn't prevent me from forgetting particular cases or certain unexpected behaviors. I think we can make progress on this, for example by involving zasfree more in the write-up of features to be implemented.

## **Eternaltwin:** What are you most eager for in the remaining work?

**Breut:** This is a bit presumptuous, but I'm really looking forward to adding my personal touch to eMush. It's still under discussion, but I'd love to make a small addition like a new artifact or skill. A much less controversial idea I'd like to add is an equipment appearance system (who hasn't dreamed of tagging a skull and crossbones on their patrol boat). I'm chomping at the bit because I don't have the time right now, but I've been wanting to implement this for 1 month.

**Evian:** I can't wait for the investigation game to come back, because that's what I liked best about Mush and I really enjoy reading the stories behind it. What's more, I'd like to get more and more involved in communicating about the game as the project progresses. We'll never have the same number of players as in 2013, but I think it's possible to attract a lot more players, even just among those who have already played Mush. If it goes well, I have much more ambitious (and controversial) ambitions than Breut for what comes next. 😛 

## **Eternaltwin:** Do you have any idea of the daily or total number of players on this alpha?

**Evian** There were several resets during development, which makes it impossible to provide exact stats. That said, as I speak, there are four ships in progress and 255 players have launched at least one game.

## **Eternaltwin:** Motion Twin closed all its games a little over a month ago. Were you mentally ready for this shutdown? How did you feel about it?

****Evian:**  be honest, I hadn't really played on Twinoid for at least two years, but I still read the forums out of habit, while communicating on them for eMush. So I was less affected by others, but in fact, the few days after the closure, it felt strange: I would regularly try to go to Twinoid and come across the closing page.

**Breut:** My goal was to be able to offer players an open alpha before the total end of Mush. Even if the game is far from complete, I think the gamble has paid off, since the end of Twinoid we've seen an increase in player numbers and bug reports. Concretely, for me, this has meant additional bugfix work, as it fell at the same time as a busy period at work, so it was a bit complicated to combine all that.

## How do you see the year ahead for emush? Any plans in particular, any surprise features?

**Evian:** I see 2024 as a continuation of 2023.

Indeed, a year ago to the day, eMush was still in closed alpha: you could only launch one ship at a time, there were no endgame pages, no hunters, no planets, no exploration... The home page left much to be desired etc... 😅

I'm happy with what we've achieved and highly motivated to continue updating regularly, which remains the most important thing, especially for a volunteer project like this.

I've also learned a lot in 2023, which has made me much more comfortable with the eMush code. I've got lots of ideas to test to deliver more reliable updates and in the communication around the game.

In terms of functionality, my priority is to ensure that eMush has the same features as Mush, and with fewer bugs, as quickly as possible: so we're not going to spread ourselves too thin.

So we're going to finish the explorations, then tackle the searches, projects or skills.

Don't hesitate to tell us on the forum or Discord what you prefer!
Thanks for the interview!

**Breut:** For me, 2024 is going to be busier than 2023 IRL, which means that I'll be there mainly to support Evian in resolving bugs and making a few technical changes so that adding skills/projects/research goes as smoothly as possible.
As Evian said, the most urgent thing is to implement all the Mush features before getting into the surprise features, although I do have a few ideas that I'll try out if I have time.