# Brainbox

**Thanks for taking your time to This Month In Eternal Twin. BrainBox is a name that a lot of people know but can you do a presentation for the others?**

_Sure. I'm Brainbox, 32 years old, from Germany, and I'm the project lead for MyHordes. I've been a DieVerdammten (German version of Hordes) player since Season 2 and started developing External Apps for it with Season 3._

**Hordes (and its other brothers) was one of the most emblematic games of the Motion Twin. When did you start the development of MyHordes?**

_I've started at the end of November 2019, way before Eternaltwin was formed. The idea came up when I was discussing the end of Flash Player support with a fellow DV player. We ended our discussion on the notion that it would be great if someone would remake DieVerdammten without Flash; the next day, I though "Hey, why wait for someone else to do it when I could do it myself?" and got to work._

**And yesterday, the 11 February 2023, 3 and half years later, the game is finally ready for its begun with the season 15. That's some impressive works. How do you feel about that?**

_It has indeed been a lot of work. Looking back, I'm actually impressed myself that we managed to pull it off. You see it with these kinds of Open Source projects all the time; developers leave to do other things, updates get slower and slower and in the end, the project just dies. But Hordes has a very active community, so the constant feedback - good and bad - has kept us motivated._
_In a way, I'm happy that we finally made it. On the other hand, we all have tons of ideas for future improvements and content, things we want to improve on et cetera. So the end of the beta is pretty much just the beginning of more development._

**For now, what is your biggest challenge accomplished on this project? And what is your proudest accomplishment?**

_The biggest challenge was getting all the details right; even during the beta, we kept finding things that were different from Hordes. It's a very complex game, but I didn't expect it to be THIS complex going in._

_My proudest accomplishment goes a bit in the same direction; It is that we managed to add many things that were removed from Hordes at one point or another; like the Shaman profession for example, or the automatic ghouls. All of these things are now back in MyHordes, select-able as options for private or event towns._

**Did the publication of the source code from the Motion Twin helped you or were the benefits anecdotic?**

_It did help a lot. We were able to obtain many values, like drop chances, that we previously just had estimates from the community for. Additionally, we were able to get lots of original texts that we would otherwise have to mine from the original games._

**Did you still miss some gameplay data? I heard the zombie spread is not yet balanced? Do you except the MT give us more source code?**

_We do have zombie spread data in the source code that MT gave us, but it's strange. We've actually managed to get the code running as is, without any modification, and it produces results that are nothing like the zombie spread we know. Our assumption is that this is either a new spread algorithm that was never actually enabled or an experiment of some sort. So yes, the actual zombie spread algorithm is still not available to us._

_There's also some smaller things, for example in regards to item drop rates in explorable ruins, that are missing._

_However, as far as I remember, skool mentioned somewhere that this was all the code they could share. So I have little hope that the rest of the code will be made available to us._

_I guess we will just have to make the best of what we have._

**I think we can all agreed that you already nailed it. Your work on MH is impressive. How did you do to deliver an update every monday? By looking at the git graph we can see you work on the project almost every week ends?**
![Interview - Git Graph of BrainBox](./interview_brain.png)

_I usually spent time with my friends or family in the evening, so on weekends, I can work during the day. There are some exceptions of course, like vacations. But even then, most of the time I manage to work at least one day per weekend, which is enough to push out an update with important fixes._

**Your full time job is web developer too? Don't you ever get tired of doing code all the week and the weekends?**

_Not really, I guess I fulfill the stereotype of a work-obsessed German. I love coding and I enjoy doing it, with or without pay._

**Ahah that's good to heard. And did MH help you progress in skills for your job?**

_To be perfectly honest, MyHordes helped me get this job in the first place. When I applied, I listed it as a reference. I worked as a university researcher before, mostly in C++ and OpenGL. Although I did lots of web projects on the side, none of them were really suitable to be used as references. I think if I hadn't had MyHordes to show my web development skills, it would have been harder to reach my current position._

_But regarding skills, it's actually the other way around. My job gives me plenty of opportunity to learn and experiment with new things that I can then incorporate into MyHordes._

**Wow that's a good story! And for MH's future, do you have think about a season's schedule or will they be released when they're ready? Now that the season 15 is online we're already waiting for the season 16 and its new features!**

_We do plan to roughly follow a schedule, although it's not finally decided what the intervals will be. Of course, we can only release a new season when its development is complete, and since we're all volunteers, it's a bit hard to make accurate estimations._

_But rest assured, there will be future seasons, and we will push them out as regularly as we can._

**It's reassuring to heard things like this. I think I asked all I have in my sleeves, do you want to add something? Maybe a hint about a future feature?**

_I'd like to give a shout-out to the rest of the team; Dylan, Nayr, Ludofloria and Adri, as well as all the others who have contributed to MyHordes! Couldn't have done it without them!_

_And as a hint... well, lets say we found some traces of unused content or future ideas for Hordes that were never implemented like..._

**Hmmm it seems this interview is over because a zombies ate Brainbox. Maybe it didn't want Brain leaks their plans...**
