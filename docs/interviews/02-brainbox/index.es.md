# Brainbox

**Gracias por dedicar tu tiempo a Este Mes en Eternal Twin. BrainBox es un nombre que mucha gente conoce, pero ¿puedes hacer una presentación para los demás?**

_Seguro. Soy Brainbox, tengo 32 años, soy de Alemania y soy el líder del proyecto MyHordes. Soy jugador de DieVerdammten (versión alemana de Hordes) desde la Temporada 2 y comencé a desarrollar aplicaciones externas para él con la Temporada 3._

**Hordes (y sus otros hermanos) fue uno de los juegos más emblemáticos de Motion Twin. ¿Cuándo comenzaste el desarrollo de MyHordes?**

_Comencé a fines de noviembre de 2019, mucho antes de que se formara Eternaltwin. La idea surgió cuando estaba discutiendo el final del soporte de Flash Player con un jugador de DV. Terminamos nuestra discusión sobre la idea de que sería genial si alguien hiciera una nueva versión de DieVerdammten sin Flash; Al día siguiente, pensé: "Oye, ¿por qué esperar a que alguien más lo haga cuando podría hacerlo yo mismo?" y me puse a trabajar._

**Y ayer, el 11 de febrero de 2023, 3 años y medio después, el juego finalmente está listo para comenzar con la temporada 15. Eso es un trabajo impresionante. ¿Cómo te sientes sobre eso?**

_La verdad es que ha sido mucho trabajo. Mirando hacia atrás, estoy realmente impresionado de que lo hayamos logrado. Lo ves con este tipo de proyectos de código abierto todo el tiempo; los desarrolladores se van para hacer otras cosas, las actualizaciones se vuelven cada vez más lentas y, al final, el proyecto simplemente muere. Pero Hordes tiene una comunidad muy activa, por lo que los comentarios constantes - buenos y malos - nos han mantenido motivados._

_En cierto modo, estoy feliz de que finalmente lo hayamos logrado. Por otro lado, todos tenemos toneladas de ideas para futuras mejoras y contenido, cosas que queremos mejorar, etcétera. Entonces, el final de la versión beta es más o menos el comienzo de más desarrollo._

**Por ahora, ¿cuál es tu mayor desafío logrado en este proyecto? ¿Y cuál es tu mayor logro?**

_El mayor reto fue acertar en todos los detalles; incluso durante la versión beta, seguimos encontrando cosas que eran diferentes a Hordes. Es un juego muy complejo, pero no esperaba que fuera así de complejo._

_Mi mayor logro va un poco en la misma dirección; Es que logramos agregar muchas cosas que fueron eliminadas de Hordes en un momento u otro; como la profesión de Chamán por ejemplo, o los ghouls automáticos. Todas estas cosas ahora están de vuelta en MyHordes, seleccionables como opciones para ciudades privadas o de eventos._

**¿Te ayudó la publicación del código fuente de Motion Twin o los beneficios fueron anecdóticos?**

_Ayudó mucho. Pudimos obtener muchos valores, como probabilidades de objetos, para los que anteriormente solo teníamos estimaciones de la comunidad. Además, pudimos obtener muchos textos originales que, de otro modo, tendríamos que extraer de los juegos originales._

**¿Aún así perdiste algunos datos del juego? Escuché que la propagación de zombis aún no está equilibrada. ¿Esperas que MT nos dé más código fuente?**

_Tenemos datos de propagación de zombis en el código fuente que nos dio MT, pero es extraño. De hecho, hemos logrado que el código se ejecute tal cual, sin ninguna modificación, y produce resultados que no se parecen en nada a la propagación de zombis que conocemos. Nuestra suposición es que se trata de un nuevo algoritmo de propagación que en realidad nunca se habilitó o de algún tipo de experimento. Entonces, sí, el algoritmo real de propagación de zombis todavía no está disponible para nosotros._

_También faltan algunas cosas más pequeñas, por ejemplo, con respecto a las probabilidades de elementos en ruinas explorables._

_Sin embargo, por lo que recuerdo, skool mencionó en alguna parte que este era todo el código que podían compartir. Así que tengo pocas esperanzas de que el resto del código esté disponible para nosotros._

_Supongo que tendremos que sacar lo mejor de lo que tenemos._

**Creo que todos podemos estar de acuerdo en que ya lo lograste. Tu trabajo en MH es impresionante. ¿Cómo hiciste para entregar una actualización todos los lunes? Al mirar el gráfico de git, podemos ver que trabajas en el proyecto casi todos los fines de semana.**
![Entrevista - Git Graph de BrainBox](./interview_brain.png)

_Por lo general, paso tiempo con mis amigos o familiares por la noche, así que los fines de semana puedo trabajar durante el día. Hay algunas excepciones, por supuesto, como las vacaciones. Pero incluso entonces, la mayor parte del tiempo me las arreglo para trabajar al menos un día por fin de semana, lo cual es suficiente para lanzar una actualización con correcciones importantes._

**¿Tu trabajo de tiempo completo es también desarrollador web? ¿Nunca te cansas de codificar toda la semana y los fines de semana?**

_No realmente, supongo que cumplo con el estereotipo de alemán obsesionado con el trabajo. Me encanta programar y disfruto haciéndolo, con o sin paga._

**Ja ja ja, eso es bueno de escuchar. ¿Y MH te ayudó a progresar en tus habilidades para tu trabajo?**

_Para ser completamente honesto, MyHordes me ayudó a conseguir este trabajo en primer lugar. Cuando apliqué, lo enumeré como referencia. Trabajé como investigador universitario antes, principalmente en C++ y OpenGL. Aunque hice muchos proyectos web secundarios, ninguno de ellos era realmente adecuado para ser utilizado como referencia. Creo que si no hubiera tenido MyHordes para mostrar mis habilidades de desarrollo web, habría sido más difícil llegar a mi posición actual._

_Pero con respecto a las habilidades, en realidad es al revés. Mi trabajo me brinda muchas oportunidades para aprender y experimentar con cosas nuevas que luego puedo incorporar a MyHordes._

**¡Vaya, esa es una buena historia! Y para el futuro de MH, ¿has pensado en el calendario de una temporada o se lanzarán cuando estén listas? ¡Ahora que la temporada 15 está en línea, ya estamos esperando la temporada 16 y sus nuevas funciones!**

_Planeamos seguir ligeramente un cronograma, aunque finalmente no está decidido cuáles serán los intervalos. Por supuesto, solo podemos lanzar una nueva temporada cuando se completa su desarrollo, y dado que todos somos voluntarios, es un poco difícil hacer estimaciones precisas._

_Pero tengan la seguridad de que habrá temporadas futuras, y las publicaremos tan regularmente como podamos._

**Es reconfortante escuchar palabras como estas. Creo que pregunté todo lo que tengo en mis mangas, ¿quieres agregar algo? ¿Quizás una pista sobre una función futura?**

_Me gustaría dar un saludo al resto del equipo; Dylan, Nayr, Ludofloria y Adri, ¡así como todos los demás que han contribuido a MyHordes! ¡No podría haberlo hecho sin ellos!_

_Y como pista... bueno, digamos que encontramos algunos rastros de contenido sin usar o ideas futuras para Hordes que nunca se implementaron como..._

**Hmmm, parece que esta entrevista se terminó porque los zombis se comieron a Brainbox. Tal vez no querían que Brain filtrara sus planes...**
