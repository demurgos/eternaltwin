# eKingdom équipe de développement

## **Eternaltwin:** Bonjour à vous, merci de vous prêter à cette exercice d'interview. Pouvez vous vous présenter rapidement ?

**Bibni:** Hello ! C'est Bibni, j'ai découvert les jeux de Motion Twin il y a fort longtemps (via PiouzPiouz, Hammerfest puis Hyperliner). Leurs jeux et les personnes que j'ai pu rencontré sur Muxxu/Twinoid m'ont créé une passion pour le développement de jeux vidéo que j'exerce aujourd'hui professionnellement avec Unreal Engine. Dans le projet EternalTwin, je m'occupe d'EternalKingdom depuis maintenant 3 ans et Dinocard Rebirth depuis une grosse année.

D'ailleurs je ne sais pas si on pourra mettre un mot pour ✨ **Glaurung** ✨ qui n'a plus donné de nouvelles depuis bientôt 3 ans 🥺 

**Thanhatos:** Thanhatos, "Barbare Légendaire" d'après le titre sur feu Muxxu, j'ai découvert les jeux Motion Twin vers 2010/11 et j'ai immédiatement accroché à Kingdom. A l'époque je jouais (à) beaucoup de jeux de rôle et de stratégie, avec plus ou moins de rôle play.
Kingdom m'a tapé dans l'oeil car il réunissait à la fois mon vice personnel lié à la stratégie 😈 , la joie des échanges rôle play loufoques 🤪 , et une gestion des ressources en plus d'avoir le bon goût de renouveler les parties et les joueurs 😋 . Tout ça en un seul jeu ! Dans le projet EternalTwin, je m'occupe d'EternalKingdom en tant que scribe et conseil sur les règles.

**Talsi:** Coucou ! Je suis Talsi, j'ai découvert les jeux Twinoid avec Hordes que m'avaient recommandé des amis. puis j'ai testé quelques-uns des autres jeux MT, dont Kingdom auquel j'ai particulièrement accroché. Je fais pas mal de développement dans mon boulot, même si ce n'est pas mon "coeur de métier" à la base, et j'ai toujours quelques projets de dév perso pour mon temps libre, qui aboutiront peut-être un jour ou à défaut m'auront permis d'apprendre plein de choses. EternalKingdom est l'occasion d'utiliser quelques unes de mes compétences pour un chouette projet avec des gens sympas, d'apprendre de nouveaux trucs, et de faire renaître un jeu auquel j'ai beaucoup joué.

**Drange:** Drange, "Explorateur en herbe" me correspondait assez bien, ou "Testeur en machins idiots", je suis arrivée sur Kingdom quelques jours après son lancement  au début 2010. J'ai découvert ensuite qu'un ami IRL y jouait on a échangé nos impressions. Ma seconde partie a été une révélation avec la rencontre de Glaurung et Logortho. Ayant pleins de questions, les tests ont commencé suite à des discussions avec Musaran. De questionnement en investigations j'ai fini par débroussailler beaucoup de sujets (même si je restais une bille en gestion du blé). J'ai vite compris que la déstabilisation était un excellent terreau pour ceux qui veulent saisir des opportunités.

## **Eternalwin:** Pourquoi avoir choisis Kingdom comme projet ? Était-ce votre jeu de prédilection ? Le seul jeu twinoid auquel vous jouiez ?

**Thanhatos:** J'ai joué rapidement à Snake, testé deux ou trois autres jeux, fait quelques (trop) rapides tours sur Hordes (que j'aimerais jouer plus sérieusement à l'avenir d'ailleurs), mais Kingdom a clairement été mon grand favori. Il alliait plusieurs formes de jeux dans un seul, se joue solo comme à plusieurs, sérieux comme absolument pas, et chaque partie diffère de la précédente.

**Talsi:** Kingdom est clairement mon jeu de prédilection, celui où j'ai passé le plus temps ! J'aime beaucoup l'équilibre entre stratégie/tactique et diplomatie, qui peut changer complètement d'une partie à l'autre. C'est aussi un jeu où les parties sont suffisamment longues (en moyenne environ un mois) pour arriver à construire quelque chose, et suffisamment courtes pour se renouveler et permettre d'explorer d'autres stratégies et rencontrer de nouveaux joueurs. Ou de retrouver d'anciennes connaissances mais dans une situation totalement différentes, les ennemis mortels d'une partie pouvant devenir les indispensables alliés d'une autre.

**Bibni:** Étonnement Kingdom n'était pas mon jeu de prédilection, quand Muxxu est sorti, j'ai d'abord accroché à Intrusion puis Snake auquel j'ai énormément joué. J'ai d'ailleurs essayé énormément de jeux MT, même les moins connus comme FormulaWan, CaféJeux, CroqueMonster et HyperLiner qui est définitivement mon jeu de cœur où j'y ai passé 4 ans à me faire exploser par des joueurs avec le Kortex et où j'avais rencontré de nombreux amis de l'époque. J'en profite pour faire une dédicace à Lifaen35 et Terminator35, Sodimel, Dingonikolas et tous les autres membres des HyPeRaCtIfS 😄 . Pour en revenir à Kingdom. J'avais choisi ce projet par curiosité, je me souvenais du principe du match 3 que l'on retrouve dans quelques jeux MT et voulait me tenter à ce challenge car j'adore développer des jeux multijoueurs. J'ai découvert un jeu bien plus profond que je ne le pensais et ça m'a donné envie de m'accrocher et reproduire l'expérience originale.

**Drange:** C'est mon jeu de prédilection, des concepts simples ont produit un environnement complexe et riche. Kingdom est un ovni avec ses cartes ouvertes où les joueurs arrivent grandissent puis meurent sans qu'une hégémonie ne s'installe. La messagerie c'est 80% du jeu tout en étant en dehors du jeu, incroyable. J'ai joué à d'autres jeux mais plus pour attendre un déplacement de général qu'autre chose. Je suis dans l'IT mais du coté des bases de données. Les développements que je pratique ne sont pas utiles sur le projet ou alors de façon très marginale.

Je sers d'archive sur des points du jeu.

## **Eternalwin:** Aujourd'hui eKingdom est toujours en phase d'alpha ouverte. Quels sont vos plans pour le futur ? Un passage en beta est-il prévu ?

**Bibni:** Actuellement il reste trois gros systèmes à mettre en place avant de passer à une phase de bêta. Les batailles (alpha 6), le commerce et gestion des faillites (alpha 7) et la vassalisation (alpha 8) L'alpha 9 sera logiquement la dernière avec les systèmes manquants comme les tours et l'inactivité. Une bêta verra donc le jour une fois tous les systemes majeurs mises en place, le panthéon et les statistiques seront en plus ajoutés.

**Talsi:** Bibni a fait une super road map qu'il a fort bien résumé 😄 Le principal objectif est d'avoir à nouveau un jeu jouable en reprenant les mécanismes de l'ancien Kingdom. Ensuite, nous souhaitons améliorer l'ergonomie du jeu pour le rendre plus agréable à jouer en particulier sur mobile. Nous avons déjà fait quelques modifications en ce sens, comme supprimer un certain nombre de popups qui imposaient de recharger toute la page à chaque clic sur "ok", ou revoir l'affichage de la carte pour y intégrer un zoom. Mais il resterait beaucoup d'aspects à reprendre sur l'ergonomie. Enfin, on verra ce qu'on pourrait modifier concernant le gameplay, mais c'est plus délicat, il faut arriver à rendre le jeu encore plus mieux sans le dénaturer ! Nous avons de toute façon encore beaucoup de travail à faire avant de nous poser vraiment ce genre de questions !

**Thanhatos:** Aujourd'hui, la super équipe de dev' du projet a mis en place une roadmap pour les aspects développement, ce qui montre qu'on avance avec une vision pas trop chaotique. 😄 Me concernant il est temps que je m'active à l'écriture/réécriture des règles pour en simplifier l'approche selon les différents niveaux d'implication des joueurs à venir (on se souvient tous du tutorial qui nous poussait à la faillite et la famine dès le 1ère partie ? Ouais, et bien on va essayer de faire ça plus doucement. :D). J'ai hâte qu'on parvienne à la version beta pour donner vie à ce projet, et qu'on remette des paillettes et des catapultes à nos quotidiens !

## **Eternalwin:** Combien comptes de joueurs les parties Kingdom en cours ? Vous attendiez vous à autant de persones ?

**Bibni:** Actuellement il y a un peu plus de 350 joueurs en jeu ayant rejoint depuis la mise à jour debut novembre. Il y a également une quarantaine de joueurs sur la version anticipée des bataille. Ce qui a permis de remonter des bugs majeurs. Merci encore à toutes et tous pour votre soutien et vos retours sur le forum ou discord !

**Talsi:** J'ai été très agréablement surprise par le nombre de joueurs qui sont venus tester le jeu. Kingdom était en perte de joueurs depuis des années, avec un vague regain d'intérêt lorsque MT a rendu gratuites les options à jetons muxxu pour se revider une fois passé l'engouement pour ces options qui en réalité cassent le jeu. Cela fait super plaisir de voir qu'il y a autant de joueurs qui s'intéressent à Kingdom !

Personnellement c'est une surprise, je ne me rendais pas compte de l'affluence que pouvait encore avoir le jeu.

## **Eternalwin:** Bibni tu es lead sur eKingdom ainsi que sur DinoCard, comment priorise-tu ces deux projets ?

**Bibni:** Question piège 😅 De manière général, je me concentre principalement sur Kingdom.

Il m'arrive de faire des efforts sur Dinocard lorsqu'un besoin urgent se fait ressentir, par exemple dernièrement avec la récupération des donnés.

Mais il est certain que le développement de Kingdom se terminera avant Dinocard.
Le manque de développeurs bénévoles et le temps que je peux allouer ne me permettent pas d'avancer autant sur les deux projets

## **Eternalwin:** Comment avez vous vécu la fermeture de twinoid ?

**Talsi:** Pour ma part, j'avais l'impression que Twinoid n'en finissait pas de mourir depuis des années. Kingdom n'avait pas été mis à jour depuis plus de 10 ans, alors que certains problèmes remontés depuis longtemps par la communauté auraient pu être corrigés (je pense en particulier aux gros tas d'unités barbares, appelés "bouses" par la commu', déposés juste avant de mourir par des joueurs mal intentionnés dans un but de pourrissage des cartes). Cela faisait aussi des années que rien n'était fait contre les multicomptes en jeu, et pas grand chose pour modérer les forums.  Enfin, le passage en open-bar sur les jetons muxxu, rendant n'importe quel joueur potentiellement immortel, a complètement cassé le gameplay (en fait, j'ai toujours trouvé que les options à jetons sur Kingdom cassaient le gameplay, mais elles étaient relativement peu utilisées avant). Permettre à la communauté de refaire le jeu a été à mes yeux la meilleure décision que MT puisse prendre, dans la mesure où ils ne souhaitaient pas réinvestir du temps/des gens/des sous dans le jeu. À partir de là, la fermeture du jeu était assez logique. Je ne l'ai donc pas mal vécu pour ma part : on a été prévenus longtemps à l'avance et on a pu avoir le temps de récupérer quasiment tout ce dont on avait besoin (il nous manque toujours la formule de la révolte ^^).

## **Eternalwin:** Avez vous un petit teasing, nouveauté ou plan a dire aux lecteurs ?

**Talsi:** Du teasing, je ne sais pas 🙂 On va essayer de refaire le jeu au mieux en tout cas ! Les contributions sont les bienvenues, les tests et les retours aussi, ou simplement passer nous faire un petit coucou ça fait toujours super plaisir 🙂 Bref, viendez !
