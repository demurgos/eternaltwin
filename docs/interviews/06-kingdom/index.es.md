# Equipo de desarrollo de eKingdom

## Hola, gracias por participar en esta entrevista. ¿Pueden presentarse brevemente?

**Bibni:** Soy Bibni, descubrí los juegos Motion Twin hace mucho tiempo (a través de PiouzPiouz, Hammerfest y luego Hyperliner). Sus juegos y la gente que conocí en Muxxu/Twinoid me dieron pasión por el desarrollo de videojuegos, que ahora practico profesionalmente con Unreal Engine. Como parte del proyecto EternalTwin, llevo 3 años trabajando en EternalKingdom y en Dinocard Rebirth un año entero.

Por cierto, no sé si podremos dar una mención a ✨ **Glaurung** ✨ que no nos ha dado noticias desde hace casi 3 años 🥺

**Thanhatos:** Thanhatos, "Bárbaro legendario" por el título del fallecido Muxxu, descubrí los juegos de Motion Twin alrededor de 2010/11 e inmediatamente me enganché a Kingdom. En aquella época jugaba muchos juegos de rol y de estrategia, con distintos grados de juego de rol.
Kingdom me llamó la atención porque reunía mi vicio personal por la estrategia 😈, la alegría de los alocados intercambios de juegos de rol 🤪 y la gestión de recursos, además de tener el buen gusto para renovar juegos y jugadores 😋. ¡Todo en un solo juego! En el proyecto EternalTwin, me ocupo de EternalKingdom como escriba y asesor de reglas.

**Talsi:** ¡Hola! Soy Talsi y descubrí los juegos Twinoid con Hordes, recomendado por amigos. Luego probé algunos de los otros juegos de MT, incluido Kingdom, que me gustó especialmente. Hago bastante desarrollo en mi trabajo, incluso si no es mi "negocio principal", y todavía tengo algunos proyectos de desarrollo personal en mi tiempo libre, que algún día pueden llegar a buen término, o al menos me han enseñado un montón. EternalKingdom es una oportunidad para usar algunas de mis habilidades en un buen proyecto con gente agradable, aprender nuevos trucos y revivir un juego al que he jugado mucho.

**Drange:** Drange, "explorador en ciernes" era mi nombre, o "probador de tonterías", y me uní a Kingdom unos días después de su lanzamiento a principios de 2010. Luego descubrí que un amigo en la vida real estaba jugando e intercambiamos impresiones. Mi segunda partida fue una revelación cuando conocí a Glaurung y Logortho. Con muchas preguntas, las pruebas comenzaron luego de conversaciones con Musaran. Desde el interrogatorio hasta la investigación, terminé llegando al fondo de muchas cosas (aunque todavía era un tonto en lo que respecta al manejo del trigo). Pronto me di cuenta de que la desestabilización es un excelente caldo de cultivo para quienes quieren aprovechar las oportunidades.

## ¿Por qué eligieron Kingdom como su proyecto? ¿Fue su juego preferido? ¿El único juego Twinoid que jugaron?

**Thanhatos:** Jugué Snake rápidamente, probé un par de juegos más, hice algunos giros (demasiado) rápidos en Hordes (que, por cierto, me gustaría jugar más en serio en el futuro), pero Kingdom Era claramente mi gran favorito. Combina varias formas de juego en una, se puede jugar solo o con otros, en serio o no, y cada partida es diferente del anterior.

**Talsi:** Kingdom es claramente mi juego preferido, ¡en el que pasé más tiempo! Me gusta mucho el equilibrio entre estrategia/táctica y diplomacia, que puede cambiar completamente de una partida a otra. También es un juego en el que las partidas son lo suficientemente largas (en promedio, alrededor de un mes) para construir algo, y lo suficientemente cortas para renovarse y permitirte explorar otras estrategias y conocer nuevos jugadores. O reencontrarnos con viejos conocidos en situaciones totalmente diferentes, ya que los enemigos mortales de una partida pueden convertirse en aliados indispensables de otra.

**TRANSLATOR NOTE: original text doesn't clarify who is saying this. I assumed it was Bibni. Please, verify before releasing. If this makes the final cut, then... Hi world :)**

**Bibni**: Cuando salió Muxxu, primero me enganché a Intrusion y luego a Snake, al que jugué mucho. También he probado muchos juegos de MT, incluso los menos conocidos como FormulaWan, CaféJeux, CroqueMonster y HyperLiner, que definitivamente es mi juego favorito, donde pasé 4 años siendo explotado por jugadores con el Kortex y donde conocí a un muchos amigos de esa época. Me gustaría aprovechar esta oportunidad para hacer una dedicatoria a Lifaen35 y Terminator35, Sodimel, Dingonikolas y todos los demás miembros de HyPeRaCtIfS 😄. Volviendo a Kingdom. Elegí este proyecto por curiosidad, recordando el principio de combinar-3 que se encuentra en algunos juegos de MT y queriendo probar suerte en este desafío porque me encanta desarrollar juegos multijugador. Descubrí un juego que era mucho más profundo de lo que pensaba y me hizo querer seguir con él y reproducir la experiencia original.

**Drange:** Este es mi juego favorito, donde conceptos simples han producido un entorno complejo y rico. Kingdom es un OVNI, con sus mapas abiertos donde los jugadores llegan, crecen y luego mueren, sin que se establezca ninguna hegemonía. La mensajería es el 80% del juego estando fuera del juego, increíble. He jugado otros juegos, pero más para esperar a que se mueva un general que otra cosa. Estoy en servicio técnico pero en el lado de la base de datos. Los desarrollos que hago no son útiles para el proyecto, o sólo marginalmente.

Sirvo como archivo para ciertos aspectos del juego.

## Hoy en día, eKingdom todavía se encuentra en la fase alfa abierta. ¿Cuáles son sus planes para el futuro? ¿Hay algún plan para entrar en versión beta?

**Bibni:** Por el momento, todavía tenemos tres sistemas principales que implementar antes de que podamos pasar a una fase beta. Batallas (alfa 6), gestión comercial y de quiebras (alfa 7) y vasallaje (alfa 8). Alfa 9 lógicamente será la última, faltando sistemas como torres e inactividad. Se lanzará una versión beta una vez que todos los sistemas principales estén instalados y se agregarán el panteón y las estadísticas.

**Talsi:** Bibni hizo un gran mapa de ruta que resumió muy bien 😄 El objetivo principal es volver a tener un juego jugable, utilizando los mecanismos del antiguo Kingdom. En segundo lugar, queremos mejorar la ergonomía del juego para que sea más divertido jugarlo, especialmente en dispositivos móviles. Ya hemos realizado algunos cambios en esta dirección, como eliminar una cierta cantidad de ventanas emergentes que requerían que se recargara toda la página cada vez que se hacía clic en el botón "Aceptar", o rediseñar la visualización del mapa para incluir una función de zoom. Pero aún quedan muchos aspectos ergonómicos que es necesario mejorar. Finalmente, veremos qué podemos hacer con la jugabilidad, pero eso es complicado - ¡tenemos que mejorar el juego sin cambiar su carácter! En cualquier caso, ¡todavía tenemos mucho trabajo por hacer antes de que realmente podamos plantearnos este tipo de preguntas!

**Thanhatos:** Hoy, el súper equipo de desarrollo del proyecto ha elaborado un mapa de ruta para los aspectos de desarrollo, lo que demuestra que estamos avanzando con una visión que no es demasiado caótica. 😄 En lo que a mí respecta, es hora de ocuparnos de escribir/reescribir las reglas para simplificar el enfoque de acuerdo con los diferentes niveles de participación de los jugadores por venir (¿recuerdan el tutorial que nos llevó a la bancarrota y al hambre en la primer partida? Sí, bueno, vamos a intentar hacerlo un poco más fácil :D). ¡No puedo esperar hasta que lleguemos a la versión beta para darle vida a este proyecto y poner brillo y catapultas de nuevo en nuestra vida diaria!

## **Eternalwin:** ¿Cuántos jugadores hay en las partidas de Kingdom? ¿Esperaban tanta gente?

**Bibni:** Actualmente hay poco más de 350 jugadores que se han unido desde la actualización a principios de noviembre. También hay alrededor de 40 jugadores en la versión de batalla inicial. Esto nos ha permitido solucionar algunos errores importantes. ¡Gracias nuevamente a todos por su apoyo y comentarios en el foro o en Discord!

**Talsi:** Me sorprendió gratamente la cantidad de jugadores que vinieron a probar el juego. Kingdom había estado perdiendo jugadores durante años, con un vago resurgimiento del interés cuando MT hizo gratis las opciones de tokens muxxu, solo para recuperarse una vez que pasó la locura por estas opciones, que en realidad arruinan el juego. ¡Es fantástico ver a tantos jugadores interesados en Kingdom!

Personalmente es una sorpresa, no me di cuenta de lo frecuentado que podría estar el juego todavía.

## **Eternalwin:** Bibni, eres líder tanto en eKingdom como en DinoCard, ¿cómo priorizas estos dos proyectos?

**Bibni:** Pregunta capciosa 😅 En términos generales, me concentro principalmente en Kingdom.

De vez en cuando, pongo un poco de esfuerzo en Dinocard cuando hay una necesidad urgente, como recientemente con la recuperación de datos.

Pero es seguro que el desarrollo de Kingdom terminará antes que Dinocard.
La falta de desarrolladores voluntarios y el tiempo que puedo dedicar significan que no puedo avanzar tanto en ambos proyectos.

## **Eternalwin:** ¿Cómo llevaron adelante el cierre de Twinoid?

**Talsi:** Personalmente, tuve la impresión de que Twinoid llevaba años muriendo. Kingdom no se había actualizado desde hacía más de 10 años, incluso aunque la comunidad ya había señalado desde hacía tiempo ciertos problemas que podrían haberse corregido (pienso en particular en los grandes montones de unidades bárbaras, llamadas "bouses" por la comu', lanzado justo antes de morir por jugadores mal intencionados con el objetivo de pudrir los mapas). También habían pasado años desde que se hizo algo con respecto a las cuentas múltiples en el juego, y no se había hecho mucho para moderar los foros. Finalmente, el cambio a la barra abierta en los tokens muxxu, que convertía a cualquier jugador en potencialmente inmortal, rompió por completo la jugabilidad (de hecho, siempre he encontrado que las opciones de tokens en Kingdom rompen el juego, pero antes se usaban relativamente poco). Permitir que la comunidad rehaga el juego fue, en mi opinión, la mejor decisión que MT pudo haber tomado, ya que no querían reinvertir tiempo/personas/centavos en el juego. A partir de ahí cerrar el juego era bastante lógico. Yo no la pasé mal: nos avisaron con mucha antelación y pudimos recuperar casi todo lo que necesitábamos (todavía nos falta la fórmula de la revuelta ^^)

## **Eternalwin:** ¿Tienen algún avance, noticia o planes para los lectores?

**Talsi:** Avances, no lo sé 🙂 ¡Intentaremos  hacer que el juego sea tan bueno como podamos! Las contribuciones son bienvenidas, al igual que las pruebas y los comentarios, o simplemente visítenos para un saludo rápido, siempre es un gran placer 🙂 De todos modos, ¡dense una vuelta!