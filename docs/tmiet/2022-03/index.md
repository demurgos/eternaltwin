# 2022-03: This Month In Eternaltwin #6

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

Bibni updated the [game list](https://eternaltwin.org/games) and added a
[list of all news articles](https://eternaltwin.org/docs/tmiet/).

# Neoparc

Hello Dinoz masters of [Neoparc](https://neoparc.eternaltwin.org/)!

We migrated all our data to a new database. This will solve most of our recent
issues, and you should already feel improved performance.

Weekly Raids will now be a single 30 minutes party! The Flamebath skill has been
slightly recalibrated and the point recounts on player profiles have been fixed.

We are now working on clans and wars.

![Neoparc](./neoparc.png)

# Eternal Kingdom

During the last few months, we released the 0.3.2 Alpha for [Eternal Kingdom](https://kingdom.eternaltwin.org/).
This version was mainly fixing several bugs and brings us the option to kill our
Lord by using "/death". The full patch notes are on Discord.

The 4th Alpha is now in progress. You can find more details on what is planned
on [the GitLab Wiki - Alpha 4 Roadmap](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4).
We finished refactoring the Event Logs System (for the City, World & Management pages).

The next big step is to implement the WorldMap. The most anticipated part of the game 💃
See you soon 😸!

# Eternal DinoRPG

[EternalDinoRPG](https://dinorpg.eternaltwin.org/) keeps moving forward!
We deployad a new preproduction version. There, you can buy some dinoz, travel,
use the flying shop, check the leaderboard, and see player profiles!

Following the upload of the version 0.2.0, many small bugs were reported and
are already fixed. We are completing our admin page, and we should upload the new
version 0.2.1 soon. It will let us give you gold, epic reward or even
statuses on your dinoz, so you can discover today's world.

The biggest news for this month is the ability to display a dinoz without Flash.
This is only a proof-of-concept for now, and we will improve it. It already lets
us display a full Moueffe without Flash. Once it is ready and working, all the
races will follow!

Regarding the import system, it is in its final stages, and we're only waiting
for some Eternaltwin updates.

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_ Hello everyone, _scrambled_ Ben here, once again coming live to
your shacks via the marvellous radio-phonic wireless system. _scrambled_ There
are many things to report, so let's get right on to it! _scrambled_

![MyHordes - Wireless technology ad](./myhordes.png)

First and foremost, it seems the undead hordes have lost all traces of manners.
Reportedly, they no longer _scrambled_ wait for small towns to reach a
population of 40 brave citizens before beginning their vicious nightly assault!
Luckily, a Mysterious Stranger of ambiguous origin seems to be helping these poor
folks out _scrambled_ ... and here I am, having to still put on my pants myself!

Speaking of general strangeness, Connor seems to be avoiding me ever since I've
used my leprechaun costume to steal his favourite Järpen table. _scrambled_ seen
him building a weird cross-shaped contraption out of chocolate behind the barn.
Not sure what this is all about, but I've feel like I shouldn't leave the
recording room between the 13th and 21st of April...

Alright, I think this should cover most of the latest going-ons in our beautiful
zombie-infested desert. _scrambled_ Hopefully, we will all survive the night
and _grrrrrrrrz_

# Closing words

You can send [your messages for April](https://gitlab.com/eternaltwin/etwin/-/issues/42).

This article was edited by: Bibni, Biosha, Brainbox, Demurgos, Jonathan, MisterSimple,
Patate404, and Schroedinger.
