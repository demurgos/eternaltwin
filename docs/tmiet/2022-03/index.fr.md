# 2022-03 : Ce mois-ci sur Eternaltwin n°6

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

Bibni a mis à jour la [list des jeux](https://eternaltwin.org/games) et ajouté
[une liste avec tous les articles de news](https://eternaltwin.org/docs/tmiet/).

# Neoparc

Bonjour maîtres Dinoz de [Neoparc](https://neoparc.eternaltwin.org/) !
Nous avons migré toutes nos données vers une nouvelle base de données. Cela
devrait résoudre la plupart des problèmes récents, et vous devriez déjà
ressentir les gains de performance.

Les Raids hebdomadaires se feront désormais à un seul groupe de 30 minutes !
La compétence Bain de Flammes a été légèrement recalibrée et les recomptages de
points sur les fiches des joueurs ont été réparés.

Nous travaillons maintenant sur les clans et les guerres.

![Neoparc](./neoparc.png)

# EternalKingdom

Durant les mois précédents, nous avons déployé avec succès la version Alpha 0.3.2
d'[Eternal Kingdom](https://kingdom.eternaltwin.org/).
Celle-ci apportait son lot de corrections et la possibilité de tuer son seigneur en suivant l'URL "/death".
Le contenu détaillé est présent sur Discord.

L'Alpha 4 est en route. Vous trouverez plus de détails sur ce qui est prévu sur
[le wiki GitLab : Feuille de route Alpha 4](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4).
Nous avons terminé la ré-implémentation de l'historique des événements (pour les pages Ville, Carte et Gestion).

La prochaine étape est désormais d'implémenter la carte du monde. Partie du jeu la plus attendue 💃
A très bientôt 😸 !

# Eternal DinoRPG

[EternalDinoRPG](https://dinorpg.eternaltwin.org/) continue sa route !
Nous avons pu mettre en ligne une nouvelle
version de préproduction. Il est possible d'y acheter des dinoz, se déplacer,
utiliser la boutique volante, voir le classement, mais aussi de voir le profil
des autres joueurs !

Suite à la mise en ligne de la version de test 0.2.0, plein de petits bugs ont
été remontés et sont déjà corrigés. Nous sommes en train de terminer notre page
d'administration et allons pouvoir mettre en ligne la version 0.2.1 qui nous
permettra de vous redonner de l'or, des récompenses épiques ou même des statuts
sur vos dinoz afin que vous puissiez découvrir à quoi ressemble le monde aujourd'hui.

La grande nouvelle de ce mois par contre est la possibilité d'afficher des dinoz
sans Flash. Ça ne reste encore qu'une preuve de concept que nous allons encore
peaufiner, mais il nous est possible de faire afficher un moueffe dans sa
totalité sans avoir recours à Flash. Une fois ce composant fonctionnel et stable,
les autres races vont pouvoir suivre !

L'import quant à lui est en phase finale de développement, nous n'attendons plus
que quelques mises à jour du côté d'Eternaltwin.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

Bonjour tout le monde ! _brouillé_ C'est Ben, à nouveau sur les ondes via le
merveilleux système radiophonique sans fil. _brouillé_ Il y a beaucoup de
choses à dire, alors allons y ! _brouillé_

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

En premier lieu, il semblerait que les hordes de zombies ont perdu leurs bonnes
manières. On nous signale _brouillé_ qu'ils n'attendent plus que les régions
non éloignées disposent de 40 courageux citoyens avant de lancer leurs vicieuses
attaques nocturnes ! Heureusement, un Mystérieux Étranger d'origine ambigüe
semble aider ces pauvres gens _brouillé_... Tandis que moi, je dois toujours
m'habiller par moi-même ! Bande d'assistés

En parlant de choses étranges, Connor semble m'éviter depuis que j'ai utilisé
mon costume de lutin vert pour voler sa table Järpen préférée. _brouillé_ l'ai
vu construire une construction bizarre en forme de croix à partir de chocolats
derrière la grange. Je ne sais pas pourquoi, mais je sens que je ne devrais pas
quitter la salle d'enregistrement entre le 13 et le 21 avril...

Okay ! Je pense qu'on a communiqué les dernières informations sur notre
magnifique désert infesté de zombies. _brouillé_ En espérant que nous survivrons
cette nuit et _grrrrrrrrrrz_

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition d'avril](https://gitlab.com/eternaltwin/etwin/-/issues/42).

Cet article a été édité par : Bibni, Biosha, Brainbox, Demurgos, Jonathan, MisterSimple,
Patate404 et Schroedinger.
