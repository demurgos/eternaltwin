# 2022-09 : Ce mois-ci sur Eternaltwin n°12

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

Tous les jeux sont passés sur le nouveau système de déploiement. Les chefs de
projets peuvent désormais envoyer de nouvelles versions en ligne dès qu'ils le
souhaitent.

# eMush

Bienvenue à tous dans cette section de consacrés à
[eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

## Quoi de neuf ?

La dernière fois que nous avions donné des nouvelles en juillet-août 2022, nous
avions commencé à travailler sur les maladies dans le cadre de la **Disease Update**.

![eMush1](./emush1.png)

Nous avons continué à travailler sur cette mise à jour et nous avons le plaisir
de vous annoncer que **la totalité des maladies** physiques, psychologiques et
des blessures **sont fonctionnelles** !

![eMush2](./emush2.png)

Une autre grande avancée en cours est la réalisation d'un **panel administrateur**.

![eMush3](./emush3.png)

Cela nous permettra de modifier les paramètres du jeu beaucoup plus facilement :
nous n'aurons plus besoin de réinitialiser la base de données à chaque modification.

A moyen terme, cela nous rapproche d'une **alpha-test ouverte**. 🤞

## Prochainement

Maintenant que les maladies fonctionnent, nous développons les moyens de les
attraper. Les **maladies spontanées, alimentaires, traumatismes, IST, contagieuses**
et **liées aux spores** sont d'ores et déjà fonctionnelles.

![eMush4](./emush4.png)

Il nous manque principalement à vous infliger des blessures via les **armes**, qui
sont le prochain grand chantier de cette mise à jour.

Enfin, nous vous donnerons quelques informations sur les symptômes des maladies
via leurs info-bulles (ça peut servir) et nous en aurons fini avec la **Disease Update** !
(Ce qui veut aussi dire qu'une nouvelle alpha est proche. 👀)

## Super ! Quand ça ? Comment on joue ?

Nous ne pouvons nous avancer avec certitude sur une date pour le moment. Nous
vous tiendrons informés dès que nous avons une date précise.

Si vous êtes impatients, le meilleur moyen d'être informé de la progression du
projet est de rejoindre [le discord Eternaltwin](https://discord.gg/SMJavjs3gk),
cela permet également de nous soutenir !

eMush étant un projet open-source, nos avancées sont aussi [disponibles sur Gitlab](https://gitlab.com/eternaltwin/mush/mush/-/milestones/8#tab-issues).

Merci de nous avoir lu, et à bientôt sur le Daedalus !

# Neoparc

C'est le début non officiel de la **guerre des clans** sur [Neoparc](https://neoparc.eternaltwin.org/) !

Bonjour maîtres Dinoz !

Nous avons désormais quelques nouveautés pour votre
clan adoré. Vous pouvez dès maintenant commencer à remplir les totems qu'il vous
demandera et ainsi commencer à marquer des points pour la guerre en cours.

Attention, cette première guerre des clans est une guerre de calibration et
d'intégration de fonctionnalités, ce qui veut dire qu'elle aura une durée
indéterminée et qu'elle présentera sans doute quelques bugs à résoudre.
D'ici là prochaine intégration, je vous souhaite à tous d'excellentes fouilles !

Prenez garde, la guerre n'est pas le seul danger, on raconte que des esprits
malfaisants rôdent dans Dinoland ! Venez participer à **l'événement Halloween** !

Ah et j'oubliais : vous pouvez aussi dès maintenant télécharger les données
de vos Dinoz depuis la section Mon compte.

Bon jeu à tous.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_crépitement radio_

Bonjour à vous tous, j'espère que tout va bien pour vous, et pensez surtout à sortir av _brouillé_

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

La chasse aux bugs continue, notre équipe de développeurs, accompagnés de 2
gardiens et d'un éclaireur parcourent tout l'Outre-Monde pour vous fournir une expérience
des plus _brouillé_

Les préparations pour la 15ème saison sont en cours, soyez en sûrs que ça v _brouillé_

De plus on me signale que les chamans ont enfin compris qu'arroser deux fois la
même zone était inutiles ! _crépitement radio_

_tousse_ _crépitement radio_

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition d'octobre](https://gitlab.com/eternaltwin/etwin/-/issues/54).

Cet article a été édité par : Demurgos, Dylan57, Evian, Fer, Jonathan, Patate404 et Schroedinger.
