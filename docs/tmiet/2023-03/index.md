# 2023-03 : Ce mois-ci sur Eternaltwin n°18

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

Eternaltwin needs developers! All games progress at their own rythm but the main websiteThe different games are progressing at their own pace, but the main site used to unify everything is having a hard time. If you are interested, contact on Discord Patate404#8620, Demurgos#8218 or Biosha#2584.

Update 0.12 is approaching and focuses on the release of its packages for developers.

# DinoCard

The first version of the site is now online. It is already possible to create your deck and test it against other players.

In these first months of development, we have implemented the combat loop and implemented a total of 96 cards (71 dinoz and 25 incantations). It is already possible to test battles with almost all special abilities of the cards (25 / 28).

The next step of the project is now to implement the inner workings of the effect system which will be used to implement almost all other cards in the game. We also discussed how to give rewards to the player. You can read it on dinocard.net or on the associated discord thread.

Here are some statistics about the cards in the game

![DinoCard - Cards stats](./dinocard.png)

# Popotamo

After a (very) long interruption, the coding resumed in the last days of March under the impulse of @Aredhel. His first action was to tackle the most blocking and difficult bug: some legitimate words were refused in case of masonry, preventing to validate the move. The patch is waiting to be uploaded to the ePopotamo website.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

Hello everybody !

Small announcement to let you know that an art contest is now open. It ends on April 30th, [all the information is available on this subject](https://myhordes.eu/jx/forum/9/68041). To your pencils!

# eMush

Welcome to this section dedicated to [🍄 eMush](https://emush.eternaltwin.org/),
the project to save Mush.

## What is new ?

Last month we announced the release of the Spores for All! Update in open alpha.

More than 150 of you joined a ship!

We have spent the month of March stabilizing the game thanks to your numerous feedbacks: the whole eMush team thanks you very much! ❤

![eMush Spore Update for All](./emush1.png)

On the other hand, we also had the opportunity to talk this month with blackmagic, one of the developers of Mush, followed by a series of questions and answers with the community.

It was an opportunity to discuss the choices that were made on Mush, which was very enriching for us.

We came out of this interview with a lot of good advice and a lot of motivation to offer you a great Mush remake.

## Coming next

During this busy month and following your feedback, we also started working on the next version of eMush which will introduce hunters.

We can already eliminate the starting ships with turrets on the development version:

![eMush Coming soon](./emush2.png)

We intend to offer you this update very soon and add features (patrollers, debris) regularly.

## Nice ! How can we play ?

🎮 Play [eMush](https://emush.eternaltwin.org)

💬 [Discord](https://discord.gg/SMJavjs3gk)

Thanks for reading, and see you soon on the Daedalus!

# Eternal DinoRPG

Hello everyone and thank you for your attention for this part dedicated to [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

Greetings Masters Dinoz!

Version 0.6.0 is ready to be put online. Unfortunately there are no imports available yet, but we are doing our best to get there as soon as possible.

No big news visible, but a big refactoring of the code is in progress now that we have been able to step back from the project.

The team is still looking for a new name for the game. Don't hold back!

See you soon. The EternalDino team.

# Closing words

You can send [your messages for April](https://gitlab.com/eternaltwin/etwin/-/issues/60).

This article was edited by: Biosha, BrainBox, Demurgos, Dylan57, Evian, Fer, Patate404, Schroedinger et Zenoo.
