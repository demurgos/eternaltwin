# 2022-06 : Ce mois-ci sur Eternaltwin n°9

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

Nous travaillons toujours à permettre aux chefs de projet à envoyer leurs mises
à jour eux-mêmes.

# Neoparc

Bonjour maîtres Dinoz ! Une belle mise à jour de [Neoparc](https://neoparc.eternaltwin.org/) est désormais en ligne !

Elle ajoute la section du _Chaudron Magique_ aux ruines Mayincas pour enfin mettre
vos Dinoz à la cuisine ! De nouveaux objets sont disponibles via quelques-unes
des recettes de ce Chaudron, alors n'attendez-plus !

De plus, une bonne vague de bugfix a été réalisée. La compétence Course ne fait
désormais plus crasher un déplacement. Le téléchargement de ses messages dans
la messagerie est maintenant réparé. Le clic sur un profil joueur depuis un
clan ne devrait normalement plus pointer vers une page blanche et un
pop-up de confirmation a été ajouté sur le level-up des Dinoz. Finalement, le
Pruniac n'est plus donné lors d'une fusion et il a été ajouté au bazar, comme
tous les nouveaux objets !

Merci et à bientôt sur Dinoland.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_crépitement radio_ Bonjour, Hello, Guten Tag et Buenos Días à vous tous !

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Ne détestez-vous pas voir partout des bouts de texte dans une langue étrangère ?
C'est certainement le cas, c'est pourquoi je n'ai pas pris de vacances depuis des années !
Bon après, il y a aussi tout ce truc d'apocalypse zombie... _crépitement radio_
En tirant parti de la puissance de Crowdin, vous tous, vous pouvez maintenant
aider à éliminer les étrangers _grésillements radio étranges_ directement depuis
MyHordes en utilisant notre nouveau système de haute technologie de traductions
en-contexte !

Mais ce n'est pas tout ! Nous avons fait quelques mises à jour plus
techniques de notre _crépitement radio_ donc tout est maintenant mie _beeeeeep_
_crépitement radio_ stable et rapide ! _Minutes de silence et crépitements radio_
et si vous ajoutez juste un peu d'acide de batterie, vous avez maintenant un
sérum de goule maison 100% efficace et sûr à utiliser.

En parlant de communication efficace, nous avons amélioré nos antennes radio et
réattribué des longueurs d'onde supplémentaires pour améliorer les forums
mondiaux ! Chaque langue dispose désormais de forums distincts pour discuter
des aspects du jeu, demander de l'aide ou simplement passer du temps et s'amuser
avec d'autres joueurs de MyHordes.

# Eternal DinoRPG

Bonjour chers Maîtres [DinoRPG](https://dinorpg.eternaltwin.org/).

La fin de ce mois de juin annonce pour nous la sortie de la version 0.3.0 !
Celle-ci apporte un des éléments cœur du jeu : **le leveling** !

Il est à présent possible de déplacer son dinoz pour lui faire gagner de
l'expérience et ainsi monter aux niveaux supérieurs pour apprendre de nouvelles
compétences.

À cette mise à jour s'ajoute les boutiques disponibles dans tout DinoLand ainsi
que la page d'accueil avec les nouveautés ! Quelques petites nouveautés
annexes sont également disponible telles que la page des ingrédients, un nouveau
bandeau de bas de page ou un mécanisme pour trier les compétences.

À l'instar de MyHordes, nous avons mis en place [Crowdin](https://crowdin.com/project/edinorpg)
afin que tout le monde puisse nous aider dans les traductions. Donc n'hésitez
surtout pas à aller faire un tour dessus et y participer !

# Eternal Kingdom

Bonjour seigneurs de [Kingdom](https://kingdom.eternaltwin.org/) !

Ce mois ci, l'équipe de développement s’agrandit et accueille désormais **Benevolens**.
Bienvenue à lui !

Le développement de l'Alpha 4 touche à sa fin et sera bientôt disponible entre
vos mains.

![Kingdom](./kingdom.jpg)

Son déploiement se déroulera courant juillet. Vous pourrez alors rejoindre les
différents mondes et contempler leurs cartes et voir qui s'y trouve. N'hésitez
pas à passer sur [le discord Eternaltwin](https://discord.gg/ERc3svy) pour être
tenu informé de la date de sortie. Puis venir tester et signaler tout problème
pouvant survenir durant cette nouvelle version.

Le travail sur l'Alpha 5 a également commencé. Elle permettra l'interaction avec
ses généraux (transfert d'unités, les déplacements, prises de territoires si le
territoire est vide, ...).

Retrouvez toutes les avancées ici :
- [Alpha 4 Roadmap](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4)
- [Alpha 5 Roadmap](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-5)

Merci pour votre lecture et à bientôt.

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de juillet](https://gitlab.com/eternaltwin/etwin/-/issues/51).

Cet article a été édité par : Bibni, Biosha, Brainbox, Demurgos, Dylan57,
Jonathan, MisterSimple, Patate404 et Schroedinger.
