2022-12: Dieser Monat in Eternaltwin #15

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

_Biosha_ hat ein Tool zur Überwachung des Website-Status und zur Versendung von Warnungen bei Problemen implementiert. Dieses Tool wird in Kürze bereitgestellt werden.

_Moulins_ hat das Hammerfest-Archiv aktualisiert, um Vorräte und Quest-Status zu erhalten.

# Eternalfest

[Eternalfest](https://eternalfest.net/) lässt dich sowohl klassische als auch neue
Levels des Hammerfest-Plattformspiels spielen.

_Rarera_ und _Maxdefolsch_ haben am 25. Dezember ein neues Spiel veröffentlicht: [L'ancienne source](https://eternalfest.net/games/eb10aaf1-23dc-4b3f-a543-66a0596459d4).
Es enthält Hunderte von großartigen Levels, wir empfehlen dir, es auszuprobieren.

Es gab auch eine Menge Arbeit, um die interne Verwaltung der Spiele zu ändern. Diese
sollte die Unterstützung für einige lang erwartete Funktionen wie Inventarseiten,
vollständige Übersetzungsunterstützung oder präziser Ladefortschritt. Die Serverseite ist größtenteils
abgeschlossen, und wir arbeiten jetzt an der Benutzeroberfläche. Diese Arbeit dient auch als
Prototyp für eine verbesserte Unterstützung von Spielen auf Eternaltwin-Ebene (und ersetzt
die manuelle Konfiguration).

# eMush

Willkommen in diesem Bereich, der [eMush](https://emush.eternaltwin.org/) gewidmet ist, dem Projekt zur Rettung von Mush.

## Was ist neu?

In diesem schönen Monat Dezember werden eMush-Admins und Anpassungsfreaks
frohlocken. In der Tat haben wir hart gearbeitet, damit verschiedene Daedalus Schiffe mit ihren eigenen
Einstellungen Seite an Seite starten können. Und das Beste von allem ist, dass alles direkt über das Admin-Panel auf der Website konfiguriert werden kann.

![eMush](./emush.png)

Neben anderen Verbesserungen haben wir die Archive für ältere Schiffe verbessert. Sobald ein Spiel beendet ist, löschen wir die meisten Entitäten und behalten nur noch Informationen zu eurem digitalen Erbe.

Letzten Monat haben wir die Fertigstellung der englischen Übersetzungen angekündigt. Nun,
Administratoren können jetzt die Sprache auswählen, wenn sie eine Daedalus erstellen.

## In Kürze

Wie im letzten Monat angekündigt, werden wir weiterhin an den
Migrationen unserer Datenbank arbeiten, damit wir neue
Funktionen ermöglichen, ohne die laufenden Schiffe zu zerstören.

Nachdem die Archivierung der Schiffe abgeschlossen ist, müssen sie noch korrekt in deinem Profil angezeigt werden.

Wir werden auch an Verbesserungen der Benutzerprofile arbeiten.

## Wann findet die offene Alpha statt?

Wir werden euch über TMIET auf dem Laufenden halten, aber der beste Weg, den Fortschritt zu verfolgen, ist dem [Eternaltwin-Discord](https://discord.gg/SMJavjs3gk) beizutreten.

eMush ist ein Open-Source-Projekt, du kannst unsere Fortschritte auch auf GitLab verfolgen. Insbesondere den [Fortschritt des **Spores for All!**-Updates](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Vielen Dank für deine Aufmerksamkeit, und bis bald an Bord der Daedalus!

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

_Radioknistern_

Hallo zusammen, ich hoffe, dass es euch in dieser kalten Jahreszeit gut geht _verschlüsselt_

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Ich hoffe, ihr habt nicht zu viel Weihnachtsschokolade gegessen, denn offenbar sind sie _verschlüsselt_

Ich denke auch, dass ihr _verschlüsselt_ vergleichen möchtet, deshalb haben wir ein einzigartiges
Ranking-System für alle Auszeichnungen entwickelt.

Die Vorbereitungen für Saison 15 sind in Gange. Wir haben eine Umfrage eröffnet und bitten um
Teilnehmer für die geschlossene Beta, verfügbar auf der Frequenz 102,4 MHz
(Weltforum), bis zum 8. Januar 2023! _Radioknistern_

Darüber hinaus haben wir verschiedene Aspekte unserer Website optimiert, um sie
für dein Handy-Anthrax-GPS-System zu optimieren. _Radioknistern_

Mmmmh, ich glaube, es ist _verschlüsselt_ aber was ist das _verschlüsselt_ BEN! Sag der
Krähe, dass sie von dem _verschlüsselt_ runterkommen _verschlüsselt_

# Abschließende Worte

Du kannst deine [Nachrichten für Januar](https://gitlab.com/eternaltwin/etwin/-/issues/57) senden.

Dieser Artikel wurde bearbeitet von:  Breut, Demurgos, Dylan57, Fer, Patate404 und Schroedinger.
