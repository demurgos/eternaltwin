# 2022-03 : Ce mois-ci sur Eternaltwin n°8

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

Après une brève pause en avril, [le forum Eternaltwin](https://eternaltwin.org/forum)
est de nouveau actif.

Eternaltwin est dorénavant un projet Open Source officiel sur GitLab. GitLab
est la plateforme collaborative que nous utilisons pour tous les projets
Eternaltwin. Grâce à ce nouveau statut, nous avons gratuitement accès à toutes
les fonctionnalités de GitLab.

En juin, nous allons travailler à améliorer les permissions GitLab. En ce
moment, la plupart des contributeurs ont accès à _tous_ les jeux. Nous allons
changer ça afin de n'octroyer les permissions que pour des projets _précis_.
Cela permettra à chaque chef de projet de mieux gérer qui travaille sur chaque
projet.

Finalement, nous nous occupons de configurer le nouveau serveur mentionné il y
a quelques mois. Le chefs de projet auront alors le contrôle complet des mises
en lignes des nouvelles versions, sans devoir se coordonner avec les
administrateurs Eternaltwin.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_grésillements de radio_

_long silence_

_froissements_

Houf houf... salut tout le monde... Ici Ben, à nouveau. Désolé, je suis un peu
essouflé, Connor m'a fait bouger tout un tas de boîtes remplies de papiers. Il
dit que c'est pour pour une sorte de vote, ou quelque chose du genre...

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

En parlant de papiers, nous avons un nouveau système de tri dernier cri
pour trier vos messages privés. Tout vos messages sont enfin triés correctement.
Nous avons même ajouté une boîte de réception dédiée au courrier de fans de
notre station radio, elle déborde déjà de vos lettres chers _grésillements_
citoyens. Je me demande juste pourquoi Connor l'a intitulée "plaintes et
menaces de mort".

Peu importe, passons à autre chose. C'est l'heure des infos ! On a eu des
retours comme quoi les gens veulent s'amuser à nouveau avec les esprits, et se
réunir dans des bâtiments bizarres de leur ville. Qu'est-ce qui passe avec les
âmes par ici ? Préparez-vous ! Les charlatans... je veux dire les
chamans seront de retour dans une ville spécial !

_grésillement_ ...hier, un rapport est paru sur les habitants qui se sont mis
à agiter des drapaux. J'en ai vu du Chili, de l'Autriche, du Japon, et même
des étranges qui viennent d'un pays imaginaire soi-disant appelé "la France".
C'est farfelu... _grésillements_

Et le meilleur pour la fin, un message d'intérêt public. Quelques âmes
courageuses s'apprêtent à voyager jusqu'au Monde Au-Delà de Hordes afin de
collecter des informations sur tout un tas phénomènes étranges. Combien d'objets
sont cachés dans le sable ? Quel est le risque de mourir à cause de l'eau
nauséabonde trouvée dans les ruines ? De quand date la dernière douche
de notre chaman ? _grésillement_ Si vous voulez aider, merci de passer
voir [ce message sur le tableau d'annonces de notre station
radio](https://myhordes.eu/jx/forum/9/42008).

# eMush

Après une longue période sans nouvelles, nous revoilà avec un petit point sur
l'avancée du projet [eMush](https://emush.eternaltwin.org/). Le jeu avance bien
plus lentement depuis quelques mois, mais ne vous inquiétez pas, nous sommes
toujours là.

Première bonne nouvelle, l'interface graphique avance à grand pas, comptez
maintenant 18 pièces sur les 26 que compte le Daedalus. Un gros travail a dû
être produit sur les déplacements et l'ordre d'affichage des sprites mais la
nouvelle solution implémentée devrait être plus rapide que la précédente.

La version 0.4.3, qui proposera une interface graphique complète, est donc en
bonne voie. Il ne reste plus qu'à ajouter les positions assises et allongées
des personnages et ajouter les pièces manquantes (tourelles, Salle des moteurs
et pont).

J'espère revenir vers vous le mois prochain avec la version 0.4.3 d'eMush et
une alpha pour tester tous nos ajouts.

# Kube

Depuis quelques semaines, les joueurs _Booti_, _Moulins_ et _TheToto_
ont repris en main le projet Kube.

## État des lieux

Fin 2020, _Moulins_ a créé une sauvegarde du monde de Kube. [Un topic avait été
créé sur le forum de Kube](http://kube.muxxu.com/tid/forum#!view/492|thread/65361326).
En 2021, la sauvegarde a été mise à jour. Elle contient même les forums de zone
maintenant.

## Ré-implémentation de la génération du monde

Plus récemment, _Booti_ a commencé à ré-implémenter l'algorithme de génération du
monde. Heureusement, le code était dans le fichier SWF du jeu.

Kube a un bug depuis longtemps qui n'avait pas été résolu : la disparition
des îles volantes et les biomes fusionnés. Il s'avère que ce bug était causé
par les nouvelles versions de Flash qui cassait la compatibilité avec le code
initial.

La ré-implémentation corrige ce bug.

## Nouveau client de jeu

Par ailleurs, _TheToto_ crée un nouveau client de jeu qui ne nécessite pas Flash.
Ce n'est que le début, mais il est déjà possible de poser ou prendre des kubes,
et charger les données. Les données peuvent être chargées depuis le serveur
officiel, la sauvegarde mentionnée plus haut ou bien des plans Kub3dit.

[Le code est disponible ici.](https://gitlab.com/eternaltwin/kube/kube-client-html5)

## Démonstration

Île volante générée par l'algorithme de _Booti_, vue avec le nouveau client de
_TheToto_.

![Kube](./kube.png)

Passez sur le salon "Kube", du [Discord Eternaltwin](https://discord.gg/ERc3svy),
si vous voulez discuter de Kube. 😺

# Eternal DinoRPG

Bonjour chers Maîtres [DinoRPG](https://dinorpg.eternaltwin.org/).

La version 0.2.2 a été déployée le mois dernier comme certains ont pu le remarquer.
Cette version apporte quelques changements graphiques avec l'affichage complet
des dinoz partout où ils sont censés apparaître.

Nous sommes à présent sur la partie leveling. Grâce à ceci, vous pourrez voir
évoluer vos dinoz (bon uniquement moueffe pour le moment) et parcourir Dinoland !

# EternalKingdom

Le développement de l'Alpha 4 continue ! La page de **choix du monde** est terminée.
Une ville nous est désormais assigné lorsqu'on rejoint un monde. Et cette même
ville se transforme en ville barbare lorsque l'on meurt.

![EternalKingdom - choix du monde](./kingdom-select.png)

Nous nous concentrons désormais sur la page de la **carte du monde** comprenant
l'affichage des nœuds (capitale du Joueur, ville barbare, Ressource), le menu
latéral et l'historique des événements du monde.

![EternalKingdom - carte du monde](./kingdom-world.png)

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de juin](https://gitlab.com/eternaltwin/etwin/-/issues/48).

Cet article a été édité par : Bibni, Biosha, Brainbox, Breut, Demurgos,
MisterSimple, Patate404, Schroedinger et TheToto.
