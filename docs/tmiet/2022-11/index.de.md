2022-11: Dieser Monat in Eternaltwin #14

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Mit der [Eternaltwin-App](https://eternaltwin.org/docs/desktop) könnt ihr
einfach die Flash-Spiele von Motion Twin spielen. Das Update 0.6.4 wurde veröffentlicht, um
den Zugang zu Minitroopers zu verbessern.

# eMush

Willkommen in diesem Bereich, der [eMush](https://emush.eternaltwin.org/) gewidmet ist, dem Projekt zur Rettung von Mush.

## Was ist neu?

Letztes Mal haben wir die drei Hauptaufgaben zur Veröffentlichung der eMush Version 0.5.0
als offene Alpha-Version beschrieben, das **Sporen für alle!**-Update:

- 🗃️ Raumschiff-Archivierung, um deine Erfolge zu dokumentieren
- 🇬🇧 Englische Übersetzungen
- 💾 Datenbankmigrationen, um die neue Version einzusetzen ohne Zurücksetzen der bestehenden Raumschiffe

Wir machen langsam aber sicher Fortschritte.

eMush verfügt jetzt über ein einfaches Archivierungssystem, das einen Teil der Daten über dein
Raumschiff speichert, sobald ein Spiel beendet ist.

Außerdem kannst du jetzt auf Englisch spielen!

![eMush](./emush.png)

Demnächst

Das **Sporen für alle!**-Update wird uns einige Monate lang beschäftigen. Wir müssen
unsere Datenbankstruktur überarbeiten, um alle Daten zu speichern, um einige
epische Bestenlisten zu erstellen. 👀

Ein weiterer wichtiger Punkt ist die Datenbankmigrationen, um zu vermeiden, dass dein Fortschritt verloren geht.
Punkt. Du sollst in der Lage sein, bis ans Ende aller Tage mit deiner Leistung angeben zu können. 💪

Sobald diese Probleme gelöst sind, werden wir die offene Alpha-Version veröffentlichen.

## Großartig! Und wann?

Wir werden euch über TMIET auf dem Laufenden halten, aber der beste Weg, den Fortschritt zu verfolgen, ist
dem [Eternaltwin-Diskord](https://discord.gg/SMJavjs3gk) beizutreten.

eMush ist ein Open-Source-Projekt, du kannst unsere Fortschritte auch auf GitLab verfolgen.
Insbesondere den [Fortschritt des **Spores for All!**-Updates](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Vielen Dank für deine Aufmerksamkeit, und bis bald an Bord der Daedalus!

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

_Radioknistern_

Hallo zusammen, hier ist Ben, der euch die neuesten Ereignisse aus der Wüste berichtet.

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Es tut mir leid, _verschlüsselt_ dass ihr möglicherweise Probleme mit dem
Empfang habt. _verschlüsselt_ Schwarm Raben sitzen auf unserer Antenne
_verschlüsselt_. Es scheint, als hätten sie sich vermehrt... Ich habe Connor
losgeschickt, um sie zu vertreiben, aber er ist noch nicht zurück.
_verschlüsselt_ vielleicht den Praktikanten schicken, um ihn zu suchen.

Gute Nachrichten für diejenigen unter euch, die diese neumodischen tragbaren
Empfangsgeräte verwenden. Wir haben zahlreiche Verbesserungen vorgenommen, um
_verschlüsselt_ angenehmer und einfacher zu nutzen.

Zu guter Letzt können wir dank des neuen Statisto-Tron 40K, das unsere
Buddler mitgebracht haben, live übertragen, wie viele Hörer wir haben. Keine
Sorge, _verschlüsselt_ keine Datenschutzbedenken, wir sammeln definitiv keine
privaten Informationen über euch! Bis zum nächsten Mal.... und wir sind weg.
_Stille_ _verschlüsselt_ alles an Werbetreibende **verkaufen** _verschlüsselt_
endlich aus diesem Dreckloch entkommen _verschlüsselt_ **WIR SENDEN NOCH?!**
_piep_ _verschlüsselt_

# Abschließende Worte

Du kannst deine [Nachrichten für Dezember](https://gitlab.com/eternaltwin/etwin/-/issues/56) senden.

Dieser Artikel wurde bearbeitet von: Brainbox, Demurgos, Evian, Fer, Patate404 und Schroedinger.
