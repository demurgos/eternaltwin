# 2022-11: This Month In Eternaltwin #14

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

[The Eternaltwin application](https://eternaltwin.org/docs/desktop) lets you
easily play to Motion Twin's Flash games. The update 0.6.4 was released to
fix access to Minitroopers.

# eMush

Welcome to this section dedicated to [eMush](https://emush.eternaltwin.org/),
the project to save Mush.

## What's new?

Last time we described the three main tasks to release the eMush version 0.5.0
as an open alpha, the **Spores for All!** update:

- 🗃️ spaceship archival, to keep track of your achievements
- 🇬🇧 English translations
- 💾 database migrations, to deploy new version without resetting the existing
  spaceships

We're advancing slowly but surely.

eMush now has a simple archival system to record part of the data about your
spaceship once a game completes.

Furthermore, you can now play in English!

![eMush](./emush.png)

## Coming next

The **Spores for All!** update will keep us busy for a few months. We have to
rework our database structure to store all the data we need to provide some
epic leaderboards 👀

The database migrations to avoid wiping your progress are also an important
point. You should be able to flex on others 'till the end of time 💪

Once these problems are solved, we'll release the open alpha.

## Great! When?

We'll keep updating your through TMIET, but the best way to follow progress is
to join [the Eternaltwin discord](https://discord.gg/SMJavjs3gk).

eMush is an open source project, you can also follow our progress on GitLab.
In particular, [progress on the **Spores for All!** update](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Thank you for your attention, see you later aboard the Daedalus! ❤️

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Hello everyone, this is Ben, bringing you the latest happenings from all over the desert.

![MyHordes - Wireless technology ad](./myhordes.png)

I'm sorry for _scrambled_ reception issues you may have. _scrambled_ swarm of
crows sitting around on the antenna _scrambled_. Seems they have grown in
numbers... I've sent Connor to clear them out, but he hasn't returned yet.
_scrambled_ maybe sent the Intern to look for him.

Good news for those of you using these newfangled portable receiving units.
We've made numerous improvements to _scrambled_ more enjoyable and easier to use.

Last but not least, thanks to the new Statisto-Tron 40K our scavengers brought
in, we can broadcast to you live information about how many listeners we have.
Do not worry, _scrambled_ no privacy concerns, we definitely do not collect
private information from you! Until next time.... and we're **off**. _silence_
_scrambled_ sell everything to advertisers _scrambled_ finally escape from this
dump _scrambled_ **IT'S STILL ON?** _beep_ _scrambled_

# Closing words

You can send [your messages for December](https://gitlab.com/eternaltwin/etwin/-/issues/56).

This article was edited by: Brainbox, Demurgos, Evian, Fer, Patate404, and Schroedinger.
