# 2023-02 : Ce mois-ci sur Eternaltwin n°17

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux et communautés de joueurs de Motion Twin.

# Eternaltwin

Eternaltwin a besoin de développeurs ! Les différents jeux avancent à leur rythme mais le site principal servant à tout unifier a du mal à avancer. Si vous êtes intéressez, contacter sur Discord Patate404#8620, Demurgos#8218 ou Biosha#2584.

# eMush

Bienvenue à tous dans cette section de consacrés à
[🍄 eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

![eMush - Spores for All](./emush.fr.png)

Avec sa **Spores for All Update!**, eMush est entré en **alpha ouverte** ! 🎉
👉 [🍄 eMush](https://emush.eternaltwin.org/) 👈

Les principales nouveautés de la "Spores for All Update!" sont :

🇬🇧 **Le support de l'anglais** : NERON peut désormais vous insulter dans la langue de Shaskespeare ! 🧐

🗃 **Les pages de classement et de fin de partie** ! Vous pouvez maintenant exposer au monde entier comment vous êtes mort (pathétiquement) en essayant de sauver la race humaine. 🏅

💾 **Le panneau d'administration** et les migrations de bases de données : nous pouvons désormais ajouter des fonctionnalités beaucoup plus facilement, et sans supprimer vos exploits ! 😭 🎉

Il s'agit d'une étape importante pour eMush. Nous sommes très heureux du chemin parcouru et nous espérons que cela nous permettra de vous proposer des changements plus fréquents à l'avenir.
Gardez à l'esprit qu'il s'agit toujours d'une alpha incomplète, et que vos Daedalus ne sont pas à l'abri de trous noirs qui apparaissent sans crier gare entre deux lignes de code...
Ceci dit : amusez-vous bien et à bientôt pour les recherches et les projets !

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

La Saison 15 de MyHordes vient d'être déployée !

Comme convenu, cette saison est seulement une saison de transition, qui contiendra plein de choses identiques aux données gameplay d'Hordes, partagées par Skool.

Mais pas seulement ! Nous avons également apporté diverses modifications et améliorations.

Je vous invite à vous connecter sur le jeu afin de découvrir la liste de changements complète.

**Et n'oubliez pas, la mort n'est pas grave dans MyHordes.**

# Eternal DinoRPG

Bonjour tout le monde et merci de votre attention pour cette partie dédiée à [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

Salutations Maîtres Dinoz !
Depuis la version 0.5.0, une partie de l'équipe a changé sa priorité pour s'occuper de l'import des comptes. Tout comme vous, nous ne voudrions pas voir tout vos beaux dinozs disparaître ! Nous espérons pouvoir partager et publier de bonnes avancées dans ce domaine pour le prochain mois.

L'autre partie de l'équipe a pu avancé sur le développement du jeu en lui même et vient de publier la version 0.5.1:

- Les monstres ont été équilibrés et devrait être un peu moins dur à battre.
- Papy Joe a regroupé ses notes et a bientôt d'autres missions à vous proposer pour aider les habitants de Dinoland
- Les compétences affectant la vie et les éléments d'un dinoz sont désormais prise en compte

On me signale à l'oreille en toute dernière minute que l'équipe recherche un nouveau nom pour le jeu. Ne vous retenez pas !

À bientôt. L'équipe EternalDino.

# DinoCard

DinoCard est de retour ! Voici un petit aperçus.
![Dinocard - Sneak peak](./dinocard.png)

Nous avons commencer à ramener le jeu à la vie.

Le site n'est pas encore disponible mais nous avons besoin de vous pour [choisir le nom de la nouvelle version](https://forms.gle/eZrtjfrcfUVgfAxu9).

Plus d'informations à la gazette de Mars 😄

# Closing words

Vous pouvez envoyer [vos messages pour l'édition de Mars](https://gitlab.com/eternaltwin/etwin/-/issues/59).

Cet article a été édité par : Biosha, BrainBox, Demurgos, Dylan57, Evian, Fer, Jahaa, Patate404, Schroedinger et Zenoo.

