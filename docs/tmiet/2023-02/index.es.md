# 2023-02: Este Mes En Eternaltwin #17

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar
los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

¡Eternaltwin necesita desarrolladores! Todos los juegos progresan a su propio ritmo, pero el sitio web principal utilizado para unificar todo está teniendo tiempos difíciles. Si estás interesado, contacta en Discord a Patate404#8620, Demurgos#8218 o Biosha#2584.

# eMush

Bienvenidos a esta sección dedicada a [🍄 eMush](https://emush.eternaltwin.org/),
el proyecto dedicado a salvar Mush.

![eMush - Spores for All](./emush.png)

Con su **Spores for All Update!**, ¡eMush ha entrado a **la alfa abierta**! 🎉
👉 [🍄 eMush](https://emush.eternaltwin.org/) 👈


Las principales características nuevas de "Spores for All Update!" son:

🇬🇧 **Soporte de lenguaje inglés**: NERON ahora puede insultarte en el lenguaje de Shaskespeare! 🧐

🗃 **Páginas de clasificación y endgame**! Ahora puedes decirle al mundo cómo moriste (patéticamente) tratando de salvar a la raza humana. 🏅

💾 **Panel de administración** y migraciones de base de datos: ahora podemos agregar nuevas características mucho más fácilmente, ¡y sin remover sus exploits! 😭 🎉

Este es un hito mayor para eMush. Estamos muy felices con cuánto hemos avanzado, y esperamos que esto nos permitirá brindarles cambios más frecuentes en el futuro.
Tengan en cuenta que esto es todavía una alfa incompleta, y que vuestro Daedalus no es inmune a los Agujeros Negros que aparecen sin aviso alguno entre dos líneas de código...
Habiendo aclarado eso: ¡diviértanse, y espero verlos pronto para investigaciones y proyectos!

# MyHordes

Juega [MyHordes](https://myhordes.eu/).  _ruido de radio_ _interferencia_

Hola a todos, espero que todo esté bien para ustedes en esta temporada dura y con mucho viento _interferencia_

![MyHordes - Wireless technology ad](./myhordes.png)

¡La Temporada 15 de MyHordes ha sido lanzada!

Como habíamos acordado, esta temporada sólo será de transición, que contendrá muchas cuestiones idénticas a la información de jugabilidad de Hordes, compartida por Skool.

¡Pero no sólo eso! También hemos hecho varios cambios y mejoras.
Los invito a conectarse al juego para descubrir la lista completa de cambios.

**Y no lo olvides, la muerte no es nada en MyHordes.**

# Eternal DinoRPG

Bienvenidos a esta sección dedicada a [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

¡Saludos, Maestros Dinoz!
Después de la versión 0.5.0, parte del equipo cambió su foco hacia la importación de cuentas originales de DinoRPG. ¡Tampoco queremos que ningún dino se desvanezca! El equipo espera poder compartir buen progreso sobre esto el próximo mes.

La otra parte del equipo trabajó en contenido relacionado al juego y publicó la 0.5.1:

- monstruos más balanceados, que serán apenas menos agresivos a sus dinoz
- El Viejo Joe reunió los rumores de Dinoland y puede decirte cómo ayudar
- Finalmente, las habilidades de los dinoz que afectan a la vida máxima y elementos deberían tenerse en cuenta.

Además, alguien me está diciendo al último minuto en mi oído que el juego está buscando un nuevo nombre. ¡Dejen correr su imaginación!
¡Saludos! El equipo EternalDino.

# DinoCard

¡DinoCard ha vuelto! Aquí tienen un breve adelanto.
![Dinocard - Sneak peak](./dinocard.png)

¡Empezamos a trabajar para traer el juego de vuelta a la vida!

El sitio web todavía no está disponible, pero te necesitamos para [elegir el nombre de la futura versión del Juego](https://forms.gle/eZrtjfrcfUVgfAxu9).

Más noticias en marzo 😄

# Palabras finales

Pueden enviarnos [sus mensajes de marzo](https://gitlab.com/eternaltwin/etwin/-/issues/59).

Este artículo ha sido editado por: Biosha, BrainBox, Demurgos, Dylan57, Evian, Fer, Jahaa, Patate404, Schroedinger et Zenoo.
