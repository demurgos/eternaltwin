<?php declare(strict_types=1);

namespace Eternaltwin\Exe;

use Eternaltwin\PathTools;
use Linfo\Linfo;
use Demurgos\Process\Process;

final class Install {
  public static function install() {
    $packageRoot = PathTools\System::join(__DIR__, "../..");

    fwrite(STDERR, "Installing Eternaltwin executable in package: " . $packageRoot . PHP_EOL);
    // Executable path, relative to the package root
    $relExe = self::download($packageRoot);
    if ($relExe == null) {
      fwrite(STDERR, "Precompiled download is not available" . PHP_EOL);
      $relExe = Install::build($packageRoot, "cargo");
    }
    if ($relExe == null) {
      fwrite(STDERR, "Failed to install eternaltwin executable" . PHP_EOL);
      die(1);
    }

    fwrite(STDERR, "Installed eternaltwin executable inside package at: " . $relExe . PHP_EOL);

    $script = self::getScript($relExe);
    $scriptPath = PathTools\System::join($packageRoot, "src/Exe.php");

    $scriptResult = file_put_contents($scriptPath, $script,);
    if ($scriptResult === false) {
      fwrite(STDERR, "Failed to write PHP script: " . $scriptPath . PHP_EOL);
      die(0);
    }
    fwrite(STDERR, "Generated export script" . $scriptPath . PHP_EOL);
    die(0);
  }

  /**
   * Download the precompiled binary if available.
   *
   * Throws if an unexpected error occurs.
   *
   * @return ?string Path if the download succeed, `null` if no download was available
   */
  private static function download(string $packageRoot): ?string {
    $package = self::precompiledPackage();
    if ($package == null) {
      return null;
    }
    $version = Version::VERSION;
    $base = "https://gitlab.com/api/v4/projects/eternaltwin%2Feternaltwin/packages/generic/" . urlencode($package) . "/" . urlencode($version) . "/";
    $metaUrl = $base . "meta.json";
    $metaRaw = file_get_contents($metaUrl);
    if ($metaRaw === false) {
      fwrite(STDERR, "failed to read precompiled meta: " . $metaUrl . PHP_EOL);
      return null;
    }
    $meta = json_decode($metaRaw, true, 512, JSON_THROW_ON_ERROR);
    $exeOut = PathTools\System::join($packageRoot, $meta["executable"]); // TODO: Only use the basename
    if (!file_exists($exeOut)) {
      $exeUrl = $base . $meta["executable"];
      $exe = file_get_contents($exeUrl);
      if ($exe === false) {
        fwrite(STDERR, "failed to read precompiled exe: " . $exeUrl . PHP_EOL);
        return null;
      }
      if (file_put_contents($exeOut, $exe) === false) {
        fwrite(STDERR, "failed to write precompiled precompiled exe: " . $exeOut . PHP_EOL);
        die(1);
      }
    }
    if (chmod($exeOut, 0o555) === false) {
      fwrite(STDERR, "failed to set executable permissions: " . $exeOut . PHP_EOL);
      die(1);
    }
    return PathTools\System::relative($packageRoot, $exeOut);
  }

  /**
   * Get the name of the precompiled package for the current system.
   *
   * Throws if an unexpected error occurs.
   *
   * @return ?string Package name, `null` if no package is available
   */
  private static function precompiledPackage(): ?string {
    switch (self::detectPlatform()) {
      case "Darwin.x86_64":
        return "eternaltwin-x86_64-apple-darwin";
      case "Windows.x86_64":
        return "eternaltwin-x86_64-pc-windows-gnu";
      case "Linux.x86_64":
        return "eternaltwin-x86_64-unknown-linux-gnu";
      default:
        return null;
    }
  }

  private static function detectPlatform(): string {
    $os = PHP_OS_FAMILY;
    $supported = ["Darwin", "Linux", "Windows"];
    if (!in_array($os, $supported, true)) {
      fwrite(STDERR, "expected `PHP_OS_FAMILY` to be one of Darwin, Linux, or Windows; got: " . $os . PHP_EOL);
      die(1);
    }
    if ($os === "Windows") {
      fwrite(STDERR, "CPU architecture detection is not available on Windows; assuming `x64_64`" . PHP_EOL);
      return "Windows.x86_64";
    }

    $linfo = new Linfo();
    $parser = $linfo->getParser();
    if ($parser === null) {
      fwrite(STDERR, "failed to load system information; assuming `x64_64` CPU architecture" . PHP_EOL);
      return $os . ".x86_64";
    }
    $arch = $parser->getCPUArchitecture();
    return $os . "." . $arch;
  }

  /**
   * Build the executable locally.
   */
  private static function build(string $packageRoot, string $cargoBin): string {
    fwrite(STDERR, "Starting local build" . PHP_EOL);

    // TODO: Consider using [proc_open](https://www.php.net/manual/en/function.proc-open.php) directly to avoid a dependency on symfony?
    $process = new Process(
      [$cargoBin, "build", "--manifest-path", "./Cargo.toml", "--bin", "eternaltwin", "--release", "--message-format=json"],
      $packageRoot,
      null,
      null,
      null,
    );

    $outputReader = new CargoBuildOutputReader();

    $output = $process->run(function ($type, $buffer) use ($outputReader) {
      if (Process::OUT === $type) {
        $outputReader->push($buffer);
      } else {
        fwrite(STDERR, $buffer);
      }
    });
    if ($output !== 0) {
      fwrite(STDERR, "non-success `cargo build` exit code" . PHP_EOL);
      die(1);
    }

    $executable = $outputReader->end();
    if ($executable === null) {
      throw new \Error("failed to read executable location");
    }
    $exeName = basename($executable);
    $exeOut = PathTools\System::join($packageRoot, $exeName); // TODO: Only use the basename
    if (copy($executable, $exeOut) === false) {
      fwrite(STDERR, "failed to set executable permissions: " . $exeOut . PHP_EOL);
      die(1);
    }
    // TODO: Clean `target`
    $process = new Process(
      [$cargoBin, "clean"],
      $packageRoot,
      null,
      null,
      null,
    );
    $output = $process->run();
    if ($output !== 0) {
      fwrite(STDERR, "non-success `cargo clean` exit code" . PHP_EOL);
      die(1);
    }

    return PathTools\System::relative($packageRoot, $exeOut);
  }

  private static function getScript(string $exePath): string {
    return "<?php declare(strict_types=1);

namespace Eternaltwin;

final class Exe {
  public static function path(): string {
    \$packageRoot = \\Eternaltwin\\PathTools\\System::join(__DIR__, \"..\");
    return \\Eternaltwin\\PathTools\\System::join(\$packageRoot, " . json_encode($exePath, JSON_UNESCAPED_SLASHES) . ");
  }
}
";
  }
}

final class CargoBuildOutputReader {
  private string $buffer;
  private ?string $executable;

  final function __construct() {
    $this->buffer = "";
    $this->executable = null;
  }

  final function push(string $chunk) {
    $this->buffer .= $chunk;
    $this->consumeLines(false);
  }

  final function end(): ?string {
    $this->consumeLines(true);
    return $this->executable;
  }

  private function consumeLines(bool $flush) {
    $encoding = "UTF-8";
    $newlineLen = mb_strlen("\n", $encoding);
    for (; ;) {
      $index = mb_strpos($this->buffer, "\n", 0, $encoding);
      $enfOfLine = 0;
      if ($index === false) {
        if ($flush) {
          $enfOfLine = mb_strlen($this->buffer, $encoding);
        }
      } else {
        $enfOfLine = $index + $newlineLen;
      }
      if ($enfOfLine === 0) {
        return;
      }
      $line = mb_strcut($this->buffer, 0, $enfOfLine, $encoding);
      $this->buffer = mb_strcut($this->buffer, $enfOfLine, null, $encoding);
      $executable = self::getExecutablePath($line);
      if ($this->executable === null) {
        $this->executable = $executable;
      }
    }
  }

  /**
   * Retrieve the executable path from a cargo JSON line diagnostic
   *
   * It searches for a row such as:
   *
   * ```json
   * {"reason":"compiler-artifact","package_id":"etwin_native 0.11.0 (path+file:///data/projects/eternaltwin/etwin/packages/native/native)","manifest_path":"/data/projects/eternaltwin/etwin/packages/native/native/Cargo.toml","target":{"kind":["bin"],"crate_types":["bin"],"name":"etwin_native","src_path":"/data/projects/eternaltwin/etwin/packages/native/native/src/main.rs","edition":"2021","doc":true,"doctest":false,"test":true},"profile":{"opt_level":"3","debuginfo":null,"debug_assertions":false,"overflow_checks":false,"test":false},"features":[],"filenames":["/data/projects/eternaltwin/etwin/target/release/etwin_native"],"executable":"/data/projects/eternaltwin/etwin/target/release/etwin_native","fresh":true}
   * ```
   */
  private static function getExecutablePath(string $line): ?string {
    $diagnostic = json_decode($line, true, 512, JSON_THROW_ON_ERROR);
    if (gettype($diagnostic) !== "array") {
      throw new \Error("expected cargo diagnostic line to be a JSON object: " . $line);
    }
    $reason = $diagnostic["reason"];
    if ($reason !== "compiler-artifact") {
      return null;
    }
    $target = $diagnostic["target"];
    if (gettype($target) !== "array") {
      return null;
    }
    $targetKind = $target["kind"];
    $targetName = $target["name"];
    if (!(gettype($targetKind) == "array" && in_array("bin", $targetKind, true) && $targetName === "eternaltwin")) {
      return null;
    }
    $executable = $diagnostic["executable"];
    if (gettype($executable) !== "string") {
      return null;
    }
    return $executable;
  }
}
