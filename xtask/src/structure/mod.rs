use crate::{Target, REPO_DIR};
use cargo_metadata::{DependencyKind, MetadataCommand};
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, BTreeSet, HashSet};
use std::fs;
use std::path::PathBuf;
use std::process::Command;

/// A component from the Eternaltwin repository
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Component {
  /// Unique component id
  pub id: ComponentId,
  pub kind: Kind,
  /// Local name of the component
  /// For rust libs: crate name
  /// For node packages: package name
  pub name: String,
  /// Private: do not publish
  pub private: bool,
  pub local_version: String,
  pub source: String,
  pub deps: Vec<Dependency>,
  pub target: Option<Target>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Dependency {
  pub id: ComponentId,
  pub kind: DepKind,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct ComponentId(String);

impl ComponentId {
  pub fn new<S: AsRef<str>>(s: S) -> Self {
    Self(s.as_ref().to_string())
  }

  pub fn as_str(&self) -> &str {
    self.0.as_str()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Kind {
  /// Rust binary
  RustBin,
  /// Rust library
  RustLib,
  /// TypeScript library
  TsLib,
  /// PHP library
  PhpLib,
  /// Native executable,
  Executable,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum DepKind {
  Regular,
  Dev,
}

#[derive(Debug)]
pub struct Structure {
  pub repo_root: PathBuf,
  pub components: BTreeMap<ComponentId, Component>,
  #[allow(unused)]
  pub reverse_build_dependencies: BTreeMap<ComponentId, BTreeSet<ComponentId>>,
  pub topo: Vec<ComponentId>,
}

impl Structure {
  pub fn load() -> Self {
    let cargo = cargo_components();
    let yarn = yarn_components();
    let composer = composer_components();
    let eternaltwin_bin_crate = cargo
      .iter()
      .find(|c| c.id.as_str() == "rs_bin_eternaltwin")
      .expect("`rs_bin_eternaltwin` component exists");
    let builtin = builtin_components(eternaltwin_bin_crate);

    let mut all = Vec::from(builtin);
    all.extend(cargo);
    all.extend(yarn);
    all.extend(composer);

    let mut components = BTreeMap::new();
    for c in all {
      components.insert(c.id.clone(), c);
    }
    Self::from_map(components)
  }

  fn from_map(components: BTreeMap<ComponentId, Component>) -> Self {
    let mut reverse_build_dependencies: BTreeMap<ComponentId, BTreeSet<ComponentId>> =
      components.keys().map(|id| (id.clone(), BTreeSet::new())).collect();
    for c in components.values() {
      for d in c.deps.iter() {
        match reverse_build_dependencies.get_mut(&d.id) {
          Some(rdeps) => {
            rdeps.insert(c.id.clone());
          }
          None => panic!("component exists: {:?}", d.id.as_str()),
        }
      }
    }

    let mut visited: HashSet<ComponentId> = HashSet::new();
    let mut stack: Vec<(bool, ComponentId)> = reverse_build_dependencies
      .iter()
      .filter(|(_id, rdeps)| rdeps.is_empty())
      .map(|(id, _rdeps)| (false, id.clone()))
      .collect();
    let mut is_open: HashSet<ComponentId> = HashSet::new();
    let mut topo = Vec::new();
    while let Some((is_close, id)) = stack.pop() {
      if is_close {
        let was_removed = is_open.remove(&id);
        debug_assert!(was_removed);
        topo.push(id);
        continue;
      }
      let c = components.get(&id).expect("component exists");
      visited.insert(id.clone());
      if is_open.contains(&id) {
        panic!("cycle detected");
      }
      stack.push((true, id.clone()));
      is_open.insert(id);
      for dep in &c.deps {
        if !visited.contains(&dep.id) {
          stack.push((false, dep.id.clone()))
        }
      }
    }

    Self {
      repo_root: REPO_DIR.as_path().to_path_buf(),
      components,
      reverse_build_dependencies,
      topo: topo.into_iter().collect(),
    }
  }
}

fn builtin_components(eternaltwin_bin_crate: &Component) -> [Component; 4] {
  Target::ALL.map(|t| Component {
    id: ComponentId(format!("exe_eternaltwin_{}", t.name.replace('-', "_"))),
    kind: Kind::Executable,
    name: format!("eternaltwin-{}", t.name),
    source: String::from("xtask/src/precompile.rs"),
    private: false,
    deps: vec![Dependency {
      id: eternaltwin_bin_crate.id.clone(),
      kind: DepKind::Regular,
    }],
    target: Some(t),
    local_version: eternaltwin_bin_crate.local_version.clone(),
  })
}

fn cargo_components() -> Vec<Component> {
  let repo_dir = REPO_DIR.as_path();
  let cargo_meta = MetadataCommand::new()
    .no_deps()
    .current_dir(repo_dir)
    .exec()
    .expect("cargo metadata failed");

  let workspace_ids = BTreeSet::from_iter(cargo_meta.workspace_members.iter().cloned());
  let mut workspace_name_to_id = BTreeMap::new();
  for pkg in &cargo_meta.packages {
    if workspace_ids.contains(&pkg.id) {
      workspace_name_to_id.insert(pkg.name.clone(), pkg.id.clone());
    }
  }

  let mut components = Vec::new();

  for pkg in cargo_meta.packages {
    if !workspace_ids.contains(&pkg.id) {
      continue;
    }
    for t in pkg.targets {
      for k in t.kind {
        let (kind, pfx) = match k.as_str() {
          "bin" => (Kind::RustBin, "rs_bin_"),
          "lib" => (Kind::RustLib, "rs_lib_"),
          "custom-build" | "test" => continue,
          k => panic!("unexpected rust target kind {k:?}"),
        };
        let mut c = Component {
          id: ComponentId(format!("{pfx}{}", t.name)),
          kind,
          name: t.name.clone(),
          // With `cargo_metadata`, empty registry list means private
          private: pkg
            .publish
            .as_ref()
            .map(|registries| registries.is_empty())
            .unwrap_or(false),
          local_version: pkg.version.to_string(),
          source: pkg
            .manifest_path
            .clone()
            .into_std_path_buf()
            .parent()
            .expect("parent dir exists")
            .strip_prefix(REPO_DIR.as_path())
            .unwrap()
            .to_string_lossy()
            .to_string(),
          deps: vec![],
          target: None,
        };
        match c.id.as_str() {
          "rs_lib_eternaltwin_app" => {
            c.private = false;
            c.deps.push(Dependency {
              id: ComponentId(String::from("ts_lib_eternaltwin_website")),
              kind: DepKind::Regular,
            });
          }
          "rs_bin_xtask" => {
            c.deps.push(Dependency {
              id: ComponentId(String::from("rs_lib_xtask")),
              kind: DepKind::Regular,
            });
          }
          _ => {
            // no custom dependencies
          }
        }
        for d in &pkg.dependencies {
          match d.kind {
            DependencyKind::Normal | DependencyKind::Build | DependencyKind::Development => {
              if workspace_name_to_id.contains_key(&d.name) {
                c.deps.push(Dependency {
                  id: ComponentId(format!("rs_lib_{}", d.name)),
                  kind: if matches!(d.kind, DependencyKind::Normal | DependencyKind::Build) {
                    DepKind::Regular
                  } else {
                    debug_assert_eq!(d.kind, DependencyKind::Development);
                    DepKind::Dev
                  },
                });
              }
            }
            d => panic!("unknown dependency kind {d:?}"),
          }
        }
        components.push(c);
      }
    }
  }

  components
}

fn yarn_components() -> Vec<Component> {
  let repo_dir = REPO_DIR.as_path();
  let yarn_out = Command::new("yarn")
    .args(["workspaces", "list", "--json"])
    .current_dir(repo_dir)
    .output()
    .expect("failed to read yarn workspaces");
  if !yarn_out.status.success() {
    panic!("non-success exit status for yarn");
  }
  let packages = String::from_utf8(yarn_out.stdout).expect("non-utf8 yarn output");
  let packages = packages.lines();
  let mut package_locations = BTreeMap::new();
  for p in packages {
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
    struct PackageRef {
      location: String,
      name: String,
    }
    let p: PackageRef = serde_json::from_str(p).expect("invalid package ref");
    package_locations.insert(p.name, p.location);
  }

  let mut components = Vec::new();

  for (pname, ploc) in &package_locations {
    let pkg = REPO_DIR.join(ploc).join("package.json");
    let pkg = fs::read_to_string(pkg).expect("failed to read package manifest");
    let pkg: PackageJson = match serde_json::from_str(pkg.as_str()) {
      Ok(pkg) => pkg,
      Err(e) => panic!("invalid package.json: {pname}: {e:?}"),
    };
    let short_name = node_package_name_to_identifer(&pkg.name);
    let mut c = Component {
      id: ComponentId(format!("ts_lib_{}", short_name)),
      kind: Kind::TsLib,
      name: pkg.name.clone(),
      private: pkg.private,
      local_version: pkg.version,
      source: ploc.clone(),
      deps: vec![],
      target: None,
    };
    for d in pkg.dependencies.keys() {
      if !package_locations.contains_key(d) {
        continue;
      }
      let short_name = node_package_name_to_identifer(d);
      c.deps.push(Dependency {
        id: ComponentId(format!("ts_lib_{short_name}")),
        kind: DepKind::Regular,
      });
    }
    for d in pkg.optional_dependencies.keys() {
      if !package_locations.contains_key(d) {
        continue;
      }
      let short_name = node_package_name_to_identifer(d);
      c.deps.push(Dependency {
        id: ComponentId(format!("ts_lib_{short_name}")),
        kind: DepKind::Dev,
      });
    }
    match pkg.name.as_str() {
      "@eternaltwin/exe-x86_64-apple-darwin" => c.deps.push(Dependency {
        id: ComponentId::new("exe_eternaltwin_x86_64_apple_darwin"),
        kind: DepKind::Dev,
      }),
      "@eternaltwin/exe-x86_64-unknown-linux-gnu" => c.deps.push(Dependency {
        id: ComponentId::new("exe_eternaltwin_x86_64_unknown_linux_gnu"),
        kind: DepKind::Dev,
      }),
      "@eternaltwin/exe-x86_64-pc-windows-gnu" => c.deps.push(Dependency {
        id: ComponentId::new("exe_eternaltwin_x86_64_pc_windows_gnu"),
        kind: DepKind::Dev,
      }),
      "@eternaltwin/exe-x86_64-unknown-linux-musl" => c.deps.push(Dependency {
        id: ComponentId::new("exe_eternaltwin_x86_64_unknown_linux_musl"),
        kind: DepKind::Dev,
      }),
      _ => {
        // no custom dependencies
      }
    }
    components.push(c);
  }
  components
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct PackageJson {
  name: String,
  #[serde(default = "default_version")]
  version: String,
  #[serde(default)]
  private: bool,
  #[serde(default)]
  dependencies: BTreeMap<String, String>,
  #[serde(default, rename = "optionalDependencies")]
  optional_dependencies: BTreeMap<String, String>,
}

fn default_version() -> String {
  String::from("0.0.0")
}

fn node_package_name_to_identifer(package_name: &str) -> String {
  package_name.replace(['/', '-'], "_").replace('@', "")
}

fn composer_components() -> Vec<Component> {
  let repo_dir = REPO_DIR.as_path();
  let php_dir = repo_dir.join("./sdk/php");
  let package_dirs = fs::read_dir(php_dir).expect("php sdk dir exists").filter_map(|res| {
    let entry = res.expect("reading php sdk dir content succeeds");
    let meta = entry.metadata().expect("php sdk entry metadata read succeeds");
    if !meta.is_dir() {
      return None;
    };
    let dir_name = entry.file_name();
    let dir_name = dir_name.to_str().expect("php sdk entry is valid UTF-8");
    Some(format!("./sdk/php/{dir_name}"))
  });

  let mut package_locations: BTreeMap<String, (String, ComposerJson)> = BTreeMap::new();
  for pdir in package_dirs {
    let pkg = REPO_DIR.join(&pdir).join("composer.json");
    let pkg = fs::read_to_string(pkg).expect("failed to read package manifest");
    let pkg: ComposerJson = match serde_json::from_str(pkg.as_str()) {
      Ok(pkg) => pkg,
      Err(e) => panic!("invalid composer.json: {pdir}: {e:?}"),
    };
    package_locations.insert(pkg.name.clone(), (pdir, pkg));
  }

  let mut components: Vec<Component> = Vec::new();
  for (pname, (ploc, pkg)) in &package_locations {
    let short_name = composer_package_name_to_identifer(pname);
    let mut c = Component {
      id: ComponentId(format!("php_lib_{}", short_name)),
      kind: Kind::PhpLib,
      name: pkg.name.clone(),
      private: false,
      local_version: pkg.version.clone(),
      source: ploc.clone(),
      deps: vec![],
      target: None,
    };
    for d in pkg.require.keys() {
      if !package_locations.contains_key(d) {
        continue;
      }
      let short_name = composer_package_name_to_identifer(d);
      c.deps.push(Dependency {
        id: ComponentId(format!("php_lib_{short_name}")),
        kind: DepKind::Regular,
      });
    }
    for d in pkg.require_dev.keys() {
      if !package_locations.contains_key(d) {
        continue;
      }
      let short_name = composer_package_name_to_identifer(d);
      c.deps.push(Dependency {
        id: ComponentId(format!("php_lib_{short_name}")),
        kind: DepKind::Dev,
      });
    }
    match pkg.name.as_str() {
      "eternaltwin/exe" => {
        c.deps.push(Dependency {
          id: ComponentId::new("exe_eternaltwin_x86_64_apple_darwin"),
          kind: DepKind::Dev,
        });
        c.deps.push(Dependency {
          id: ComponentId::new("exe_eternaltwin_x86_64_unknown_linux_gnu"),
          kind: DepKind::Dev,
        });
        c.deps.push(Dependency {
          id: ComponentId::new("exe_eternaltwin_x86_64_pc_windows_gnu"),
          kind: DepKind::Dev,
        });
        c.deps.push(Dependency {
          id: ComponentId::new("exe_eternaltwin_x86_64_unknown_linux_musl"),
          kind: DepKind::Dev,
        });
      }
      _ => {
        // no custom dependencies
      }
    }
    components.push(c);
  }
  components
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct ComposerJson {
  name: String,
  version: String,
  #[serde(default)]
  require: BTreeMap<String, serde_json::Value>,
  #[serde(default, rename = "require-dev")]
  require_dev: BTreeMap<String, serde_json::Value>,
}

fn composer_package_name_to_identifer(package_name: &str) -> String {
  package_name.replace(['/', '-'], "_").replace('@', "")
}
