use crate::structure::{Component, Kind, Structure};
use crate::{InvalidTargetName, Target, REPO_DIR};
use cargo_metadata::{Message, MetadataCommand};
use clap::Parser;
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, BTreeSet};
use std::fs;
use std::io::ErrorKind;
use std::path::{Path, PathBuf, StripPrefixError};
use std::process::{Command, Stdio};

/// Arguments to the `precompile` task.
#[derive(Debug, Parser)]
pub struct Args {
  /// Targets to compile (all if none is provided)
  targets: Vec<String>,
  /// Clean after build
  #[arg(long)]
  clean: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("unexpected precompilation failure")]
  PrecompileAll(#[from] PrecompileAllError),
}

/// Generated precompiled packages
pub fn run(args: &Args) -> Result<(), Error> {
  let mut wanted_targets = BTreeSet::new();
  for t in &args.targets {
    wanted_targets.insert(Target::from_name(t.as_str()).map_err(PrecompileAllError::TargetName)?);
  }
  if wanted_targets.is_empty() {
    wanted_targets.extend(Target::ALL)
  }
  let cmd = Precompile {
    targets: wanted_targets,
    remote: true,
    clean_after: true,
  };

  precompile_all(&cmd)?;
  Ok(())
}

pub struct Precompile {
  pub targets: BTreeSet<Target>,
  pub remote: bool,
  pub clean_after: bool,
}

impl Precompile {
  pub fn one(target: Target) -> Self {
    Self {
      targets: {
        let mut targets = BTreeSet::new();
        targets.insert(target);
        targets
      },
      remote: true,
      clean_after: true,
    }
  }

  pub fn all() -> Self {
    Self {
      targets: BTreeSet::from_iter(Target::ALL),
      remote: true,
      clean_after: true,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum PrecompileAllError {
  #[error("invalid target name")]
  TargetName(#[source] InvalidTargetName),
  #[error("request precompilation target {0:?} is invalid")]
  InvalidTarget(String),
  #[error("failed to create dist dir at {1:?}")]
  CreateDistDir(#[source] WeakError, PathBuf),
  #[error("failed to retrieve cargo workspace metadata")]
  CargoMetadata(#[source] WeakError),
  #[error("failed to read version of the `eternaltwin` executable crate")]
  VersionNotFound,
  #[error("failed to create target dir at {1:?}")]
  CreateTargetDir(#[source] WeakError, PathBuf),
  #[error("failed to read old build metadata")]
  GetOldMeta(#[source] GetMetaError),
  #[error("failed to retrieve cross workspace metadata (make sure cargo-cross is installed and working)")]
  CrossMetadata(#[source] WeakError),
  #[error("failed to precompile package {1:?}")]
  Precompile(#[source] PrecompileError, String),
  #[error("failed to write meta at {1:?}")]
  WriteMeta(#[source] WeakError, PathBuf),
}

pub fn precompile_all(cmd: &Precompile) -> Result<BTreeMap<Component, (PathBuf, DistMeta)>, PrecompileAllError> {
  let s = Structure::load();

  let repo_dir: PathBuf = REPO_DIR.clone();
  let repo_dir = repo_dir.as_path();
  let dist_dir = REPO_DIR.join("dist");
  fs::create_dir_all(&dist_dir).map_err(|e| PrecompileAllError::CreateDistDir(WeakError::wrap(e), dist_dir.clone()))?;
  let cargo_meta = MetadataCommand::new()
    .no_deps()
    .current_dir(repo_dir)
    .exec()
    .map_err(|e| PrecompileAllError::CargoMetadata(WeakError::wrap(e)))?;
  let version = cargo_meta
    .packages
    .iter()
    .find(|p| p.name.as_str() == "eternaltwin")
    .map(|p| p.version.to_string())
    .ok_or(PrecompileAllError::VersionNotFound)?;
  let version = version.as_str();
  let host_target_dir = PathBuf::from(cargo_meta.target_directory);
  let host_target_dir = host_target_dir.as_path();

  let mut result: BTreeMap<Component, (PathBuf, DistMeta)> = BTreeMap::new();

  for package in s.components.values().filter(|c| c.kind == Kind::Executable) {
    let target = package.target.expect("target is present");
    if !cmd.targets.contains(&target) {
      continue;
    }
    eprintln!("dist: {}", package.name);
    let out_dir_buf = dist_dir.join(target.name);
    let out_dir = out_dir_buf.as_path();
    fs::create_dir_all(out_dir)
      .map_err(|e| PrecompileAllError::CreateTargetDir(WeakError::wrap(e), out_dir.to_path_buf()))?;
    let old_meta: Option<DistMeta> = get_meta(out_dir).map_err(PrecompileAllError::GetOldMeta)?;
    let new_meta = match old_meta {
      Some(m) if m.version == version => m,
      _ => {
        let cross_meta = MetadataCommand::new()
          .cargo_path("cross")
          .no_deps()
          .current_dir(repo_dir)
          .env("CROSS_REMOTE", if cmd.remote { "1" } else { "0" })
          .other_options(vec!["--target".to_string(), target.name.to_string()])
          .exec()
          .map_err(|e| PrecompileAllError::CrossMetadata(WeakError::wrap(e)))?;

        let guest_target_dir = if cmd.remote {
          // workaround for https://github.com/cross-rs/cross/issues/1397
          cross_meta.workspace_root.join_os("target")
        } else {
          cross_meta.target_directory.into_std_path_buf()
        };
        let guest_target_dir = guest_target_dir.as_path();
        eprintln!("target directory: host = {host_target_dir:?}, guest = {guest_target_dir:?}");
        let exe = precompile(
          host_target_dir,
          guest_target_dir,
          repo_dir,
          out_dir,
          target.name,
          cmd.remote,
          cmd.clean_after,
        )
        .map_err(|e| PrecompileAllError::Precompile(e, package.name.to_string()))?;
        DistMeta {
          version: version.to_string(),
          executable: exe,
        }
      }
    };
    let meta_path = out_dir.join("meta.json");
    let mut meta = serde_json::to_string_pretty(&new_meta).expect("serializing metadata never fails");
    meta.push('\n');
    fs::write(meta_path.as_path(), meta).map_err(|e| PrecompileAllError::WriteMeta(WeakError::wrap(e), meta_path))?;
    result.insert(package.clone(), (out_dir_buf, new_meta));
  }

  Ok(result)
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct DistMeta {
  pub version: String,
  pub executable: String,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetMetaError {
  #[error("failed to read metadata at {1:?}")]
  ReadError(#[source] WeakError, PathBuf),
  #[error("invalid metadata format at {1:?}")]
  Format(#[source] WeakError, PathBuf),
}

/// Get metadata for a compiled Eternaltwin build
fn get_meta(target_dir: &Path) -> Result<Option<DistMeta>, GetMetaError> {
  let meta_path = target_dir.join("meta.json");
  match fs::read_to_string(meta_path.as_path()) {
    Ok(old_meta) => {
      let meta: DistMeta =
        serde_json::from_str(old_meta.as_str()).map_err(|e| GetMetaError::Format(WeakError::wrap(e), meta_path))?;
      Ok(Some(meta))
    }
    Err(e) => {
      if e.kind() == ErrorKind::NotFound {
        Ok(None)
      } else {
        Err(GetMetaError::ReadError(WeakError::wrap(e), meta_path))
      }
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum PrecompileError {
  #[error("cross build failed")]
  CrossBuild(#[source] CrossBuildError),
  #[error("failed to strip target dir prefix {2:?} from exe {1:?}")]
  StripPrefix(#[source] StripPrefixError, PathBuf, PathBuf),
  #[error("failed to copy executable from {1:?} to {2:?}")]
  CopyExe(#[source] WeakError, PathBuf, PathBuf),
  #[error("failed to run `cargo clean`")]
  CargoCleanRun(#[source] WeakError),
  #[error("`cargo clean` returned a non-success exit status")]
  CargoClean,
}

fn precompile(
  host_target_dir: &Path,
  guest_target_dir: &Path,
  repo_dir: &Path,
  out_dir: &Path,
  target: &str,
  cross_remote: bool,
  clean: bool,
) -> Result<String, PrecompileError> {
  eprintln!("starting cross-compilation for precompiled target {target}");
  let container_executable = cross_build(repo_dir, target, cross_remote).map_err(PrecompileError::CrossBuild)?;
  let relative_executable = container_executable
    .strip_prefix(guest_target_dir)
    .map_err(|e| PrecompileError::StripPrefix(e, guest_target_dir.to_path_buf(), container_executable.clone()))?;
  let host_executable = host_target_dir.join(relative_executable);
  eprintln!("compilation complete, executable: {}", host_executable.display());
  let executable = host_executable
    .file_name()
    .expect("missing exe name")
    .to_str()
    .expect("invalid exe name")
    .to_string();
  let dist_executable = out_dir.join(&executable);
  fs::copy(host_executable.as_path(), dist_executable.as_path())
    .map_err(|e| PrecompileError::CopyExe(WeakError::wrap(e), host_executable, dist_executable))?;
  if clean {
    let out = Command::new("cargo")
      .args(["clean"])
      .current_dir(repo_dir)
      .env("CROSS_REMOTE", if cross_remote { "1" } else { "0" })
      .output()
      .map_err(|e| PrecompileError::CargoCleanRun(WeakError::wrap(e)))?;
    if !out.status.success() {
      return Err(PrecompileError::CargoClean);
    }
  }
  Ok(executable)
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum CrossBuildError {
  #[error("failed to read build event")]
  ReadBuildEvent(#[source] WeakError),
  #[error("no executable found during build")]
  ExeNotFound,
  #[error("failed to wait on build end")]
  BuildWait(#[source] WeakError),
  #[error("build command returned non-success exit status")]
  BuildError,
}

fn cross_build(repo_dir: &Path, target: &str, cross_remote: bool) -> Result<PathBuf, CrossBuildError> {
  let mut command = Command::new("cross")
    .args([
      "build",
      "--bin",
      "eternaltwin",
      "--release",
      "--message-format=json",
      "--target",
      target,
      "--config",
      "profile.release.opt-level=\"z\"",
      "--config",
      "profile.release.lto=true",
    ])
    .env("CROSS_CONFIG", "./Cross.toml")
    .env("CROSS_REMOTE", if cross_remote { "1" } else { "0" })
    .stdout(Stdio::piped())
    .current_dir(repo_dir)
    .spawn()
    .expect("failed to run `cross`");

  let reader = std::io::BufReader::new(command.stdout.take().unwrap());
  let mut executable: Option<PathBuf> = None;
  for message in cargo_metadata::Message::parse_stream(reader) {
    let message = message.map_err(|e| CrossBuildError::ReadBuildEvent(WeakError::wrap(e)))?;
    if let Message::CompilerArtifact(artifact) = message {
      if artifact.target.kind.contains(&"bin".to_string()) && artifact.target.name == "eternaltwin" {
        let exe = artifact
          .executable
          .map(PathBuf::from)
          .expect("`executable` field is present");
        let old = executable.replace(exe);
        assert!(old.is_none(), "duplicate executable");
      }
    }
  }

  let output = command
    .wait()
    .map_err(|e| CrossBuildError::BuildWait(WeakError::wrap(e)))?;
  if !output.success() {
    return Err(CrossBuildError::BuildError);
  }
  executable.ok_or(CrossBuildError::ExeNotFound)
}

// fn generate_node_package(target: &str, version: &str, executable: &Path) -> Result<(), Box<dyn Error>> {
//   let working_dir = std::env::current_dir()?;
//   let package_dir = working_dir.join(format!("packages/exe-{target}"));
//   fs::create_dir_all(&package_dir)?;
//   let executable_name = executable
//     .file_name()
//     .ok_or_else(|| "failed to resolve executable name".to_string())?;
//   fs::copy(executable, package_dir.join(executable_name))?;
//   let executable_name = executable_name.to_str().expect("invalid executable name");
//   fs::write(package_dir.join("index.mjs"), get_index_mjs(executable_name))?;
//   fs::write(package_dir.join("index.d.mts"), get_index_d_mts())?;
//   fs::write(package_dir.join("README.md"), get_readme(target))?;
//   fs::write(
//     package_dir.join("package.json"),
//     get_package_json(target, version, executable_name),
//   )?;
//   Ok(())
// }
//
// fn get_index_mjs(executable_name: &str) -> String {
//   format!(
//     r#"// Auto-generated by `cargo xtask precompile`, do not edit manually
// export const executableName = {};
// export const executableUri = new URL(executableName, import.meta.url);
// "#,
//     serde_json::to_string(executable_name).expect("serializing the executable name succeeds")
//   )
// }
//
// fn get_index_d_mts() -> String {
//   r#"// Auto-generated by `cargo xtask precompile`, do not edit manually
// export const executableName: string;
// export const executableUri: URL;
// "#
//   .to_string()
// }
//
// fn get_package_json(target: &str, version: &str, executable_name: &str) -> String {
//   let (cpu, os) = match target {
//     "x86_64-unknown-linux-gnu" => ("x64", "linux"),
//     "x86_64-apple-darwin" => ("x64", "darwin"),
//     "x86_64-pc-windows-msvc" => ("x64", "win32"),
//     "armv7-unknown-linux-gnueabihf" => ("arm", "linux"),
//     t => panic!("unexpected target {t}"),
//   };
//
//   let pkg = json!({
//     "name": format!("@eternaltwin/exe-{target}"),
//     "version": version,
//     "description": format!("Precompiled Eternaltwin executable for {target}"),
//     "licenses": [
//       {
//         "type": "AGPL-3.0-or-later",
//         "url": "https://spdx.org/licenses/AGPL-3.0-or-later.html"
//       }
//     ],
//     "publishConfig": {
//       "access": "public",
//       "registry": "https://registry.npmjs.org/"
//     },
//     "type": "module",
//     "os": [os],
//     "cpu": [cpu],
//     "exports": {
//       ".": "./index.mjs",
//       "./package.json": "./package.json",
//     },
//     "bin": {
//       format!("etwin-{target}"): format!("./{executable_name}")
//     },
//     "files": [
//       format!("./{executable_name}"),
//       "./index.mjs",
//       "./index.d.mts",
//       "./package.json",
//       "./README.md",
//     ],
//     "preferUnplugged": true
//   });
//   let mut pkg = serde_json::to_string_pretty(&pkg).expect("writing the package.json succeeds");
//   pkg.push('\n');
//   pkg
// }
//
// fn get_readme(target: &str) -> String {
//   format!(
//     r#"# Precompiled eternaltwin for `{target}`
//
// This package is internal implementation of `@eternaltwin/exe`. You should
// not depend on it directly.
//
// This package was autogenerated through `cargo xtask precompile`, do not edit it
// manually.
// "#
//   )
// }
