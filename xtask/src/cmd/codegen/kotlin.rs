use crate::cmd::CliContext;
use clap::Parser;
use eternaltwin_core::types::WeakError;

#[derive(Debug, Parser)]
pub struct Args {
  // no arguments currently
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("kotlin codegen error")]
  Inner(#[source] WeakError),
}

pub async fn run(_cx: CliContext<'_, Args>) -> Result<(), Error> {
  crate::metagen::kotlin().map_err(Error::Inner)?;
  Ok(())
}
