use crate::cmd::CliContext;
use crate::REPO_DIR;
use clap::Parser;
use eternaltwin_core::twinoid::api::{Achievement, Missing, SiteIcon, Stat};
use eternaltwin_core::twinoid::client::{AchievementMetadata, FullSite, FullSiteInfo, SafeUser, StatMetadata};
use eternaltwin_core::twinoid::TwinoidSiteId;
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use std::collections::BTreeMap;
use std::fs;
use std::io;
use std::io::Write;
use std::str::FromStr;

const INVALID_ID_RESPONSE: &str = r#"{"error":"server_error","error_description":"Invalid field access : id"}"#;

/// Arguments to the `codegen constants` task.
#[derive(Debug, Parser)]
pub struct Args {}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("failed to generate twinoid sites")]
  TwinoidSites(#[source] WeakError),
  #[error("failed to generate twinoid stats and achievements")]
  TwinoidStatsAndAchievements(#[source] WeakError),
}

pub fn run(_cx: CliContext<'_, Args>) -> Result<(), Error> {
  codegen_twinoid_sites().map_err(Error::TwinoidSites)?;
  codegen_twinoid_stats_and_achievements().map_err(Error::TwinoidStatsAndAchievements)?;
  Ok(())
}

pub fn codegen_twinoid_stats_and_achievements() -> Result<(), WeakError> {
  let users = REPO_DIR.join("./test-resources/scraping/twinoid/api/twinoid/safe-user");
  let users = match fs::read_dir(users.as_path()) {
    Ok(users) => users,
    Err(e) => panic!("failed to read dir {users:?}: {}", DisplayErrorChain(&e)),
  };
  let mut out_stats: BTreeMap<(TwinoidSiteId, String), StatMetadata> = BTreeMap::new();
  let mut out_achievements: BTreeMap<(TwinoidSiteId, String), AchievementMetadata> = BTreeMap::new();
  for entry in users {
    let entry = entry.expect("failed to read users entry");
    let meta = entry.metadata().expect("failed to read user entry meta");
    if !meta.is_dir() {
      continue;
    }
    let json_path = entry.path().join("input.json");
    let json = fs::read_to_string(json_path.as_path()).expect("failed to read json");
    let user: SafeUser = match serde_json::from_str(&json) {
      Ok(user) => user,
      Err(_) => continue,
    };
    for site_user in user.sites {
      if let Some(stats) = site_user.stats {
        for stat in stats {
          let key = stat.id.clone();
          out_stats.insert(
            (site_user.site.id, key),
            Stat {
              id: stat.id,
              score: Missing,
              name: stat.name,
              icon: stat.icon,
              description: stat.description,
              rare: stat.rare,
              social: stat.social,
            },
          );
        }
      }
      if let Some(achievements) = site_user.achievements {
        for achievement in achievements {
          let key = achievement.id.clone();
          out_achievements.insert(
            (site_user.site.id, key),
            Achievement {
              id: achievement.id,
              name: achievement.name,
              stat: achievement.stat,
              score: achievement.score,
              points: achievement.points,
              npoints: achievement.npoints,
              description: achievement.description,
              data: achievement.data,
              date: Missing,
              index: achievement.index,
            },
          );
        }
      }
    }
  }

  {
    let rs_out = REPO_DIR.join("../../../../crates/constants/src/generated/twinoid_stats.rs");
    let mut rs_out = fs::OpenOptions::new()
      .create(true)
      .truncate(true)
      .write(true)
      .open(rs_out.as_path())
      .expect("failed to open rs output");
    write_rs_stats(&mut rs_out, &out_stats).map_err(WeakError::wrap)?;
    rs_out.flush().map_err(WeakError::wrap)?;
  }

  {
    let rs_out = REPO_DIR.join("../../../../crates/constants/src/generated/twinoid_achievements.rs");
    let mut rs_out = fs::OpenOptions::new()
      .create(true)
      .truncate(true)
      .write(true)
      .open(rs_out.as_path())
      .expect("failed to open rs output");
    write_rs_achievements(&mut rs_out, &out_achievements).map_err(WeakError::wrap)?;
    rs_out.flush().map_err(WeakError::wrap)?;
  }

  Ok(())
}

pub fn codegen_twinoid_sites() -> Result<(), WeakError> {
  let sites = REPO_DIR.join("../../../../test-resources/scraping/twinoid/api/twinoid/site");
  let sites = match fs::read_dir(sites.as_path()) {
    Ok(sites) => sites,
    Err(e) => panic!("failed to read dir {sites:?}: {}", DisplayErrorChain(&e)),
  };
  let mut allowed_sites: Vec<FullSite> = Vec::new();
  let mut restricted_sites: Vec<TwinoidSiteId> = Vec::new();
  for entry in sites {
    let entry = entry.expect("failed to read sites entry");
    let meta = entry.metadata().expect("failed to read site entry meta");
    if !meta.is_dir() {
      continue;
    }
    let json_path = entry.path().join("input.json");
    let json = fs::read_to_string(json_path.as_path()).expect("failed to read json");
    if json.trim() == INVALID_ID_RESPONSE {
      let id = entry.file_name();
      let id = id.to_str().expect("invalid entry name");
      let id = u32::from_str(id).expect("invalid TwinoidSiteId format");
      restricted_sites.push(TwinoidSiteId::new(id).expect("invalid TwinoidSiteId range"));
      continue;
    }
    let site: FullSite = match serde_json::from_str(&json) {
      Ok(site) => site,
      Err(_) => continue,
    };
    if site.name.is_deny() {
      restricted_sites.push(site.id);
    } else {
      allowed_sites.push(site);
    }
  }

  let rs_out = REPO_DIR.join("../../../../crates/constants/src/generated/twinoid_sites.rs");

  let mut rs_out = fs::OpenOptions::new()
    .create(true)
    .truncate(true)
    .write(true)
    .open(rs_out.as_path())
    .expect("failed to open rs output");

  write_rs(&mut rs_out, &allowed_sites, &restricted_sites).map_err(WeakError::wrap)?;
  rs_out.flush().map_err(WeakError::wrap)?;

  Ok(())
}

fn write_rs<W: io::Write>(w: &mut W, sites: &[FullSite], restricted_sites: &[TwinoidSiteId]) -> io::Result<()> {
  writeln!(
    w,
    "// Auto-generated by `xtask codegen constants`, do not edit manually."
  )?;
  writeln!(w, r"use eternaltwin_core::twinoid::api::{{TwinoidSiteStatus}};")?;
  writeln!(
    w,
    r"use eternaltwin_core::twinoid::{{TwinoidLocale, TwinoidSiteId, TwinoidSiteInfoId}};"
  )?;
  writeln!(w, r"use super::{{ConstFullSite, ConstSiteIcon, ConstSiteInfo}};")?;
  writeln!(w)?;
  writeln!(w, r#"#[rustfmt::skip]"#)?;
  writeln!(w, r"pub const SITES: [ConstFullSite; {}] = [", sites.len())?;
  for site in sites {
    write_rs_site(w, site)?;
  }
  writeln!(w, "];")?;
  writeln!(w)?;
  writeln!(w, r#"#[rustfmt::skip]"#)?;
  writeln!(
    w,
    r"pub const RESTRICTED_SITES: [TwinoidSiteId; {}] = [",
    restricted_sites.len()
  )?;
  for site_id in restricted_sites {
    writeln!(w, r#"  unsafe {{ TwinoidSiteId::new_unchecked({}) }},"#, site_id)?;
  }
  writeln!(w, "];")?;
  Ok(())
}

fn write_rs_site<W: Write>(w: &mut W, site: &FullSite) -> io::Result<()> {
  writeln!(w, r"  ConstFullSite {{")?;
  writeln!(w, r#"    id: unsafe {{ TwinoidSiteId::new_unchecked({}) }},"#, site.id)?;
  writeln!(w, r"    name: {:?},", site.name.as_ref().expect("name is present"))?;
  writeln!(
    w,
    r"    host: {:?},",
    site.host.as_ref().expect("host is present").to_string()
  )?;
  writeln!(
    w,
    r"    icon: {:?},",
    site.icon.as_ref().expect("icon is present").url.as_prurl()
  )?;
  match site.lang.expect("lang is present") {
    None => writeln!(w, r"    lang: None,")?,
    Some(l) => writeln!(w, r"    lang: Some(TwinoidLocale::{l:?}),")?,
  }
  let like = site.like.as_ref().expect("like is present");
  writeln!(w, r"    like_url: {:?},", like.url.to_string())?;
  writeln!(w, r"    like_likes: {},", like.likes)?;
  writeln!(w, r"    like_title: {:?},", like.title)?;
  writeln!(w, r"    infos: &[")?;
  for info in site.infos.as_ref().expect("`infos` is present") {
    write_rs_site_info(w, info)?;
  }
  writeln!(w, r"    ],")?;
  writeln!(
    w,
    r"    status: TwinoidSiteStatus::{:?},",
    site.status.expect("status is present"),
  )?;
  writeln!(w, "  }},")?;
  Ok(())
}

fn write_rs_site_info<W: Write>(w: &mut W, info: &FullSiteInfo) -> io::Result<()> {
  writeln!(w, r"      ConstSiteInfo {{")?;
  writeln!(
    w,
    r#"        id: unsafe {{ TwinoidSiteInfoId::new_unchecked({}) }},"#,
    info.id
  )?;
  writeln!(w, r"        lang: TwinoidLocale::{:?},", info.lang,)?;
  writeln!(w, r"        cover: {:?},", info.cover.url.as_prurl())?;
  writeln!(w, r"        tag_line: {:?},", info.tag_line)?;
  writeln!(w, r"        description: {:?},", info.description)?;
  writeln!(w, r"        tid: {:?},", info.tid)?;
  writeln!(w, r"        icons: &[")?;
  for icon in &info.icons {
    write_rs_site_icon(w, icon)?;
  }
  writeln!(w, r"        ],")?;
  writeln!(w, "      }},")?;
  Ok(())
}

fn write_rs_site_icon<W: Write>(w: &mut W, icon: &SiteIcon) -> io::Result<()> {
  writeln!(w, r"          ConstSiteIcon {{")?;
  writeln!(w, r"            tag: {:?},", icon.tag)?;
  writeln!(w, r"            alt_tag: {:?},", icon.alt_tag)?;
  writeln!(w, r"            url: {:?},", icon.url.as_prurl())?;
  writeln!(w, r"            typ: {:?},", icon.typ)?;
  writeln!(w, "          }},")?;
  Ok(())
}

fn write_rs_stats<W: io::Write>(w: &mut W, stats: &BTreeMap<(TwinoidSiteId, String), StatMetadata>) -> io::Result<()> {
  writeln!(
    w,
    "// Auto-generated by `xtask codegen constants`, do not edit manually."
  )?;
  writeln!(w, r"use eternaltwin_core::twinoid::TwinoidSiteId;")?;
  writeln!(w, r"use super::ConstTwinoidStat;")?;
  writeln!(w)?;
  writeln!(w, r#"#[rustfmt::skip]"#)?;
  writeln!(w, r"pub static STATS: [ConstTwinoidStat; {}] = [", stats.len())?;
  for ((site_id, _), stat) in stats {
    write_rs_stat(w, *site_id, stat)?;
  }
  writeln!(w, "];")?;
  Ok(())
}

fn write_rs_stat<W: Write>(w: &mut W, site_id: TwinoidSiteId, stat: &StatMetadata) -> io::Result<()> {
  writeln!(w, r"  ConstTwinoidStat {{")?;
  writeln!(
    w,
    r#"    site_id: unsafe {{ TwinoidSiteId::new_unchecked({}) }},"#,
    site_id
  )?;
  writeln!(w, r"    key: {:?},", stat.id)?;
  writeln!(w, r"    name: {:?},", stat.name.as_str())?;
  writeln!(
    w,
    r"    icon: {:?},",
    stat.icon.as_ref().map(|u| u.url.as_url().as_str())
  )?;
  writeln!(w, r"    description: {:?},", stat.description.as_ref())?;
  writeln!(w, r"    rare: {},", stat.rare)?;
  writeln!(w, r"    social: {},", stat.social)?;
  writeln!(w, "  }},")?;
  Ok(())
}

fn write_rs_achievements<W: io::Write>(
  w: &mut W,
  achievements: &BTreeMap<(TwinoidSiteId, String), AchievementMetadata>,
) -> io::Result<()> {
  writeln!(
    w,
    "// Auto-generated by `xtask codegen constants`, do not edit manually."
  )?;
  writeln!(w, r"use eternaltwin_core::twinoid::TwinoidSiteId;")?;
  writeln!(w, r"use super::ConstTwinoidAchievement;")?;
  writeln!(w)?;
  writeln!(w, r#"#[rustfmt::skip]"#)?;
  writeln!(
    w,
    r"pub static ACHIEVEMENTS: [ConstTwinoidAchievement; {}] = [",
    achievements.len()
  )?;
  for ((site_id, _), achievement) in achievements {
    write_rs_achievement(w, *site_id, achievement)?;
  }
  writeln!(w, "];")?;
  Ok(())
}

fn write_rs_achievement<W: Write>(
  w: &mut W,
  site_id: TwinoidSiteId,
  achievement: &AchievementMetadata,
) -> io::Result<()> {
  writeln!(w, r"  ConstTwinoidAchievement {{")?;
  writeln!(
    w,
    r#"    site_id: unsafe {{ TwinoidSiteId::new_unchecked({}) }},"#,
    site_id
  )?;
  writeln!(w, r"    key: {:?},", achievement.id)?;
  writeln!(w, r"    name: {:?},", achievement.name.as_str())?;
  writeln!(w, r"    stat: {:?},", achievement.stat.as_str())?;
  writeln!(w, r"    score: {},", achievement.score)?;
  writeln!(w, r"    points: {},", achievement.points)?;
  writeln!(w, r"    npoints: {:?},", achievement.npoints)?;
  writeln!(w, r"    description: {:?},", achievement.description.as_str())?;
  writeln!(w, r"    index: {},", achievement.index)?;
  writeln!(w, r"    data_type: {:?},", achievement.data.typ.as_str())?;
  writeln!(w, r"    data_title: {:?},", achievement.data.title.as_deref())?;
  writeln!(
    w,
    r"    data_url: {:?},",
    achievement.data.url.as_ref().map(|u| u.as_url().as_str())
  )?;
  writeln!(w, r"    data_prefix: {:?},", achievement.data.prefix)?;
  writeln!(w, r"    data_suffix: {:?},", achievement.data.suffix)?;
  writeln!(w, "  }},")?;
  Ok(())
}
