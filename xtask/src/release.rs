use crate::structure::{Kind, Structure};
use clap::Parser;
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use std::collections::HashSet;
use std::process::Command;
use std::str::FromStr;
use tokio::fs;

/// Arguments to the `release` task.
#[derive(Debug, Parser)]
pub struct Args {
  /// Version of the release to create
  version: String,
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("unexpected release failure")]
  Inner(#[source] WeakError),
}

pub async fn run(args: &Args) -> Result<(), Error> {
  let version = args.version.as_str();
  let s = Structure::load();
  release_rust(&s, version).await.map_err(Error::Inner)?;
  release_yarn(&s, version).map_err(Error::Inner)?;
  Ok(())
}

async fn release_rust(s: &Structure, version: &str) -> Result<(), WeakError> {
  let rust_components = s
    .components
    .values()
    .filter(|c| matches!(c.kind, Kind::RustBin | Kind::RustLib));

  let crate_names: HashSet<String> = rust_components.clone().map(|c| c.name.clone()).collect();
  let manifest_paths = rust_components.map(|rc| s.repo_root.join(&rc.source).join("Cargo.toml"));

  for manifest_path in manifest_paths {
    let manifest = fs::read_to_string(manifest_path.as_path())
      .await
      .expect("failed to read manifest");
    let mut doc = toml_edit::DocumentMut::from_str(&manifest).expect("invalid manifest");
    doc["package"]["version"] = toml_edit::value(version);

    for dep_table_key in ["dependencies", "dev-dependencies"] {
      let dep_table = match doc.get_mut(dep_table_key) {
        Some(t) => t,
        None => continue,
      };
      let dep_table = dep_table.as_table_mut().expect("not a table");
      for (key, value) in dep_table.iter_mut() {
        if !crate_names.contains(key.get()) {
          continue;
        }
        if value.is_str() {
          *value = toml_edit::value(version);
        } else if let Some(value) = value.as_inline_table_mut() {
          value["version"] = toml_edit::value(version).into_value().expect("string is a valid value");
        } else {
          panic!("invalid value");
        }
      }
    }
    let manifest = doc.to_string();
    fs::write(manifest_path, manifest)
      .await
      .expect("failed to write manifest");
  }

  Ok(())
}

fn release_yarn(s: &Structure, version: &str) -> Result<(), WeakError> {
  let mut cmd = Command::new("yarn");
  cmd
    .current_dir(&s.repo_root)
    .args(["workspaces", "foreach", "--all"])
    .args(["version", version]);
  let output = match cmd.output() {
    Ok(out) => out,
    Err(e) => panic!(
      "error while running `yarn version` in {:?}: {}",
      &s.repo_root,
      DisplayErrorChain(&e)
    ),
  };
  if !output.status.success() {
    panic!("`yarn version` exited with non-success status");
  }
  Ok(())
}
