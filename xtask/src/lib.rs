pub mod cmd;
pub mod dns;
mod docs;
pub mod metagen;
pub mod precompile;
pub mod publish;
pub mod release;
mod structure;

use clap::Parser;
pub use docs::docs;
use once_cell::sync::Lazy;
use std::path::PathBuf;

pub static REPO_DIR: Lazy<PathBuf> = Lazy::new(|| {
  PathBuf::from(env!("CARGO_MANIFEST_DIR"))
    .join("..")
    .canonicalize()
    .expect("REPO_DIR exists")
});

#[derive(Debug, Parser)]
#[clap(author = "Eternaltwin")]
pub struct CliArgs {
  #[clap(subcommand)]
  command: CliCommand,
}

#[derive(Debug, Parser)]
pub enum CliCommand {}

/// Arguments to the `kotlin` task.
#[derive(Debug, Parser)]
pub struct KotlinArgs {}

/// Arguments to the `php` task.
#[derive(Debug, Parser)]
pub struct PhpArgs {}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Target {
  pub name: &'static str,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("invalid build target {input:?}")]
pub struct InvalidTargetName {
  pub input: String,
}

impl Target {
  const X86_64_APPLE_DARWIN: Self = Self {
    name: "x86_64-apple-darwin",
  };
  const X86_64_PC_WINDOWS_GNU: Self = Self {
    name: "x86_64-pc-windows-gnu",
  };
  const X86_64_UNKNOWN_LINUX_GNU: Self = Self {
    name: "x86_64-unknown-linux-gnu",
  };
  const X86_64_UNKNOWN_LINUX_MUSL: Self = Self {
    name: "x86_64-unknown-linux-musl",
  };

  const ALL: [Self; 4] = [
    Self::X86_64_APPLE_DARWIN,
    Self::X86_64_PC_WINDOWS_GNU,
    Self::X86_64_UNKNOWN_LINUX_GNU,
    Self::X86_64_UNKNOWN_LINUX_MUSL,
  ];

  pub fn from_name(input: &str) -> Result<Self, InvalidTargetName> {
    for t in Self::ALL {
      if input == t.name {
        return Ok(t);
      }
    }
    Err(InvalidTargetName {
      input: input.to_string(),
    })
  }
}
