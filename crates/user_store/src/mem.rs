use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, RawUserDot};
use eternaltwin_core::email::EmailAddress;
use eternaltwin_core::password::PasswordHash;
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::temporal::Temporal;
use eternaltwin_core::user::{
  CompleteSimpleUser, CreateUserOptions, GetShortUserOptions, GetUserOptions, RawCreateUserError, RawDeleteUser,
  RawDeleteUserError, RawGetShortUserError, RawGetUser, RawGetUserError, RawGetUserResult, RawGetUserWithPasswordError,
  RawHardDeleteUser, RawHardDeleteUserError, RawUpdateUserError, RawUpdateUserOptions, RawUpsertSeedUserError,
  ShortUser, ShortUserWithPassword, SimpleUser, UpsertSeedUserOptions, UserDisplayName, UserDisplayNameVersion,
  UserDisplayNameVersions, UserFields, UserId, UserIdRef, UserRef, UserStore, Username, UsernameVersion,
  UsernameVersions, USERNAME_LOCK_DURATION, USER_DISPLAY_NAME_LOCK_DURATION, USER_PASSWORD_LOCK_DURATION,
};
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet};
use std::sync::RwLock;

struct MemUserSnapshot<'a> {
  user: &'a MemUser,
  time: Option<Instant>,
}

impl From<MemUserSnapshot<'_>> for ShortUser {
  fn from(user: MemUserSnapshot<'_>) -> Self {
    let MemUserSnapshot { user, time } = user;
    Self {
      id: user.id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: user.display_name.at(time).unwrap().into_value().clone(),
        },
      },
    }
  }
}

impl From<MemUserSnapshot<'_>> for SimpleUser {
  fn from(user: MemUserSnapshot<'_>) -> Self {
    let MemUserSnapshot { user, time } = user;
    Self {
      id: user.id,
      created_at: user.created_at,
      deleted_at: user.deleted.map(|d| d.time),
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: user.display_name.at(time).unwrap().into_value().clone(),
        },
      },
      is_administrator: user.is_administrator,
    }
  }
}

impl From<MemUserSnapshot<'_>> for CompleteSimpleUser {
  fn from(user: MemUserSnapshot<'_>) -> Self {
    let MemUserSnapshot { user, time } = user;
    Self {
      id: user.id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: user.display_name.at(time).unwrap().into_value().clone(),
        },
      },
      is_administrator: user.is_administrator,
      created_at: user.created_at,
      deleted_at: None,
      username: user.username.at(time).unwrap().into_value().clone(),
      email_address: user.email_address.at(time).unwrap().into_value().clone(),
      has_password: user.password.at(time).unwrap().into_value().is_some(),
    }
  }
}

pub(crate) struct StoreState {
  users: HashMap<UserId, MemUser>,
  users_by_username: HashMap<Username, Temporal<Option<UserId>>>,
  users_by_email: HashMap<EmailAddress, Temporal<Option<UserId>>>,
}

impl StoreState {
  fn new() -> Self {
    Self {
      users: HashMap::new(),
      users_by_username: HashMap::new(),
      users_by_email: HashMap::new(),
    }
  }

  fn create(&mut self, time: Instant, user_id: UserId, options: &CreateUserOptions) -> &MemUser {
    let mem_user = MemUser {
      id: user_id,
      created_at: time,
      deleted: None,
      display_name: Temporal::new(time, options.display_name.clone()),
      email_address: Temporal::new(time, options.email.clone()),
      username: Temporal::new(time, options.username.clone()),
      password: Temporal::new(time, options.password.clone()),
      is_administrator: self.users.is_empty(),
    };
    let mem_user = match self.users.entry(mem_user.id) {
      Entry::Occupied(_) => panic!("UserIdConflict"),
      Entry::Vacant(e) => e.insert(mem_user),
    };
    if let Some(username) = &options.username {
      match self.users_by_username.entry(username.clone()) {
        Entry::Occupied(_) => panic!("UsernameConflict"),
        Entry::Vacant(e) => e.insert(Temporal::new(time, Some(mem_user.id))),
      };
    }
    if let Some(email) = &options.email {
      match self.users_by_email.entry(email.clone()) {
        Entry::Occupied(_) => panic!("EmailConflict"),
        Entry::Vacant(e) => e.insert(Temporal::new(time, Some(mem_user.id))),
      };
    }
    mem_user
  }

  fn upsert_seed(
    &mut self,
    time: Instant,
    fresh_id: UserId,
    cmd: &UpsertSeedUserOptions,
  ) -> Result<&MemUser, RawUpsertSeedUserError> {
    let (user_id, is_new) = match self.get(&cmd.r#ref, Some(time)) {
      Some(old) => {
        // update existing user
        assert!(old.created_at <= time);
        (old.id, false)
      }
      None => {
        // insert new user
        let id = cmd.r#ref.as_id_ref().map(|r| r.id).unwrap_or(fresh_id);
        (id, true)
      }
    };
    if let Some(username) = cmd.username.as_ref() {
      let current = self
        .users_by_username
        .get(username)
        .and_then(|t| t.at(Some(time)).and_then(|s| **s.value()));
      if let Some(current) = current {
        if current != user_id {
          return Err(RawUpsertSeedUserError::UsernameConflict(
            username.clone(),
            UserIdRef::new(current),
          ));
        }
      }
    }
    if let Some(email) = cmd.email.as_ref() {
      let current = self
        .users_by_email
        .get(email)
        .and_then(|t| t.at(Some(time)).and_then(|s| **s.value()));
      if let Some(current) = current {
        if current != user_id {
          return Err(RawUpsertSeedUserError::EmailConflict(
            email.clone(),
            UserIdRef::new(current),
          ));
        }
      }
    }
    // At this point we checked there are no conflicts and the write will succeed
    if is_new {
      let mem_user = MemUser {
        id: user_id,
        created_at: time,
        deleted: None,
        display_name: Temporal::new(time, cmd.display_name.clone()),
        email_address: Temporal::new(time, cmd.email.clone()),
        username: Temporal::new(time, cmd.username.clone()),
        password: Temporal::new(time, cmd.password.clone()),
        is_administrator: cmd.is_administrator,
      };
      let mem_user = match self.users.entry(user_id) {
        Entry::Occupied(_) => panic!("user id conflict is not possible at this point"),
        Entry::Vacant(e) => e.insert(mem_user),
      };
      if let Some(username) = &cmd.username {
        match self.users_by_username.entry(username.clone()) {
          Entry::Occupied(_) => panic!("username conflict is not possible at this point"),
          Entry::Vacant(e) => e.insert(Temporal::new(time, Some(user_id))),
        };
      }
      if let Some(email) = &cmd.email {
        match self.users_by_email.entry(email.clone()) {
          Entry::Occupied(_) => panic!("email address conflict is not possible at this point"),
          Entry::Vacant(e) => e.insert(Temporal::new(time, Some(user_id))),
        };
      }
      Ok(mem_user)
    } else {
      let mem_user = self.users.get_mut(&user_id).expect("user exists at this point");
      let cur_display_name = mem_user
        .display_name
        .at(Some(time))
        .map(|s| s.into_value())
        .expect("display name is defined at this time");
      if cur_display_name != &cmd.display_name {
        mem_user.display_name.set(time, cmd.display_name.clone());
      }
      let cur_email = mem_user
        .email_address
        .at(Some(time))
        .map(|s| s.into_value())
        .expect("email address is defined at this time");
      if cur_email != &cmd.email {
        if let Some(cur_email) = cur_email {
          if let Some(old) = self.users_by_email.get_mut(cur_email) {
            old.set(time, None);
          }
        }
        mem_user.email_address.set(time, cmd.email.clone());
        if let Some(new_email) = cmd.email.clone() {
          match self.users_by_email.entry(new_email) {
            Entry::Occupied(mut e) => e.get_mut().set(time, Some(user_id)),
            Entry::Vacant(e) => {
              e.insert(Temporal::new(time, Some(user_id)));
            }
          };
        }
      }
      let cur_username = mem_user
        .username
        .at(Some(time))
        .map(|s| s.into_value())
        .expect("username is defined at this time");
      if cur_username != &cmd.username {
        if let Some(cur_username) = cur_username {
          if let Some(old) = self.users_by_username.get_mut(cur_username) {
            old.set(time, None);
          }
        }
        mem_user.username.set(time, cmd.username.clone());
        if let Some(new_username) = cmd.username.clone() {
          match self.users_by_username.entry(new_username) {
            Entry::Occupied(mut e) => e.get_mut().set(time, Some(user_id)),
            Entry::Vacant(e) => {
              e.insert(Temporal::new(time, Some(user_id)));
            }
          };
        }
      }
      let cur_password = mem_user
        .password
        .at(Some(time))
        .map(|s| s.into_value())
        .expect("password is defined at this time");
      if cur_password != &cmd.password {
        mem_user.password.set(time, cmd.password.clone());
      }
      mem_user.is_administrator = cmd.is_administrator;
      Ok(mem_user)
    }
  }

  /// Converts a user reference at a point in time into an id
  ///
  /// - `None` if no user found
  /// - `Some(user_id, verified_to_exist)`
  fn ref_to_id(&self, user_ref: &UserRef, time: Option<Instant>) -> Option<(UserId, bool)> {
    Some(match user_ref {
      UserRef::Id(r) => (r.id, false),
      UserRef::Username(r) => {
        let uid = self
          .users_by_username
          .get(&r.username)? // Never used
          .at(time)? // time < firstUse
          .into_value();
        let uid = (*uid)?; // unused at `time`
        (uid, true)
      }
      UserRef::Email(r) => {
        let uid = self
          .users_by_email
          .get(&r.email)? // Never used
          .at(time)? // time < firstUse
          .into_value();
        let uid = (*uid)?; // unused at `time`
        (uid, true)
      }
    })
  }

  fn get(&self, user_ref: &UserRef, time: Option<Instant>) -> Option<&MemUser> {
    let (uid, verified_to_exist) = self.ref_to_id(user_ref, time)?;
    let user = self.users.get(&uid);
    if verified_to_exist {
      debug_assert!(user.is_some())
    }
    user
  }

  fn update(&mut self, options: &RawUpdateUserOptions, now: Instant) -> Result<&MemUser, RawUpdateUserError> {
    let user = self.users.get_mut(&options.r#ref.id);
    let user = match user {
      Some(u) => u,
      None => return Err(RawUpdateUserError::NotFound(options.r#ref)),
    };
    if options.patch.display_name.is_set() {
      let lock_period = user.display_name.time()..(user.display_name.time() + USER_DISPLAY_NAME_LOCK_DURATION);
      // Do not lock on creation
      if lock_period.start != user.created_at && lock_period.contains(&now) {
        return Err(RawUpdateUserError::LockedDisplayName(
          options.r#ref,
          lock_period.into(),
          now,
        ));
      }
    }
    if options.patch.username.is_set() {
      let lock_period = user.username.time()..(user.username.time() + USERNAME_LOCK_DURATION);
      if user.username.current_value().is_some() && lock_period.contains(&now) {
        return Err(RawUpdateUserError::LockedUsername(
          options.r#ref,
          lock_period.into(),
          now,
        ));
      }
    }
    if options.patch.password.is_set() {
      let lock_period = user.password.time()..(user.password.time() + USER_PASSWORD_LOCK_DURATION);
      if user.password.current_value().is_some() && lock_period.contains(&now) {
        return Err(RawUpdateUserError::LockedPassword(
          options.r#ref,
          lock_period.into(),
          now,
        ));
      }
    }
    if let SimplePatch::Set(display_name) = &options.patch.display_name {
      user.display_name.set(now, display_name.clone());
    }
    if let SimplePatch::Set(username) = &options.patch.username {
      user.username.set(now, username.clone());
    }
    if let SimplePatch::Set(password) = &options.patch.password {
      user.password.set(now, password.clone());
    }
    Ok(user)
  }

  fn delete(&mut self, cmd: &RawDeleteUser) -> Result<&MemUser, RawDeleteUserError> {
    let now = cmd.now;
    let user_ref = cmd.user;
    let user = self.users.get_mut(&user_ref.id);
    let user = match user {
      Some(u) => u,
      None => return Err(RawDeleteUserError::NotFound(cmd.user)),
    };
    if user.deleted.is_some() {
      return Err(RawDeleteUserError::Gone(cmd.user));
    }
    user.deleted = Some(RawUserDot {
      time: now,
      user: cmd.actor,
    });
    Ok(user)
  }

  fn hard_delete(&mut self, cmd: &RawHardDeleteUser) -> Result<MemUser, RawHardDeleteUserError> {
    let user_ref = cmd.user;
    let user = self.users.remove(&user_ref.id);
    let user = match user {
      Some(u) => u,
      None => return Err(RawHardDeleteUserError::NotFound(user_ref)),
    };
    let mut usernames: HashSet<&Username> = HashSet::new();
    for snapshot in user.username.iter() {
      if let Some(username) = snapshot.value() {
        usernames.insert(username);
      }
    }
    for username in usernames {
      let history = self.users_by_username.get_mut(username).unwrap();
      *history = history.map(|snapshot| snapshot.value().filter(|uid| *uid != user.id));
    }
    Ok(user)
  }
}

pub(crate) struct MemUser {
  id: UserId,
  created_at: Instant,
  deleted: Option<RawUserDot>,
  display_name: Temporal<UserDisplayName>,
  email_address: Temporal<Option<EmailAddress>>,
  username: Temporal<Option<Username>>,
  password: Temporal<Option<PasswordHash>>,
  is_administrator: bool,
}

impl MemUser {
  fn at(&self, time: Option<Instant>) -> MemUserSnapshot {
    if let Some(time) = time {
      assert!(self.created_at <= time);
    }
    MemUserSnapshot { user: self, time }
  }
}

pub struct MemUserStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub(crate) clock: TyClock,
  pub(crate) uuid_generator: TyUuidGenerator,
  pub(crate) state: RwLock<StoreState>,
}

impl<TyClock, TyUuidGenerator> MemUserStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(clock: TyClock, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      uuid_generator,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock, TyUuidGenerator> UserStore for MemUserStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_user(&self, options: &CreateUserOptions) -> Result<CompleteSimpleUser, RawCreateUserError> {
    let user_id = UserId::from(self.uuid_generator.uuid_generator().next());
    let time = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    let mem_user = state.create(time, user_id, options);
    let user: CompleteSimpleUser = mem_user.at(Some(time)).into();
    Ok(user)
  }

  async fn upsert_seed_user(
    &self,
    options: &UpsertSeedUserOptions,
  ) -> Result<CompleteSimpleUser, RawUpsertSeedUserError> {
    let fresh_id = UserId::from(self.uuid_generator.uuid_generator().next());
    let time = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    let mem_user = state.upsert_seed(time, fresh_id, options)?;
    let user: CompleteSimpleUser = mem_user.at(Some(time)).into();
    Ok(user)
  }

  async fn get_user(&self, options: &GetUserOptions) -> Result<RawGetUserResult, RawGetUserError> {
    let state = &self.state.read().unwrap();

    let mem_user: Option<&MemUser> = state.get(&options.r#ref, options.time);

    match mem_user {
      None => Err(RawGetUserError::NotFound),
      Some(u) => {
        let user = u.at(options.time);
        Ok(match options.fields {
          UserFields::Complete => RawGetUserResult::Complete(user.into()),
          UserFields::CompleteIfSelf { self_user_id } => {
            if self_user_id == user.user.id {
              RawGetUserResult::Complete(user.into())
            } else {
              RawGetUserResult::Default(user.into())
            }
          }
          UserFields::Default => RawGetUserResult::Default(user.into()),
          UserFields::Short => RawGetUserResult::Short(user.into()),
        })
      }
    }
  }

  async fn get_user_with_password(
    &self,
    options: RawGetUser,
  ) -> Result<ShortUserWithPassword, RawGetUserWithPasswordError> {
    let state = &self.state.read().unwrap();

    let mem_user: Option<&MemUser> = state.get(&options.r#ref, Some(options.time));

    match mem_user {
      None => Err(RawGetUserWithPasswordError::NotFound(options.r#ref)),
      Some(u) => {
        let user = u.at(Some(options.time));
        let username = user.user.username.current_value().clone();
        let password = user.user.password.current_value().clone();
        let created_at = user.user.created_at;
        let deleted = user.user.deleted;
        let is_administrator = user.user.is_administrator;
        let short: ShortUser = user.into();
        Ok(ShortUserWithPassword {
          id: short.id,
          created_at,
          deleted,
          is_administrator,
          display_name: short.display_name,
          username: UsernameVersions {
            current: UsernameVersion { value: username },
          },
          password,
        })
      }
    }
  }

  async fn get_short_user(&self, options: &GetShortUserOptions) -> Result<ShortUser, RawGetShortUserError> {
    let state = &self.state.read().unwrap();
    let mem_user: Option<&MemUser> = state.get(&options.r#ref, options.time);
    match mem_user {
      Some(u) => Ok(ShortUser::from(u.at(options.time))),
      None => Err(RawGetShortUserError::NotFound(options.r#ref.clone())),
    }
  }

  async fn update_user(&self, options: &RawUpdateUserOptions) -> Result<CompleteSimpleUser, RawUpdateUserError> {
    let mut state = self.state.write().unwrap();
    let user = state.update(options, self.clock.clock().now())?;
    Ok(user.at(None).into())
  }

  async fn delete_user(&self, cmd: &RawDeleteUser) -> Result<CompleteSimpleUser, RawDeleteUserError> {
    let mut state = self.state.write().unwrap();
    let user = state.delete(cmd)?;
    Ok(user.at(None).into())
  }

  async fn hard_delete_user(&self, cmd: &RawHardDeleteUser) -> Result<(), RawHardDeleteUserError> {
    let mut state = self.state.write().unwrap();
    let _user = state.hard_delete(cmd)?;
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemUserStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn UserStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(clock.clone(), uuid_generator));

    TestApi { clock, user_store }
  }

  test_user_store!(|| make_test_api());
}
