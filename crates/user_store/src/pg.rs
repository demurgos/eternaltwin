use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, PeriodLower, RawUserDot, SecretString};
use eternaltwin_core::email::{touch_email_address, EmailAddress};
use eternaltwin_core::password::PasswordHash;
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{
  CompleteSimpleUser, CreateUserOptions, GetShortUserOptions, GetUserOptions, RawCreateUserError, RawDeleteUser,
  RawDeleteUserError, RawGetShortUserError, RawGetUser, RawGetUserError, RawGetUserResult, RawGetUserWithPasswordError,
  RawHardDeleteUser, RawHardDeleteUserError, RawUpdateUserError, RawUpdateUserOptions, RawUpsertSeedUserError,
  ShortUser, ShortUserWithPassword, UpsertSeedUserOptions, UserDisplayName, UserDisplayNameVersion,
  UserDisplayNameVersions, UserFields, UserId, UserIdRef, UserRef, UserStore, Username, UsernameVersion,
  UsernameVersions, USERNAME_LOCK_DURATION, USER_DISPLAY_NAME_LOCK_DURATION, USER_PASSWORD_LOCK_DURATION,
};
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use opentelemetry::trace::{Span, Tracer};
use opentelemetry::KeyValue;
use opentelemetry_semantic_conventions::trace as otel_keys;
use sqlx::postgres::PgPool;
use sqlx::{Executor, Postgres};

pub struct PgUserStore<TyClock, TyDatabase, TyTracer, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  database_secret: SecretString,
  tracer: TyTracer,
  uuid_generator: TyUuidGenerator,
}

impl<TyClock, TyDatabase, TyTracer, TyUuidGenerator> PgUserStore<TyClock, TyDatabase, TyTracer, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyTracer: Tracer,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(
    clock: TyClock,
    database: TyDatabase,
    database_secret: SecretString,
    tracer: TyTracer,
    uuid_generator: TyUuidGenerator,
  ) -> Self {
    Self {
      clock,
      database,
      database_secret,
      tracer,
      uuid_generator,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum PgUpsertSeedUserError {
  #[error("failed to create transaction")]
  CreateTx(#[source] WeakError),
  #[error("failed to retrieve current user data")]
  GetUser(#[source] WeakError),
  #[error("failed to update `is_administrator` flag")]
  UpdateIsAdminsitrator(#[source] WeakError),
  #[error("failed to insert user")]
  InsertUser(#[source] WeakError),
  #[error("failed to set user history")]
  SetUserHistory(#[source] WeakError),
  #[error("failed to touch email address")]
  TouchEmail(#[source] WeakError),
  #[error("failed to delete old username")]
  DeleteUsername(#[source] WeakError),
  #[error("failed to insert new username")]
  InsertUsername(#[source] WeakError),
  #[error("failed to commmit transaction")]
  CommitTx(#[source] WeakError),
}

impl From<PgUpsertSeedUserError> for RawUpsertSeedUserError {
  fn from(value: PgUpsertSeedUserError) -> Self {
    Self::Other(WeakError::wrap(value))
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyTracer, TyUuidGenerator> UserStore
  for PgUserStore<TyClock, TyDatabase, TyTracer, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_user(&self, options: &CreateUserOptions) -> Result<CompleteSimpleUser, RawCreateUserError> {
    let user_id = UserId::from_uuid(self.uuid_generator.uuid_generator().next());
    let now = self.clock.clock().now();

    let mut tx = self.database.begin().await.map_err(RawCreateUserError::other)?;

    let row = {
      let email_hash = match &options.email {
        Some(email) => Some(
          touch_email_address(
            &mut tx,
            &self.database_secret,
            &email.to_raw(),
            self.clock.clock().now(),
          )
          .await
          .map_err(RawCreateUserError::other)?,
        ),
        None => None,
      };

      let r = {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          period: PeriodLower,
          is_administrator: bool,
        }
        // language=PostgreSQL
        sqlx::query_as::<_, Row>(
          r"
          WITH administrator_exists AS (SELECT 1 FROM users WHERE is_administrator)
          INSERT
          INTO users(user_id, period, is_administrator)
          VALUES (
            $1::USER_ID, PERIOD($2::INSTANT, NULL), (NOT EXISTS(SELECT 1 FROM administrator_exists))
          )
          RETURNING period, is_administrator;
        ",
        )
        .bind(user_id)
        .bind(now)
        .fetch_one(&mut *tx)
        .await
        .map_err(RawCreateUserError::other)?
      };
      {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          user_id: UserId,
        }
        // language=PostgreSQL
        let row = sqlx::query_as::<_, Row>(
          r"
          INSERT
          INTO users_history(user_id, period, _is_current, updated_by, display_name, email, password)
          VALUES (
            $1::USER_ID, PERIOD($2::INSTANT, NULL), TRUE, $1::USER_ID, $3::USER_DISPLAY_NAME, $4::EMAIL_ADDRESS_HASH, pgp_sym_encrypt_bytea($5::PASSWORD_HASH, $6::TEXT)
          )
          RETURNING user_id;
        ",
        )
          .bind(user_id)
          .bind(now)
          .bind(&options.display_name)
          .bind(email_hash)
          .bind(options.password.as_ref())
          .bind(self.database_secret.as_str())
          .fetch_one(&mut *tx)
          .await.map_err(RawCreateUserError::other)?;
        debug_assert_eq!(row.user_id, user_id);
      }
      if let Some(username) = options.username.as_ref() {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          user_id: UserId,
        }
        // language=PostgreSQL
        let row = sqlx::query_as::<_, Row>(
          r"
          INSERT
          INTO user_username_history(period, user_id, username, created_by, deleted_by)
          VALUES (
            PERIOD($1::INSTANT, NULL), $2::USER_ID, $3::USERNAME, $2::USER_ID, NULL
          )
          RETURNING user_id;
        ",
        )
        .bind(now)
        .bind(user_id)
        .bind(username)
        .fetch_one(&mut *tx)
        .await
        .map_err(RawCreateUserError::other)?;
        debug_assert_eq!(row.user_id, user_id);
      }
      r
    };
    tx.commit().await.map_err(RawCreateUserError::other)?;

    let user = CompleteSimpleUser {
      id: user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: options.display_name.clone(),
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: options.username.clone(),
      email_address: None,
      has_password: options.password.is_some(),
    };

    Ok(user)
  }

  async fn upsert_seed_user(&self, cmd: &UpsertSeedUserOptions) -> Result<CompleteSimpleUser, RawUpsertSeedUserError> {
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(WeakError::wrap)
      .map_err(PgUpsertSeedUserError::CreateTx)?;
    let now = cmd.now;

    let old_user = get_user_with_password(
      &mut *tx,
      &self.database_secret,
      RawGetUser {
        r#ref: cmd.r#ref.clone(),
        fields: UserFields::Short,
        time: now,
        skip_deleted: false,
      },
    )
    .await;
    let old_user = match old_user {
      Ok(user) => Some(user),
      Err(RawGetUserError::NotFound) => None,
      Err(e) => {
        let e = PgUpsertSeedUserError::GetUser(WeakError::wrap(e));
        return Err(RawUpsertSeedUserError::Other(WeakError::wrap(e)));
      }
    };
    let email_hash = match &cmd.email {
      Some(email) => Some(
        touch_email_address(
          &mut tx,
          &self.database_secret,
          &email.to_raw(),
          self.clock.clock().now(),
        )
        .await
        .map_err(WeakError::wrap)
        .map_err(PgUpsertSeedUserError::TouchEmail)?,
      ),
      None => None,
    };

    if email_hash.is_some() {
      return Err(RawUpsertSeedUserError::Other(WeakError::new(
        "not supported: seed user with email",
      )));
    }

    let res = match old_user {
      Some(old_user) => {
        let user_ref = UserIdRef::new(old_user.id);

        if old_user.is_administrator != cmd.is_administrator {
          // language=PostgreSQL
          let res = sqlx::query(
            r"
                UPDATE users
                SET is_administrator = $1::boolean
                WHERE user_id = $2::user_id;
              ",
          )
          .bind(cmd.is_administrator)
          .bind(user_ref.id)
          .execute(&mut *tx)
          .await
          .map_err(WeakError::wrap)
          .map_err(PgUpsertSeedUserError::UpdateIsAdminsitrator)?;
          assert_eq!(res.rows_affected(), 1);
        }

        if old_user.display_name.current.value != cmd.display_name || old_user.password != cmd.password {
          let display_name = SimplePatch::Set(cmd.display_name.clone());
          let password = SimplePatch::Set(cmd.password.clone());
          set_user_history(
            &mut *tx,
            &self.database_secret,
            now,
            user_ref,
            user_ref,
            &display_name,
            &password,
          )
          .await
          .map_err(PgUpsertSeedUserError::SetUserHistory)?;
        }
        if old_user.username.current.value != cmd.username {
          if old_user.username.current.value.is_some() {
            delete_username(&mut *tx, now, user_ref, user_ref)
              .await
              .map_err(PgUpsertSeedUserError::DeleteUsername)?;
          }
          if let Some(username) = &cmd.username {
            insert_username(&mut *tx, now, user_ref, user_ref, username)
              .await
              .map_err(PgUpsertSeedUserError::InsertUsername)?;
          }
        }
        CompleteSimpleUser {
          id: user_ref.id,
          display_name: UserDisplayNameVersions {
            current: UserDisplayNameVersion {
              value: cmd.display_name.clone(),
            },
          },
          is_administrator: cmd.is_administrator,
          created_at: old_user.created_at,
          deleted_at: old_user.deleted.map(|d| d.time),
          username: cmd.username.clone(),
          email_address: None,
          has_password: cmd.password.is_some(),
        }
      }
      None => {
        let user_ref = match cmd.r#ref {
          UserRef::Id(r) => r,
          _ => UserIdRef::new(UserId::from_uuid(self.uuid_generator.uuid_generator().next())),
        };
        {
          // language=PostgreSQL
          let res = sqlx::query(
            r"
          INSERT
          INTO users(user_id, period, is_administrator)
          VALUES (
            $1::USER_ID, PERIOD($2::INSTANT, NULL), $3::BOOLEAN
          );
        ",
          )
          .bind(user_ref.id)
          .bind(now)
          .bind(cmd.is_administrator)
          .execute(&mut *tx)
          .await
          .map_err(WeakError::wrap)
          .map_err(PgUpsertSeedUserError::InsertUser)?;
          assert_eq!(res.rows_affected(), 1);
        }
        set_user_history(
          &mut *tx,
          &self.database_secret,
          now,
          user_ref,
          user_ref,
          &SimplePatch::Set(cmd.display_name.clone()),
          &SimplePatch::Set(cmd.password.clone()),
        )
        .await
        .map_err(PgUpsertSeedUserError::SetUserHistory)?;
        if let Some(username) = &cmd.username {
          insert_username(&mut *tx, now, user_ref, user_ref, username)
            .await
            .map_err(PgUpsertSeedUserError::InsertUsername)?;
        }
        CompleteSimpleUser {
          id: user_ref.id,
          display_name: UserDisplayNameVersions {
            current: UserDisplayNameVersion {
              value: cmd.display_name.clone(),
            },
          },
          is_administrator: cmd.is_administrator,
          created_at: now,
          deleted_at: None,
          username: cmd.username.clone(),
          email_address: None,
          has_password: cmd.password.is_some(),
        }
      }
    };
    tx.commit()
      .await
      .map_err(WeakError::wrap)
      .map_err(PgUpsertSeedUserError::CommitTx)?;

    Ok(res)
  }

  async fn get_user(&self, options: &GetUserOptions) -> Result<RawGetUserResult, RawGetUserError> {
    let time = options.time.unwrap_or_else(|| self.clock.clock().now());

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      period: PeriodLower,
      is_administrator: bool,
      display_name: UserDisplayName,
      username: Option<Username>,
      has_password: bool,
    }

    let mut ref_id: Option<UserId> = None;
    let mut ref_username: Option<Username> = None;
    let mut ref_email: Option<EmailAddress> = None;
    match &options.r#ref {
      UserRef::Id(r) => ref_id = Some(r.id),
      UserRef::Username(r) => ref_username = Some(r.username.clone()),
      UserRef::Email(r) => ref_email = Some(r.email.clone()),
    }
    if ref_email.is_some() {
      return Err(RawGetUserError::NotFound);
    }

    let mut span = self.tracer.build(
      self
        .tracer
        .span_builder("SELECT eternaltwin.users")
        .with_attributes(vec![KeyValue::new(otel_keys::DB_SYSTEM, "postgresql")]),
    );
    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r#"
        WITH current_username AS (
          SELECT user_id, username FROM user_username_history WHERE period @> $1::INSTANT
        )
        SELECT u.user_id, u.period, u.is_administrator, uh.display_name, uh.password IS NOT NULL AS has_password, cu.username
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN current_username AS cu USING (user_id)
        WHERE (user_id = $2::USER_ID OR cu.username = $3::USERNAME)
          AND ($4::BOOLEAN OR u.period @> $1::INSTANT)
          AND (uh.period @> $1::INSTANT OR ($4::BOOLEAN AND uh._is_current));
      "#,
    )
    .bind(time)
    .bind(ref_id)
    .bind(ref_username)
    .bind(!options.skip_deleted)
    .fetch_optional(&*self.database)
    .await.map_err(RawGetUserError::other)?;
    span.end();

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Err(RawGetUserError::NotFound);
    };

    let user: CompleteSimpleUser = CompleteSimpleUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: row.username,
      email_address: None,
      has_password: row.has_password,
    };

    let user = match options.fields {
      UserFields::Complete => RawGetUserResult::Complete(user),
      UserFields::CompleteIfSelf { self_user_id } => {
        if self_user_id == user.id {
          RawGetUserResult::Complete(user)
        } else {
          RawGetUserResult::Default(user.into())
        }
      }
      UserFields::Default => RawGetUserResult::Default(user.into()),
      UserFields::Short => RawGetUserResult::Short(user.into()),
    };

    Ok(user)
  }

  async fn get_user_with_password(
    &self,
    options: RawGetUser,
  ) -> Result<ShortUserWithPassword, RawGetUserWithPasswordError> {
    match get_user_with_password(&*self.database, &self.database_secret, options.clone()).await {
      Ok(u) => Ok(u),
      Err(RawGetUserError::NotFound) => Err(RawGetUserWithPasswordError::NotFound(options.r#ref)),
      Err(RawGetUserError::Other(e)) => Err(RawGetUserWithPasswordError::Other(e)),
    }
  }

  async fn get_short_user(&self, options: &GetShortUserOptions) -> Result<ShortUser, RawGetShortUserError> {
    let time = options.time.unwrap_or_else(|| self.clock.clock().now());

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      period: PeriodLower,
      display_name: UserDisplayName,
    }

    let mut ref_id: Option<UserId> = None;
    let mut ref_username: Option<Username> = None;
    let mut ref_email: Option<EmailAddress> = None;
    match &options.r#ref {
      UserRef::Id(r) => ref_id = Some(r.id),
      UserRef::Username(r) => ref_username = Some(r.username.clone()),
      UserRef::Email(r) => ref_email = Some(r.email.clone()),
    }
    if ref_email.is_some() {
      return Err(RawGetShortUserError::NotFound(options.r#ref.clone()));
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r#"
        WITH current_username AS (
          SELECT user_id, username FROM user_username_history WHERE period @> $1::INSTANT
        )
        SELECT u.user_id, u.period, uh.display_name
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN current_username AS cu USING (user_id)
        WHERE (user_id = $2::USER_ID OR cu.username = $3::USERNAME)
          AND ($4::BOOLEAN OR u.period @> $1::INSTANT)
          AND (uh.period @> $1::INSTANT OR ($4::BOOLEAN AND uh._is_current));
      "#,
    )
    .bind(time)
    .bind(ref_id)
    .bind(ref_username)
    .bind(!options.skip_deleted)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Err(RawGetShortUserError::NotFound(options.r#ref.clone()));
    };

    if row.period.end().is_some() && options.skip_deleted {
      return Err(RawGetShortUserError::Other(WeakError::new(
        "assertion error: expected user to not be deleted",
      )));
    }

    let user = ShortUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
    };

    Ok(user)
  }

  async fn update_user(&self, options: &RawUpdateUserOptions) -> Result<CompleteSimpleUser, RawUpdateUserError> {
    let now = self.clock.clock().now();

    let mut tx = self.database.begin().await.map_err(RawUpdateUserError::other)?;

    if options.patch.display_name.is_set() {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        start_time: Instant,
        // TODO: Skip if new is same as old
        // value: UserDisplayName,
        created_at: Instant,
      }

      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
        SELECT
          lower(cur.period) AS start_time,
          display_name AS value,
          lower(users.period) AS created_at
        FROM
          users_history AS cur
          INNER JOIN users USING (user_id)
        WHERE
          user_id = $1::USER_ID
          AND NOT EXISTS(
            SELECT 1
            FROM users_history AS next
            WHERE
              next.user_id = $1::USER_ID
              AND next.period >> cur.period
              AND NOT (next.display_name = cur.display_name)
          )
        ORDER BY start_time ASC
        LIMIT 1;
      ",
      )
      .bind(options.r#ref.id)
      .fetch_one(&mut *tx)
      .await
      .map_err(RawUpdateUserError::other)?;

      let lock_period = row.start_time..(row.start_time + USER_DISPLAY_NAME_LOCK_DURATION);
      // Do not lock the display name on creation
      if !options.skip_rate_limit && lock_period.start != row.created_at && lock_period.contains(&now) {
        return Err(RawUpdateUserError::LockedDisplayName(
          options.r#ref,
          lock_period.into(),
          now,
        ));
      }
    }

    if let SimplePatch::Set(new_username) = options.patch.username.as_ref() {
      // Note: when you have no username, you can always set the username immediately. Once set, it can't be changed
      // or removed until the lock expires.

      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        period: PeriodLower,
        username: Username,
      }

      // language=PostgreSQL
      let row: Option<Row> = sqlx::query_as::<_, Row>(
        r"
        SELECT period, username
        FROM user_username_history AS uuh
        WHERE
          user_id = $2::USER_ID
          AND period @> $1::INSTANT;
      ",
      )
      .bind(now)
      .bind(options.r#ref.id)
      .fetch_optional(&mut *tx)
      .await
      .map_err(RawUpdateUserError::other)?;

      let changed = match &row {
        None => new_username.is_some(),
        Some(row) => {
          let changed = new_username.as_ref() != Some(&row.username);
          let lock_period = row.period.start()..(row.period.start() + USERNAME_LOCK_DURATION);
          if !options.skip_rate_limit && changed && lock_period.contains(&now) {
            return Err(RawUpdateUserError::LockedUsername(
              options.r#ref,
              lock_period.into(),
              now,
            ));
          }
          changed
        }
      };

      if changed {
        delete_username(&mut *tx, now, options.r#ref, options.actor)
          .await
          .map_err(RawUpdateUserError::other)?;
        if let Some(new_username) = new_username {
          insert_username(&mut *tx, now, options.r#ref, options.actor, new_username)
            .await
            .map_err(RawUpdateUserError::other)?;
        }
      }
    }

    if options.patch.password.is_set() {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        start_time: Instant,
        value: Option<PasswordHash>,
      }

      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
        SELECT lower(period) AS start_time, password AS value FROM users_history AS cur
        WHERE
          user_id = $1::USER_ID
          AND NOT EXISTS(
            SELECT 1
            FROM users_history AS next
            WHERE
              next.user_id = $1::USER_ID
              AND next.period >> cur.period
              AND NOT ((next.password IS NULL AND cur.password IS NULL) OR (next.password IS NOT NULL AND cur.password IS NOT NULL AND next.password = cur.password))
          )
        ORDER BY start_time ASC
        LIMIT 1;
      ",
      )
        .bind(options.r#ref.id)
        .fetch_one(&mut *tx)
        .await
        .map_err(RawUpdateUserError::other)?;

      let lock_period = row.start_time..(row.start_time + USER_PASSWORD_LOCK_DURATION);
      if !options.skip_rate_limit && row.value.is_some() && lock_period.contains(&now) {
        return Err(RawUpdateUserError::LockedPassword(
          options.r#ref,
          lock_period.into(),
          now,
        ));
      }
    }
    set_user_history(
      &mut *tx,
      &self.database_secret,
      now,
      options.r#ref,
      options.actor,
      &options.patch.display_name,
      &options.patch.password,
    )
    .await
    .map_err(RawUpdateUserError::other)?;

    let row = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        user_id: UserId,
        period: PeriodLower,
        is_administrator: bool,
        display_name: UserDisplayName,
        username: Option<Username>,
        has_password: bool,
      }

      // language=PostgreSQL
      sqlx::query_as::<_, Row>(
        r"
      SELECT user_id, period, is_administrator, display_name, username, password IS NOT NULL as has_password
      FROM user_current
      WHERE user_id = $1::USER_ID;
      ",
      )
      .bind(options.r#ref.id)
      .fetch_one(&mut *tx)
      .await
      .map_err(RawUpdateUserError::other)?
    };

    tx.commit().await.map_err(RawUpdateUserError::other)?;

    let user: CompleteSimpleUser = CompleteSimpleUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: row.username,
      email_address: None,
      has_password: row.has_password,
    };

    Ok(user)
  }

  async fn delete_user(&self, cmd: &RawDeleteUser) -> Result<CompleteSimpleUser, RawDeleteUserError> {
    let now = cmd.now;

    let mut tx = self.database.begin().await.map_err(RawDeleteUserError::other)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      period: PeriodLower,
      is_administrator: bool,
      display_name: UserDisplayName,
      has_password: bool,
      username: Option<Username>,
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r#"
        SELECT u.user_id, u.period, u.is_administrator, uh.display_name, uh.password IS NOT NULL AS has_password, uuh.username
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN user_username_history AS uuh USING (user_id)
        WHERE user_id = $2::USER_ID
          AND uh._is_current
          AND upper_inf(uuh.period);
      "#,
    )
    .bind(now)
    .bind(cmd.user.id)
    .fetch_optional(&*self.database)
    .await.map_err(RawDeleteUserError::other)?;

    let row: Row = if let Some(r) = row {
      r
    } else {
      return Err(RawDeleteUserError::NotFound(cmd.user));
    };

    if row.period.end().is_some() {
      return Err(RawDeleteUserError::Gone(cmd.user));
    }

    {
      // language=PostgreSQL
      let res = sqlx::query(
        r"
      UPDATE users AS u
      SET
        period = PERIOD(LOWER(u.period), $1::INSTANT),
        deleted_by = $3::USER_ID
      WHERE u.user_id = $2::USER_ID AND upper_inf(u.period)
      RETURNING user_id;
      ",
      )
      .bind(now)
      .bind(cmd.user.id)
      .bind(cmd.actor.id)
      .execute(&mut *tx)
      .await
      .map_err(RawDeleteUserError::other)?;
      assert!(res.rows_affected() == 1);
    }

    delete_username(&mut *tx, now, cmd.user, cmd.actor)
      .await
      .map_err(RawDeleteUserError::other)?;

    tx.commit().await.map_err(RawDeleteUserError::other)?;

    let user: CompleteSimpleUser = CompleteSimpleUser {
      id: row.user_id,
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: row.display_name,
        },
      },
      is_administrator: row.is_administrator,
      created_at: row.period.start(),
      deleted_at: row.period.end(),
      username: row.username,
      email_address: None,
      has_password: row.has_password,
    };

    Ok(user)
  }

  async fn hard_delete_user(&self, cmd: &RawHardDeleteUser) -> Result<(), RawHardDeleteUserError> {
    // language=PostgreSQL
    let res = sqlx::query(
      r"
        DELETE
        FROM users
        WHERE user_id = $1::USER_ID;
    ",
    )
    .bind(cmd.user.id)
    .execute(&*self.database)
    .await
    .map_err(RawHardDeleteUserError::other)?;

    match res.rows_affected() {
      0 => Err(RawHardDeleteUserError::NotFound(cmd.user)),
      1 => Ok(()),
      _ => panic!("AssertionError: Expected 0 or 1 rows to be affected"),
    }
  }
}

async fn get_user_with_password<'e, 'c: 'e, E>(
  db: E,
  db_secret: &SecretString,
  query: RawGetUser,
) -> Result<ShortUserWithPassword, RawGetUserError>
where
  E: 'e + Executor<'c, Database = Postgres>,
{
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    user_id: UserId,
    period: PeriodLower,
    deleted_by: Option<UserId>,
    is_administrator: bool,
    display_name: UserDisplayName,
    username: Option<Username>,
    password: Option<PasswordHash>,
  }

  let mut ref_id: Option<UserId> = None;
  let mut ref_username: Option<Username> = None;
  let mut ref_email: Option<EmailAddress> = None;
  match &query.r#ref {
    UserRef::Id(r) => ref_id = Some(r.id),
    UserRef::Username(r) => ref_username = Some(r.username.clone()),
    UserRef::Email(r) => ref_email = Some(r.email.clone()),
  }
  if ref_email.is_some() {
    return Err(RawGetUserError::NotFound);
  }

  // language=PostgreSQL
  let row = sqlx::query_as::<_, Row>(
    r#"
        WITH current_username AS (
          SELECT user_id, username FROM user_username_history WHERE period @> $2::INSTANT
        )
        SELECT u.user_id, u.period, u.deleted_by, u.is_administrator, uh.display_name, pgp_sym_decrypt_bytea(uh.password, $1::TEXT) AS password, cu.username
        FROM users AS u
          INNER JOIN users_history AS uh USING (user_id)
          LEFT OUTER JOIN current_username AS cu USING (user_id)
        WHERE (user_id = $3::USER_ID OR cu.username = $4::USERNAME)
          AND ($5::BOOLEAN OR u.period @> $2::INSTANT)
          AND (uh.period @> $2::INSTANT OR ($5::BOOLEAN AND uh._is_current));
      "#,
  )
    .bind(db_secret.as_str())
    .bind(query.time)
    .bind(ref_id)
    .bind(ref_username)
    .bind(!query.skip_deleted)
    .fetch_optional(db)
    .await.map_err(WeakError::wrap)?;

  let row: Row = if let Some(r) = row {
    r
  } else {
    return Err(RawGetUserError::NotFound);
  };

  if row.period.end().is_some() && query.skip_deleted {
    return Err(RawGetUserError::Other(WeakError::new(
      "assertion error: expected user to not be deleted".to_string(),
    )));
  }

  let user = ShortUserWithPassword {
    id: row.user_id,
    created_at: row.period.start(),
    deleted: match (row.period.end(), row.deleted_by) {
      (Some(time), Some(user)) => Some(RawUserDot {
        time,
        user: user.into(),
      }),
      (None, None) => None,
      _ => panic!("data consistency error: expected `upper(period)` and `deleted_by` to have same nullability"),
    },
    is_administrator: row.is_administrator,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: row.display_name,
      },
    },
    username: UsernameVersions {
      current: UsernameVersion { value: row.username },
    },
    password: row.password,
  };

  Ok(user)
}

async fn delete_username<'e, 'c: 'e, E>(db: E, now: Instant, user: UserIdRef, actor: UserIdRef) -> Result<(), WeakError>
where
  E: 'e + Executor<'c, Database = Postgres>,
{
  // language=PostgreSQL
  let res = sqlx::query(
    r"
    UPDATE user_username_history
    SET period = PERIOD(LOWER(period), $1::INSTANT),
      deleted_by = $3::USER_ID
    WHERE user_id = $2::USER_ID
      AND period @> $1::INSTANT;
    ",
  )
  .bind(now)
  .bind(user.id)
  .bind(actor.id)
  .execute(db)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);
  Ok(())
}

async fn insert_username<'e, 'c: 'e, E>(
  db: E,
  now: Instant,
  user: UserIdRef,
  actor: UserIdRef,
  username: &Username,
) -> Result<(), WeakError>
where
  E: 'e + Executor<'c, Database = Postgres>,
{
  // language=PostgreSQL
  let res = sqlx::query(
    r"
    INSERT INTO user_username_history(period, user_id, username, created_by, deleted_by)
    VALUES (PERIOD($1::INSTANT, NULL), $2::USER_ID, $4::USERNAME, $3::USER_ID, NULL);
    ",
  )
  .bind(now)
  .bind(user.id)
  .bind(actor.id)
  .bind(username)
  .execute(db)
  .await
  .map_err(WeakError::wrap)?;
  assert_eq!(res.rows_affected(), 1);
  Ok(())
}

async fn set_user_history<'e, 'c: 'e, E>(
  db: E,
  db_secret: &SecretString,
  now: Instant,
  user: UserIdRef,
  actor: UserIdRef,
  display_name: &SimplePatch<UserDisplayName>,
  password: &SimplePatch<Option<PasswordHash>>,
) -> Result<(), WeakError>
where
  E: 'e + Executor<'c, Database = Postgres>,
{
  let (set_display_name, display_name) = match display_name {
    SimplePatch::Skip => (false, None),
    SimplePatch::Set(new_display_name) => (true, Some(new_display_name)),
  };
  let (set_password, password) = match password {
    SimplePatch::Skip => (false, None),
    SimplePatch::Set(new_password) => (true, new_password.as_ref()),
  };

  #[derive(Debug, sqlx::FromRow)]
  #[allow(unused)]
  struct Row {
    period: PeriodLower,
    user_id: UserId,
    _is_current: Option<bool>,
    display_name: UserDisplayName,
    password: Option<PasswordHash>,
  }

  // language=PostgreSQL
  let rows = sqlx::query_as::<_, Row>(
    r"
      with
        input_row(time, user_id, is_display_name_update, new_display_name, is_password_update, new_password) as (values(
          $1::instant, $2::user_id, $4::BOOLEAN, $5::USER_DISPLAY_NAME, $6::BOOLEAN, pgp_sym_encrypt_bytea($7::PASSWORD_HASH, $8::TEXT)
        )),
        prev_row as (
          select period, i.user_id, display_name, password
          from input_row as i left outer join users_history as uh
            on uh.user_id = i.user_id and uh._is_current
        ),
        new_row as (
          select
            period(i.time, null) as period,
            i.user_id as user_id,
            true as _is_current,
            $3::user_id as updated_by,
            case when i.is_display_name_update then i.new_display_name else pr.display_name end as display_name,
            case when i.is_password_update then i.new_password else pr.password end as password
          from input_row as i left outer join prev_row as pr using (user_id)
        ),
        invalidated_row as (
          update users_history
          set period = period(lower(period), $1::instant), _is_current = null
          where (period, user_id) in (select period, user_id from prev_row)
          returning period, user_id, _is_current, display_name, password
        ),
        inserted_row as (
          insert into users_history(period, user_id, _is_current, updated_by, display_name, password)
          select * from new_row
          returning period, user_id, _is_current, display_name, password
        )
      select * from invalidated_row
      union all
      select * from inserted_row;
      ",
  )
  .bind(now)
  .bind(user.id)
  .bind(actor.id)
  .bind(set_display_name)
  .bind(display_name)
  .bind(set_password)
  .bind(password)
  .bind(db_secret.as_str())
  .fetch_all(db)
  .await
  .map_err( WeakError::wrap)?;
  assert!(rows.len() <= 2);

  Ok(())
}

#[cfg(test)]
mod test {
  use super::PgUserStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use opentelemetry::trace::noop::NoopTracerProvider;
  use opentelemetry::trace::TracerProvider;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn UserStore>> {
    let config = eternaltwin_config::Config::for_test();
    let tracer_provider = NoopTracerProvider::new();
    let tracer = tracer_provider.tracer("user_store_test");

    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let database_secret = SecretString::new("dev_secret".to_string());
    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(
      clock.clone(),
      database,
      database_secret,
      tracer,
      uuid_generator,
    ));

    TestApi { clock, user_store }
  }

  test_user_store!(
    #[serial]
    || make_test_api().await
  );
}
