use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, VirtualClock};
use eternaltwin_core::core::{Duration, Instant};
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::user::{
  CompleteSimpleUser, CreateUserOptions, GetUserOptions, RawGetUserError, RawGetUserResult, RawHardDeleteUser,
  RawUpdateUserError, RawUpdateUserOptions, RawUpdateUserPatch, ShortUser, SimpleUser, UserDisplayNameVersion,
  UserDisplayNameVersions, UserFields, UserIdRef, UserRef, UserStore, UserStoreRef, USER_DISPLAY_NAME_LOCK_DURATION,
};

#[macro_export]
macro_rules! test_user_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_create_admin);
    register_test!($(#[$meta])*, $api, test_register_the_admin_and_retrieve_short);
    register_test!($(#[$meta])*, $api, test_register_the_admin_and_retrieve_default);
    register_test!($(#[$meta])*, $api, test_register_the_admin_and_retrieve_complete);
    register_test!($(#[$meta])*, $api, test_update_display_name_after_creation);
    register_test!($(#[$meta])*, $api, test_update_locked_display_name);
    register_test!($(#[$meta])*, $api, test_update_display_after_unlock);
    register_test!($(#[$meta])*, $api, test_update_display_name_then_username);
    register_test!($(#[$meta])*, $api, test_update_locked_display_name_after_update);
    register_test!($(#[$meta])*, $api, test_update_display_name_afte_multiple_unlocks);
    register_test!($(#[$meta])*, $api, test_hard_delete_user);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyClock, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) user_store: TyUserStore,
}

pub(crate) async fn test_create_admin<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  let expected = CompleteSimpleUser {
    id: actual.id,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Alice".parse().unwrap(),
      },
    },
    is_administrator: true,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    deleted_at: None,
    username: Some("alice".parse().unwrap()),
    email_address: None,
    has_password: false,
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_register_the_admin_and_retrieve_short<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  let actual = api
    .user_store
    .user_store()
    .get_user(&GetUserOptions {
      fields: UserFields::Short,
      r#ref: UserRef::Id(UserIdRef::new(alice.id)),
      time: None,
      skip_deleted: false,
    })
    .await
    .unwrap();
  let expected = RawGetUserResult::Short(ShortUser {
    id: alice.id,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Alice".parse().unwrap(),
      },
    },
  });
  assert_eq!(actual, expected);
}

pub(crate) async fn test_register_the_admin_and_retrieve_default<TyClock, TyUserStore>(
  api: TestApi<TyClock, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  let actual = api
    .user_store
    .user_store()
    .get_user(&GetUserOptions {
      fields: UserFields::Default,
      r#ref: UserRef::Id(UserIdRef::new(alice.id)),
      time: None,
      skip_deleted: false,
    })
    .await
    .unwrap();
  let expected = RawGetUserResult::Default(SimpleUser {
    id: alice.id,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    deleted_at: None,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Alice".parse().unwrap(),
      },
    },
    is_administrator: true,
  });
  assert_eq!(actual, expected);
}

pub(crate) async fn test_register_the_admin_and_retrieve_complete<TyClock, TyUserStore>(
  api: TestApi<TyClock, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  let actual = api
    .user_store
    .user_store()
    .get_user(&GetUserOptions {
      fields: UserFields::Complete,
      r#ref: UserRef::Id(UserIdRef::new(alice.id)),
      time: None,
      skip_deleted: false,
    })
    .await
    .unwrap();
  let expected = RawGetUserResult::Complete(CompleteSimpleUser {
    id: alice.id,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Alice".parse().unwrap(),
      },
    },
    is_administrator: true,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    deleted_at: None,
    username: Some("alice".parse().unwrap()),
    email_address: None,
    has_password: false,
  });
  assert_eq!(actual, expected);
}

pub(crate) async fn test_update_display_name_after_creation<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alicia".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  let expected = CompleteSimpleUser {
    id: alice.id,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Alicia".parse().unwrap(),
      },
    },
    is_administrator: true,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    deleted_at: None,
    username: Some("alice".parse().unwrap()),
    email_address: None,
    has_password: false,
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_update_locked_display_name<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  let start = Instant::ymd_hms(2021, 1, 1, 0, 0, 0);
  api.clock.advance_to(start);
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alicia".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  let lock_start = api.clock.now();

  api.clock.advance_by(USER_DISPLAY_NAME_LOCK_DURATION / 2);

  let actual = api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Allison".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await;

  let expected = Err(RawUpdateUserError::LockedDisplayName(
    alice.id.into(),
    (lock_start..(lock_start + USER_DISPLAY_NAME_LOCK_DURATION)).into(),
    lock_start + USER_DISPLAY_NAME_LOCK_DURATION / 2,
  ));

  assert_eq!(actual, expected)
}

pub(crate) async fn test_update_display_after_unlock<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alicia".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  api.clock.advance_by(USER_DISPLAY_NAME_LOCK_DURATION);

  let actual = api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Allison".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  let expected = CompleteSimpleUser {
    id: alice.id,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Allison".parse().unwrap(),
      },
    },
    is_administrator: true,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    deleted_at: None,
    username: Some("alice".parse().unwrap()),
    email_address: None,
    has_password: false,
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_update_locked_display_name_after_update<TyClock, TyUserStore>(
  api: TestApi<TyClock, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  let start = Instant::ymd_hms(2021, 1, 1, 0, 0, 0);
  api.clock.advance_to(start);
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alicia".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  api.clock.advance_by(USER_DISPLAY_NAME_LOCK_DURATION);

  api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Allison".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  let lock_start = api.clock.now();

  api.clock.advance_by(USER_DISPLAY_NAME_LOCK_DURATION / 2);

  let actual = api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alexandra".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await;

  let expected = Err(RawUpdateUserError::LockedDisplayName(
    alice.id.into(),
    ((lock_start)..(lock_start + USER_DISPLAY_NAME_LOCK_DURATION)).into(),
    lock_start + USER_DISPLAY_NAME_LOCK_DURATION / 2,
  ));

  assert_eq!(actual, expected)
}

pub(crate) async fn test_update_display_name_afte_multiple_unlocks<TyClock, TyUserStore>(
  api: TestApi<TyClock, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alicia".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  api.clock.advance_by(USER_DISPLAY_NAME_LOCK_DURATION);

  api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Allison".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  api.clock.advance_by(USER_DISPLAY_NAME_LOCK_DURATION);

  let actual = api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alexandra".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  let expected = CompleteSimpleUser {
    id: alice.id,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Alexandra".parse().unwrap(),
      },
    },
    is_administrator: true,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    deleted_at: None,
    username: Some("alice".parse().unwrap()),
    email_address: None,
    has_password: false,
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_update_display_name_then_username<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: None,
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Set("Alicia".parse().unwrap()),
        username: SimplePatch::Skip,
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .user_store
    .user_store()
    .update_user(&RawUpdateUserOptions {
      r#ref: alice.id.into(),
      actor: alice.id.into(),
      skip_rate_limit: false,
      patch: RawUpdateUserPatch {
        display_name: SimplePatch::Skip,
        username: SimplePatch::Set(Some("alicia".parse().unwrap())),
        password: SimplePatch::Skip,
      },
    })
    .await
    .unwrap();

  let expected = CompleteSimpleUser {
    id: alice.id,
    display_name: UserDisplayNameVersions {
      current: UserDisplayNameVersion {
        value: "Alicia".parse().unwrap(),
      },
    },
    is_administrator: true,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    deleted_at: None,
    username: Some("alicia".parse().unwrap()),
    email_address: None,
    has_password: false,
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_hard_delete_user<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  {
    let actual = api
      .user_store
      .user_store()
      .hard_delete_user(&RawHardDeleteUser {
        actor: UserIdRef::new(alice.id),
        user: UserIdRef::new(alice.id),
      })
      .await;
    assert!(actual.is_ok());
  }

  let actual = api
    .user_store
    .user_store()
    .get_user(&GetUserOptions {
      fields: UserFields::Short,
      r#ref: UserRef::Id(UserIdRef::new(alice.id)),
      time: None,
      skip_deleted: false,
    })
    .await;
  let expected = Err(RawGetUserError::NotFound);
  assert_eq!(actual, expected);
}
