use crate::extract::Extractor;
use crate::EternaltwinSystem;
use async_trait::async_trait;
use axum::extract::FromRequest;
use axum::http::{header, HeaderMap, HeaderName, HeaderValue, StatusCode};
use axum::response::{IntoResponse, Response};
use axum::routing::post;
use axum::{Extension, Router};
use compact_str::{CompactString, ToCompactString};
use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use opentelemetry::trace::{Event, Link, SpanContext, SpanId, SpanKind, Status, TraceFlags, TraceId, TraceState};
use opentelemetry::{Array, InstrumentationLibrary, InstrumentationLibraryBuilder, Key, KeyValue, StringValue, Value};
use opentelemetry_proto::tonic::collector::trace::v1::ExportTraceServiceResponse;
use opentelemetry_proto::tonic::trace::v1::{ResourceSpans, ScopeSpans, SpanFlags};
use opentelemetry_sdk::export::trace::SpanData;
use opentelemetry_sdk::trace::{SpanLinks, SpanProcessor};
use opentelemetry_sdk::Resource;
use std::borrow::Cow;
use std::str::FromStr;
use std::time::{Duration, SystemTime};

pub fn router() -> Router<()> {
  Router::new().route("/", post(create_traces))
}

#[derive(Debug, thiserror::Error)]
enum CreateTracesError {
  #[error("forbidden: {0}")]
  Forbidden(&'static str),
  #[error("failed to read provided input data, maybe due to invalid or unsupported format")]
  Read(#[from] SpanDataListFromProtobufResourceSpansError),
  #[error("invalid span, all span resources must have attributes service.name={0:?}, deployment.environment={1:?}")]
  InvalidSpan(String, String),
  #[error("internal error")]
  Internal(#[from] WeakError),
}

impl IntoResponse for CreateTracesError {
  fn into_response(self) -> Response {
    let (http_status, pb_status) = match &self {
      Self::Forbidden(..) => (StatusCode::FORBIDDEN, tonic::Code::PermissionDenied),
      Self::InvalidSpan(..) | Self::Read(..) => (StatusCode::UNPROCESSABLE_ENTITY, tonic::Code::InvalidArgument),
      Self::Internal(..) => (StatusCode::INTERNAL_SERVER_ERROR, tonic::Code::Internal),
    };
    let grpc_status = tonic::Status::new(pb_status, DisplayErrorChain(&self).to_string());

    let mut headers = HeaderMap::new();
    headers.insert(
      header::CONTENT_TYPE,
      header::HeaderValue::from_static("application/grpc"),
    );
    // tonic uses an old `http` version, so we need to handle compat here to emit the proper headers for the status
    let mut legacy_headers = http02::HeaderMap::new();
    grpc_status
      .add_header(&mut legacy_headers)
      .expect("adding grpc headers always succeeds");
    for (key, value) in legacy_headers.iter() {
      headers.insert(
        HeaderName::from_bytes(key.as_str().as_bytes()).expect("converting to a header name succeeds"),
        HeaderValue::from_bytes(value.as_bytes()).expect("converting to a header value succeeds"),
      );
    }
    (http_status, headers).into_response()
  }
}

pub struct ProstError(WeakError);

impl IntoResponse for ProstError {
  fn into_response(self) -> Response {
    let http_status = StatusCode::UNPROCESSABLE_ENTITY;
    let pb_status = tonic::Code::InvalidArgument;
    let grpc_status = tonic::Status::new(pb_status, DisplayErrorChain(&self.0).to_string());
    let mut headers = HeaderMap::new();
    headers.insert(
      header::CONTENT_TYPE,
      header::HeaderValue::from_static("application/grpc"),
    );
    // tonic uses an old `http` version, so we need to handle compat here to emit the proper headers for the status
    let mut legacy_headers = http02::HeaderMap::new();
    grpc_status
      .add_header(&mut legacy_headers)
      .expect("adding grpc headers always succeeds");
    for (key, value) in legacy_headers.iter() {
      headers.insert(
        HeaderName::from_bytes(key.as_str().as_bytes()).expect("converting to a header name succeeds"),
        HeaderValue::from_bytes(value.as_bytes()).expect("converting to a header value succeeds"),
      );
    }
    (http_status, headers).into_response()
  }
}

#[derive(Debug, Clone, Copy, Default)]
#[must_use]
pub struct Prost013<T>(pub T);

#[async_trait]
impl<T, S> FromRequest<S> for Prost013<T>
where
  T: prost::Message + Default,
  S: Send + Sync,
{
  type Rejection = ProstError;

  async fn from_request(req: axum::extract::Request, state: &S) -> Result<Self, Self::Rejection> {
    let mut bytes = bytes::Bytes::from_request(req, state)
      .await
      .map_err(|e| ProstError(WeakError::wrap(e)))?;

    match T::decode(&mut bytes) {
      Ok(value) => Ok(Prost013(value)),
      Err(err) => Err(ProstError(WeakError::wrap(err))),
    }
  }
}

impl<T> IntoResponse for Prost013<T>
where
  T: prost::Message + Default,
{
  fn into_response(self) -> Response {
    let mut buf = bytes::BytesMut::with_capacity(128);
    match &self.0.encode(&mut buf) {
      Ok(()) => buf.into_response(),
      Err(err) => (StatusCode::INTERNAL_SERVER_ERROR, err.to_string()).into_response(),
    }
  }
}

// todo: assert media type is `"application/x-protobuf"` before parsing
// todo: support JSON format
async fn create_traces(
  Extension(api): Extension<EternaltwinSystem>,
  acx: Extractor<AuthContext>,
  Prost013(req): Prost013<opentelemetry_proto::tonic::collector::trace::v1::ExportTraceServiceRequest>,
) -> Result<Prost013<ExportTraceServiceResponse>, CreateTracesError> {
  let acx = match acx.value() {
    AuthContext::OauthClient(acx) => acx,
    _ => return Err(CreateTracesError::Forbidden("must provide OAuth client authentication")),
  };
  let client_key = match acx.client.key.as_ref() {
    Some(key) => key,
    None => {
      return Err(CreateTracesError::Forbidden(
        "oauth client must have a register human-readable key such as `game_channel@clients`",
      ))
    }
  };
  let (app, channel) = client_key.parts();
  let channel = channel.unwrap_or("production");

  for resource_spans in req.resource_spans.into_iter() {
    let (resource, mut span_list) = span_data_list_from_protobuf_resource_spans(resource_spans)?;
    if !check_resource_permission(app, channel, &resource) {
      return Err(CreateTracesError::InvalidSpan(app.to_string(), channel.to_string()));
    }
    for span in &mut span_list {
      // Force `SAMPLED` flag to be present
      // Some exporters (e.g. OTLPTraceExporter for Node) are not propagating this flag properly.
      span.span_context = SpanContext::new(
        span.span_context.trace_id(),
        span.span_context.span_id(),
        span.span_context.trace_flags() | TraceFlags::SAMPLED,
        span.span_context.is_remote(),
        span.span_context.trace_state().clone(),
      );
    }
    for processor in api.opentelemetry_backend.span_processors() {
      processor.shared_set_resource(&resource);
      for span in &span_list {
        processor.on_end(span.clone());
      }
    }
  }

  // See <https://opentelemetry.io/docs/specs/otlp/#full-success-1>
  let res = ExportTraceServiceResponse { partial_success: None };
  Ok(Prost013(res))
}

fn check_resource_permission(app: &str, channel: &str, resource: &Resource) -> bool {
  let Some(service_name) = get_resource_str(resource, Key::from_static_str("service.name")) else {
    return false;
  };
  let Some(deployment_environement) = get_resource_str(resource, Key::from_static_str("deployment.environment")) else {
    return false;
  };
  service_name.as_str() == app && deployment_environement.as_str() == channel
}

fn get_resource_str(resource: &Resource, key: Key) -> Option<StringValue> {
  match resource.get(key) {
    Some(Value::String(s)) => Some(s),
    _ => None,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum SpanDataListFromProtobufResourceSpansError {
  #[error("missing `resource`")]
  MissingResource,
  #[error("invalid `resource.attributes`")]
  InvalidResourceAttributes(#[source] KeyValueListFromProtobufError),
  #[error("invalid `scope_spans` item at index {1}")]
  InvalidScopeSpan(#[source] ScopeSpanFromProtobufScopeSpansError, usize),
}

fn span_data_list_from_protobuf_resource_spans(
  resource_spans: ResourceSpans,
) -> Result<(Resource, Vec<SpanData>), SpanDataListFromProtobufResourceSpansError> {
  let mut result: Vec<SpanData> = Vec::new();

  let resource: Resource = {
    let pb_resource = resource_spans
      .resource
      .ok_or(SpanDataListFromProtobufResourceSpansError::MissingResource)?;
    let attrs = kv_list_from_pb(pb_resource.attributes)
      .map_err(SpanDataListFromProtobufResourceSpansError::InvalidResourceAttributes)?;
    if resource_spans.schema_url.is_empty() {
      Resource::new(attrs)
    } else {
      Resource::from_schema_url(attrs, resource_spans.schema_url)
    }
  };
  for (scope_span_i, scope_spans) in resource_spans.scope_spans.into_iter().enumerate() {
    let span_data = span_data_list_from_protobuf_scope_spans(scope_spans)
      .map_err(|e| SpanDataListFromProtobufResourceSpansError::InvalidScopeSpan(e, scope_span_i))?;
    result.extend(span_data.into_iter())
  }

  Ok((resource, result))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum ScopeSpanFromProtobufScopeSpansError {
  #[error("missing `scope`")]
  MissingScope,
  #[error("invalid `scope.attributes`")]
  InvalidScopeAttributes(#[source] KeyValueListFromProtobufError),
  #[error("invalid `spans` item at index {1}")]
  InvalidSpan(#[source] SpanDataFromProtobufError, usize),
}

fn span_data_list_from_protobuf_scope_spans(
  scope_spans: ScopeSpans,
) -> Result<Vec<SpanData>, ScopeSpanFromProtobufScopeSpansError> {
  let mut result: Vec<SpanData> = Vec::new();
  let instrumentation_lib: InstrumentationLibrary = {
    let pb_instrumentation_scope = scope_spans
      .scope
      .ok_or(ScopeSpanFromProtobufScopeSpansError::MissingScope)?;

    let name: String = pb_instrumentation_scope.name;
    let version: Option<String> = if pb_instrumentation_scope.version.is_empty() {
      None
    } else {
      Some(pb_instrumentation_scope.version)
    };
    let schema_url: Option<String> = if scope_spans.schema_url.is_empty() {
      None
    } else {
      Some(scope_spans.schema_url)
    };
    let attrs = kv_list_from_pb(pb_instrumentation_scope.attributes)
      .map_err(ScopeSpanFromProtobufScopeSpansError::InvalidScopeAttributes)?;
    let attributes = if attrs.is_empty() { None } else { Some(attrs) };
    let builder: InstrumentationLibraryBuilder = InstrumentationLibrary::builder(name);
    let builder = match version {
      Some(v) => builder.with_version(v),
      None => builder,
    };
    let builder = match schema_url {
      Some(su) => builder.with_schema_url(su),
      None => builder,
    };
    let builder = match attributes {
      Some(a) => builder.with_attributes(a),
      None => builder,
    };
    builder.build()
  };

  for (span_i, span) in scope_spans.spans.into_iter().enumerate() {
    let span = span_from_protobuf(instrumentation_lib.clone(), span)
      .map_err(|e| ScopeSpanFromProtobufScopeSpansError::InvalidSpan(e, span_i))?;
    result.push(span);
  }
  Ok(result)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum KeyValueListFromProtobufError {
  #[error("failed to read protobuf key-value list due to missing value at index {0}")]
  MissingValue(usize),
  #[error("failed to read protobuf key-value list due to invalid value at index {1}")]
  InvalidValue(#[source] ValueFromProtobufError, usize),
}

fn kv_list_from_pb(
  pb_kv_list: Vec<opentelemetry_proto::tonic::common::v1::KeyValue>,
) -> Result<Vec<KeyValue>, KeyValueListFromProtobufError> {
  let mut kv_list: Vec<KeyValue> = Vec::with_capacity(pb_kv_list.len());
  for (i, pb_kv) in pb_kv_list.into_iter().enumerate() {
    let key = pb_kv.key;
    let value = pb_kv.value.ok_or(KeyValueListFromProtobufError::MissingValue(i))?;
    let value = value_from_pb(value).map_err(|e| KeyValueListFromProtobufError::InvalidValue(e, i))?;
    kv_list.push(KeyValue::new(key, value));
  }
  Ok(kv_list)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("`SpanId` must be a byte array of size exactly 0 or 8, input has size {0}")]
pub struct SpanIdFromSliceError(usize);

fn span_id_from_slice(span_id: &[u8]) -> Result<SpanId, SpanIdFromSliceError> {
  Ok(if span_id.is_empty() {
    SpanId::INVALID
  } else {
    SpanId::from_bytes(<[u8; 8]>::try_from(span_id).map_err(|_| SpanIdFromSliceError(span_id.len()))?)
  })
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("`TraceId` must be a byte array of size exactly 0 or 16, input has size {0}")]
pub struct TraceIdFromSliceError(usize);

fn trace_id_from_slice(trace_id: &[u8]) -> Result<TraceId, TraceIdFromSliceError> {
  Ok(if trace_id.is_empty() {
    TraceId::INVALID
  } else {
    TraceId::from_bytes(<[u8; 16]>::try_from(trace_id).map_err(|_| TraceIdFromSliceError(trace_id.len()))?)
  })
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("received unspecified `SpanKind` from protobuf")]
struct UnspecifiedSpanKind;

fn span_kind_from_pb(
  pb_span_kind: opentelemetry_proto::tonic::trace::v1::span::SpanKind,
) -> Result<SpanKind, UnspecifiedSpanKind> {
  use opentelemetry_proto::tonic::trace::v1::span::SpanKind as PbSpanKind;
  match pb_span_kind {
    PbSpanKind::Unspecified => Err(UnspecifiedSpanKind),
    PbSpanKind::Internal => Ok(SpanKind::Internal),
    PbSpanKind::Server => Ok(SpanKind::Server),
    PbSpanKind::Client => Ok(SpanKind::Client),
    PbSpanKind::Producer => Ok(SpanKind::Producer),
    PbSpanKind::Consumer => Ok(SpanKind::Consumer),
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("failed to read protobuf value")]
pub enum ValueFromProtobufError {
  #[error("missing internal `value` field")]
  Empty,
  #[error("invalid array value")]
  Array(#[from] ArrayFromProtobufError),
  #[error("invalid composite value")]
  Composite,
}

fn value_from_pb(pb_value: opentelemetry_proto::tonic::common::v1::AnyValue) -> Result<Value, ValueFromProtobufError> {
  let pb_value = match pb_value.value {
    Some(pbv) => pbv,
    None => return Err(ValueFromProtobufError::Empty),
  };

  use opentelemetry_proto::tonic::common::v1::any_value::Value as PbValue;
  match pb_value {
    PbValue::StringValue(v) => Ok(Value::String(StringValue::from(v))),
    PbValue::BoolValue(v) => Ok(Value::Bool(v)),
    PbValue::IntValue(v) => Ok(Value::I64(v)),
    PbValue::DoubleValue(v) => Ok(Value::F64(v)),
    PbValue::ArrayValue(v) => Ok(Value::Array(array_from_pb(v)?)),
    PbValue::KvlistValue(..) | PbValue::BytesValue(..) => Err(ValueFromProtobufError::Composite),
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum ArrayFromProtobufError {
  #[error("failed to read array value")]
  ArrayValue(#[from] PrimitiveValueFromProtobufError),
  #[error("invalid array due to mixed primitive value types")]
  MixedArray,
}

/// Decode a protobuf array
///
/// If the array is empty, it defaults to be typed as `Array::String`
fn array_from_pb(
  pb_array: opentelemetry_proto::tonic::common::v1::ArrayValue,
) -> Result<Array, ArrayFromProtobufError> {
  let size = pb_array.values.len();
  let mut items = pb_array.values.into_iter();
  match items.next() {
    None => {
      // default to a string array if the array is empty
      Ok(Array::String(Vec::new()))
    }
    Some(first) => {
      let first = primitive_value_from_pb(first)?;
      match first {
        PrimitiveValue::String(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::String(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::String(result))
        }
        PrimitiveValue::Bool(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::Bool(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::Bool(result))
        }
        PrimitiveValue::I64(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::I64(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::I64(result))
        }
        PrimitiveValue::F64(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::F64(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::F64(result))
        }
      }
    }
  }
}

pub enum PrimitiveValue {
  String(StringValue),
  Bool(bool),
  I64(i64),
  F64(f64),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("non-primitive protobuf value forbidden inside array")]
pub struct PrimitiveValueFromProtobufError;

fn primitive_value_from_pb(
  pb_value: opentelemetry_proto::tonic::common::v1::AnyValue,
) -> Result<PrimitiveValue, PrimitiveValueFromProtobufError> {
  let pb_value = match pb_value.value {
    Some(pbv) => pbv,
    None => return Err(PrimitiveValueFromProtobufError),
  };

  use opentelemetry_proto::tonic::common::v1::any_value::Value as PbValue;
  match pb_value {
    PbValue::StringValue(v) => Ok(PrimitiveValue::String(StringValue::from(v))),
    PbValue::BoolValue(v) => Ok(PrimitiveValue::Bool(v)),
    PbValue::IntValue(v) => Ok(PrimitiveValue::I64(v)),
    PbValue::DoubleValue(v) => Ok(PrimitiveValue::F64(v)),
    PbValue::ArrayValue(..) | PbValue::KvlistValue(..) | PbValue::BytesValue(..) => {
      Err(PrimitiveValueFromProtobufError)
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum SpanDataFromProtobufError {
  #[error("invalid `trace_id`")]
  TraceId(#[source] TraceIdFromSliceError),
  #[error("invalid `span_id`")]
  SpanId(#[source] SpanIdFromSliceError),
  #[error("invalid `flags`, raw={0}")]
  Flags(u32),
  #[error("invalid `trace_state`")]
  TraceState(#[source] WeakError),
  #[error("invalid `parent_span_id`")]
  ParentSpanId(#[source] SpanIdFromSliceError),
  #[error("invalid `kind`")]
  Kind(#[source] UnspecifiedSpanKind),
  #[error("invalid `start_time_unix_nano`")]
  StartTimeUnixNano(#[source] SystemTimeFromUnixNanoError),
  #[error("invalid `end_time_unix_nano`")]
  EndTimeUnixNano(#[source] SystemTimeFromUnixNanoError),
  #[error("invalid `attributes`")]
  Attributes(#[source] KeyValueListFromProtobufError),
  #[error("invalid `events` item at index {1}")]
  Event(#[source] EventFromProtobufError, usize),
  #[error("invalid `links` item at index {1}")]
  Link(#[source] SpanLinkFromProtobufError, usize),
  #[error("missing `status`")]
  Status,
}

fn span_from_protobuf(
  instrumentation_library: InstrumentationLibrary,
  pb_span: opentelemetry_proto::tonic::trace::v1::Span,
) -> Result<SpanData, SpanDataFromProtobufError> {
  Ok(SpanData {
    span_context: SpanContext::new(
      trace_id_from_slice(&pb_span.trace_id).map_err(SpanDataFromProtobufError::TraceId)?,
      span_id_from_slice(&pb_span.span_id).map_err(SpanDataFromProtobufError::SpanId)?,
      {
        let trace_flags_mask = u32::try_from(SpanFlags::TraceFlagsMask as i32)
          .expect("`SpanFlags::TraceFlagsMask` always converts to `u32`");

        let raw_trace_flags = pb_span.flags & trace_flags_mask;
        TraceFlags::new(u8::try_from(raw_trace_flags).map_err(|_| SpanDataFromProtobufError::Flags(raw_trace_flags))?)
      },
      {
        let context_has_is_remote_mask = u32::try_from(SpanFlags::ContextHasIsRemoteMask as i32)
          .expect("`SpanFlags::ContextHasIsRemoteMask` always converts to `u32`");
        let is_remote = if (pb_span.flags & context_has_is_remote_mask) != 0 {
          Some({
            let context_is_remote_mask = u32::try_from(SpanFlags::ContextIsRemoteMask as i32)
              .expect("`SpanFlags::ContextHasIsRemoteMask` always converts to `u32`");
            (pb_span.flags & context_is_remote_mask) != 0
          })
        } else {
          None
        };
        is_remote.unwrap_or(true)
      },
      TraceState::from_str(&pb_span.trace_state)
        .map_err(WeakError::wrap)
        .map_err(SpanDataFromProtobufError::TraceState)?,
    ),
    parent_span_id: span_id_from_slice(&pb_span.parent_span_id).map_err(SpanDataFromProtobufError::ParentSpanId)?,
    span_kind: span_kind_from_pb(pb_span.kind()).map_err(SpanDataFromProtobufError::Kind)?,
    name: Cow::Owned(pb_span.name),
    start_time: system_time_from_nanos(pb_span.start_time_unix_nano)
      .map_err(SpanDataFromProtobufError::StartTimeUnixNano)?,
    end_time: system_time_from_nanos(pb_span.end_time_unix_nano).map_err(SpanDataFromProtobufError::EndTimeUnixNano)?,
    attributes: kv_list_from_pb(pb_span.attributes).map_err(SpanDataFromProtobufError::Attributes)?,
    dropped_attributes_count: pb_span.dropped_attributes_count,
    events: {
      let mut events = opentelemetry_sdk::trace::SpanEvents::default();
      events.dropped_count = pb_span.dropped_events_count;
      for (event_i, pb_event) in pb_span.events.into_iter().enumerate() {
        events
          .events
          .push(event_from_protobuf(pb_event).map_err(|e| SpanDataFromProtobufError::Event(e, event_i))?);
      }
      events
    },
    links: {
      let mut links = SpanLinks::default();
      links.dropped_count = pb_span.dropped_links_count;
      for (link_i, pb_link) in pb_span.links.into_iter().enumerate() {
        links
          .links
          .push(span_link_from_protobuf(pb_link).map_err(|e| SpanDataFromProtobufError::Link(e, link_i))?);
      }
      links
    },
    status: {
      use opentelemetry_proto::tonic::trace::v1::status::StatusCode as PbStatusCode;
      let pb_status = pb_span.status.ok_or(SpanDataFromProtobufError::Status)?;
      match pb_status.code() {
        PbStatusCode::Unset => Status::Unset,
        PbStatusCode::Ok => Status::Ok,
        PbStatusCode::Error => Status::Error {
          description: Cow::Owned(pb_status.message),
        },
      }
    },
    instrumentation_lib: instrumentation_library,
  })
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum EventFromProtobufError {
  #[error("invalid `time_unix_nano`")]
  TimeUnixNano(#[source] SystemTimeFromUnixNanoError),
  #[error("invalid `attributes`")]
  Attributes(#[source] KeyValueListFromProtobufError),
}

fn event_from_protobuf(
  pb_event: opentelemetry_proto::tonic::trace::v1::span::Event,
) -> Result<Event, EventFromProtobufError> {
  Ok(Event::new(
    Cow::Owned(pb_event.name),
    system_time_from_nanos(pb_event.time_unix_nano).map_err(EventFromProtobufError::TimeUnixNano)?,
    kv_list_from_pb(pb_event.attributes).map_err(EventFromProtobufError::Attributes)?,
    pb_event.dropped_attributes_count,
  ))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum SpanLinkFromProtobufError {
  #[error("invalid `trace_id`")]
  TraceId(#[source] TraceIdFromSliceError),
  #[error("invalid `span_id`")]
  SpanId(#[source] SpanIdFromSliceError),
  #[error("invalid `flags`")]
  Flags,
  #[error("invalid `trace_state`: {0}")]
  TraceState(CompactString),
  #[error("invalid `attributes`")]
  Attributes(#[source] KeyValueListFromProtobufError),
}

fn span_link_from_protobuf(
  pb_link: opentelemetry_proto::tonic::trace::v1::span::Link,
) -> Result<Link, SpanLinkFromProtobufError> {
  let link = Link::new(
    SpanContext::new(
      trace_id_from_slice(&pb_link.trace_id).map_err(SpanLinkFromProtobufError::TraceId)?,
      span_id_from_slice(&pb_link.span_id).map_err(SpanLinkFromProtobufError::SpanId)?,
      TraceFlags::new(u8::try_from(pb_link.flags).map_err(|_| SpanLinkFromProtobufError::Flags)?),
      true, // is_remote
      TraceState::from_str(&pb_link.trace_state).map_err(|e| {
        SpanLinkFromProtobufError::TraceState(CompactString::new(DisplayErrorChain(&e).to_compact_string()))
      })?,
    ),
    kv_list_from_pb(pb_link.attributes).map_err(SpanLinkFromProtobufError::Attributes)?,
    pb_link.dropped_attributes_count,
  );
  Ok(link)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("overflow when computing system time from UNIX nanosecond timestamp")]
pub struct SystemTimeFromUnixNanoError;

fn system_time_from_nanos(nanos: u64) -> Result<SystemTime, SystemTimeFromUnixNanoError> {
  SystemTime::UNIX_EPOCH
    .checked_add(Duration::from_nanos(nanos))
    .ok_or(SystemTimeFromUnixNanoError)
}
