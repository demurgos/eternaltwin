use axum::http::Method;
use std::net::{IpAddr, Ipv6Addr};

/// Implements the normalization of the HTTP method
///
/// See notes for `http.request.method` at <https://opentelemetry.io/docs/specs/semconv/http/http-spans/#common-attributes>
#[inline]
pub(super) fn normalize_http_method(method: &Method) -> (&'static str, Option<&str>) {
  match method {
    &Method::GET => ("GET", None),
    &Method::HEAD => ("HEAD", None),
    &Method::POST => ("POST", None),
    &Method::PUT => ("PUT", None),
    &Method::DELETE => ("DELETE", None),
    &Method::CONNECT => ("CONNECT", None),
    &Method::OPTIONS => ("OPTIONS", None),
    &Method::TRACE => ("TRACE", None),
    &Method::PATCH => ("PATCH", None),
    other => ("_OTHER", Some(other.as_str())),
  }
}

/// Normalize to ipv6 by embedding ipv4 into ipv6
///
/// See IPv4-Mapped IPv6 Address at <https://www.rfc-editor.org/rfc/rfc4291#section-2.5.5.2>
pub(super) fn normalize_ip_addr(ip_addr: IpAddr) -> Ipv6Addr {
  match ip_addr {
    IpAddr::V6(ip) => ip,
    IpAddr::V4(ip) => ip.to_ipv6_mapped(),
  }
}
