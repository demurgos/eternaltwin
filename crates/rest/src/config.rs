use crate::EternaltwinSystem;
use axum::routing::get;
use axum::{Extension, Json, Router};
use eternaltwin_services::forum::ForumConfig;
use serde::{Deserialize, Serialize};

pub fn router() -> Router<()> {
  Router::new().route("/", get(get_config))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Config {
  forum: ForumConfig,
}

async fn get_config(Extension(api): Extension<EternaltwinSystem>) -> Json<Config> {
  Json(Config {
    forum: api.forum.config(),
  })
}
