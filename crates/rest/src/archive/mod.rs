mod dinoparc;
mod hammerfest;
mod twinoid;

use axum::Router;

pub fn router() -> Router<()> {
  Router::new()
    .nest("/dinoparc", dinoparc::router())
    .nest("/hammerfest", hammerfest::router())
    .nest("/twinoid", twinoid::router())
}
