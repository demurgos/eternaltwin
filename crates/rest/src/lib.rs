mod app;
mod archive;
pub mod auth;
mod bff;
mod clock;
mod config;
mod extract;
mod forum;
mod job;
mod oauth_clients;
mod outbound_email;
#[cfg(test)]
pub(crate) mod test;
mod traces;
mod users;
pub(crate) mod utils;

pub use crate::extract::InternalAuthKey;
use crate::utils::trace::TraceLayer;
use axum::extract::Extension;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::get;
use axum::{Json, Router};
use eternaltwin_serde_tools::Deserialize;
use eternaltwin_services::error::AnyErrorInfo;
use eternaltwin_system::EternaltwinSystem;
pub use serde::Serialize;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ErrorResponse {
  pub http_code: StatusCode,
  pub error: AnyErrorInfo,
}

impl IntoResponse for ErrorResponse {
  fn into_response(self) -> Response {
    (self.http_code, Json(self.error)).into_response()
  }
}

pub fn router() -> Router<()> {
  Router::new()
    .route("/", get(get_api_home))
    .nest("/app", app::router())
    .nest("/archive", archive::router())
    .nest("/auth", auth::router())
    .nest("/clock", clock::router())
    .nest("/config", config::router())
    .nest("/forum", forum::router())
    .nest("/job", job::router())
    .nest("/oauth_clients", oauth_clients::router())
    .nest("/outbound_email", outbound_email::router())
    .nest("/users", users::router())
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct ApiHome {
  pub eternaltwin: EternaltwinMeta,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct EternaltwinMeta {
  pub version: String,
}

async fn get_api_home() -> Json<ApiHome> {
  Json(ApiHome {
    eternaltwin: EternaltwinMeta {
      version: String::from(env!("CARGO_PKG_VERSION")),
    },
  })
}

pub fn app(system: EternaltwinSystem) -> Router<()> {
  let tracer_provider = system.tracer_provider.clone();
  Router::new()
    .nest("/api/v1", router())
    .nest("/oauth", bff::oauth::router())
    .nest("/actions", bff::actions::router())
    .nest("/v1/traces", traces::router())
    .layer(Extension(system.dev.clone()))
    .layer(Extension(system))
    .layer(TraceLayer::new(tracer_provider))
}
