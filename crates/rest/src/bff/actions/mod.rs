use axum::routing::post;
use axum::Router;

pub mod link;
pub mod login;
pub mod register;

pub fn router() -> Router<()> {
  Router::new()
    .route("/register", post(register::register))
    .nest("/link", link::router())
    .nest("/login", login::router())
}
