use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::VirtualClock;
use eternaltwin_core::core::{Duration, Instant, Listing, ListingCount, LocaleId};
use eternaltwin_core::forum::{
  ForumPostRevisionContent, ForumRole, ForumSection, ForumSectionKey, ForumSectionSelf, ForumStore, ForumStoreRef,
  GetForumSectionMetaOptions, RawAddModeratorOptions, RawCreateForumPostResult, RawCreateForumPostRevisionResult,
  RawCreateForumThreadResult, RawCreatePostOptions, RawCreatePostRevisionOptions, RawCreateThreadOptions,
  RawDeleteModeratorOptions, RawForumActor, RawForumPost, RawForumPostRevision, RawForumRoleGrant, RawForumSectionMeta,
  RawForumThreadMeta, RawGetForumPostOptions, RawGetForumThreadMetaOptions, RawGetPostsOptions, RawGetSectionsOptions,
  RawGetThreadsOptions, RawLatestForumPostRevisionListing, RawShortForumPost, RawUserForumActor,
  UpsertSystemSectionOptions,
};
use eternaltwin_core::user::{CreateUserOptions, UserStore, UserStoreRef};
use std::str::FromStr;

#[macro_export]
macro_rules! test_forum_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_upsert_system_section);
    register_test!($(#[$meta])*, $api, test_upsert_system_section_is_idempotent);
    register_test!($(#[$meta])*, $api, test_get_all_sections_empty);
    register_test!($(#[$meta])*, $api, test_get_all_sections_with_one_section);
    register_test!($(#[$meta])*, $api, test_create_thread);
    register_test!($(#[$meta])*, $api, test_create_thread_in_right_section);
    register_test!($(#[$meta])*, $api, test_create_and_retrieve_thread);
    register_test!($(#[$meta])*, $api, test_create_thread_and_retrieve_first_post);
    register_test!($(#[$meta])*, $api, test_create_thread_and_post_10_replies);
    register_test!($(#[$meta])*, $api, test_create_a_few_threads_with_messages_in_the_main_forum_section);
    register_test!($(#[$meta])*, $api, test_add_moderator);
    register_test!($(#[$meta])*, $api, test_add_moderator_is_idempotent);
    register_test!($(#[$meta])*, $api, test_add_multiple_moderators);
    register_test!($(#[$meta])*, $api, test_delete_moderator);
    register_test!($(#[$meta])*, $api, test_delete_moderator_is_idempotent);
    register_test!($(#[$meta])*, $api, test_delete_post);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyClock, TyForumStore, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) forum_store: TyForumStore,
  pub(crate) user_store: TyUserStore,
}

pub(crate) async fn test_upsert_system_section<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  let expected = ForumSection {
    id: actual.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: Listing {
      offset: 0,
      limit: 20,
      count: 0,
      items: vec![],
    },
    role_grants: vec![],
    this: ForumSectionSelf { roles: vec![] },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_upsert_system_section_is_idempotent<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let first = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  let expected = ForumSection {
    id: first.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: Listing {
      offset: 0,
      limit: 20,
      count: 0,
      items: vec![],
    },
    role_grants: vec![],
    this: ForumSectionSelf { roles: vec![] },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_get_all_sections_empty<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  let actual = api
    .forum_store
    .forum_store()
    .get_sections(&RawGetSectionsOptions { offset: 0, limit: 20 })
    .await
    .unwrap();
  let expected = Listing {
    offset: 0,
    limit: 20,
    count: 0,
    items: vec![],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_get_all_sections_with_one_section<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_sections(&RawGetSectionsOptions { offset: 0, limit: 20 })
    .await
    .unwrap();
  let expected = Listing {
    offset: 0,
    limit: 20,
    count: 1,
    items: vec![RawForumSectionMeta {
      id: section.id,
      key: Some("fr_main".parse().unwrap()),
      display_name: "Forum Général".parse().unwrap(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
      locale: Some(LocaleId::FrFr),
      threads: ListingCount { count: 0 },
      role_grants: vec![],
    }],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_thread<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Hello".parse().unwrap(),
      body_mkt: "**First** discussion thread".to_string(),
      body_html: "<strong>First</strong> discussion thread".to_string(),
    })
    .await
    .unwrap();
  let expected = RawCreateForumThreadResult {
    id: actual.id,
    key: None,
    title: "Hello".parse().unwrap(),
    section: section.as_ref(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
    is_pinned: false,
    is_locked: false,
    post_id: actual.post_id,
    post_revision: RawForumPostRevision {
      id: actual.post_revision.id,
      time: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
      author: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      content: Some(ForumPostRevisionContent {
        marktwin: "**First** discussion thread".to_string(),
        html: "<strong>First</strong> discussion thread".to_string(),
      }),
      moderation: None,
      comment: None,
    },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_thread_in_right_section<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "en_main".parse().unwrap(),
      display_name: "Main Forum".parse().unwrap(),
      locale: Some(LocaleId::EnUs),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Hello".parse().unwrap(),
      body_mkt: "**First** discussion thread".to_string(),
      body_html: "<strong>First</strong> discussion thread".to_string(),
    })
    .await
    .unwrap();
  let expected = RawCreateForumThreadResult {
    id: actual.id,
    key: None,
    title: "Hello".parse().unwrap(),
    section: section.as_ref(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    is_pinned: false,
    is_locked: false,
    post_id: actual.post_id,
    post_revision: RawForumPostRevision {
      id: actual.post_revision.id,
      time: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      author: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      content: Some(ForumPostRevisionContent {
        marktwin: "**First** discussion thread".to_string(),
        html: "<strong>First</strong> discussion thread".to_string(),
      }),
      moderation: None,
      comment: None,
    },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_and_retrieve_thread<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let thread = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Hello".parse().unwrap(),
      body_mkt: "**First** discussion thread".to_string(),
      body_html: "<strong>First</strong> discussion thread".to_string(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_thread_meta(&RawGetForumThreadMetaOptions {
      thread: thread.id.into(),
    })
    .await
    .unwrap();
  let expected = RawForumThreadMeta {
    id: actual.id,
    key: None,
    title: "Hello".parse().unwrap(),
    section: section.as_ref(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
    is_pinned: false,
    is_locked: false,
    posts: ListingCount { count: 1 },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_thread_and_retrieve_first_post<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let thread = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Hello".parse().unwrap(),
      body_mkt: "**First** discussion thread".to_string(),
      body_html: "<strong>First</strong> discussion thread".to_string(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_post(&RawGetForumPostOptions {
      post: thread.post_id.into(),
      offset: 0,
      limit: 1,
    })
    .await
    .unwrap();
  let expected = RawForumPost {
    id: thread.post_id,
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
    author: RawForumActor::UserForumActor(RawUserForumActor {
      role: None,
      user: alice.id.into(),
    }),
    revisions: Listing {
      offset: 0,
      limit: 1,
      count: 1,
      items: vec![RawForumPostRevision {
        id: thread.post_revision.id,
        time: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
        author: RawForumActor::UserForumActor(RawUserForumActor {
          role: None,
          user: alice.id.into(),
        }),
        content: Some(ForumPostRevisionContent {
          marktwin: "**First** discussion thread".parse().unwrap(),
          html: "<strong>First</strong> discussion thread".to_string(),
        }),
        moderation: None,
        comment: None,
      }],
    },
    thread: RawForumThreadMeta {
      id: thread.id,
      key: None,
      title: "Hello".parse().unwrap(),
      section: section.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
      is_pinned: false,
      is_locked: false,
      posts: ListingCount { count: 1 },
    },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_thread_and_post_10_replies<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let thread = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Hello".parse().unwrap(),
      body_mkt: "**First** discussion thread".to_string(),
      body_html: "<strong>First</strong> discussion thread".to_string(),
    })
    .await
    .unwrap();
  let mut posts: Vec<RawCreateForumPostResult> = Vec::new();
  for post_idx in 0..10 {
    api.clock.advance_by(Duration::from_seconds(1));
    let post = api
      .forum_store
      .forum_store()
      .create_post(&RawCreatePostOptions {
        actor: RawForumActor::UserForumActor(RawUserForumActor {
          role: None,
          user: alice.id.into(),
        }),
        thread: thread.id.into(),
        body: ForumPostRevisionContent {
          marktwin: format!("Reply {}", post_idx),
          html: format!("Reply {}", post_idx),
        },
      })
      .await
      .unwrap();
    posts.push(post);
  }

  let actual = api
    .forum_store
    .forum_store()
    .get_posts(&RawGetPostsOptions {
      thread: thread.id.into(),
      offset: 7,
      limit: 5,
    })
    .await
    .unwrap();
  let expected = Listing {
    offset: 7,
    limit: 5,
    count: 11,
    items: vec![
      RawShortForumPost {
        id: posts[6].id,
        ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 9),
        author: RawForumActor::UserForumActor(RawUserForumActor {
          role: None,
          user: alice.id.into(),
        }),
        revisions: RawLatestForumPostRevisionListing {
          count: 1,
          last: RawForumPostRevision {
            id: posts[6].revision.id,
            time: Instant::ymd_hms(2021, 1, 1, 0, 0, 9),
            author: RawForumActor::UserForumActor(RawUserForumActor {
              role: None,
              user: alice.id.into(),
            }),
            content: Some(ForumPostRevisionContent {
              marktwin: "Reply 6".parse().unwrap(),
              html: "Reply 6".parse().unwrap(),
            }),
            moderation: None,
            comment: None,
          },
        },
      },
      RawShortForumPost {
        id: posts[7].id,
        ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 10),
        author: RawForumActor::UserForumActor(RawUserForumActor {
          role: None,
          user: alice.id.into(),
        }),
        revisions: RawLatestForumPostRevisionListing {
          count: 1,
          last: RawForumPostRevision {
            id: posts[7].revision.id,
            time: Instant::ymd_hms(2021, 1, 1, 0, 0, 10),
            author: RawForumActor::UserForumActor(RawUserForumActor {
              role: None,
              user: alice.id.into(),
            }),
            content: Some(ForumPostRevisionContent {
              marktwin: "Reply 7".parse().unwrap(),
              html: "Reply 7".parse().unwrap(),
            }),
            moderation: None,
            comment: None,
          },
        },
      },
      RawShortForumPost {
        id: posts[8].id,
        ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 11),
        author: RawForumActor::UserForumActor(RawUserForumActor {
          role: None,
          user: alice.id.into(),
        }),
        revisions: RawLatestForumPostRevisionListing {
          count: 1,
          last: RawForumPostRevision {
            id: posts[8].revision.id,
            time: Instant::ymd_hms(2021, 1, 1, 0, 0, 11),
            author: RawForumActor::UserForumActor(RawUserForumActor {
              role: None,
              user: alice.id.into(),
            }),
            content: Some(ForumPostRevisionContent {
              marktwin: "Reply 8".parse().unwrap(),
              html: "Reply 8".parse().unwrap(),
            }),
            moderation: None,
            comment: None,
          },
        },
      },
      RawShortForumPost {
        id: posts[9].id,
        ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 12),
        author: RawForumActor::UserForumActor(RawUserForumActor {
          role: None,
          user: alice.id.into(),
        }),
        revisions: RawLatestForumPostRevisionListing {
          count: 1,
          last: RawForumPostRevision {
            id: posts[9].revision.id,
            time: Instant::ymd_hms(2021, 1, 1, 0, 0, 12),
            author: RawForumActor::UserForumActor(RawUserForumActor {
              role: None,
              user: alice.id.into(),
            }),
            content: Some(ForumPostRevisionContent {
              marktwin: "Reply 9".parse().unwrap(),
              html: "Reply 9".parse().unwrap(),
            }),
            moderation: None,
            comment: None,
          },
        },
      },
    ],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_a_few_threads_with_messages_in_the_main_forum_section<
  TyClock,
  TyForumStore,
  TyUserStore,
>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let thread1 = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Thread 1".parse().unwrap(),
      body_mkt: "This is the first thread".to_string(),
      body_html: "This is the first thread".to_string(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Thread 2".parse().unwrap(),
      body_mkt: "This is the second thread".to_string(),
      body_html: "This is the second thread".to_string(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .create_post(&RawCreatePostOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      thread: thread1.id.into(),
      body: ForumPostRevisionContent {
        marktwin: "Reply to thread 1".parse().unwrap(),
        html: "Reply to thread 1".parse().unwrap(),
      },
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let thread3 = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Thread 3".parse().unwrap(),
      body_mkt: "This is the third thread".to_string(),
      body_html: "This is the third thread".to_string(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .create_post(&RawCreatePostOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      thread: thread1.id.into(),
      body: ForumPostRevisionContent {
        marktwin: "Another reply to thread 1".parse().unwrap(),
        html: "Another reply to thread 1".parse().unwrap(),
      },
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .forum_store
    .forum_store()
    .get_threads(&RawGetThreadsOptions {
      section: ForumSectionKey::from_str("fr_main").unwrap().into(),
      offset: 0,
      limit: 2,
    })
    .await
    .unwrap();
  let expected = Listing {
    offset: 0,
    limit: 2,
    count: 3,
    items: vec![
      RawForumThreadMeta {
        id: thread1.id,
        key: None,
        title: "Thread 1".parse().unwrap(),
        section: section.id.into(),
        ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
        is_pinned: false,
        is_locked: false,
        posts: ListingCount { count: 3 },
      },
      RawForumThreadMeta {
        id: thread3.id,
        key: None,
        title: "Thread 3".parse().unwrap(),
        section: section.id.into(),
        ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 5),
        is_pinned: false,
        is_locked: false,
        posts: ListingCount { count: 1 },
      },
    ],
  };
  assert_eq!(actual, expected);

  let actual = api
    .forum_store
    .forum_store()
    .get_section_meta(&GetForumSectionMetaOptions {
      section: ForumSectionKey::from_str("fr_main").unwrap().into(),
    })
    .await
    .unwrap();
  let expected = RawForumSectionMeta {
    id: section.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: ListingCount { count: 3 },
    role_grants: vec![],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_add_moderator<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Bob".parse().unwrap(),
      username: Some("bob".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_section_meta(&GetForumSectionMetaOptions {
      section: section.as_ref().into(),
    })
    .await
    .unwrap();
  let expected = RawForumSectionMeta {
    id: section.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: ListingCount { count: 0 },
    role_grants: vec![RawForumRoleGrant {
      role: ForumRole::Moderator,
      user: bob.id.into(),
      start_time: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      granted_by: alice.id.into(),
    }],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_add_moderator_is_idempotent<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Bob".parse().unwrap(),
      username: Some("bob".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_section_meta(&GetForumSectionMetaOptions {
      section: section.as_ref().into(),
    })
    .await
    .unwrap();
  let expected = RawForumSectionMeta {
    id: section.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: ListingCount { count: 0 },
    role_grants: vec![RawForumRoleGrant {
      role: ForumRole::Moderator,
      user: bob.id.into(),
      start_time: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      granted_by: alice.id.into(),
    }],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_add_multiple_moderators<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Bob".parse().unwrap(),
      username: Some("bob".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let charlie = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Charlie".parse().unwrap(),
      username: Some("charlie".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: charlie.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_section_meta(&GetForumSectionMetaOptions {
      section: section.as_ref().into(),
    })
    .await
    .unwrap();
  let expected = RawForumSectionMeta {
    id: section.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: ListingCount { count: 0 },
    role_grants: vec![
      RawForumRoleGrant {
        role: ForumRole::Moderator,
        user: bob.id.into(),
        start_time: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
        granted_by: alice.id.into(),
      },
      RawForumRoleGrant {
        role: ForumRole::Moderator,
        user: charlie.id.into(),
        start_time: Instant::ymd_hms(2021, 1, 1, 0, 0, 5),
        granted_by: alice.id.into(),
      },
    ],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_delete_moderator<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Bob".parse().unwrap(),
      username: Some("bob".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let charlie = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Charlie".parse().unwrap(),
      username: Some("charlie".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: charlie.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .delete_moderator(&RawDeleteModeratorOptions {
      section: section.as_ref().into(),
      revoker: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_section_meta(&GetForumSectionMetaOptions {
      section: section.as_ref().into(),
    })
    .await
    .unwrap();
  let expected = RawForumSectionMeta {
    id: section.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: ListingCount { count: 0 },
    role_grants: vec![RawForumRoleGrant {
      role: ForumRole::Moderator,
      user: charlie.id.into(),
      start_time: Instant::ymd_hms(2021, 1, 1, 0, 0, 5),
      granted_by: alice.id.into(),
    }],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_delete_moderator_is_idempotent<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Bob".parse().unwrap(),
      username: Some("bob".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let charlie = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Charlie".parse().unwrap(),
      username: Some("charlie".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .add_moderator(&RawAddModeratorOptions {
      section: section.as_ref().into(),
      granter: alice.id.into(),
      target: charlie.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .delete_moderator(&RawDeleteModeratorOptions {
      section: section.as_ref().into(),
      revoker: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .forum_store
    .forum_store()
    .delete_moderator(&RawDeleteModeratorOptions {
      section: section.as_ref().into(),
      revoker: alice.id.into(),
      target: bob.id.into(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .get_section_meta(&GetForumSectionMetaOptions {
      section: section.as_ref().into(),
    })
    .await
    .unwrap();
  let expected = RawForumSectionMeta {
    id: section.id,
    key: Some("fr_main".parse().unwrap()),
    display_name: "Forum Général".parse().unwrap(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    locale: Some(LocaleId::FrFr),
    threads: ListingCount { count: 0 },
    role_grants: vec![RawForumRoleGrant {
      role: ForumRole::Moderator,
      user: charlie.id.into(),
      start_time: Instant::ymd_hms(2021, 1, 1, 0, 0, 5),
      granted_by: alice.id.into(),
    }],
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_delete_post<TyClock, TyForumStore, TyUserStore>(
  api: TestApi<TyClock, TyForumStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyForumStore: ForumStoreRef,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let section = api
    .forum_store
    .forum_store()
    .upsert_system_section(&UpsertSystemSectionOptions {
      key: "fr_main".parse().unwrap(),
      display_name: "Forum Général".parse().unwrap(),
      locale: Some(LocaleId::FrFr),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let alice = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let thread = api
    .forum_store
    .forum_store()
    .create_thread(&RawCreateThreadOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      section: section.as_ref().into(),
      title: "Hello".parse().unwrap(),
      body_mkt: "**First** discussion thread".to_string(),
      body_html: "<strong>First</strong> discussion thread".to_string(),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .forum_store
    .forum_store()
    .create_post_revision(&RawCreatePostRevisionOptions {
      actor: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      post: thread.post_id.into(),
      body: None,
      mod_body: None,
      comment: Some("Deletion comment".parse().unwrap()),
    })
    .await
    .unwrap();

  let expected = RawCreateForumPostRevisionResult {
    revision: RawForumPostRevision {
      id: actual.revision.id,
      time: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      author: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: alice.id.into(),
      }),
      content: None,
      moderation: None,
      comment: Some("Deletion comment".parse().unwrap()),
    },
    thread: thread.id.into(),
    section: section.id.into(),
    post: thread.post_id.into(),
  };
  assert_eq!(actual, expected);
}
