//! PURPOSE: This crate defines the core utilities of the client and the store.
//! - The client allows to use the twinoid-dinorpg API and the scraper (for what is not covered by the API). It is
//!   implemented in the dinorpg_client crate.
//! - The store allows to save the information queried from the client (either in memory or in a database) and retrieve
//!   this information.
mod api;
pub mod client;
pub mod dino;
pub mod epic_reward;
pub mod ingredient;
pub mod item;
pub mod location;
pub mod mission;
pub mod npc;
pub mod quest;
pub mod race;
pub mod skill;
pub mod status;
pub mod user;

pub use self::client::DinorpgClient;
pub use self::dino::{
  DinorpgDinozElements, DinorpgDinozExperience, DinorpgDinozId, DinorpgDinozLevel, DinorpgDinozLife,
  DinorpgDinozMaxLife, DinorpgDinozMission, DinorpgDinozName, DinorpgDinozSkin, DinorpgDinozSkinCheck,
};
pub use self::ingredient::{DinorpgIngredient, DinorpgIngredientCount};
pub use self::mission::DinorpgMission;
pub use self::npc::DinorpgNpc;
pub use self::skill::DinorpgSkill;
pub use self::user::{DinorpgProfile, DinorpgUserId, ShortDinorpgUser};
use crate::core::Instant;
use crate::dinorpg::{
  epic_reward::DinorpgEpicReward,
  item::{DinorpgItem, DinorpgItemCount},
  location::DinorpgLocation,
  quest::{DinorpgQuest, DinorpgQuestStatus},
  race::DinorpgRace,
  status::DinorpgStatus,
};
#[cfg(feature = "sqlx-postgres")]
use crate::pg_num::PgU32;
use crate::temporal::LatestTemporal;
use crate::twinoid::{TwinoidUserDisplayName, TwinoidUserId};
use crate::types::WeakError;
use async_trait::async_trait;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::serialize_ordered_map;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
use std::collections::HashMap;

/// Options that can be specified when retrieving a dino from the store
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetDinorpgDinozOptions {
  pub server: DinorpgServer,
  pub id: DinorpgDinozId,
  pub time: Option<Instant>,
}

/// Options that can be specified when retrieving a user from the store
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetDinorpgUserOptions {
  pub server: DinorpgServer,
  pub id: DinorpgUserId,
  pub time: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "DinorpgUser"))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ArchivedDinorpgDinoz {
  pub server: DinorpgServer,
  pub id: DinorpgDinozId,
  pub archived_at: Instant,
  pub name: Option<LatestTemporal<DinorpgDinozName>>,
  pub owner_id: Option<LatestTemporal<DinorpgUserId>>,
  pub level: Option<LatestTemporal<DinorpgDinozLevel>>,
  pub life: Option<LatestTemporal<DinorpgDinozLife>>,
  pub max_life: Option<LatestTemporal<DinorpgDinozMaxLife>>,
  pub experience: Option<LatestTemporal<DinorpgDinozExperience>>,
  pub race: Option<LatestTemporal<DinorpgRace>>,
  pub skin_checksum: Option<LatestTemporal<DinorpgDinozSkinCheck>>,
  pub elements: Option<LatestTemporal<DinorpgDinozElements>>,
  pub items: Option<LatestTemporal<Vec<DinorpgItem>>>,
  pub status: Option<LatestTemporal<Vec<DinorpgStatus>>>,
  pub location: Option<LatestTemporal<DinorpgLocation>>,
  pub is_frozen: Option<LatestTemporal<bool>>,
  pub is_sacrificed: Option<LatestTemporal<bool>>,
  pub skills: Option<LatestTemporal<Vec<DinorpgSkill>>>,
  pub missions: Option<LatestTemporal<HashMap<DinorpgNpc, Vec<DinorpgMission>>>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "DinorpgUser"))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ArchivedDinorpgUser {
  pub server: DinorpgServer,
  pub id: DinorpgUserId,
  pub username: String,
  pub archived_at: Instant,
  pub money: Option<LatestTemporal<DinorpgMoney>>,
  pub items: Option<LatestTemporal<HashMap<DinorpgItem, DinorpgItemCount>>>,
  pub quests: Option<LatestTemporal<HashMap<DinorpgQuest, u8>>>,
  pub ingredients: Option<LatestTemporal<HashMap<DinorpgIngredient, DinorpgIngredientCount>>>,
  pub epic_rewards: Option<LatestTemporal<Vec<DinorpgEpicReward>>>,
  pub list_dinoz: Option<LatestTemporal<Vec<DinorpgDinozId>>>,
  pub list_sacrificed_dinoz: Option<LatestTemporal<Vec<DinorpgSacrificedDinoz>>>,
}

// impl Default for ArchivedDinorpgUser {
//   fn default() -> Self {
//     Self {
//       server: DinorpgServer::DinorpgCom,
//       id: DinorpgUserId::new(1).unwrap(),
//       username: String::new(),
//       archived_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
//       epic_rewards: None,
//       ingredients: None,
//       items: None,
//       list_dinoz: None,
//       list_sacrificed_dinoz: None,
//       money: None,
//       quests: None,
//     }
//   }
// }

declare_new_enum!(
  pub enum DinorpgServer {
    #[str("www.dinorpg.com")]
    DinorpgCom,
    #[str("en.dinorpg.com")]
    EnDinorpgCom,
    #[str("es.dinorpg.com")]
    EsDinorpgCom,
  }
  pub type ParseError = DinorpgServerParseError;
  const SQL_NAME = "dinorpg_server";
);

declare_new_string! {
  /// A Dinorpg session key.
  ///
  /// It correspond to the value of the `sid` cookie.
  ///
  /// - `wKc12oxNKinxZmDMxOY38jmBD1wSEtOL`
  pub struct DinorpgSessionKey(String);
  pub type ParseError = DinorpgSessionKeyParseError;
  const PATTERN = r"^[0-9a-zA-Z]{32}$";
  const SQL_NAME = "dinorpg_session_key";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DinorpgSession {
  pub key: DinorpgSessionKey,
  pub user: ShortDinorpgUser,
}

declare_new_int! {
  pub struct DinorpgMoney(u32);
  pub type RangeError = DinorpgMoneyError;
  const BOUNDS = 0..1_000_000_000;
  type SqlType = PgU32;
  const SQL_NAME = "dinorpg_money";
}

/// Scraper specific types
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgSacrificedDinoz {
  pub name: DinorpgDinozName,
  pub level: DinorpgDinozLevel,
  pub skin_check: DinorpgDinozSkinCheck,
  pub elements: DinorpgDinozElements,
  pub skills: Vec<DinorpgSkill>,
}

// Responses
// Ideally each response is the result of a call of the Twinoid API or the Scraper API (see DinorpgClient)

// For the API
/// User data response from the Twinoid API
/// Note: The Clan field from the API is omitted
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgUserResponse {
  pub server: DinorpgServer,
  pub id: DinorpgUserId,
  pub name: TwinoidUserDisplayName,
  pub twin_id: TwinoidUserId,
  pub dinos: Vec<DinorpgDinoResponse>,
  pub items: Vec<DinorpgItem>,
  pub epic_rewards: Vec<DinorpgEpicReward>,
  pub money: DinorpgMoney,
  pub quests: Vec<DinorpgQuestStatus>,
}

/// Dino data response from the Twinoid API
/// Note 1: The Map field from the API is omitted
/// Note 2: This does not contain the list of skills of the dino as the API does not give it. Get it from the scrapper
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDinoResponse {
  pub server: DinorpgServer,
  pub id: DinorpgDinozId,
  pub name: DinorpgDinozName,
  pub display: DinorpgDinozSkin,
  pub max_life: DinorpgDinozMaxLife,
  pub level: DinorpgDinozLevel,
  pub xp: DinorpgDinozExperience,
  pub elements: DinorpgDinozElements,
  pub equip: Vec<DinorpgItem>,
  pub effects: Vec<DinorpgStatus>,
}

// For the scraper
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDinozSkillsResponse {
  pub profile: DinorpgProfile,
  pub dinoz_id: DinorpgDinozId,
  pub skills: Vec<DinorpgSkill>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgDemonShopResponse {
  pub profile: DinorpgProfile,
  pub dinoz: Vec<DinorpgSacrificedDinoz>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgAllDinozMissionsResponse {
  pub profile: DinorpgProfile,
  // This is optional in case the user does not own the item that allows to check for missions progress
  pub dinoz: Option<Vec<DinorpgDinozMission>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DinorpgIngredientResponse {
  pub profile: DinorpgProfile,
  #[cfg_attr(feature = "serde", serde(serialize_with = "serialize_ordered_map"))]
  pub ingredients: HashMap<DinorpgIngredient, DinorpgIngredientCount>,
}

// Unused types
// TODO remove
// #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
// #[derive(Clone, Debug, PartialEq, Eq)]
// pub struct DinorpgDinozResponse {
//   // session info ? -> everything is in DinorpgDinoz.profile looks like
//   pub dinoz: DinorpgDinoz,
// }

// TODO remove
// #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
// #[derive(Clone, Debug, PartialEq, Eq)]
// pub struct DinorpgUserResponse {
//   pub profile: ShortDinorpgUser,
//   pub money: DinorpgMoney,
//   pub items: HashMap<DinorpgItem, DinorpgItemCount>,
//   pub quests: HashMap<DinorpgQuest, u8>,
//   pub epic_rewards: Vec<DinorpgEpicReward>,
// }

// TODO remove
// #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
// #[derive(Clone, Debug, PartialEq, Eq)]
// pub struct DinorpgDinoz {
//   pub profile: ShortDinorpgUser,
//   pub id: DinorpgDinozId,
//   pub name: DinorpgDinozName,
//   pub owner_id: DinorpgUserId,
//   pub level: DinorpgDinozLevel,
//   pub life: DinorpgDinozLife,
//   pub max_life: DinorpgDinozMaxLife,
//   pub experience: DinorpgDinozExperience,
//   pub race: DinorpgRace,
//   // pub skin: DinorpgDinozSkin,
//   pub skin_checksum: DinorpgDinozSkinCheck,
//   pub elements: DinorpgDinozElements,
//   pub items: Vec<DinorpgItem>,
//   pub status: Vec<DinorpgStatus>,
//   pub location: DinorpgLocation,
//   pub is_frozen: bool,
//   pub is_sacrificed: bool,
//   pub skills: Vec<DinorpgSkill>,
//   pub missions: HashMap<DinorpgNpc, Vec<DinorpgMission>>,
// }

#[derive(Debug, thiserror::Error)]
pub enum RawTouchDinorpgUserError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawTouchDinorpgDinoError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawTouchDinorpgIngredientError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawTouchDinorpgAllMissionsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawTouchDinorpgDemonShopError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawTouchDinorpgDinozSkillsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawGetDinorpgUserError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawGetDinorpgDinozError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[async_trait]
pub trait DinorpgStore: Send + Sync {
  // - Writers (to memory or database) -

  // Related to API calls

  async fn touch_user(&self, user: &DinorpgUserResponse) -> Result<(), RawTouchDinorpgUserError>;

  async fn touch_dino(&self, user: &DinorpgDinoResponse) -> Result<(), RawTouchDinorpgDinoError>;

  // Related to scraper calls
  async fn touch_ingredients(&self, response: &DinorpgIngredientResponse)
    -> Result<(), RawTouchDinorpgIngredientError>;

  async fn touch_all_dinoz_missions(
    &self,
    response: &DinorpgAllDinozMissionsResponse,
  ) -> Result<(), RawTouchDinorpgAllMissionsError>;

  // TODO finish or find a solution to workaround the lack of ID from the demon shop
  async fn touch_demon_shop(&self, response: &DinorpgDemonShopResponse) -> Result<(), RawTouchDinorpgDemonShopError>;

  async fn touch_dinoz_skills(&self, user: &DinorpgDinozSkillsResponse) -> Result<(), RawTouchDinorpgDinozSkillsError>;

  // - Getters -
  async fn get_user(
    &self,
    options: &GetDinorpgUserOptions,
  ) -> Result<Option<ArchivedDinorpgUser>, RawGetDinorpgUserError>;

  async fn get_dinoz(
    &self,
    options: &GetDinorpgDinozOptions,
  ) -> Result<Option<ArchivedDinorpgDinoz>, RawGetDinorpgDinozError>;
}
