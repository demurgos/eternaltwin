use crate::core::Request;
use crate::twinoid::api::Projector;
use crate::twinoid::api::User;
use crate::twinoid::{TwinoidApiAuth, TwinoidUserId};
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum GetUserError {
  #[error("invalid token")]
  InvalidToken,
  #[error("server failed to encode response")]
  Encoding,
  #[error(transparent)]
  Other(#[from] WeakError),
}

pub struct GetUser<P: Projector<User>> {
  pub auth: TwinoidApiAuth,
  pub id: TwinoidUserId,
  pub projector: P,
}

impl<P: Projector<User>> Request for GetUser<P> {
  type Response = Result<P::Projection, GetUserError>;
}
