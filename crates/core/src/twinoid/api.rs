use crate::core::{Date, HtmlFragment, PrUrl};
use crate::twinoid::{
  TwinoidDateTime, TwinoidLocale, TwinoidSiteHost, TwinoidSiteId, TwinoidSiteInfoId, TwinoidSiteUserId,
  TwinoidUserDisplayName, TwinoidUserId,
};
use url::Url;

pub trait Projector<T> {
  type Projection;

  fn write_query(&self, buf: &mut String, field: Option<&'static str>);

  fn to_query(&self) -> String {
    let mut buf = String::new();
    self.write_query(&mut buf, None);
    buf
  }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Select;

impl<T> Projector<T> for Select {
  type Projection = T;

  fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
    if let Some(field) = field {
      buf.push_str(field);
    }
  }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Skip;

impl<T> Projector<T> for Skip {
  type Projection = Missing;

  fn write_query(&self, _buf: &mut String, _field: Option<&'static str>) {}
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Missing;

#[cfg(feature = "serde")]
impl<'de> ::serde::Deserialize<'de> for Missing {
  fn deserialize<D>(_deserializer: D) -> Result<Self, D::Error>
  where
    D: ::serde::Deserializer<'de>,
  {
    Ok(Missing)
  }
}

#[cfg(feature = "serde")]
impl ::serde::Serialize for Missing {
  fn serialize<S: ::serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
    serializer.serialize_unit()
  }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Restrict<T> {
  #[default]
  Deny,
  Allow(T),
}

impl<T> Restrict<T> {
  pub fn as_ref(&self) -> Restrict<&T> {
    match self {
      Self::Deny => Restrict::Deny,
      Self::Allow(ref value) => Restrict::Allow(value),
    }
  }

  pub fn into_option(self) -> Option<T> {
    match self {
      Self::Deny => None,
      Self::Allow(value) => Some(value),
    }
  }

  pub fn is_allow(&self) -> bool {
    matches!(self, Self::Allow(_))
  }

  pub fn is_deny(&self) -> bool {
    matches!(self, Self::Deny)
  }

  pub fn expect(self, msg: &str) -> T {
    match self {
      Self::Deny => panic!("called expect on `Restrict::Deny`: {msg}"),
      Self::Allow(val) => val,
    }
  }

  pub fn from_option(opt: Option<T>) -> Self {
    match opt {
      None => Self::Deny,
      Some(value) => Self::Allow(value),
    }
  }
}

impl<T> From<Option<T>> for Restrict<T> {
  fn from(value: Option<T>) -> Self {
    Self::from_option(value)
  }
}

impl<T> From<Restrict<T>> for Option<T> {
  fn from(value: Restrict<T>) -> Self {
    value.into_option()
  }
}

#[cfg(feature = "serde")]
impl<'de, T> ::serde::Deserialize<'de> for Restrict<T>
where
  T: ::serde::Deserialize<'de>,
{
  fn deserialize<D>(deserializer: D) -> Result<Restrict<T>, D::Error>
  where
    D: ::serde::Deserializer<'de>,
  {
    T::deserialize(deserializer).map(Restrict::Allow)
  }
}

// The `𝄞` symbol is used because it's outside of the BMP.
#[cfg(feature = "serde")]
const RESTRICT_MISSING_SENTINEL: &str = "eternaltwin_core::Restrict::deny𝄞";

/// Very hacky to way if some generic value is in fact `Restrict::<U>::Deny` (for any `U`)
#[cfg(feature = "serde")]
pub fn is_restrict_deny<T: serde::Serialize>(t: &T) -> bool {
  match serde_json::to_value(t).unwrap_or(serde_json::Value::Null).as_str() {
    Some(s) => s == RESTRICT_MISSING_SENTINEL,
    None => false,
  }
}

#[cfg(feature = "serde")]
impl<T> ::serde::Serialize for Restrict<T>
where
  T: ::serde::Serialize,
{
  fn serialize<S: ::serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
    match self {
      Restrict::Allow(value) => value.serialize(serializer),
      Restrict::Deny => serializer.serialize_str(RESTRICT_MISSING_SENTINEL),
    }
  }
}

macro_rules! declare_twinoid_record {
  {
    #[twinoid(projector = $projector_struct:ident)]
    $struct_vis:vis struct $struct_name:ident {
      $(
        #[twinoid(required, rename = $req_str:literal)]
        $(#[$req_meta:meta])*
        $req_name:ident: $req_ty_name:ident = $req_ty:ty,
      )*
      $(
        #[twinoid(rename = $opt_str:literal)]
        $(#[$opt_meta:meta])*
        $opt_name:ident: $opt_ty_name:ident = $opt_ty:ty,
      )*
    }
  } => {
    declare_twinoid_record!(@inner {
      projector = $projector_struct;
      $struct_vis struct $struct_name;
      req = [$({
        name = $req_name;
        str = $req_str;
        ty_name = $req_ty_name;
        ty = $req_ty;
        $([$req_meta];)*
      })*];
      opt = [$({
        name = $opt_name;
        str = $opt_str;
        ty_name = $opt_ty_name;
        ty = $opt_ty;
        $([$opt_meta];)*
      })*];
      all = [
        $({
          name = $req_name;
          str = $req_str;
          ty_name = $req_ty_name;
          ty = $req_ty;
          $([$req_meta];)*
        })*
        $({
          name = $opt_name;
          str = $opt_str;
          ty_name = $opt_ty_name;
          ty = $opt_ty;
          $([$opt_meta];)*
        })*
      ];
    });
  };
  {
    @to_select($t: ty)
  } => {
    crate::twinoid::api::Select
  };
  {
    @inner {
      projector = $projector_struct:ident;
      $struct_vis:vis struct $struct_name:ident;
      req = [$({
        name = $req_name:ident;
        str = $req_str:literal;
        ty_name = $req_ty_name:ident;
        ty = $req_ty:ty;
        $([$req_meta:meta];)*
      })*];
      opt = [$({
        name = $opt_name:ident;
        str = $opt_str:literal;
        ty_name = $opt_ty_name:ident;
        ty = $opt_ty:ty;
        $([$opt_meta:meta];)*
      })*];
      all = [$({
        name = $all_name:ident;
        str = $all_str:literal;
        ty_name = $all_ty_name:ident;
        ty = $all_ty:ty;
        $([$all_meta:meta];)*
      })*];
    }
  } => {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    #[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
    $struct_vis struct $struct_name<
      $($req_ty_name = $req_ty,)*
      $($opt_ty_name = crate::twinoid::api::Missing,)*
    > {
      $(
        #[cfg_attr(feature = "serde", serde(rename = $all_str, skip_serializing_if = "crate::twinoid::api::is_restrict_deny"))]
        $(#[$all_meta])*
        $struct_vis $all_name: $all_ty_name,
      )*
    }

    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    $struct_vis struct $projector_struct<
      $($req_ty_name = crate::twinoid::api::Select,)*
      $($opt_ty_name = crate::twinoid::api::Skip,)*
    > {
      $(
      #[allow(unused)]
      $struct_vis $req_name: $req_ty_name,
      )*
      $($struct_vis $opt_name: $opt_ty_name,)*
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<$struct_name>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = $struct_name<
        $($req_ty,)*
        $($opt_ty_name::Projection,)*
      >;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {

        let mut base_len = buf.len();
        if let Some(field) = field {
          buf.push_str(field);
          base_len = buf.len();
          buf.push_str(".fields(")
        }
        let fields_start = buf.len();
        let mut fields_end = buf.len();
        let mut last_len;

        $(
          last_len = buf.len();
          self.$opt_name.write_query(buf, Some($opt_str));
          if buf.len() != last_len {
            fields_end = buf.len();
            buf.push(',');
          }
        )*

        if buf.len() == fields_start {
          buf.truncate(base_len);
        } else {
          buf.truncate(fields_end);
          if field.is_some() {
            buf.push(')');
          }
        }

      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Box<$struct_name>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Box<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Option<$struct_name>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Option<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Vec<$struct_name>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Vec<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Option<Vec<$struct_name>>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Option<Vec<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Restrict<$struct_name>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Restrict<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Restrict<Vec<$struct_name>>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Restrict<Vec<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Restrict<Box<$struct_name>>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Restrict<Box<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }

    impl<$($opt_ty_name,)*>
      crate::twinoid::api::Projector<Restrict<Option<$struct_name>>>
      for $projector_struct<
        $(declare_twinoid_record!(@to_select($req_ty_name)),)*
        $($opt_ty_name,)*
      >
    where
      $($opt_ty_name: crate::twinoid::api::Projector<$opt_ty>,)*
    {
      type Projection = Restrict<Option<<Self as crate::twinoid::api::Projector<$struct_name>>::Projection>>;

      fn write_query(&self, buf: &mut String, field: Option<&'static str>) {
        <Self as crate::twinoid::api::Projector<$struct_name>>::write_query(self, buf, field)
      }
    }
  };
}
pub(crate) use declare_twinoid_record;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
pub struct UrlRef {
  pub url: PrUrl,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
pub struct UserOldName {
  pub name: TwinoidUserDisplayName,
  pub until: TwinoidDateTime,
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Type<sqlx::Postgres> for UserOldName {
  fn type_info() -> sqlx::postgres::PgTypeInfo {
    sqlx::postgres::PgTypeInfo::with_name("old_twinoid_user_display_name")
  }
}

#[cfg(feature = "sqlx-postgres")]
impl<'r> sqlx::Decode<'r, sqlx::Postgres> for UserOldName {
  fn decode(value: sqlx::postgres::PgValueRef<'r>) -> Result<Self, Box<dyn std::error::Error + 'static + Send + Sync>> {
    let mut decoder = sqlx::postgres::types::PgRecordDecoder::new(value)?;

    let name = decoder.try_decode::<TwinoidUserDisplayName>()?;
    let until = decoder.try_decode::<crate::core::Instant>()?;

    Ok(Self {
      name,
      until: TwinoidDateTime::from_instant(until),
    })
  }
}

#[cfg(feature = "sqlx-postgres")]
impl<'q> sqlx::Encode<'q, sqlx::Postgres> for UserOldName {
  fn encode_by_ref(&self, buf: &mut sqlx::postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let mut encoder = sqlx::postgres::types::PgRecordEncoder::new(buf);
    encoder.encode(&self.name);
    encoder.encode(self.until.into_instant());
    encoder.finish();
    sqlx::encode::IsNull::No
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::postgres::PgHasArrayType for UserOldName {
  fn array_type_info() -> sqlx::postgres::PgTypeInfo {
    sqlx::postgres::PgTypeInfo::with_name("_old_twinoid_user_display_name")
  }
}

declare_new_enum!(
  pub enum TwinoidGender {
    #[str("male")]
    Male,
    #[str("female")]
    Female,
  }
  pub type ParseError = TwinoidGenderParseError;
  const SQL_NAME = "twinoid_gender";
);

declare_twinoid_record! {
  #[twinoid(projector = UserProjector)]
  pub struct User {
    #[twinoid(required, rename = "id")]
    id: TyId = TwinoidUserId,
    #[twinoid(rename = "name")]
    name: TyName = TwinoidUserDisplayName,
    #[twinoid(rename = "picture")]
    picture: TyPicture = Option<UrlRef>,
    #[twinoid(rename = "locale")]
    locale: TyLocale = TwinoidLocale,
    #[twinoid(rename = "title")]
    title: TyTitle = Option<HtmlFragment>,
    #[twinoid(rename = "oldNames")]
    old_names: TyOldNames = Vec<UserOldName>,
    #[twinoid(rename = "sites")]
    sites: TySites = Vec<SiteUser>,
    #[twinoid(rename = "like")]
    like: TyLike = Like,
    #[twinoid(rename = "gender")]
    gender: TyGender = Option<TwinoidGender>,
    #[twinoid(rename = "birthday")]
    birthday: TyBirthday = Option<Date>,
    #[twinoid(rename = "city")]
    city: TyCity = Option<String>,
    #[twinoid(rename = "country")]
    country: TyCountry = Option<String>,
    #[twinoid(rename = "desc")]
    desc: TyDesc = Option<HtmlFragment>,
    #[twinoid(rename = "status")]
    status: TyStatus = Option<HtmlFragment>,
    #[twinoid(rename = "contacts")]
    #[cfg_attr(feature = "serde", serde(default))]
    contacts: TyContacts = Restrict<Vec<Contact>>,
    #[twinoid(rename = "groups")]
    groups: TyGroups = Vec<GroupMember>,
    #[twinoid(rename = "devApps")]
    dev_apps: TyDevAps = Vec<Application>,
  }
}

pub type ShortUser = User<TwinoidUserId, TwinoidUserDisplayName, Missing, String, Missing, Missing, Missing>;

declare_twinoid_record! {
  #[twinoid(projector = ContactProjector)]
  pub struct Contact {
    #[twinoid(rename = "friend")]
    friend: TyFriend = bool,
    #[twinoid(rename = "user")]
    user: TyUser = Box<User>,
  }
}

declare_twinoid_record! {
  #[twinoid(projector = SiteUserProjector)]
  pub struct SiteUser {
    #[twinoid(rename = "site")]
    site: TySite = Box<Site>,
    // `null` is possible, may mean that the user linked and unlinked his account
    #[twinoid(rename = "realId")]
    real_id: TyId = Option<TwinoidSiteUserId>,
    #[twinoid(rename = "user")]
    user: TyUser = Box<User>,
    // `null` if site no longer exists
    #[twinoid(rename = "link")]
    link: TyLink = Option<String>,
    #[twinoid(rename = "stats")]
    stats: TyStats = Option<Vec<Stat>>,
    #[twinoid(rename = "achievements")]
    achievements: TyAchievements = Option<Vec<Achievement>>,
    #[twinoid(rename = "points")]
    points: TyPoints = Option<i32>,
    #[twinoid(rename = "npoints")]
    npoints: TyNpoints = Option<f64>,
    #[twinoid(rename = "isBetaTester")]
    is_beta_tester: TyIsBetaTester = bool,
  }
}

declare_new_enum!(
  pub enum TwinoidSiteStatus {
    #[str("hide")]
    Hide,
    #[str("soon")]
    Soon,
    #[str("beta")]
    Beta,
    #[str("open")]
    Open,
  }
  pub type ParseError = TwinoidSiteStatusParseError;
  const SQL_NAME = "twinoid_site_status";
);

declare_twinoid_record! {
  #[twinoid(projector = SiteProjector)]
  pub struct Site {
    #[twinoid(required, rename = "id")]
    id: TyId = TwinoidSiteId,
    #[twinoid(rename = "name")]
    #[cfg_attr(feature = "serde", serde(default))]
    name: TyName = Restrict<String>,
    #[twinoid(rename = "host")]
    #[cfg_attr(feature = "serde", serde(default))]
    host: TyHost = Restrict<TwinoidSiteHost>,
    #[twinoid(rename = "icon")]
    #[cfg_attr(feature = "serde", serde(default))]
    icon: TyIcon = Restrict<UrlRef>,
    #[twinoid(rename = "lang")]
    #[cfg_attr(feature = "serde", serde(default))]
    lang: TyLang = Restrict<Option<TwinoidLocale>>,
    #[twinoid(rename = "like")]
    #[cfg_attr(feature = "serde", serde(default))]
    like: TyLike = Restrict<Like>,
    #[twinoid(rename = "infos")]
    #[cfg_attr(feature = "serde", serde(default))]
    infos: TyInfos = Restrict<Vec<SiteInfo>>,
    #[twinoid(rename = "me")]
    #[cfg_attr(feature = "serde", serde(default))]
    me: TyMe = Restrict<Box<User>>,
    #[twinoid(rename = "status")]
    #[cfg_attr(feature = "serde", serde(default))]
    status: TyStatus = Restrict<TwinoidSiteStatus>,
  }
}

impl Site {
  pub fn id_ref(id: TwinoidSiteId) -> Self {
    Self {
      id,
      name: Missing,
      host: Missing,
      icon: Missing,
      lang: Missing,
      like: Missing,
      infos: Missing,
      me: Missing,
      status: Missing,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
pub struct SiteIcon {
  pub tag: String,
  #[cfg_attr(feature = "serde", serde(rename = "altTag"))]
  pub alt_tag: Option<String>,
  pub url: PrUrl,
  #[cfg_attr(feature = "serde", serde(rename = "type"))]
  pub typ: String,
}

declare_twinoid_record! {
  #[twinoid(projector = SiteInfoProjector)]
  pub struct SiteInfo {
    #[twinoid(required, rename = "id")]
    id: TyId = TwinoidSiteInfoId,
    #[twinoid(rename = "site")]
    site: TySite = Box<Site>,
    #[twinoid(rename = "lang")]
    lang: TyIcon = TwinoidLocale,
    #[twinoid(rename = "cover")]
    cover: TyCover = UrlRef,
    #[twinoid(rename = "tagLine")]
    tag_line: TyTagLine = Option<HtmlFragment>,
    #[twinoid(rename = "description")]
    description: TyDescription = HtmlFragment,
    #[twinoid(rename = "tid")]
    tid: TyTid = HtmlFragment,
    #[twinoid(rename = "icons")]
    icons: TyIcons = Vec<SiteIcon>,
  }
}

declare_twinoid_record! {
  #[twinoid(projector = LikeProjector)]
  pub struct Like {
    #[twinoid(required, rename = "url")]
    url: TyUrl = Url,
    #[twinoid(rename = "likes")]
    likes: TyLikes = u32,
    #[twinoid(rename = "title")]
    title: TyTitle = Option<HtmlFragment>,
  }
}

declare_twinoid_record! {
  #[twinoid(projector = StatProjector)]
  pub struct Stat {
    #[twinoid(required, rename = "id")]
    id: TyId = String,
    #[twinoid(rename = "score")]
    score: TyScore = i32,
    #[twinoid(rename = "name")]
    name: TyName = String,
    #[twinoid(rename = "icon")]
    icon: TyIcon = Option<UrlRef>,
    #[twinoid(rename = "description")]
    description: TyDescription = Option<HtmlFragment>,
    #[twinoid(rename = "rare")]
    rare: TyRare = i32,
    #[twinoid(rename = "social")]
    social: TySocial = bool,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
pub struct AchievementData {
  #[cfg_attr(feature = "serde", serde(rename = "type"))]
  pub typ: String,
  pub title: Option<HtmlFragment>,
  pub url: Option<PrUrl>,
  pub prefix: Option<bool>,
  pub suffix: Option<bool>,
}

declare_twinoid_record! {
  #[twinoid(projector = AchievementProjector)]
  pub struct Achievement {
    #[twinoid(required, rename = "id")]
    id: TyId = String,
    #[twinoid(rename = "name")]
    name: TyName = String,
    #[twinoid(rename = "stat")]
    stat: TyStat = String,
    // Required stat value
    #[twinoid(rename = "score")]
    score: TyScore = i32,
    // Points for the game overall progress
    #[twinoid(rename = "points")]
    points: TyPoints = i32,
    // Normalized points
    #[twinoid(rename = "npoints")]
    npoints: TyNpoints = f64,
    #[twinoid(rename = "description")]
    description: TyDescription = HtmlFragment,
    #[twinoid(rename = "data")]
    data: TyData = AchievementData,
    #[twinoid(rename = "date")]
    date: TyDate = TwinoidDateTime,
    #[twinoid(rename = "index")]
    index: TyIndex = i32,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
pub struct ApplicationStat {
  date: Date,
  calls: i32,
  users: i32,
}

declare_twinoid_record! {
  #[twinoid(projector = ApplicationProjector)]
  pub struct Application {
    #[twinoid(required, rename = "id")]
    id: TyId = u32,
    #[twinoid(rename = "name")]
    name: TyName = String,
    #[twinoid(rename = "url")]
    url: TyUrl = Url,
    #[twinoid(rename = "owner")]
    owner: TyOwner = User,
    #[twinoid(rename = "picture")]
    picture: TyPicture = UrlRef,
    #[twinoid(rename = "icon")]
    icon: TyIcon = UrlRef,
    #[twinoid(rename = "description")]
    description: TyDescription = HtmlFragment,
    #[twinoid(rename = "access")]
    access: TyAccess = String,
    #[twinoid(rename = "dailyQuota")]
    daily_quota: TyDailyQuota = i32,
    #[twinoid(rename = "stats")]
    stats: TyStats = Vec<ApplicationStat>,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
pub struct NameRef {
  name: String,
}

declare_twinoid_record! {
  #[twinoid(projector = GroupProjector)]
  pub struct Group {
    #[twinoid(required, rename = "id")]
    id: TyId = u32,
    #[twinoid(rename = "name")]
    name: TyName = String,
    #[twinoid(rename = "link")]
    link: TyString = String,
    #[twinoid(rename = "banner")]
    banner: TyBanner = Option<UrlRef>,
    #[twinoid(rename = "roles")]
    roles: TyRoles = Vec<NameRef>,
    #[twinoid(rename = "owner")]
    owner: TyOwner = User,
    #[twinoid(rename = "members")]
    members: TyMembers = Vec<GroupMember>,
    #[twinoid(rename = "size")]
    size: TySize = i32,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(::serde::Serialize, ::serde::Deserialize))]
pub struct GroupRole {
  id: u32,
  name: String,
}

declare_twinoid_record! {
  #[twinoid(projector = GroupMemberProjector)]
  pub struct GroupMember {
    #[twinoid(rename = "group")]
    group: TyGroup = Group,
    #[twinoid(rename = "user")]
    user: TyUser = User,
    #[twinoid(rename = "title")]
    title: TyTitle = HtmlFragment,
    #[twinoid(rename = "role")]
    role: TyRole = GroupRole,
  }
}

declare_twinoid_record! {
  #[twinoid(projector = LangProjector)]
  pub struct Lang {
    #[twinoid(required, rename = "id")]
    id: TyId = String,
    #[twinoid(rename = "name")]
    name: TyName = String,
    #[twinoid(rename = "titleLinkWords")]
    title_link_words: TyTitleLinkWords = Vec<String>,
    #[twinoid(rename = "moderator")]
    moderator: TyModerator = String,
  }
}

declare_twinoid_record! {
  #[twinoid(projector = AppliScopeProjector)]
  pub struct AppliScope {
    #[twinoid(rename = "scope")]
    scope: TyScope = String,
    #[twinoid(rename = "name")]
    name: TyName = String,
    #[twinoid(rename = "site")]
    site: TySite = Option<Site>,
    #[twinoid(rename = "active")]
    active: TyActive = bool,
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::twinoid::api::{Missing, Skip};

  #[cfg(feature = "serde")]
  #[test]
  pub fn test_parse() {
    let actual: Like<Url, u32> = serde_json::from_str(r#"{"likes":91,"url":"http://twinoid.com/user/38"}"#).unwrap();
    let expected: Like<Url, u32> = Like {
      url: Url::parse("http://twinoid.com/user/38").unwrap(),
      likes: 91,
      title: Missing,
    };
    assert_eq!(actual, expected);
  }

  #[test]
  pub fn project0() {
    let projector = LikeProjector {
      url: Select,
      likes: Select,
      title: Skip,
    };
    let actual = Projector::<Like>::to_query(&projector);
    let expected = "likes";
    assert_eq!(actual, expected);
  }

  #[test]
  pub fn project1() {
    let projector = LikeProjector {
      url: Select,
      likes: Select,
      title: Select,
    };
    let actual = Projector::<Like>::to_query(&projector);
    let expected = "likes,title";
    assert_eq!(actual, expected);
  }

  mod restrict {
    use super::super::Restrict;
    use super::*;
    use crate::core::HtmlFragment;
    use crate::twinoid::api::Like;
    use crate::twinoid::{TwinoidUserId, TwinoidUserIdRef};

    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
    #[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
    pub struct MinUser {
      id: u32,
      #[cfg_attr(feature = "serde", serde(default))]
      secret: Restrict<String>,
    }

    #[cfg(feature = "serde")]
    #[test]
    pub fn restrict_deser_allow() {
      let input = r#"{"id": 123, "secret": "hunter2"}"#;
      let actual: MinUser = serde_json::from_str(input).unwrap();
      let expected = MinUser {
        id: 123,
        secret: Restrict::Allow("hunter2".to_string()),
      };

      assert_eq!(actual, expected);
    }

    #[cfg(feature = "serde")]
    #[test]
    pub fn restrict_deser_deny() {
      let input = r#"{"id": 123}"#;
      let actual: MinUser = serde_json::from_str(input).unwrap();
      let expected = MinUser {
        id: 123,
        secret: Restrict::Deny,
      };

      assert_eq!(actual, expected);
    }

    #[cfg(feature = "serde")]
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
    struct State {
      contacts: Restrict<Vec<TwinoidUserIdRef>>,
    }

    #[cfg(feature = "serde")]
    #[test]
    pub fn restrict_ser_allow_array() {
      let input = State {
        contacts: Restrict::Allow(vec![
          TwinoidUserIdRef {
            id: TwinoidUserId::new(1).unwrap(),
          },
          TwinoidUserIdRef {
            id: TwinoidUserId::new(2).unwrap(),
          },
        ]),
      };

      let actual = serde_json::to_string(&input).unwrap();
      let expected = r#"{"contacts":[{"type":"TwinoidUser","id":"1"},{"type":"TwinoidUser","id":"2"}]}"#;
      assert_eq!(actual, expected);
    }

    #[cfg(feature = "serde")]
    #[test]
    pub fn restrict_ser() {
      type RestrictedLike = Like<Url, u32, Restrict<Option<HtmlFragment>>>;

      {
        let input: RestrictedLike = Like {
          url: Url::parse("http://localhost/example").unwrap(),
          likes: 123,
          title: Restrict::Allow(Some("Example".to_string())),
        };
        let actual = serde_json::to_string(&input).unwrap();
        let expected = r#"{"url":"http://localhost/example","likes":123,"title":"Example"}"#;
        assert_eq!(actual, expected);
      }
      {
        let input: RestrictedLike = Like {
          url: Url::parse("http://localhost/example").unwrap(),
          likes: 123,
          title: Restrict::Allow(None),
        };
        let actual = serde_json::to_string(&input).unwrap();
        let expected = r#"{"url":"http://localhost/example","likes":123,"title":null}"#;
        assert_eq!(actual, expected);
      }
      {
        let input: RestrictedLike = Like {
          url: Url::parse("http://localhost/example").unwrap(),
          likes: 123,
          title: Restrict::Deny,
        };
        let actual = serde_json::to_string(&input).unwrap();
        let expected = r#"{"url":"http://localhost/example","likes":123}"#;
        assert_eq!(actual, expected);
      }
    }
  }
}
