use crate::core::{Instant, Request};
use crate::twinoid::{ShortTwinoidUser, TwinoidUserIdRef};
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum GetShortUserError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetShortUser {
  pub user: TwinoidUserIdRef,
  pub time: Instant,
}

impl Request for GetShortUser {
  type Response = Result<ShortTwinoidUser, GetShortUserError>;
}
