use crate::core::Request;
use crate::twinoid::ShortTwinoidUser;
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum TouchShortUserError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

pub struct TouchShortUser {
  pub user: ShortTwinoidUser,
}

impl Request for TouchShortUser {
  type Response = Result<(), TouchShortUserError>;
}
