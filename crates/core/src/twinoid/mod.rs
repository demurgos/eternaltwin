pub mod api;
pub mod client;
pub mod store;

pub use self::client::TwinoidClient;
pub use self::store::TwinoidStore;
use crate::core::{Instant, LocaleId};
use crate::email::EmailAddress;
use crate::link::VersionedEtwinLink;
use crate::oauth::RfcOauthAccessTokenKey;
use crate::temporal::LatestTemporal;
pub use client::FullSite;
use once_cell::sync::Lazy;
use regex::Regex;

declare_decimal_id! {
  pub struct TwinoidUserId(u32);
  pub type ParseError = TwinoidUserIdParseError;
  const BOUNDS = 1..1_000_000_000;
  // const SQL_NAME = "twinoid_user_id_old";
}

impl TwinoidUserId {
  pub fn as_ref(self) -> TwinoidUserIdRef {
    TwinoidUserIdRef { id: self }
  }
}

declare_new_string! {
  pub struct TwinoidUserDisplayName(String);
  pub type ParseError = TwinoidUserDisplayNameParseError;
  const PATTERN = r"^.{1,100}$";
  const SQL_NAME = "twinoid_user_display_name";
}

declare_new_string! {
  pub struct TwinoidDateTime(String);
  pub type ParseError = TwinoidDateTimeParseError;
  const PATTERN = r"^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$";
  const SQL_NAME = "twinoid_date_time";
}

declare_new_string! {
  pub struct TwinoidSiteHost(String);
  pub type ParseError = TwinoidSiteHostParseError;
  const PATTERN = r"^[0-9a-z-]+(\.[0-9a-z-]+)+$";
  const SQL_NAME = "twinoid_site_host";
}

impl TwinoidDateTime {
  pub fn from_instant(t: Instant) -> Self {
    const TWINOID_FORMAT: &str = "%F %T";
    Self(t.into_chrono().format(TWINOID_FORMAT).to_string())
  }

  pub fn into_instant(&self) -> Instant {
    static TWINOID_DATE_TIME_PATTERN: Lazy<Regex> =
      Lazy::new(|| Regex::new(r"^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$").unwrap());
    let captures = TWINOID_DATE_TIME_PATTERN
      .captures(self.as_str())
      .expect("`TwinoidDateTime` always matches its pattern");
    let y: i32 = captures
      .get(1)
      .expect("`year` component is present")
      .as_str()
      .parse()
      .expect("`year` component is decimal");
    let m: u32 = captures
      .get(2)
      .expect("`month` component is present")
      .as_str()
      .parse()
      .expect("`month` component is decimal");
    let d: u32 = captures
      .get(3)
      .expect("`day` component is present")
      .as_str()
      .parse()
      .expect("`day` component is decimal");
    let h: u32 = captures
      .get(4)
      .expect("`hour` component is present")
      .as_str()
      .parse()
      .expect("`hour` component is decimal");
    let min: u32 = captures
      .get(5)
      .expect("`minute` component is present")
      .as_str()
      .parse()
      .expect("`minute` component is decimal");
    let s: u32 = captures
      .get(6)
      .expect("`second` component is present")
      .as_str()
      .parse()
      .expect("`second` component is decimal");
    Instant::ymd_hms(y, m, d, h, min, s)
  }
}

declare_new_string! {
  /// A Twinoid session key.
  ///
  /// It correspond to the value of the `tw_sid` cookie.
  ///
  /// - `LuTuRdIOmZU4JVOt7U70bQzEzzSlkAG2`
  pub struct TwinoidSessionKey(String);
  pub type ParseError = TwinoidSessionKeyParseError;
  const PATTERN = r"^[0-9a-zA-Z]{32}$";
  const SQL_NAME = "twinoid_session_key";
}

declare_new_int! {
  pub struct TwinoidSiteId(u32);
  pub type RangeError = TwinoidSiteIdRangeError;
  const BOUNDS = 0..1_000_000;
  type SqlType = crate::pg_num::PgU32;
  const SQL_NAME = "twinoid_site_id";
}

declare_new_int! {
  pub struct TwinoidSiteUserId(u32);
  pub type RangeError = TwinoidSiteUserIdRangeError;
  const BOUNDS = 0..1_000_000_000;
  type SqlType = crate::pg_num::PgU32;
  const SQL_NAME = "twinoid_site_user_id";
}

declare_new_int! {
  pub struct TwinoidSiteInfoId(u32);
  pub type RangeError = TwinoidSiteInfoIdRangeError;
  const BOUNDS = 0..1_000_000;
  type SqlType = crate::pg_num::PgU32;
  const SQL_NAME = "twinoid_site_info_id";
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidUser"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortTwinoidUser {
  pub id: TwinoidUserId,
  pub display_name: TwinoidUserDisplayName,
}

impl ShortTwinoidUser {
  pub fn as_ref(&self) -> TwinoidUserIdRef {
    TwinoidUserIdRef { id: self.id }
  }
}

impl From<ArchivedTwinoidUser> for ShortTwinoidUser {
  fn from(value: ArchivedTwinoidUser) -> Self {
    Self {
      id: value.id,
      display_name: value.display_name,
    }
  }
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidUser"))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidUserIdRef {
  pub id: TwinoidUserId,
}

impl From<TwinoidUserId> for TwinoidUserIdRef {
  fn from(id: TwinoidUserId) -> Self {
    Self { id }
  }
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidSite"))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidSiteIdRef {
  pub id: TwinoidSiteId,
}

impl From<TwinoidSiteId> for TwinoidSiteIdRef {
  fn from(id: TwinoidSiteId) -> Self {
    Self { id }
  }
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidSiteUser"))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidSiteUserIdRef {
  pub site: TwinoidSiteIdRef,
  pub id: TwinoidSiteUserId,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidUser"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ArchivedTwinoidUser {
  pub id: TwinoidUserId,
  pub archived_at: Instant,
  pub display_name: TwinoidUserDisplayName,
  pub links: Vec<TwinoidLink>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidUser"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EtwinTwinoidUser {
  pub id: TwinoidUserId,
  pub archived_at: Instant,
  pub display_name: TwinoidUserDisplayName,
  pub links: Vec<TwinoidLink>,
  pub etwin: VersionedEtwinLink,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidLink {
  pub site: ShortTwinoidSite,
  pub user: LatestTemporal<Option<TwinoidLinkUser>>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidSite"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortTwinoidSite {
  pub id: TwinoidSiteId,
  pub host: Option<TwinoidSiteHost>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "TwinoidSiteUser"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidLinkUser {
  pub id: TwinoidSiteUserId,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetTwinoidUserOptions {
  pub id: TwinoidUserId,
  pub time: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TwinoidApiAuth {
  Guest,
  Token(RfcOauthAccessTokenKey),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidPassword(String);

impl TwinoidPassword {
  pub fn new(raw: String) -> Self {
    Self(raw)
  }

  pub fn as_str(&self) -> &str {
    &self.0
  }
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidCredentials {
  pub email: EmailAddress,
  pub password: TwinoidPassword,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TwinoidSession {
  pub key: TwinoidSessionKey,
  pub user: ShortTwinoidUser,
}

declare_new_enum!(
  pub enum TwinoidLocale {
    #[str("de")]
    De,
    #[str("en")]
    En,
    #[str("es")]
    Es,
    #[str("fr")]
    Fr,
    #[str("it")]
    It,
    #[str("pt")]
    Pt,
  }
  pub type ParseError = TwinoidLocaleParseError;
  const SQL_NAME = "twinoid_locale";
);

impl TwinoidLocale {
  pub fn into_locale_id(self) -> LocaleId {
    match self {
      Self::De => LocaleId::DeDe,
      Self::En => LocaleId::EnUs,
      Self::Es => LocaleId::EsSp,
      Self::Fr => LocaleId::FrFr,
      Self::It => LocaleId::ItIt,
      Self::Pt => LocaleId::PtPt,
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use std::str::FromStr;

  #[test]
  fn twinoid_date_time_from_instant() {
    let input = Instant::ymd_hms(2020, 12, 30, 23, 35, 51);
    let actual = TwinoidDateTime::from_instant(input);
    let expected = TwinoidDateTime::from_str("2020-12-30 23:35:51").unwrap();
    assert_eq!(actual, expected);
  }
}
