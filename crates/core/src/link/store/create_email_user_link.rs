#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawCreateEmailUserLink {
  pub now: Instant,
  pub actor: UserIdRef,
  pub user_id: UserIdRef,
  pub email_address: EmailAddress,
  pub visibility: UserLinkVisibility,
  pub rank: u16,
  // pub proof: Option<...>
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawCreateEmailUserLinkError {
  #[error("already linked with user {0:?}")]
  Conflict(UserIdRef),
  #[error(transparent)]
  Other(#[source] WeakError),
}

impl Request for RawCreateEmailUserLink {
  type Response = Result<AnyUserLink<EmailUserLink>, WeakError>;
}
