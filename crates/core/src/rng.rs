use crate::types::WeakError;
use core::ops::Deref;

/// Random number generator
pub trait Rng: Send + Sync {
  fn try_fill_bytes(&self, dest: &mut [u8]) -> Result<(), WeakError>;
}

/// Like [`Deref`], but the target has the bound [`Rng`]
pub trait RngRef: Send + Sync {
  type Rng: Rng + ?Sized;

  fn rng(&self) -> &Self::Rng;
}

impl<TyRef> RngRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: Rng,
{
  type Rng = TyRef::Target;

  fn rng(&self) -> &Self::Rng {
    self.deref()
  }
}

/// RNG implementation using the `getrandom` crate.
pub struct GetRandomRng;

impl Rng for GetRandomRng {
  fn try_fill_bytes(&self, dest: &mut [u8]) -> Result<(), WeakError> {
    getrandom::getrandom(dest).map_err(|e| WeakError::new(e.to_string()))
  }
}
