mod sqlx_error;

pub use sqlx_error::{SafeDatabaseError, SafePgDatabaseError, SafePgSeverity, SafeSqlxError};

use core::fmt;
use core::fmt::Debug;
use core::num::NonZeroU32;
use core::str::FromStr;
use core::sync::atomic::AtomicU16;
use futures_util::stream::StreamExt;
use include_dir::Dir;
use once_cell::sync::Lazy;
use petgraph::algo::astar;
use petgraph::graphmap::DiGraphMap;
use regex::Regex;
use serde::{Deserialize, Serialize};
use sqlx::{Connection, Executor};
use sqlx::{PgPool, Postgres, Transaction};
use std::cmp::{max, Ordering};
use std::collections::HashMap;
use std::error::Error;

static SQL_NODE_PATTERN: Lazy<Regex> = Lazy::new(|| Regex::new(r"^([0-9]{1,4})\.sql$").unwrap());
static SQL_EDGE_PATTERN: Lazy<Regex> = Lazy::new(|| Regex::new(r"^([0-9]{1,4})-([0-9]{1,4})\.sql$").unwrap());
const POSTGRESQL_FUNCTION_NOT_FOUND_ERROR_CODE: &str = "42883";

/// Opaque handle representing a database state recognized by the issuing resolver.
#[derive(Clone, Copy)]
pub struct SchemaStateRef<'r> {
  resolver: &'r SchemaResolver,
  state: SchemaState,
}

impl<'r> SchemaStateRef<'r> {
  pub const fn state(self) -> SchemaState {
    self.state
  }
}

impl<'r> PartialEq for SchemaStateRef<'r> {
  fn eq(&self, other: &Self) -> bool {
    std::ptr::eq(self.resolver, other.resolver) && self.state == other.state
  }
}

impl<'r> Eq for SchemaStateRef<'r> {}

impl<'r> Debug for SchemaStateRef<'r> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.debug_struct("SchemaStateRef")
      .field("resolver", &(self.resolver as *const _))
      .field("state", &self.state)
      .finish()
  }
}

/// Opaque handle representing a database version recognized by the issuing resolver.
#[derive(Clone, Copy)]
pub struct SchemaVersionRef<'r> {
  resolver: &'r SchemaResolver,
  version: SchemaVersion,
}

impl<'r> PartialEq for SchemaVersionRef<'r> {
  fn eq(&self, other: &Self) -> bool {
    std::ptr::eq(self.resolver, other.resolver) && self.version == other.version
  }
}

impl<'r> Eq for SchemaVersionRef<'r> {}

impl<'r> Debug for SchemaVersionRef<'r> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "SchemaVersionRef {{ resolver: {:?}, version: {:?} }}",
      self.resolver as *const _, self.version
    )
  }
}

impl<'r> From<SchemaVersionRef<'r>> for SchemaStateRef<'r> {
  fn from(version: SchemaVersionRef<'r>) -> Self {
    Self {
      resolver: version.resolver,
      state: version.version.into(),
    }
  }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum SchemaState {
  Empty,
  Version(SchemaVersion),
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SchemaVersion(NonZeroU32);

impl FromStr for SchemaVersion {
  type Err = ();

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let version = s.parse::<u32>().map_err(|_| ())?;
    let version = NonZeroU32::new(version).ok_or(())?;
    Ok(Self(version))
  }
}

impl From<SchemaVersion> for SchemaState {
  fn from(version: SchemaVersion) -> Self {
    Self::Version(version)
  }
}

pub struct SchemaMigration<'a> {
  resolver: &'a SchemaResolver,
  states: Vec<SchemaState>,
}

impl<'r> Debug for SchemaMigration<'r> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "SchemaMigration {{ resolver: {:?}, states: [",
      self.resolver as *const _
    )?;
    for (i, s) in self.states.iter().enumerate() {
      write!(f, "{}{:?}", if i == 0 { "" } else { ", " }, s)?;
    }
    write!(f, "] }}")
  }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
struct SaturatingU32(u32);

impl SaturatingU32 {
  const MAX: Self = Self(u32::MAX);
}

impl core::ops::Add for SaturatingU32 {
  type Output = SaturatingU32;

  fn add(self, rhs: Self) -> Self::Output {
    SaturatingU32(u32::saturating_add(self.0, rhs.0))
  }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct SchemaMeta {
  version: NonZeroU32,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum MigrationDirection {
  UpgradeOnly,
  DowngradeOnly,
}

#[derive(Debug)]
struct EdgeState {
  /// SQL script to transition the schema version
  schema: &'static str,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum TxApplyEdgeError {
  #[error("failed to retrieve initial state")]
  GetInitialState(#[source] GetStateError),
  #[error("failed to set schema meta")]
  SetSchemaMeta(#[source] TxSetSchemaMetaError),
  #[error("failed to retrieve final state")]
  GetFinalState(#[source] GetStateError),
  #[error("failed to apply migration from state {1:?} to {2:?}")]
  Apply(#[source] SafeSqlxError, SchemaState, SchemaState),
}

#[derive(Debug)]
pub struct SchemaResolver {
  graph: DiGraphMap<SchemaState, EdgeState>,
  states: HashMap<SchemaState, ()>,
  latest: SchemaVersion,
  drop: Option<&'static str>,
  grant: Option<&'static str>,
  latest_force_created_state: tokio::sync::RwLock<Option<SchemaState>>,
  cache_buster_len: AtomicU16,
}

impl SchemaResolver {
  pub fn new(d: &'static Dir) -> Self {
    let mut graph: DiGraphMap<SchemaState, EdgeState> = DiGraphMap::new();
    let mut states: HashMap<SchemaState, ()> = HashMap::new();
    graph.add_node(SchemaState::Empty);
    states.insert(SchemaState::Empty, ());
    let mut latest = SchemaState::Empty;
    if let Some(create_dir) = d.get_dir("create") {
      for f in create_dir.files() {
        let file_name: &str = f.path().file_name().unwrap().to_str().unwrap();
        let caps = if let Some(caps) = SQL_NODE_PATTERN.captures(file_name) {
          caps
        } else {
          eprintln!("Unexpected file name format: {:?}", f.path());
          continue;
        };
        let schema = f.contents_utf8().unwrap();
        let edge = EdgeState { schema };
        let state: SchemaState = caps[1].parse::<SchemaVersion>().unwrap().into();
        graph.add_node(state);
        assert!(states.insert(state, ()).is_none());
        graph.add_edge(SchemaState::Empty, state, edge);
        latest = max(latest, state);
      }
    }
    if let Some(upgrade_dir) = d.get_dir("upgrade") {
      for f in upgrade_dir.files() {
        let file_name: &str = f.path().file_name().unwrap().to_str().unwrap();
        let caps = if let Some(caps) = SQL_EDGE_PATTERN.captures(file_name) {
          caps
        } else {
          eprintln!("Unexpected file name format: {:?}", f.path());
          continue;
        };
        let schema = f.contents_utf8().unwrap();
        let edge = EdgeState { schema };
        let start: SchemaState = caps[1].parse::<SchemaVersion>().unwrap().into();
        let end: SchemaState = caps[2].parse::<SchemaVersion>().unwrap().into();
        #[allow(clippy::nonminimal_bool)] // This reads better IMO
        if !(start < end) {
          panic!("invalid migration file name {file_name:?}");
        }
        graph.add_node(start);
        states.insert(start, ());
        graph.add_node(end);
        states.insert(end, ());
        graph.add_edge(start, end, edge);
        latest = max(latest, end);
      }
    }
    let drop = d
      .get_file("drop.sql")
      .map(|f| f.contents_utf8().expect("Invalid drop script encoding"));
    let grant = d
      .get_file("grant.sql")
      .map(|f| f.contents_utf8().expect("Invalid drop script encoding"));
    let latest = match latest {
      SchemaState::Empty => panic!("No schema version found"),
      SchemaState::Version(v) => v,
    };
    SchemaResolver {
      graph,
      states,
      latest,
      drop,
      grant,
      latest_force_created_state: tokio::sync::RwLock::new(None),
      cache_buster_len: AtomicU16::new(0),
    }
  }

  pub fn get_version(&self, v: NonZeroU32) -> Option<SchemaVersionRef> {
    let version = SchemaVersion(v);
    if self.states.contains_key(&version.into()) {
      Some(self.issue_version(version))
    } else {
      None
    }
  }

  pub fn get_empty(&self) -> SchemaStateRef {
    self.issue_state(SchemaState::Empty)
  }

  pub fn get_latest(&self) -> SchemaVersionRef {
    self.issue_version(self.latest)
  }

  fn validate_state(&self, state: SchemaStateRef) -> SchemaState {
    assert!(std::ptr::eq(self, state.resolver));
    state.state
  }

  fn validate_version(&self, version: SchemaVersionRef) -> SchemaVersion {
    assert!(std::ptr::eq(self, version.resolver));
    version.version
  }

  fn issue_state(&self, state: SchemaState) -> SchemaStateRef {
    SchemaStateRef { resolver: self, state }
  }

  fn issue_version(&self, version: SchemaVersion) -> SchemaVersionRef {
    SchemaVersionRef {
      resolver: self,
      version,
    }
  }

  pub fn create_migration(
    &self,
    start: SchemaStateRef,
    end: SchemaVersionRef,
    dir: MigrationDirection,
  ) -> Option<SchemaMigration> {
    let start = self.validate_state(start);
    let end = self.validate_version(end).into();
    self.inner_create_migration(start, end, dir)
  }

  pub async fn apply_migration(&self, db: &PgPool, migration: &'_ SchemaMigration<'_>) -> Result<(), Box<dyn Error>> {
    let mut tx = db.begin().await?;
    self.tx_apply_migration(&mut tx, migration).await?;
    tx.commit().await?;
    Ok(())
  }

  fn inner_create_migration(
    &self,
    start: SchemaState,
    end: SchemaState,
    dir: MigrationDirection,
  ) -> Option<SchemaMigration> {
    #[allow(clippy::type_complexity)]
    let edge_cost: Box<dyn FnMut((SchemaState, SchemaState, &EdgeState)) -> SaturatingU32> = match dir {
      MigrationDirection::UpgradeOnly => Box::new(
        |(source, target, _): (SchemaState, SchemaState, &EdgeState)| -> SaturatingU32 {
          match source.cmp(&target) {
            Ordering::Less => SaturatingU32(1),
            Ordering::Equal => SaturatingU32(0),
            Ordering::Greater => SaturatingU32::MAX,
          }
        },
      ),
      MigrationDirection::DowngradeOnly => Box::new(
        |(source, target, _): (SchemaState, SchemaState, &EdgeState)| -> SaturatingU32 {
          match source.cmp(&target) {
            Ordering::Less => SaturatingU32::MAX,
            Ordering::Equal => SaturatingU32(0),
            Ordering::Greater => SaturatingU32(1),
          }
        },
      ),
    };
    let estimate_cost = match dir {
      MigrationDirection::UpgradeOnly => |_| SaturatingU32(1),
      MigrationDirection::DowngradeOnly => |_| SaturatingU32(1),
    };
    let (cost, path) = astar(&self.graph, start, |n| n == end, edge_cost, estimate_cost)?;
    if cost == SaturatingU32::MAX {
      return None;
    }
    let migration = SchemaMigration {
      resolver: self,
      states: path,
    };
    Some(migration)
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetStateError {
  #[error("failed to begin DB transaction")]
  BeginTx(#[source] SafeSqlxError),
  #[error("failed to retrieve schema")]
  GetSchemaMeta(#[source] GetSchemaMetaError),
  #[error("failed to commit DB transaction")]
  CommitTx(#[source] SafeSqlxError),
}

impl SchemaResolver {
  pub async fn get_state(&self, db: &PgPool) -> Result<SchemaStateRef<'_>, GetStateError> {
    let mut tx = db
      .begin()
      .await
      .map_err(|e| GetStateError::BeginTx(SafeSqlxError::from(e)))?;
    let state = self.inner_get_state(&mut tx).await?;
    tx.commit()
      .await
      .map_err(|e| GetStateError::CommitTx(SafeSqlxError::from(e)))?;
    Ok(self.issue_state(state))
  }

  async fn inner_get_state(&self, tx: &mut Transaction<'_, Postgres>) -> Result<SchemaState, GetStateError> {
    let meta: Option<SchemaMeta> = self
      .inner_get_schema_meta_from_fn(&mut *tx)
      .await
      .map_err(GetStateError::GetSchemaMeta)?;
    let state: SchemaState = match meta {
      None => SchemaState::Empty,
      Some(meta) => SchemaState::Version(SchemaVersion(meta.version)),
    };
    assert!(self.states.contains_key(&state));
    Ok(state)
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetSchemaMetaError {
  #[error("failed to begin DB transaction")]
  BeginTx(#[source] SafeSqlxError),
  #[error("unexpected failure during DB fetch")]
  Fetch(#[source] SafeSqlxError),
  #[error("unexpected Postgres failure during DB fetch")]
  FetchPostgres(#[source] Box<SafePgDatabaseError>),
  #[error("failed to read DB schema value")]
  Read(#[source] ReadSchemaMetaError),
  #[error("failed to commit DB transaction")]
  CommitTx(#[source] SafeSqlxError),
  #[error("failed to rollback DB transaction")]
  RollbackTx(#[source] SafeSqlxError),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum ReadSchemaMetaError {
  #[error("`version` field must be unsigned and non-zero, actual = {0}")]
  Version(i32),
}

impl SchemaResolver {
  /// Retrieve the schema metadata using the `get_schema_meta` function.
  ///
  /// As of 2024-07, this is the recommended method to retrieve the current schema state.
  async fn inner_get_schema_meta_from_fn(
    &self,
    db: &mut Transaction<'_, Postgres>,
  ) -> Result<Option<SchemaMeta>, GetSchemaMetaError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      version: i32,
    }

    impl TryFrom<Row> for SchemaMeta {
      type Error = ReadSchemaMetaError;

      fn try_from(row: Row) -> Result<Self, Self::Error> {
        let version = u32::try_from(row.version).map_err(|_| ReadSchemaMetaError::Version(row.version))?;
        let version = NonZeroU32::try_from(version).map_err(|_| ReadSchemaMetaError::Version(row.version))?;
        Ok(Self { version })
      }
    }

    // todo: check if nested transactions are supported
    let mut tx = db
      .begin()
      .await
      .map_err(|e| GetSchemaMetaError::BeginTx(SafeSqlxError::from(e)))?;

    let res: Result<Row, SafeSqlxError> = sqlx::query_as("SELECT version FROM get_schema_meta();")
      .fetch_one(&mut *tx)
      .await
      .map_err(SafeSqlxError::from);

    let mut commit_tx: bool = true;
    let res: Result<Option<SchemaMeta>, GetSchemaMetaError> = match res {
      Ok(row) => SchemaMeta::try_from(row).map(Some).map_err(GetSchemaMetaError::Read),
      Err(SafeSqlxError::Database(e)) => match e {
        SafeDatabaseError::Postgres(e) => match e.code {
          code if code == POSTGRESQL_FUNCTION_NOT_FOUND_ERROR_CODE => {
            commit_tx = false;
            Ok(None)
          }
          _ => Err(GetSchemaMetaError::FetchPostgres(e)),
        },
        e => Err(GetSchemaMetaError::Fetch(SafeSqlxError::Database(e))),
      },
      Err(e) => Err(GetSchemaMetaError::Fetch(e)),
    };

    if commit_tx {
      tx.commit()
        .await
        .map_err(|e| GetSchemaMetaError::CommitTx(SafeSqlxError::from(e)))?;
    } else {
      tx.rollback()
        .await
        .map_err(|e| GetSchemaMetaError::RollbackTx(SafeSqlxError::from(e)))?;
    }
    res
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum TxSetSchemaMetaError {
  #[error("failed to execute schema meta update statement at index {1}")]
  Statement(#[source] SafeSqlxError, u64),
}

impl SchemaResolver {
  async fn tx_set_schema_meta(
    &self,
    tx: &mut Transaction<'_, Postgres>,
    meta: &SchemaMeta,
  ) -> Result<(), TxSetSchemaMetaError> {
    // Workaround for PG issue #17053 causing a memory corruption issue when the
    // request below is used as a prepared statement. The workaround is to
    // slightly change the request each time by adding whitespace.
    // <https://www.postgresql.org/message-id/17053-3ca3f501bbc212b4%40postgresql.org>
    let cache_buster_len = self.cache_buster_len.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
    let cache_buster = " ".repeat(cache_buster_len.into());
    let create_type = format!(
      "CREATE DOMAIN schema_meta AS raw_schema_meta CHECK ((value).version IS NOT NULL AND (value).version >= 1){};",
      cache_buster
    );

    let create_meta_fn = format!("CREATE FUNCTION get_schema_meta() RETURNS schema_meta LANGUAGE sql IMMUTABLE STRICT PARALLEL SAFE AS $$ SELECT ROW({version})::SCHEMA_META; $$;", version = meta.version);

    let queries = [
      "DROP FUNCTION IF EXISTS get_schema_meta;",
      "DROP TYPE IF EXISTS schema_meta;",
      "DROP TYPE IF EXISTS raw_schema_meta;",
      "CREATE TYPE raw_schema_meta AS (version int4);",
      create_type.as_str(),
      create_meta_fn.as_str(),
    ];

    for (i, query) in queries.into_iter().enumerate() {
      let i = u64::try_from(i).expect("`i` should fit `u64`");
      sqlx::query(query)
        .execute(&mut **tx)
        .await
        .map_err(|e| TxSetSchemaMetaError::Statement(SafeSqlxError::from(e), i))?;
    }

    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum EmptyError {
  #[error("failed to begin DB transaction")]
  BeginTx(#[source] SafeSqlxError),
  #[error("failed to execute drop statement at index {1}")]
  DropStatement(#[source] SafeSqlxError, u64),
  #[error("failed to execute grant statement at index {1}")]
  GrantStatement(#[source] SafeSqlxError, u64),
  #[error("failed to commit DB transaction")]
  CommitTx(#[source] SafeSqlxError),
}

impl SchemaResolver {
  /// Remove all data from the current schema: tables, types, functions, etc.
  pub async fn empty(&self, db: &PgPool) -> Result<(), EmptyError> {
    let mut tx = db
      .begin()
      .await
      .map_err(|e| EmptyError::BeginTx(SafeSqlxError::from(e)))?;
    self.tx_empty(&mut tx).await?;
    tx.commit()
      .await
      .map_err(|e| EmptyError::CommitTx(SafeSqlxError::from(e)))?;
    Ok(())
  }

  /// Remove all data from the current schema: tables, types, functions, etc.
  async fn tx_empty(&self, tx: &mut Transaction<'_, Postgres>) -> Result<(), EmptyError> {
    if let Some(drop_sql) = self.drop {
      let mut stream = tx.execute_many(drop_sql);
      let mut i: u64 = 0;
      while let Some(r) = stream.next().await {
        r.map_err(|e| EmptyError::DropStatement(SafeSqlxError::from(e), i))?;
        i += 1;
      }
      drop(stream);
    }
    if let Some(grant_sql) = self.grant {
      let mut stream = tx.execute_many(grant_sql);
      let mut i: u64 = 0;
      while let Some(r) = stream.next().await {
        r.map_err(|e| EmptyError::GrantStatement(SafeSqlxError::from(e), i))?;
        i += 1;
      }
    }
    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum TruncateError {
  #[error("failed to resolve table list to truncate from current schema")]
  Resolve(#[source] SafeSqlxError),
  #[error("`truncate` call failed")]
  Truncate(#[source] SafeSqlxError),
}

impl SchemaResolver {
  /// Truncate all tables in the current schema
  ///
  /// The current schema is resolved using [`current_schema()`](https://www.postgresql.org/docs/current/functions-info.html#FUNCTIONS-INFO-SESSION-TABLE).
  async fn tx_truncate_all(&self, tx: &mut Transaction<'_, Postgres>) -> Result<(), TruncateError> {
    // language=postgresql
    let row: Vec<(String,)> = sqlx::query_as(
      r"
      SELECT tablename
      FROM   pg_catalog.pg_tables
      WHERE  schemaname = CURRENT_SCHEMA();
    ",
    )
    .fetch_all(&mut **tx)
    .await
    .map_err(|e| TruncateError::Resolve(SafeSqlxError::from(e)))?;

    let row: Vec<&str> = row.iter().map(|r| r.0.as_str()).collect();

    self.tx_truncate(tx, &row).await
  }

  /// Truncate all tables in the provided list.
  async fn tx_truncate(&self, tx: &mut Transaction<'_, Postgres>, tables: &[&str]) -> Result<(), TruncateError> {
    let mut query = String::new();
    query.push_str("TRUNCATE TABLE ");
    for (i, table) in tables.iter().cloned().enumerate() {
      if i != 0 {
        query.push_str(", ");
      }
      query.push('"');
      query.push_str(table);
      query.push('"');
    }
    query.push_str(" CASCADE;");

    sqlx::query(&query)
      .execute(&mut **tx)
      .await
      .map_err(|e| TruncateError::Truncate(SafeSqlxError::from(e)))?;

    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum ForceCreateError {
  #[error("failed to begin DB transaction")]
  BeginTx(#[source] SafeSqlxError),
  #[error("failed to retrieve current DB state")]
  GetState(#[source] GetStateError),
  #[error("failed to truncate existing tables in the current schema")]
  TruncateAll(#[source] TruncateError),
  #[error("failed to empty database")]
  Empty(#[source] EmptyError),
  #[error("failed to apply migration")]
  ApplyMigration(#[source] TxApplyMigrationError),
  #[error("failed to commit DB transaction")]
  CommitTx(#[source] SafeSqlxError),
}

impl SchemaResolver {
  pub async fn force_create_latest(&self, db: &PgPool, void: bool) -> Result<(), ForceCreateError> {
    self.inner_force_create(db, self.latest.into(), void).await
  }

  pub async fn force_create(&self, db: &PgPool, state: SchemaStateRef<'_>, void: bool) -> Result<(), ForceCreateError> {
    self.inner_force_create(db, self.validate_state(state), void).await
  }

  async fn inner_force_create(&self, db: &PgPool, state: SchemaState, void: bool) -> Result<(), ForceCreateError> {
    let mut tx = db
      .begin()
      .await
      .map_err(|e| ForceCreateError::BeginTx(SafeSqlxError::from(e)))?;
    let mut latest_force_created_state = self.latest_force_created_state.write().await;
    if void && *latest_force_created_state == Some(state) {
      let cur = self
        .inner_get_state(&mut tx)
        .await
        .map_err(ForceCreateError::GetState)?;
      if cur == state {
        self
          .tx_truncate_all(&mut tx)
          .await
          .map_err(ForceCreateError::TruncateAll)?;
        tx.commit()
          .await
          .map_err(|e| ForceCreateError::CommitTx(SafeSqlxError::from(e)))?;
        return Ok(());
      }
    }

    let migration = self
      .inner_create_migration(SchemaState::Empty, state, MigrationDirection::UpgradeOnly)
      .expect("Unreachable state from empty DB");
    self.tx_empty(&mut tx).await.map_err(ForceCreateError::Empty)?;
    self
      .tx_apply_migration(&mut tx, &migration)
      .await
      .map_err(ForceCreateError::ApplyMigration)?;
    *latest_force_created_state = Some(state);
    tx.commit()
      .await
      .map_err(|e| ForceCreateError::CommitTx(SafeSqlxError::from(e)))?;
    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum TxApplyMigrationError {
  #[error("failed to apply migration edge, start = {1:?}, end = {2:?}")]
  ApplyEdge(#[source] TxApplyEdgeError, SchemaState, SchemaState),
}

impl SchemaResolver {
  async fn tx_apply_migration(
    &self,
    tx: &mut Transaction<'_, Postgres>,
    migration: &'_ SchemaMigration<'_>,
  ) -> Result<(), TxApplyMigrationError> {
    // todo: use [`array_windows`](https://doc.rust-lang.org/std/primitive.slice.html#method.array_windows) once stable.
    for w in migration.states.windows(2) {
      let [start, end] = *TryInto::<&[SchemaState; 2]>::try_into(w).expect("iterator windows must have a size of `2`");
      self
        .tx_apply_edge(tx, start, end)
        .await
        .map_err(|e| TxApplyMigrationError::ApplyEdge(e, start, end))?;
    }
    Ok(())
  }

  async fn tx_apply_edge(
    &self,
    tx: &mut Transaction<'_, Postgres>,
    start: SchemaState,
    end: SchemaState,
  ) -> Result<(), TxApplyEdgeError> {
    let old_state = self
      .inner_get_state(&mut *tx)
      .await
      .map_err(TxApplyEdgeError::GetInitialState)?;
    assert_eq!(start, old_state);
    {
      let edge = self.graph.edge_weight(start, end).expect("edge exists");
      let mut stream = tx.execute_many(edge.schema);
      while let Some(res) = stream.next().await {
        if let Err(e) = res {
          return Err(TxApplyEdgeError::Apply(SafeSqlxError::from(e), start, end));
        }
      }
    }
    match end {
      SchemaState::Empty => panic!("UnexpectedEmptyEndState"),
      SchemaState::Version(v) => {
        self
          .tx_set_schema_meta(&mut *tx, &SchemaMeta { version: v.0 })
          .await
          .map_err(TxApplyEdgeError::SetSchemaMeta)?;
      }
    }
    let new_state = self
      .inner_get_state(&mut *tx)
      .await
      .map_err(TxApplyEdgeError::GetFinalState)?;
    debug_assert_eq!(end, new_state);
    Ok(())
  }
}
