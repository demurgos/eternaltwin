use core::fmt;
use std::fmt::Debug;

trait DisplayErrorChainExt: std::error::Error {
  fn display_chain(&self) -> DisplayErrorChain<'_, Self> {
    DisplayErrorChain(self)
  }
}

impl<E> DisplayErrorChainExt for E where E: std::error::Error + ?Sized {}

/// Helper struct to print errors with their source chain.
struct DisplayErrorChain<'e, E>(pub &'e E)
where
  E: std::error::Error + ?Sized;

impl<'e, E> fmt::Display for DisplayErrorChain<'e, E>
where
  E: std::error::Error + ?Sized,
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)?;
    for e in core::iter::successors(self.0.source(), |e| e.source()) {
      f.write_str(": ")?;
      fmt::Display::fmt(e, f)?;
    }
    Ok(())
  }
}

/// SQLx error wrapper with support for cloning, equality, ord, hashing
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum SafeSqlxError {
  #[error("error with configuration: {0}")]
  Configuration(String),
  #[error("error returned from database")]
  Database(#[source] SafeDatabaseError),
  #[error("error communicating with database: {0}")]
  Io(String),
  #[error("error occurred while attempting to establish a TLS connection: {0}")]
  Tls(String),
  #[error("encountered unexpected or invalid data: {0}")]
  Protocol(String),
  #[error("no rows returned by a query that expected to return at least one row")]
  RowNotFound,
  #[error("type named {type_name} not found")]
  TypeNotFound { type_name: String },
  #[error("column index out of bounds: the len is {len}, but the index is {index}")]
  ColumnIndexOutOfBounds { index: usize, len: usize },
  #[error("no column found for name: {0}")]
  ColumnNotFound(String),
  #[error("error occurred while decoding column {1}: {0}")]
  ColumnDecode(String, String),
  #[error("error occurred while decoding: {0}")]
  Decode(String),
  #[error("error in Any driver mapping: {0}")]
  AnyDriverError(String),
  #[error("pool timed out while waiting for an open connection")]
  PoolTimedOut,
  #[error("attempted to acquire a connection on a closed pool")]
  PoolClosed,
  #[error("attempted to communicate with a crashed background worker")]
  WorkerCrashed,
  #[error("{0}")]
  Other(String),
}

impl From<::sqlx::Error> for SafeSqlxError {
  fn from(value: ::sqlx::Error) -> Self {
    use ::sqlx::Error::*;
    match value {
      Configuration(e) => Self::Configuration(e.display_chain().to_string()),
      Database(e) => Self::Database(SafeDatabaseError::from(e)),
      Io(e) => Self::Io(e.display_chain().to_string()),
      Tls(e) => Self::Tls(e.display_chain().to_string()),
      Protocol(e) => Self::Protocol(e),
      RowNotFound => Self::RowNotFound,
      TypeNotFound { type_name } => Self::TypeNotFound { type_name },
      ColumnIndexOutOfBounds { index, len } => Self::ColumnIndexOutOfBounds { index, len },
      ColumnNotFound(c) => Self::ColumnNotFound(c),
      ColumnDecode { index, source } => Self::ColumnDecode(source.display_chain().to_string(), index),
      Decode(e) => Self::Decode(e.display_chain().to_string()),
      AnyDriverError(e) => Self::AnyDriverError(e.display_chain().to_string()),
      PoolTimedOut => Self::PoolTimedOut,
      PoolClosed => Self::PoolClosed,
      WorkerCrashed => Self::WorkerCrashed,
      e => Self::Other(e.display_chain().to_string()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum SafeDatabaseError {
  #[error(transparent)]
  Postgres(Box<SafePgDatabaseError>),
  #[error("{0}")]
  Other(String),
}

impl From<Box<dyn ::sqlx::error::DatabaseError>> for SafeDatabaseError {
  fn from(value: Box<dyn ::sqlx::error::DatabaseError>) -> Self {
    match value.try_downcast::<::sqlx::postgres::PgDatabaseError>() {
      Ok(e) => Self::Postgres(Box::new(SafePgDatabaseError::from(*e))),
      Err(e) => Self::Other(e.display_chain().to_string()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SafePgDatabaseError {
  pub severity: SafePgSeverity,
  pub code: String,
  pub message: String,
  pub detail: Option<String>,
  pub hint: Option<String>,
  pub position: Option<SafePgErrorPosition>,
  pub r#where: Option<String>,
  pub schema: Option<String>,
  pub table: Option<String>,
  pub column: Option<String>,
  pub data_type: Option<String>,
  pub constraint: Option<String>,
  pub file: Option<String>,
  pub line: Option<u64>,
  pub routine: Option<String>,
}

impl fmt::Display for SafePgDatabaseError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_str(&self.message)
  }
}

impl std::error::Error for SafePgDatabaseError {}

impl From<::sqlx::postgres::PgDatabaseError> for SafePgDatabaseError {
  fn from(value: ::sqlx::postgres::PgDatabaseError) -> Self {
    Self {
      severity: SafePgSeverity::from(value.severity()),
      code: String::from(value.code()),
      message: String::from(value.message()),
      detail: value.detail().map(String::from),
      hint: value.hint().map(String::from),
      position: value.position().map(SafePgErrorPosition::from),
      r#where: value.r#where().map(String::from),
      schema: value.schema().map(String::from),
      table: value.table().map(String::from),
      column: value.column().map(String::from),
      data_type: value.data_type().map(String::from),
      constraint: value.constraint().map(String::from),
      file: value.file().map(String::from),
      line: value
        .line()
        .map(|line| u64::try_from(line).expect("line should fit `u64`")),
      routine: value.routine().map(String::from),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum SafePgSeverity {
  Log,
  Info,
  Debug,
  Notice,
  Warning,
  Error,
  Fatal,
  Panic,
}

impl From<::sqlx::postgres::PgSeverity> for SafePgSeverity {
  fn from(value: ::sqlx::postgres::PgSeverity) -> Self {
    use ::sqlx::postgres::PgSeverity::*;

    match value {
      Log => Self::Log,
      Info => Self::Info,
      Debug => Self::Debug,
      Notice => Self::Notice,
      Warning => Self::Warning,
      Error => Self::Error,
      Fatal => Self::Fatal,
      Panic => Self::Panic,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum SafePgErrorPosition {
  Original(u64),
  Internal { position: u64, query: String },
}

impl<'a> From<::sqlx::postgres::PgErrorPosition<'a>> for SafePgErrorPosition {
  fn from(value: ::sqlx::postgres::PgErrorPosition<'a>) -> Self {
    use ::sqlx::postgres::PgErrorPosition::*;

    match value {
      Original(pos) => Self::Original(u64::try_from(pos).expect("position should fit `u64`")),
      Internal { position, query } => Self::Internal {
        position: u64::try_from(position).expect("position should fit `u64`"),
        query: String::from(query),
      },
    }
  }
}
