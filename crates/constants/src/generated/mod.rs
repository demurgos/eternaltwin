use eternaltwin_core::core::PrUrl;
use eternaltwin_core::twinoid::api::{
  Achievement, AchievementData, Like, Missing, Restrict, Site, SiteIcon, SiteInfo, Stat, TwinoidSiteStatus, UrlRef,
};
use eternaltwin_core::twinoid::client::{AchievementMetadata, FullSiteInfo, StatMetadata};
use eternaltwin_core::twinoid::{
  FullSite, TwinoidLocale, TwinoidSiteHost, TwinoidSiteId, TwinoidSiteIdRef, TwinoidSiteInfoId,
};
use std::str::FromStr;
use url::Url;

pub mod twinoid_achievements;
pub mod twinoid_sites;
pub mod twinoid_stats;

pub struct ConstFullSite {
  pub id: TwinoidSiteId,
  name: &'static str,
  host: &'static str,
  icon: &'static str,
  lang: Option<TwinoidLocale>,
  like_url: &'static str,
  like_likes: u32,
  like_title: Option<&'static str>,
  status: TwinoidSiteStatus,
  infos: &'static [ConstSiteInfo],
}

impl ConstFullSite {
  pub fn into_full_site(&self) -> FullSite {
    Site {
      id: self.id,
      name: Restrict::Allow(self.name.to_string()),
      host: Restrict::Allow(TwinoidSiteHost::from_str(self.host).expect("`ConstFullSite::host` is always valid")),
      icon: Restrict::Allow(UrlRef {
        url: PrUrl::parse(self.icon).expect("`ConstFullSite::icon` is always valid"),
      }),
      lang: Restrict::Allow(self.lang),
      like: Restrict::Allow(Like {
        url: Url::parse(self.like_url).expect("`ConstFullSite::icon` is always valid"),
        likes: self.like_likes,
        title: self.like_title.map(|t| t.to_string()),
      }),
      infos: Restrict::Allow(self.infos.iter().map(ConstSiteInfo::into_full_site_info).collect()),
      me: Missing,
      status: Restrict::Allow(self.status),
    }
  }
}

pub struct ConstSiteInfo {
  id: TwinoidSiteInfoId,
  lang: TwinoidLocale,
  cover: &'static str,
  tag_line: Option<&'static str>,
  description: &'static str,
  tid: &'static str,
  icons: &'static [ConstSiteIcon],
}

impl ConstSiteInfo {
  pub fn into_full_site_info(&self) -> FullSiteInfo {
    SiteInfo {
      id: self.id,
      site: Missing,
      lang: self.lang,
      cover: UrlRef {
        url: PrUrl::parse(self.cover).expect("`ConstSiteInfo::cover` is always valid"),
      },
      tag_line: self.tag_line.map(|t| t.to_string()),
      description: self.description.to_string(),
      tid: self.tid.to_string(),
      icons: self.icons.iter().map(ConstSiteIcon::into_site_icon).collect(),
    }
  }
}

pub struct ConstSiteIcon {
  tag: &'static str,
  alt_tag: Option<&'static str>,
  url: &'static str,
  typ: &'static str,
}

impl ConstSiteIcon {
  pub fn into_site_icon(&self) -> SiteIcon {
    SiteIcon {
      tag: self.tag.to_string(),
      alt_tag: self.alt_tag.map(|t| t.to_string()),
      url: PrUrl::parse(self.url).expect("`ConstSiteIcon::url` is always valid"),
      typ: self.typ.to_string(),
    }
  }
}

pub struct ConstTwinoidStat {
  pub site_id: TwinoidSiteId,
  pub key: &'static str,
  name: &'static str,
  icon: Option<&'static str>,
  description: Option<&'static str>,
  rare: i32,
  social: bool,
}

impl ConstTwinoidStat {
  pub fn into_stat_metadata(&self) -> (TwinoidSiteIdRef, StatMetadata) {
    let site = TwinoidSiteIdRef { id: self.site_id };
    let stat: StatMetadata = Stat {
      id: self.key.to_string(),
      score: Missing,
      name: self.name.to_string(),
      icon: self.icon.map(|i| UrlRef {
        url: PrUrl::parse(i).expect("`ConstTwinoidStat::icon` is always valid"),
      }),
      description: self.description.map(|d| d.to_string()),
      rare: self.rare,
      social: self.social,
    };
    (site, stat)
  }
}

pub struct ConstTwinoidAchievement {
  pub site_id: TwinoidSiteId,
  pub key: &'static str,
  name: &'static str,
  stat: &'static str,
  score: i32,
  points: i32,
  npoints: f64,
  description: &'static str,
  index: i32,
  data_type: &'static str,
  data_title: Option<&'static str>,
  data_url: Option<&'static str>,
  data_prefix: Option<bool>,
  data_suffix: Option<bool>,
}

impl ConstTwinoidAchievement {
  pub fn into_achievement_metadata(&self) -> (TwinoidSiteIdRef, AchievementMetadata) {
    let site = TwinoidSiteIdRef { id: self.site_id };
    let achievement: AchievementMetadata = Achievement {
      id: self.key.to_string(),
      name: self.name.to_string(),
      stat: self.stat.to_string(),
      score: self.score,
      points: self.points,
      npoints: self.npoints,
      description: self.description.to_string(),
      data: AchievementData {
        typ: self.data_type.to_string(),
        title: self.data_title.map(|t| t.to_string()),
        url: self
          .data_url
          .map(|i| PrUrl::parse(i).expect("`ConstTwinoidAchievement::data_url` is always valid")),
        prefix: self.data_prefix,
        suffix: self.data_suffix,
      },
      date: Missing,
      index: self.index,
    };
    (site, achievement)
  }
}
