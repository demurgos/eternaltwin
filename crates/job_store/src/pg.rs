use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::core::{Instant, Listing, SecretString};
use eternaltwin_core::job::{
  JobId, JobStore, OpaqueTask, OpaqueValue, ShortStoreJob, ShortStoreTask, StoreCreateJob, StoreCreateJobError,
  StoreCreateTimer, StoreCreateTimerError, StoreGetJob, StoreGetJobError, StoreGetJobs, StoreGetJobsError,
  StoreGetTask, StoreGetTaskError, StoreGetTasks, StoreGetTasksError, StoreJob, StoreNextTimerError, StoreOnTimerError,
  StoreTask, StoreUpdateTask, StoreUpdateTaskError, TaskId, TaskKind, TaskRevId, TaskStatus, Tick, TickSalt,
};
use eternaltwin_core::opentelemetry::TraceParent;
use eternaltwin_core::pg_num::PgU32;
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{UserId, UserIdRef};
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use sqlx::PgPool;

pub struct PgJobStore<TyDatabase, TyUuidGenerator> {
  database: TyDatabase,
  database_secret: SecretString,
  uuid_generator: TyUuidGenerator,
}
impl<TyDatabase, TyUuidGenerator> PgJobStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(database: TyDatabase, database_secret: SecretString, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      database,
      database_secret,
      uuid_generator,
    }
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> JobStore<OpaqueTask, OpaqueValue> for PgJobStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_job(&self, cmd: StoreCreateJob<OpaqueTask>) -> Result<StoreJob, StoreCreateJobError> {
    let job_id = JobId::from_uuid(self.uuid_generator.uuid_generator().next());
    let task_id = TaskId::from_uuid(self.uuid_generator.uuid_generator().next());
    let mut tx = self.database.begin().await.map_err(StoreCreateJobError::other)?;

    let task_str = serde_json::to_vec(&cmd.task).map_err(StoreCreateJobError::other)?;

    {
      // language=PostgreSQL
      let res = sqlx::query(
        r"
          INSERT
          INTO job(job_id, created_at, created_by, producer_span, initial_state)
          VALUES (
            $1::job_id, $2::instant, $3::user_id, $4::opentelemetry_trace_parent_bytes, pgp_sym_encrypt_bytea($5::BYTEA, $6::TEXT)::BYTEA
          );
        ",
      )
      .bind(job_id)
      .bind(cmd.now)
      .bind(cmd.user.map(|u| u.id))
      .bind(cmd.producer_span)
      .bind(&task_str)
      .bind(self.database_secret.as_str())
      .execute(&mut *tx)
      .await
      .map_err(StoreCreateJobError::other)?;
      debug_assert_eq!(res.rows_affected(), 1);
    };
    {
      // language=PostgreSQL
      let res = sqlx::query(
          r"
          INSERT
          INTO task(task_id, job_id, kind, kind_version, created_at, polled_at, revision, status, starvation, cpu_duration, poll_duration, state, output)
          VALUES (
            $1::task_id, $2::job_id, $3::task_kind, $4::U32, $5::instant, NULL, 0, 'Available', 0, '0S', '0S', pgp_sym_encrypt_bytea($6::bytea, $7::TEXT), NULL
          );
        ",
        )
          .bind(task_id)
          .bind(job_id)
          .bind(cmd.kind.clone())
          .bind(PgU32::from(cmd.kind_version))
          .bind(cmd.now)
          .bind(task_str)
          .bind(self.database_secret.as_str())
          .execute(&mut *tx)
          .await.map_err(StoreCreateJobError::other)?;
      debug_assert_eq!(res.rows_affected(), 1);
    };
    tx.commit().await.map_err(StoreCreateJobError::other)?;

    Ok(StoreJob {
      id: job_id,
      created_at: cmd.now,
      created_by: cmd.user,
      producer_span: cmd.producer_span,
      task: ShortStoreTask {
        id: task_id,
        revision: 0,
        kind: cmd.kind,
        kind_version: cmd.kind_version,
        created_at: cmd.now,
        polled_at: None,
        status: TaskStatus::Available,
        starvation: 0,
      },
    })
  }

  async fn get_jobs(&self, query: StoreGetJobs) -> Result<Listing<StoreJob>, StoreGetJobsError> {
    let mut tx = self.database.begin().await.map_err(StoreGetJobsError::other)?;
    let count = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        count: PgU32,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
          SELECT COUNT(*)::U32 AS count FROM job INNER JOIN task USING (job_id)
          WHERE
            status = COALESCE($1::task_status, status);
        ",
      )
      .bind(query.status)
      .fetch_one(&mut *tx)
      .await
      .map_err(StoreGetJobsError::other)?;
      u32::from(row.count)
    };
    let mut items: Vec<StoreJob> = Vec::new();
    if count > 0 {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        job_id: JobId,
        job_created_at: Instant,
        created_by: Option<UserId>,
        producer_span: Option<TraceParent>,
        task_id: TaskId,
        revision: PgU32,
        kind: TaskKind,
        kind_version: PgU32,
        task_created_at: Instant,
        polled_at_time: Option<Instant>,
        polled_at_salt: Option<TickSalt>,
        status: TaskStatus,
        starvation: i32,
      }
      // language=PostgreSQL
      let rows = sqlx::query_as::<_, Row>(
        r"
          SELECT job_id, job.created_at AS job_created_at, created_by, producer_span,
            task_id, revision, kind, kind_version, task.created_at AS task_created_at,
            (polled_at).time AS polled_at_time, (polled_at).salt AS polled_at_salt,
            status, starvation
          FROM job INNER JOIN task USING (job_id)
          WHERE
            status = COALESCE($1::task_status, status)
          ORDER BY job.created_at DESC
          OFFSET $2::u32 LIMIT $3::u32;
        ",
      )
      .bind(query.status)
      .bind(PgU32::from(query.offset))
      .bind(PgU32::from(query.limit))
      .fetch_all(&mut *tx)
      .await
      .map_err(StoreGetJobsError::other)?;
      for row in rows {
        items.push(StoreJob {
          id: row.job_id,
          created_at: row.job_created_at,
          created_by: row.created_by.map(UserIdRef::new),
          producer_span: row.producer_span,
          task: ShortStoreTask {
            id: row.task_id,
            revision: u32::from(row.revision),
            kind: row.kind,
            kind_version: u32::from(row.kind_version),
            created_at: row.task_created_at,
            polled_at: match (row.polled_at_time, row.polled_at_salt) {
              (Some(time), Some(salt)) => Some(Tick { time, salt }),
              (None, None) => None,
              _ => return Err(StoreGetJobsError::Other(WeakError::new("invalid `polled_at` state"))),
            },
            status: row.status,
            starvation: row.starvation,
          },
        })
      }
    };
    tx.commit().await.map_err(StoreGetJobsError::other)?;

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }

  async fn get_job(&self, query: StoreGetJob) -> Result<StoreJob, StoreGetJobError> {
    let mut tx = self.database.begin().await.map_err(StoreGetJobError::other)?;
    let job: StoreJob = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        job_id: JobId,
        job_created_at: Instant,
        created_by: Option<UserId>,
        producer_span: Option<TraceParent>,
        task_id: TaskId,
        revision: PgU32,
        kind: TaskKind,
        kind_version: PgU32,
        task_created_at: Instant,
        polled_at_time: Option<Instant>,
        polled_at_salt: Option<TickSalt>,
        status: TaskStatus,
        starvation: i32,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
          SELECT job_id, job.created_at AS job_created_at, created_by, producer_span,
            task_id, revision, kind, kind_version, task.created_at AS task_created_at,
            (polled_at).time AS polled_at_time, (polled_at).salt AS polled_at_salt,
            status, starvation
          FROM job INNER JOIN task USING (job_id)
          WHERE
            job_id = $1::job_id;
        ",
      )
      .bind(query.id)
      .fetch_optional(&mut *tx)
      .await
      .map_err(StoreGetJobError::other)?;

      let row = row.ok_or(StoreGetJobError::NotFound(query.id))?;
      StoreJob {
        id: row.job_id,
        created_at: row.job_created_at,
        created_by: row.created_by.map(UserIdRef::new),
        producer_span: row.producer_span,
        task: ShortStoreTask {
          id: row.task_id,
          revision: u32::from(row.revision),
          kind: row.kind,
          kind_version: u32::from(row.kind_version),
          created_at: row.task_created_at,
          polled_at: match (row.polled_at_time, row.polled_at_salt) {
            (Some(time), Some(salt)) => Some(Tick { time, salt }),
            (None, None) => None,
            _ => return Err(StoreGetJobError::Other(WeakError::new("invalid `polled_at` state"))),
          },
          status: row.status,
          starvation: row.starvation,
        },
      }
    };
    tx.commit().await.map_err(StoreGetJobError::other)?;

    Ok(job)
  }

  async fn get_task(&self, query: StoreGetTask) -> Result<StoreTask<OpaqueTask, OpaqueValue>, StoreGetTaskError> {
    let mut tx = self.database.begin().await.map_err(StoreGetTaskError::other)?;
    let task = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        job_id: JobId,
        job_created_at: Instant,
        created_by: Option<UserId>,
        producer_span: Option<TraceParent>,
        task_id: TaskId,
        revision: PgU32,
        kind: TaskKind,
        kind_version: PgU32,
        task_created_at: Instant,
        polled_at_time: Option<Instant>,
        polled_at_salt: Option<TickSalt>,
        status: TaskStatus,
        starvation: i32,
        state: Vec<u8>,
        output: Option<Vec<u8>>,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
          SELECT job_id, job.created_at AS job_created_at, created_by, producer_span,
            task_id, revision, kind, kind_version, task.created_at AS task_created_at,
            (polled_at).time AS polled_at_time, (polled_at).salt AS polled_at_salt,
            status, starvation,
            pgp_sym_decrypt_bytea(state, $2::text)::bytea AS state, pgp_sym_decrypt_bytea(output, $2::text)::bytea AS output
          FROM task
             INNER JOIN job USING (job_id)
          WHERE
            task_id = $1::task_id;
        ",
      )
      .bind(query.id)
      .bind(self.database_secret.as_str())
      .fetch_optional(&mut *tx)
      .await
      .map_err(StoreGetTaskError::other)?;

      let row = row.ok_or(StoreGetTaskError::NotFound(query.id))?;
      StoreTask {
        id: row.task_id,
        job: ShortStoreJob {
          id: row.job_id,
          created_at: row.job_created_at,
          created_by: row.created_by.map(UserIdRef::new),
          producer_span: row.producer_span,
        },
        revision: u32::from(row.revision),
        kind: row.kind,
        kind_version: u32::from(row.kind_version),
        created_at: row.task_created_at,
        polled_at: match (row.polled_at_time, row.polled_at_salt) {
          (Some(time), Some(salt)) => Some(Tick { time, salt }),
          (None, None) => None,
          _ => return Err(StoreGetTaskError::Other(WeakError::new("invalid `polled_at` state"))),
        },
        status: row.status,
        starvation: row.starvation,
        state: serde_json::from_slice::<OpaqueTask>(&row.state).map_err(StoreGetTaskError::other)?,
        output: row
          .output
          .map(|out| serde_json::from_slice::<OpaqueTask>(&out))
          .transpose()
          .map_err(StoreGetTaskError::other)?,
      }
    };
    tx.commit().await.map_err(StoreGetTaskError::other)?;

    Ok(task)
  }

  async fn get_tasks(
    &self,
    query: StoreGetTasks,
  ) -> Result<Listing<StoreTask<OpaqueTask, OpaqueValue>>, StoreGetTasksError> {
    let mut tx = self.database.begin().await.map_err(StoreGetTasksError::other)?;
    let count = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        count: PgU32,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
          SELECT COUNT(*)::U32 AS count FROM task
          WHERE
            status = COALESCE($1::task_status, status)
            AND (CASE
              WHEN ($2::instant IS NOT NULL AND $3::u32 IS NOT NULL) THEN (
                (polled_at).time < $2::instant OR ((polled_at).time = $2::instant) AND (polled_at).salt != $3::u32
              )
              ELSE TRUE
            END);
        ",
      )
      .bind(query.status)
      .bind(query.skip_polled.map(|tick| tick.time))
      .bind(query.skip_polled.map(|tick| tick.salt))
      .fetch_one(&mut *tx)
      .await
      .map_err(StoreGetTasksError::other)?;
      u32::from(row.count)
    };
    let mut items = Vec::new();
    if count > 0 {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        job_id: JobId,
        job_created_at: Instant,
        created_by: Option<UserId>,
        producer_span: Option<TraceParent>,
        task_id: TaskId,
        revision: PgU32,
        kind: TaskKind,
        kind_version: PgU32,
        task_created_at: Instant,
        polled_at_time: Option<Instant>,
        polled_at_salt: Option<TickSalt>,
        status: TaskStatus,
        starvation: i32,
        state: Vec<u8>,
        output: Option<Vec<u8>>,
      }
      // language=PostgreSQL
      let rows = sqlx::query_as::<_, Row>(
        r"
          SELECT job_id, job.created_at AS job_created_at, created_by, producer_span,
            task_id, revision, kind, kind_version, task.created_at AS task_created_at,
            (polled_at).time AS polled_at_time, (polled_at).salt AS polled_at_salt,
            status, starvation,
            pgp_sym_decrypt_bytea(state, $6::text)::bytea AS state, pgp_sym_decrypt_bytea(output, $6::text)::bytea AS output
          FROM task
            INNER JOIN job USING (job_id)
          WHERE
            status = COALESCE($1::task_status, status)
            AND (CASE
              WHEN ($2::instant IS NOT NULL AND $3::u32 IS NOT NULL) THEN (
                (polled_at).time < $2::instant OR ((polled_at).time = $2::instant) AND (polled_at).salt != $3::u32
              )
              ELSE TRUE
            END)
          ORDER BY starvation DESC, (polled_at).time ASC
          OFFSET $4::u32 LIMIT $5::u32;
        ",
      )
      .bind(query.status)
      .bind(query.skip_polled.map(|tick| tick.time))
      .bind(query.skip_polled.map(|tick| tick.salt))
      .bind(PgU32::from(query.offset))
      .bind(PgU32::from(query.limit))
      .bind(self.database_secret.as_str())
      .fetch_all(&mut *tx)
      .await
      .map_err(StoreGetTasksError::other)?;
      for row in rows {
        items.push(StoreTask {
          id: row.task_id,
          job: ShortStoreJob {
            id: row.job_id,
            created_at: row.job_created_at,
            created_by: row.created_by.map(UserIdRef::new),
            producer_span: row.producer_span,
          },
          revision: u32::from(row.revision),
          kind: row.kind,
          kind_version: u32::from(row.kind_version),
          created_at: row.task_created_at,
          polled_at: match (row.polled_at_time, row.polled_at_salt) {
            (Some(time), Some(salt)) => Some(Tick { time, salt }),
            (None, None) => None,
            _ => return Err(StoreGetTasksError::Other(WeakError::new("invalid `polled_at` state"))),
          },
          status: row.status,
          starvation: row.starvation,
          state: serde_json::from_slice::<OpaqueTask>(&row.state).map_err(StoreGetTasksError::other)?,
          output: row
            .output
            .map(|out| serde_json::from_slice::<OpaqueTask>(&out))
            .transpose()
            .map_err(StoreGetTasksError::other)?,
        })
      }
    };
    tx.commit().await.map_err(StoreGetTasksError::other)?;

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }

  async fn update_task(
    &self,
    cmd: StoreUpdateTask<OpaqueTask, OpaqueValue>,
  ) -> Result<TaskRevId, StoreUpdateTaskError> {
    let mut tx = self.database.begin().await.map_err(StoreUpdateTaskError::other)?;
    let rev_id: Option<TaskRevId> = {
      let state_str = serde_json::to_vec(&cmd.state).map_err(StoreUpdateTaskError::other)?;
      let output_str = cmd
        .output
        .as_ref()
        .map(serde_json::to_vec)
        .transpose()
        .map_err(StoreUpdateTaskError::other)?;

      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        task_id: TaskId,
        revision: PgU32,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
          UPDATE task
          SET
            revision = revision + 1,
            polled_at = ROW($3::instant, $4::tick_salt),
            status = $5::task_status,
            starvation = $6::i32,
            state = pgp_sym_encrypt_bytea($8::bytea, $7::text),
            output = pgp_sym_encrypt_bytea($9::bytea, $7::text)
          WHERE
            task_id = $1::task_id
            AND revision = $2::u32
          RETURNING task_id, revision;
        ",
      )
      .bind(cmd.rev_id.id)
      .bind(PgU32::from(cmd.rev_id.rev))
      .bind(cmd.tick.time)
      .bind(cmd.tick.salt)
      .bind(cmd.status)
      .bind(cmd.starvation)
      .bind(self.database_secret.as_str())
      .bind(state_str)
      .bind(output_str)
      .fetch_optional(&mut *tx)
      .await
      .map_err(StoreUpdateTaskError::other)?;
      row.map(|r| TaskRevId {
        id: r.task_id,
        rev: u32::from(r.revision),
      })
    };
    let result = match rev_id {
      None => {
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          revision: PgU32,
        }
        // language=PostgreSQL
        let row = sqlx::query_as::<_, Row>(
          r"
          SELECT revision FROM task
          WHERE task_id = $1::task_id;
        ",
        )
        .bind(cmd.rev_id.id)
        .fetch_optional(&mut *tx)
        .await
        .map_err(StoreUpdateTaskError::other)?;
        match row {
          Some(r) => {
            let actual = u32::from(r.revision);
            let expected = cmd.rev_id.rev;
            if actual != expected {
              Err(StoreUpdateTaskError::Conflict {
                task: cmd.rev_id.id,
                expected: cmd.rev_id.rev,
                actual: u32::from(r.revision),
              })
            } else {
              Err(StoreUpdateTaskError::Other(WeakError::new(
                "data consistency error: task_id found and revision matching, but no task resolved",
              )))
            }
          }
          None => Err(StoreUpdateTaskError::NotFound(cmd.rev_id.id)),
        }
      }
      Some(rev_id) => Ok(rev_id),
    };

    tx.commit().await.map_err(StoreUpdateTaskError::other)?;

    result
  }

  async fn create_timer(&self, cmd: StoreCreateTimer) -> Result<(), StoreCreateTimerError> {
    let mut tx = self.database.begin().await.map_err(StoreCreateTimerError::other)?;

    {
      // language=PostgreSQL
      let res = sqlx::query(
        r"
          INSERT
          INTO task_timer(deadline, task_id)
          VALUES (
            $1::instant, $2::task_id
          )
          ON CONFLICT DO NOTHING;
        ",
      )
      .bind(cmd.deadline)
      .bind(cmd.task_id)
      .execute(&mut *tx)
      .await
      .map_err(StoreCreateTimerError::other)?;
      debug_assert!(res.rows_affected() <= 1);
    };
    tx.commit().await.map_err(StoreCreateTimerError::other)?;

    Ok(())
  }

  async fn next_timer(&self) -> Result<Option<Instant>, StoreNextTimerError> {
    let mut tx = self.database.begin().await.map_err(StoreNextTimerError::other)?;

    let deadline = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        deadline: Instant,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<_, Row>(
        r"
          SELECT deadline FROM task_timer
          ORDER BY deadline ASC LIMIT 1;
        ",
      )
      .fetch_optional(&mut *tx)
      .await
      .map_err(StoreNextTimerError::other)?;
      row.map(|r| r.deadline)
    };
    tx.commit().await.map_err(StoreNextTimerError::other)?;

    Ok(deadline)
  }

  async fn on_timer(&self, time: Instant) -> Result<(), StoreOnTimerError> {
    let mut tx = self.database.begin().await.map_err(StoreOnTimerError::other)?;
    {
      // language=PostgreSQL
      sqlx::query(
        r"
          DELETE FROM task_timer
          WHERE deadline <= $1::instant;
        ",
      )
      .bind(time)
      .execute(&mut *tx)
      .await
      .map_err(StoreOnTimerError::other)?;
    };
    {
      // language=PostgreSQL
      sqlx::query(
        r"
          UPDATE task AS t
          SET status = 'Available'
          WHERE status = 'Blocked' AND NOT EXISTS(SELECT tt.task_id FROM task_timer AS tt WHERE tt.task_id = t.task_id);
        ",
      )
      .bind(time)
      .execute(&mut *tx)
      .await
      .map_err(StoreOnTimerError::other)?;
    };
    tx.commit().await.map_err(StoreOnTimerError::other)?;

    Ok(())
  }
}
