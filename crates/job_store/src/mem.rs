use async_trait::async_trait;
use eternaltwin_core::core::{Instant, Listing};
use eternaltwin_core::job::{
  JobId, JobStore, ShortStoreJob, ShortStoreTask, StoreCreateJob, StoreCreateJobError, StoreCreateTimer,
  StoreCreateTimerError, StoreGetJob, StoreGetJobError, StoreGetJobs, StoreGetJobsError, StoreGetTask,
  StoreGetTaskError, StoreGetTasks, StoreGetTasksError, StoreJob, StoreNextTimerError, StoreOnTimerError, StoreTask,
  StoreUpdateTask, StoreUpdateTaskError, TaskId, TaskKind, TaskRevId, TaskStatus, Tick,
};
use eternaltwin_core::opentelemetry::TraceParent;
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::UserIdRef;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use std::collections::{BTreeMap, BTreeSet, HashSet};
use std::convert::TryFrom;
use std::sync::RwLock;

pub struct MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue> {
  uuid_generator: TyUuidGenerator,
  state: RwLock<MemJobStoreState<OpaqueTask, OpaqueValue>>,
}

impl<TyUuidGenerator, OpaqueTask, OpaqueValue> MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  OpaqueTask: Clone,
  OpaqueValue: Clone,
{
  pub fn new(uuid_generator: TyUuidGenerator) -> Self {
    Self {
      uuid_generator,
      state: RwLock::new(MemJobStoreState::new()),
    }
  }
}

#[async_trait]
impl<TyUuidGenerator, OpaqueTask, OpaqueValue> JobStore<OpaqueTask, OpaqueValue>
  for MemJobStore<TyUuidGenerator, OpaqueTask, OpaqueValue>
where
  TyUuidGenerator: UuidGeneratorRef,
  OpaqueTask: Clone + Send + Sync,
  OpaqueValue: Clone + Send + Sync,
{
  async fn create_job(&self, cmd: StoreCreateJob<OpaqueTask>) -> Result<StoreJob, StoreCreateJobError> {
    let job_id = JobId::from(self.uuid_generator.uuid_generator().next());
    let task_id = TaskId::from(self.uuid_generator.uuid_generator().next());
    let mut state = self.state.write().expect("failed to lock mem job store state");
    Ok(state.create_job(cmd, job_id, task_id))
  }

  async fn get_jobs(&self, query: StoreGetJobs) -> Result<Listing<StoreJob>, StoreGetJobsError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_jobs(query)
  }

  async fn get_job(&self, query: StoreGetJob) -> Result<StoreJob, StoreGetJobError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_job(query)
  }

  async fn get_task(&self, query: StoreGetTask) -> Result<StoreTask<OpaqueTask, OpaqueValue>, StoreGetTaskError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_task(query)
  }

  async fn get_tasks(
    &self,
    query: StoreGetTasks,
  ) -> Result<Listing<StoreTask<OpaqueTask, OpaqueValue>>, StoreGetTasksError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    state.get_tasks(query)
  }

  async fn update_task(
    &self,
    cmd: StoreUpdateTask<OpaqueTask, OpaqueValue>,
  ) -> Result<TaskRevId, StoreUpdateTaskError> {
    let mut state = self.state.write().expect("failed to lock mem job store state");
    state.update_task(cmd)
  }

  async fn create_timer(&self, cmd: StoreCreateTimer) -> Result<(), StoreCreateTimerError> {
    let mut state = self.state.write().expect("failed to lock mem job store state");
    state.create_timer(cmd)
  }

  async fn next_timer(&self) -> Result<Option<Instant>, StoreNextTimerError> {
    let state = self.state.read().expect("failed to lock mem job store state");
    Ok(state.next_timer())
  }

  async fn on_timer(&self, time: Instant) -> Result<(), StoreOnTimerError> {
    let mut state = self.state.write().expect("failed to lock mem job store state");
    state.on_timer(time)
  }
}

struct MemStoreTask<OpaqueTask, OpaqueValue> {
  /// Id for this task
  pub id: TaskId,
  /// Reference to overall job containing this task.
  pub job: JobId,
  /// Revision for task, increment on every update
  pub revision: u32,
  /// Handler name / Task type
  pub kind: TaskKind,
  /// Handler version / version of the state
  pub kind_version: u32,
  /// Time when the task was created.
  ///
  /// It may not have been polled ever.
  pub created_at: Instant,
  /// Last time when task polling completed.
  pub polled_at: Option<Tick>,
  /// Task status
  pub status: TaskStatus,
  /// How strongly a task asked to be prioritized
  pub starvation: i32,
  /// Serialized state
  pub state: OpaqueTask,
  /// Serialized value (if complete)
  pub output: Option<OpaqueValue>,
}

impl<OpaqueTask, OpaqueValue> MemStoreTask<OpaqueTask, OpaqueValue> {
  pub const fn rev_id(&self) -> TaskRevId {
    TaskRevId {
      id: self.id,
      rev: self.revision,
    }
  }
}

struct MemJob {
  id: JobId,
  created_at: Instant,
  created_by: Option<UserIdRef>,
  producer_span: Option<TraceParent>,
  task: TaskId,
}

struct MemJobStoreState<OpaqueTask, OpaqueValue> {
  jobs: BTreeMap<JobId, MemJob>,
  jobs_by_created_at: BTreeSet<(Instant, JobId)>,
  tasks: BTreeMap<TaskId, MemStoreTask<OpaqueTask, OpaqueValue>>,
  timers: BTreeSet<(Instant, TaskId)>,
}

impl<OpaqueTask, OpaqueValue> Default for MemJobStoreState<OpaqueTask, OpaqueValue>
where
  OpaqueTask: Clone,
  OpaqueValue: Clone,
{
  fn default() -> Self {
    Self::new()
  }
}

impl<OpaqueTask, OpaqueValue> MemJobStoreState<OpaqueTask, OpaqueValue>
where
  OpaqueTask: Clone,
  OpaqueValue: Clone,
{
  pub fn new() -> Self {
    Self {
      jobs: BTreeMap::new(),
      jobs_by_created_at: BTreeSet::new(),
      tasks: BTreeMap::new(),
      timers: BTreeSet::new(),
    }
  }

  pub fn create_job(&mut self, cmd: StoreCreateJob<OpaqueTask>, job_id: JobId, task_id: TaskId) -> StoreJob {
    let mem_job = MemJob {
      id: job_id,
      created_at: cmd.now,
      created_by: cmd.user,
      producer_span: cmd.producer_span,
      task: task_id,
    };
    let mem_task = MemStoreTask {
      id: task_id,
      job: job_id,
      revision: 0,
      kind: cmd.kind,
      kind_version: cmd.kind_version,
      created_at: cmd.now,
      polled_at: None,
      status: TaskStatus::Available,
      starvation: 0,
      state: cmd.task,
      output: None,
    };
    let job = StoreJob {
      id: mem_job.id,
      created_at: mem_job.created_at,
      created_by: mem_job.created_by,
      producer_span: mem_job.producer_span,
      task: ShortStoreTask {
        id: mem_task.id,
        revision: mem_task.revision,
        kind: mem_task.kind.clone(),
        kind_version: mem_task.kind_version,
        created_at: mem_task.created_at,
        polled_at: mem_task.polled_at,
        status: mem_task.status,
        starvation: mem_task.starvation,
      },
    };
    self.tasks.insert(task_id, mem_task);
    self.jobs.insert(job_id, mem_job);
    job
  }

  fn get_jobs(&self, query: StoreGetJobs) -> Result<Listing<StoreJob>, StoreGetJobsError> {
    let mut matched = self
      .jobs_by_created_at
      .iter()
      .rev()
      .filter_map(|(_, job_id)| {
        let job = self.jobs.get(job_id).expect("job exists");
        let task = self.tasks.get(&job.task).expect("task exists");
        if query.status.map(|s| task.status == s).unwrap_or(true) {
          Some((job, task))
        } else {
          None
        }
      })
      .peekable();
    let is_empty = matched.peek().is_none();
    let mut matched = matched
      .skip(usize::try_from(query.offset).expect("`u32` should fit into `usize`"))
      .peekable();
    let mut items: Vec<StoreJob> = Vec::new();
    for _ in 0..query.limit {
      if matched.peek().is_some() {
        let (job, task) = matched.next().expect("task exists");
        items.push(StoreJob {
          id: job.id,
          created_at: job.created_at,
          created_by: job.created_by,
          producer_span: job.producer_span,
          task: ShortStoreTask {
            id: task.id,
            revision: task.revision,
            kind: task.kind.clone(),
            kind_version: task.kind_version,
            created_at: task.created_at,
            polled_at: task.polled_at,
            status: task.status,
            starvation: task.starvation,
          },
        });
      }
    }
    let remaining = matched.count();
    let count = if is_empty {
      0
    } else {
      query.offset + u32::try_from(items.len() + remaining).expect("`u32` should fit into `usize`")
    };

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }

  fn get_job(&self, query: StoreGetJob) -> Result<StoreJob, StoreGetJobError> {
    let job = self.jobs.get(&query.id).ok_or(StoreGetJobError::NotFound(query.id))?;
    let task = self.tasks.get(&job.task).expect("task exists");

    Ok(StoreJob {
      id: job.id,
      created_at: job.created_at,
      created_by: job.created_by,
      producer_span: job.producer_span,
      task: ShortStoreTask {
        id: task.id,
        revision: task.revision,
        kind: task.kind.clone(),
        kind_version: task.kind_version,
        created_at: task.created_at,
        polled_at: task.polled_at,
        status: task.status,
        starvation: task.starvation,
      },
    })
  }

  pub fn get_task(&self, query: StoreGetTask) -> Result<StoreTask<OpaqueTask, OpaqueValue>, StoreGetTaskError> {
    let mem_task = self.tasks.get(&query.id).ok_or(StoreGetTaskError::NotFound(query.id))?;
    self.mem_to_store_task(mem_task).map_err(StoreGetTaskError::Other)
  }

  fn get_tasks(&self, query: StoreGetTasks) -> Result<Listing<StoreTask<OpaqueTask, OpaqueValue>>, StoreGetTasksError> {
    let mut matched = self
      .tasks
      .values()
      .filter(|t| {
        let status_match = query.status.map(|s| t.status == s).unwrap_or(true);
        let skip_polled_match = query.skip_polled.map(|tick| t.polled_at == Some(tick)).unwrap_or(true);
        status_match && skip_polled_match
      })
      .peekable();
    let is_empty = matched.peek().is_none();
    let mut matched = matched
      .skip(usize::try_from(query.offset).expect("`u32` should fit into `usize`"))
      .peekable();
    let mut items: Vec<StoreTask<OpaqueTask, OpaqueValue>> = Vec::new();
    for _ in 0..query.limit {
      if matched.peek().is_some() {
        let mem_task = matched.next().expect("task exists");
        items.push(self.mem_to_store_task(mem_task).map_err(StoreGetTasksError::Other)?);
      }
    }
    let remaining = matched.count();
    let count = if is_empty {
      0
    } else {
      query.offset + u32::try_from(items.len() + remaining).expect("`u32` should fit into `usize`")
    };

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }

  fn mem_to_store_task(
    &self,
    mem_task: &MemStoreTask<OpaqueTask, OpaqueValue>,
  ) -> Result<StoreTask<OpaqueTask, OpaqueValue>, WeakError> {
    let mem_job = self.jobs.get(&mem_task.job).ok_or_else(|| {
      WeakError::new(format!(
        "mem job not found; job_id = {}, task_id = {}",
        mem_task.job, mem_task.id
      ))
    })?;
    Ok(StoreTask {
      id: mem_task.id,
      job: ShortStoreJob {
        id: mem_job.id,
        created_at: mem_job.created_at,
        created_by: mem_job.created_by,
        producer_span: mem_job.producer_span,
      },
      revision: mem_task.revision,
      kind: mem_task.kind.clone(),
      kind_version: mem_task.kind_version,
      created_at: mem_task.created_at,
      polled_at: mem_task.polled_at,
      status: mem_task.status,
      starvation: mem_task.starvation,
      state: mem_task.state.clone(),
      output: mem_task.output.clone(),
    })
  }

  pub fn update_task(
    &mut self,
    cmd: StoreUpdateTask<OpaqueTask, OpaqueValue>,
  ) -> Result<TaskRevId, StoreUpdateTaskError> {
    let task = self
      .tasks
      .get_mut(&cmd.rev_id.id)
      .ok_or(StoreUpdateTaskError::NotFound(cmd.rev_id.id))?;
    if task.revision != cmd.rev_id.rev {
      return Err(StoreUpdateTaskError::Conflict {
        task: cmd.rev_id.id,
        expected: cmd.rev_id.rev,
        actual: task.revision,
      });
    }
    task
      .revision
      .checked_add(1)
      .ok_or(StoreUpdateTaskError::RevisionOverflow(cmd.rev_id.id))?;
    task.polled_at = Some(cmd.tick);
    task.status = cmd.status;
    task.starvation = cmd.starvation;
    task.state = cmd.state;
    task.output = cmd.output;
    let new_rev_id = task.rev_id();
    Ok(new_rev_id)
  }

  pub fn create_timer(&mut self, cmd: StoreCreateTimer) -> Result<(), StoreCreateTimerError> {
    if !self.tasks.contains_key(&cmd.task_id) {
      return Err(StoreCreateTimerError::NotFound(cmd.task_id));
    }
    self.timers.insert((cmd.deadline, cmd.task_id));
    Ok(())
  }

  pub fn next_timer(&self) -> Option<Instant> {
    self.timers.first().map(|(deadline, _task_id)| *deadline)
  }

  pub fn on_timer(&mut self, time: Instant) -> Result<(), StoreOnTimerError> {
    let mut updated: HashSet<TaskId> = HashSet::new();
    self.timers.retain(|(deadline, task_id)| {
      if *deadline <= time {
        updated.insert(*task_id);
        false
      } else {
        true
      }
    });
    for task_id in updated {
      let task = self
        .tasks
        .get_mut(&task_id)
        .expect("task must exists since it had a timer");
      // TODO: this need way more care...
      task.status = TaskStatus::Available;
    }
    Ok(())
  }
}

// use async_trait::async_trait;
// use eternaltwin_core::{
//   clock::Clock,
//   core::{Duration, Instant},
//   job::{
//     JobStore, ShortStoredTask, StoredTask, StoredTaskState, TaskId, TaskStatus, UpdateTaskError, UpdateTaskOptions,
//   },
//   uuid::UuidGenerator,
// };
// use eternaltwin_core::{
//   job::{JobId, StoredJob},
//   types::AnyError,
// };
// use std::collections::{BinaryHeap, HashMap};
// use std::sync::RwLock;
//
// // A task id with its priority.
// // Tasks are ordered by decreasing time of last update.
// #[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
// struct TaskPriority {
//   priority: std::cmp::Reverse<Instant>,
//   id: TaskId,
// }
//
// struct StoreState {
//   // All the stored jobs.
//   jobs: HashMap<JobId, StoredJob>,
//   // All the stored tasks.
//   tasks: HashMap<TaskId, StoredTask<()>>,
//   // Priority queue for currently running tasks.
//   running: BinaryHeap<TaskPriority>,
//   // Number of uncompleted children preventing tasks from running.
//   blocking_children: HashMap<TaskId, u32>,
// }
//
// enum QueueEntryState {
//   Valid,
//   NeedsRemoval,
//   NeedsUpdate(std::cmp::Reverse<Instant>),
// }
//
// impl StoreState {
//   fn new() -> Self {
//     Self {
//       jobs: HashMap::new(),
//       tasks: HashMap::new(),
//       running: BinaryHeap::new(),
//       blocking_children: HashMap::new(),
//     }
//   }
//
//   fn peek_running_queue(&self) -> Option<(TaskId, QueueEntryState)> {
//     let peek = self.running.peek()?;
//     let id = peek.id;
//
//     let entry = match self.tasks.get(&id) {
//       Some(task) => {
//         if task.short.status != TaskStatus::Running || self.blocking_children.contains_key(&id) {
//           QueueEntryState::NeedsRemoval
//         } else if peek.priority.0 != task.short.advanced_at {
//           QueueEntryState::NeedsUpdate(std::cmp::Reverse(task.short.advanced_at))
//         } else {
//           QueueEntryState::Valid
//         }
//       }
//       None => QueueEntryState::NeedsRemoval,
//     };
//     Some((id, entry))
//   }
//
//   fn peek_valid_task(&self) -> Option<&StoredTask<()>> {
//     let (task_id, state) = self.peek_running_queue()?;
//     match (state, self.tasks.get(&task_id)) {
//       (QueueEntryState::Valid, Some(task)) => Some(task),
//       _ => panic!("no valid task on top of queue"),
//     }
//   }
//
//   fn update_running_queue(&mut self) {
//     while let Some((_, state)) = self.peek_running_queue() {
//       match state {
//         QueueEntryState::Valid => break,
//         QueueEntryState::NeedsUpdate(p) => {
//           self.running.peek_mut().unwrap().priority = p;
//         }
//         QueueEntryState::NeedsRemoval => {
//           self.running.pop();
//         }
//       }
//     }
//   }
//
//   fn add_task(&mut self, task: StoredTask<()>) {
//     let id = task.short.id;
//     let priority = std::cmp::Reverse(task.short.created_at);
//
//     if let Some(parent) = task.short.parent {
//       let block = self.blocking_children.entry(parent).or_insert(0);
//       *block = block.checked_add(1).expect("Blocking children overflow");
//     } else {
//       let old_job = self.jobs.insert(
//         task.short.job_id,
//         StoredJob {
//           id: task.short.job_id,
//           created_at: task.short.created_at,
//           root_task: task.short.id,
//         },
//       );
//       assert!(old_job.is_none());
//     }
//
//     let old_task = self.tasks.insert(id, task);
//     assert!(old_task.is_none());
//
//     self.running.push(TaskPriority { id, priority });
//   }
//
//   fn resolve_blocking_child(&mut self, parent: TaskId) {
//     match self.blocking_children.get_mut(&parent) {
//       None | Some(0) => panic!("No blocking children for task {}", parent),
//       Some(children) => {
//         *children -= 1;
//         if *children == 0 {
//           self.blocking_children.remove(&parent);
//           self.running.push(TaskPriority {
//             id: parent,
//             priority: std::cmp::Reverse(self.tasks[&parent].short.advanced_at),
//           })
//         }
//       }
//     }
//   }
//
//   fn reset_transient_state(&mut self) {
//     let tasks = std::mem::replace(&mut self.tasks, HashMap::new());
//     self.jobs.clear();
//     self.running.clear();
//     self.blocking_children.clear();
//
//     for (_, task) in tasks.into_iter() {
//       self.add_task(task);
//     }
//   }
// }
//
// fn make_task_raw(id: TaskId, job_id: JobId, start_time: Instant, parent: Option<TaskId>) -> ShortStoredTask {
//   ShortStoredTask {
//     id,
//     job_id,
//     parent,
//     created_at: start_time,
//     advanced_at: start_time,
//     status: TaskStatus::Running,
//     status_message: None,
//     step_count: 0,
//     running_time: Duration::zero(),
//   }
// }
//
// pub struct MemJobStore<TyClock, TyUuidGenerator> {
//   clock: TyClock,
//   uuid_generator: TyUuidGenerator,
//   state: RwLock<StoreState>,
// }
//
// impl<TyClock, TyUuidGenerator> MemJobStore<TyClock, TyUuidGenerator> {
//   pub fn new(clock: TyClock, uuid_generator: TyUuidGenerator) -> Self {
//     Self {
//       clock,
//       uuid_generator,
//       state: RwLock::new(StoreState::new()),
//     }
//   }
// }
//
// #[async_trait]
// impl<TyClock, TyUuidGenerator> JobStore for MemJobStore<TyClock, TyUuidGenerator>
// where
//   TyClock: Clock,
//   TyUuidGenerator: UuidGeneratorRef,
// {
//   async fn create_job(&self, task_state: &StoredTaskState<()>) -> Result<ShortStoredTask, AnyError> {
//     let mut state = self.state.write().unwrap();
//
//     let start_time = self.clock.now();
//     let job_id = JobId::from_uuid(self.uuid_generator.uuid_generator().next());
//     let task_id = TaskId::from_uuid(self.uuid_generator.uuid_generator().next());
//
//     let short_task = make_task_raw(task_id, job_id, start_time, None);
//
//     state.add_task(StoredTask {
//       short: short_task.clone(),
//       state: task_state.clone(),
//     });
//     state.update_running_queue();
//
//     Ok(short_task)
//   }
//
//   async fn create_subtask(
//     &self,
//     task_state: &StoredTaskState<()>,
//     parent: TaskId,
//   ) -> Result<ShortStoredTask, AnyError> {
//     let mut state = self.state.write().unwrap();
//
//     let start_time = self.clock.now();
//     let task_id = TaskId::from_uuid(self.uuid_generator.uuid_generator().next());
//     let job_id = match state.tasks.get(&parent) {
//       Some(parent) => parent.short.job_id,
//       None => return Err(format!("Unknown task ID for parent: {}", parent).into()),
//     };
//
//     let short_task = make_task_raw(task_id, job_id, start_time, Some(parent));
//
//     state.add_task(StoredTask {
//       short: short_task.clone(),
//       state: task_state.clone(),
//     });
//     state.update_running_queue();
//
//     Ok(short_task)
//   }
//
//   async fn update_task(&self, options: &UpdateTaskOptions<'_, ()>) -> Result<ShortStoredTask, UpdateTaskError> {
//     let mut state = self.state.write().unwrap();
//
//     let stored_task = state
//       .tasks
//       .get_mut(&options.id)
//       .ok_or(UpdateTaskError::NotFound(options.id))?;
//
//     let task = &mut stored_task.short;
//
//     if task.step_count != options.current_step {
//       return Err(UpdateTaskError::StepConflict {
//         task: task.id,
//         actual: task.step_count,
//         expected: options.current_step,
//       });
//     }
//
//     if !task.status.can_transition_to(options.status) {
//       return Err(UpdateTaskError::InvalidTransition {
//         task: task.id,
//         old: task.status,
//         new: options.status,
//       });
//     }
//
//     let next_step = task
//       .step_count
//       .checked_add(1)
//       .ok_or_else(|| UpdateTaskError::Other("Task step count overflowed".into()))?;
//     let running_time = task
//       .running_time
//       .checked_add(&options.step_time)
//       .ok_or_else(|| UpdateTaskError::Other("Task running time overflowed".into()))?;
//
//     task.advanced_at = self.clock.now();
//     task.status = options.status;
//     task.status_message = options.status_message.map(Into::into);
//     task.running_time = running_time;
//     task.step_count = next_step;
//     stored_task.state.state = options.state.to_owned();
//
//     let task = task.clone();
//
//     if let Some(parent) = task.parent.filter(|_| options.status == TaskStatus::Complete) {
//       state.resolve_blocking_child(parent);
//     }
//
//     state.update_running_queue();
//     Ok(task)
//   }
//
//   async fn update_job_status(&self, job: JobId, status: TaskStatus) -> Result<(), AnyError> {
//     let mut state = self.state.write().unwrap();
//
//     for task in state.tasks.values_mut() {
//       let task = &mut task.short;
//       if task.job_id == job && task.status != status && task.status.can_transition_to(status) {
//         task.status = status;
//         task.step_count = task
//           .step_count
//           .checked_add(1)
//           .ok_or_else(|| UpdateTaskError::Other("Task step count overflowed".into()))?;
//       }
//     }
//
//     state.reset_transient_state();
//     Ok(())
//   }
//
//   async fn get_task(&self, task: TaskId) -> Result<Option<StoredTask<()>>, AnyError> {
//     let state = self.state.read().unwrap();
//     Ok(state.tasks.get(&task).cloned())
//   }
//
//   async fn get_job(&self, job: JobId) -> Result<Option<StoredJob>, AnyError> {
//     let state = self.state.read().unwrap();
//     Ok(state.jobs.get(&job).cloned())
//   }
//
//   async fn get_next_task_to_run(&self) -> Result<Option<StoredTask<()>>, AnyError> {
//     let state = self.state.read().unwrap();
//     Ok(state.peek_valid_task().cloned())
//   }
// }
//
// #[cfg(feature = "neon")]
// impl<TyClock, TyUuidGenerator> neon::prelude::Finalize for MemJobStore<TyClock, TyUuidGenerator>
// where
//   TyClock: Clock,
//   TyUuidGenerator: UuidGeneratorRef,
// {
// }
//
// #[cfg(test)]
// mod test {
//   use crate::mem::MemJobStore;
//   use crate::test::TestApi;
//   use eternaltwin_core::core::Instant;
//   use eternaltwin_core::{clock::VirtualClock, job::JobStore, uuid::Uuid4Generator};
//   use std::sync::Arc;
//
//   fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn JobStore>> {
//     let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
//     let uuid_generator = Arc::new(Uuid4Generator);
//     let job_store: Arc<dyn JobStore> = Arc::new(MemJobStore::new(Arc::clone(&clock), uuid_generator));
//
//     TestApi { clock, job_store }
//   }
//
//   test_job_store!(|| make_test_api());
// }
