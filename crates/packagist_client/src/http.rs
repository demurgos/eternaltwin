use crate::PackageMeta2;
use crate::{GetPackageError, GetPackageRequestView, PackagistClient};
use async_trait::async_trait;
use compact_str::{format_compact, CompactString};
use reqwest::{Client, StatusCode};
use std::collections::BTreeMap;
use url::Url;

pub struct HttpPackagistClient {
  client: Client,
  server: Url,
}

impl HttpPackagistClient {
  pub fn new() -> Self {
    Self {
      client: Client::builder()
        .user_agent("packagist_client")
        .build()
        .expect("building the client always succeeds"),
      server: Url::parse("https://packagist.org/").expect("the packagist registry URL is well-formed"),
    }
  }

  pub fn api_url<I>(&self, segments: I) -> Url
  where
    I: IntoIterator,
    I::Item: AsRef<str>,
  {
    let mut res = self.server.clone();
    {
      let mut p = res
        .path_segments_mut()
        .expect("packagist registry URL has path segments");
      p.extend(segments);
    }
    res
  }
}

impl Default for HttpPackagistClient {
  fn default() -> Self {
    Self::new()
  }
}

#[async_trait]
impl PackagistClient for HttpPackagistClient {
  async fn get_package(&self, req: GetPackageRequestView<'_>) -> Result<Vec<PackageMeta2>, GetPackageError> {
    #[derive(Debug, serde::Deserialize)]
    struct Response {
      minified: CompactString,
      packages: BTreeMap<CompactString, Vec<serde_json::Map<String, serde_json::Value>>>,
      #[allow(unused)]
      #[serde(rename = "security-advisories")]
      security_advisories: Vec<serde_json::Value>,
    }

    let url = self.api_url(["p2", req.vendor, &format!("{}.json", req.package)]);
    let res = self
      .client
      .get(url)
      .send()
      .await
      .map_err(|e| GetPackageError::Send(format!("{e:?}")))?;

    match res.status() {
      StatusCode::OK => {
        let body: String = res
          .text()
          .await
          .map_err(|e| GetPackageError::Receive(format!("{e:?}")))?;
        let body: Response = serde_json::from_str(&body).map_err(|e| GetPackageError::Receive(format!("{e:?}")))?;
        if body.minified != "composer/2.0" {
          return Err(GetPackageError::Receive(String::from("unexpected `minified` value")));
        }
        let full = format_compact!("{}/{}", req.vendor, req.package);
        let versions = body
          .packages
          .get(&full)
          .ok_or_else(|| GetPackageError::Receive(String::from("OK status but missing entry in `packages`")))?;
        let expanded = composer_metadata_minifier::expand(versions);
        let mut versions: Vec<PackageMeta2> = Vec::with_capacity(expanded.len());
        for (i, exp) in expanded.into_iter().enumerate() {
          let version: PackageMeta2 = serde_json::from_value(serde_json::Value::Object(exp))
            .map_err(|e| GetPackageError::Receive(format!("invalid package version at index {i}: {e:?}")))?;
          versions.push(version);
        }
        Ok(versions)
      }
      StatusCode::NOT_FOUND => Err(GetPackageError::NotFound),
      code => Err(GetPackageError::Receive(format!("unexpected status code {code}"))),
    }
  }
}
