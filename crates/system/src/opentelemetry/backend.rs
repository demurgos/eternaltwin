use opentelemetry::trace::TraceResult;
use opentelemetry::Context;
use opentelemetry_sdk::export::trace::{SpanData, SpanExporter};
use opentelemetry_sdk::trace::{BatchSpanProcessor, Span, SpanProcessor};
use opentelemetry_sdk::Resource;
use smallvec::SmallVec;
use std::sync::{Arc, RwLock};

#[derive(Default, Debug, Clone)]
pub struct OpentelemetryBackend {
  span_processors: SmallVec<[SharedSpanProcessor; 3]>,
}

impl OpentelemetryBackend {
  pub fn new() -> Self {
    Self {
      span_processors: SmallVec::new(),
    }
  }

  /// Register a new span exporter using a batch processor
  pub fn batch_span_exporter<TySpanExporter: SpanExporter + 'static>(
    &mut self,
    exporter: TySpanExporter,
  ) -> SharedSpanProcessor {
    let processor = BatchSpanProcessor::builder(exporter, opentelemetry_sdk::runtime::Tokio).build();
    let shared = SharedSpanProcessor(Arc::new(RwLock::new(processor)));
    self.span_processors.push(shared.clone());
    shared
  }

  pub fn span_processors(&self) -> impl Iterator<Item = &SharedSpanProcessor> {
    self.span_processors.iter()
  }
}

#[derive(Debug, Clone)]
pub struct SharedSpanProcessor(Arc<RwLock<dyn SpanProcessor>>);

impl SharedSpanProcessor {
  pub fn shared_set_resource(&self, resource: &Resource) {
    self
      .0
      .write()
      .expect("SharedSpanProcessor::shared_set_resource should succeed locking the inner value")
      .set_resource(resource)
  }
}

impl SpanProcessor for SharedSpanProcessor {
  fn on_start(&self, span: &mut Span, cx: &Context) {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::on_start should succeed locking the inner value")
      .on_start(span, cx)
  }

  fn on_end(&self, span: SpanData) {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::on_end should succeed locking the inner value")
      .on_end(span)
  }

  fn force_flush(&self) -> TraceResult<()> {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::force_flush should succeed locking the inner value")
      .force_flush()
  }

  fn shutdown(&self) -> TraceResult<()> {
    self
      .0
      .read()
      .expect("SharedSpanProcessor::shutdown should succeed locking the inner value")
      .shutdown()
  }

  fn set_resource(&mut self, resource: &Resource) {
    self.shared_set_resource(resource)
  }
}
