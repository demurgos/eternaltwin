use core::fmt;
use eternaltwin_core::core::{FinitePeriod, Instant};
use eternaltwin_core::types::WeakError;
use futures_util::future::BoxFuture;
use opentelemetry::trace::{SpanId, TraceError};
use opentelemetry::{Array, ExportError, Value};
use opentelemetry_sdk::export::trace::{ExportResult, SpanData, SpanExporter};
use opentelemetry_sdk::Resource;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("write failed")]
struct JsonlWriteError(#[source] WeakError);

impl ExportError for JsonlWriteError {
  fn exporter_name(&self) -> &'static str {
    JsonlOtelExporter::<()>::NAME
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("flush failed")]
struct JsonlFlushError(#[source] WeakError);

impl ExportError for JsonlFlushError {
  fn exporter_name(&self) -> &'static str {
    JsonlOtelExporter::<()>::NAME
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("exporter was shut down")]
struct ShutExporterError;

impl ExportError for ShutExporterError {
  fn exporter_name(&self) -> &'static str {
    JsonlOtelExporter::<()>::NAME
  }
}

pub struct JsonlOtelExporter<W> {
  writer: Option<W>,
  resource: Resource,
}

impl<W> JsonlOtelExporter<W> {
  pub const NAME: &'static str = "JsonlOtelExporter";

  pub fn new(writer: W) -> Self {
    Self {
      writer: Some(writer),
      resource: Resource::empty(),
    }
  }
}

impl<W> fmt::Debug for JsonlOtelExporter<W> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_str("JsonOtelExporter(...)")
  }
}

impl<W> SpanExporter for JsonlOtelExporter<W>
where
  W: std::io::Write + Send + Sync,
{
  fn export(&mut self, batch: Vec<SpanData>) -> BoxFuture<'static, ExportResult> {
    let mut res: ExportResult = Ok(());
    if let Some(writer) = self.writer.as_mut() {
      for span in batch {
        let span = JsonSpan::new(&self.resource, &span);
        let write_res = serde_json::to_writer(&mut *writer, &span)
          .map_err(|e| TraceError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
        if let Err(e) = write_res {
          res = Err(e);
          break;
        }
        let write_res = writer
          .write_all(b"\n")
          .map_err(|e| TraceError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
        if let Err(e) = write_res {
          res = Err(e);
          break;
        }
      }
    } else {
      res = Err(TraceError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn shutdown(&mut self) {
    self.writer.take();
  }

  fn force_flush(&mut self) -> BoxFuture<'static, ExportResult> {
    let res = if let Some(writer) = self.writer.as_mut() {
      writer
        .flush()
        .map_err(|e| TraceError::ExportFailed(Box::new(JsonlFlushError(WeakError::wrap(e)))))
    } else {
      Err(TraceError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn set_resource(&mut self, resource: &Resource) {
    self.resource = resource.clone();
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize)]
pub struct JsonSpan<S: AsRef<str>> {
  trace_id: String,
  span_id: String,
  parent_span_id: Option<String>,
  name: S,
  period: FinitePeriod,
  attributes: Vec<(S, serde_json::Value)>, // todo: trace_state, flags, kind, dropped_attributes_count, events, dropped_events_count, links, dropped_links_count, status
}

impl<'a> JsonSpan<&'a str> {
  pub fn new(resource: &'a Resource, span: &'a SpanData) -> Self {
    Self {
      trace_id: span.span_context.trace_id().to_string(),
      span_id: span.span_context.span_id().to_string(),
      parent_span_id: if span.parent_span_id == SpanId::INVALID {
        None
      } else {
        Some(span.parent_span_id.to_string())
      },
      name: span.name.as_ref(),
      period: FinitePeriod {
        start: Instant::from_system(span.start_time),
        end: Instant::from_system(span.end_time),
      },
      attributes: resource
        .iter()
        .chain(span.attributes.iter().map(|kv| (&kv.key, &kv.value)))
        .map(|(key, value)| (key.as_str(), otel_value_to_json(value)))
        .collect(),
    }
  }
}

fn otel_value_to_json(otel: &Value) -> serde_json::Value {
  match otel {
    Value::Bool(v) => serde_json::Value::Bool(*v),
    Value::I64(v) => serde_json::Value::Number(serde_json::Number::from(*v)),
    Value::F64(v) => serde_json::Number::from_f64(*v)
      .map(serde_json::Value::Number)
      .unwrap_or(serde_json::Value::Null),
    Value::String(v) => serde_json::Value::String(v.to_string()),
    Value::Array(Array::Bool(v)) => serde_json::Value::Array(v.iter().map(|v| serde_json::Value::Bool(*v)).collect()),
    Value::Array(Array::I64(v)) => serde_json::Value::Array(
      v.iter()
        .map(|v| serde_json::Value::Number(serde_json::Number::from(*v)))
        .collect(),
    ),
    Value::Array(Array::F64(v)) => serde_json::Value::Array(
      v.iter()
        .map(|v| {
          serde_json::Number::from_f64(*v)
            .map(serde_json::Value::Number)
            .unwrap_or(serde_json::Value::Null)
        })
        .collect(),
    ),
    Value::Array(Array::String(v)) => {
      serde_json::Value::Array(v.iter().map(|v| serde_json::Value::String(v.to_string())).collect())
    }
  }
}
