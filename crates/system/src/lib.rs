extern crate core;

mod dev;
mod opentelemetry;

use ::opentelemetry::trace::TracerProvider;
use eternaltwin_config::{ClockConfig, Config, MailerType, StoreConfig};
use std::collections::HashMap;

pub use crate::dev::DevApi;
pub use crate::opentelemetry::backend::{OpentelemetryBackend, SharedSpanProcessor};
use crate::opentelemetry::human::HumanOtelExporter;
use crate::opentelemetry::hyper_client::HyperClient;
use crate::opentelemetry::jsonl::JsonlOtelExporter;
use ::opentelemetry::KeyValue;
use eternaltwin_auth_store::mem::MemAuthStore;
use eternaltwin_auth_store::pg::PgAuthStore;
use eternaltwin_auth_store::trace::TraceAuthStore;
use eternaltwin_config::postgres::PostgresConfig;
use eternaltwin_core::api::OwnRef;
use eternaltwin_core::auth::AuthStore;
use eternaltwin_core::clock::{Clock, DynScheduler, ErasedScheduler, Scheduler, SystemClock, VirtualClock};
use eternaltwin_core::core::{Instant, SecretString};
use eternaltwin_core::dinoparc::DinoparcStore;
use eternaltwin_core::email::{EmailFormatter, Mailer};
use eternaltwin_core::forum::ForumStore;
use eternaltwin_core::hammerfest::HammerfestStore;
use eternaltwin_core::job::{DynJobRuntime, JobRuntime, JobStore, OpaqueTask, OpaqueValue};
use eternaltwin_core::link::store::LinkStore;
use eternaltwin_core::mailer::store::MailerStore;
use eternaltwin_core::oauth::OauthProviderStore;
use eternaltwin_core::opentelemetry::{ArcDynTracerProvider, DynTracerProvider};
use eternaltwin_core::password::PasswordService;
use eternaltwin_core::twinoid::TwinoidStore;
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::UserStore;
use eternaltwin_core::uuid::{Uuid4Generator, UuidGenerator};
use eternaltwin_dinoparc_store::mem::MemDinoparcStore;
use eternaltwin_dinoparc_store::pg::{NewPgDinoparcStoreError, PgDinoparcStore};
use eternaltwin_email_formatter::html::HtmlEmailFormatter;
use eternaltwin_email_formatter::json::JsonEmailFormatter;
use eternaltwin_forum_store::mem::MemForumStore;
use eternaltwin_forum_store::pg::PgForumStore;
use eternaltwin_hammerfest_store::mem::MemHammerfestStore;
use eternaltwin_hammerfest_store::pg::{NewPgHammerfestStoreError, PgHammerfestStore};
use eternaltwin_job_store::mem::MemJobStore;
use eternaltwin_job_store::pg::PgJobStore;
use eternaltwin_link_store::mem::MemLinkStore;
use eternaltwin_link_store::pg::PgLinkStore;
use eternaltwin_mailer::client::mock::MemMailer;
use eternaltwin_mailer::client::smtp::{InvalidHeaderNameError, SmtpMailer};
use eternaltwin_mailer::client::trace::TraceMailer;
use eternaltwin_mailer::store::mem::MemMailerStore;
use eternaltwin_mailer::store::pg::PgMailerStore;
use eternaltwin_mailer::store::trace::TraceMailerStore;
use eternaltwin_oauth_client::RfcOauthClient;
use eternaltwin_oauth_provider_store::mem::MemOauthProviderStore;
use eternaltwin_oauth_provider_store::pg::PgOauthProviderStore;
use eternaltwin_oauth_provider_store::trace::TraceOauthProviderStore;
use eternaltwin_password::scrypt::{OsRng, ScryptPasswordService};
use eternaltwin_services::auth::{AuthService, DynAuthService};
use eternaltwin_services::dinoparc::{DinoparcService, DynDinoparcService};
use eternaltwin_services::forum::{DynForumService, ForumService};
use eternaltwin_services::hammerfest::{DynHammerfestService, HammerfestService};
use eternaltwin_services::job::{DynJobRuntimeExtra, DynJobService, JobRuntimeExtra, JobService};
use eternaltwin_services::mailer::send_email::SendEmailWorker;
use eternaltwin_services::mailer::{DynMailerService, MailerService};
use eternaltwin_services::oauth::{DynOauthService, OauthService};
use eternaltwin_services::twinoid::{DynTwinoidService, TwinoidService};
use eternaltwin_services::user::{DynUserService, UserService};
use eternaltwin_twinoid_store::mem::MemTwinoidStore;
use eternaltwin_twinoid_store::pg::PgTwinoidStore;
use eternaltwin_user_store::mem::MemUserStore;
use eternaltwin_user_store::pg::PgUserStore;
use eternaltwin_user_store::trace::TraceUserStore;
use opentelemetry_otlp::WithExportConfig;
use opentelemetry_sdk::resource::ResourceDetector;
use opentelemetry_sdk::resource::TelemetryResourceDetector;
use opentelemetry_sdk::Resource;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::types::JsonValue;
use sqlx::PgPool;
use std::future::Future;
use std::pin::Pin;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;
use tonic::metadata::{MetadataKey, MetadataMap};

/// On-demand and cached pg pool factory
struct LazyPgPool<'a, ConfigMeta> {
  config: &'a PostgresConfig<ConfigMeta>,
  pool: Option<PgPool>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetPgPoolError {
  #[error("missing database configuration")]
  MissingConfig,
  #[error("failed to connect to the database")]
  Connect(#[from] WeakError),
}

impl<'a, ConfigMeta> LazyPgPool<'a, ConfigMeta> {
  pub fn new(config: &'a PostgresConfig<ConfigMeta>) -> Self {
    Self { config, pool: None }
  }

  pub async fn get(&mut self) -> Result<PgPool, GetPgPoolError> {
    let pool = match &self.pool {
      Some(pool) => pool,
      None => {
        let pool: PgPool = PgPoolOptions::new()
          .max_connections(self.config.max_connections.value)
          .connect_with(
            PgConnectOptions::new()
              .host(&self.config.host.value)
              .port(self.config.port.value)
              .database(&self.config.name.value)
              .username(&self.config.user.value)
              .password(&self.config.password.value),
          )
          .await
          .map_err(WeakError::wrap)?;

        self.pool.insert(pool)
      }
    };
    Ok(pool.clone())
  }
}

#[derive(Clone)]
pub struct EternaltwinSystem {
  pub dev: DevApi,
  pub auth: Arc<DynAuthService>,
  pub clock: Arc<dyn Scheduler<Timer = Pin<Box<dyn Future<Output = ()> + Send>>>>,
  pub dinoparc: Arc<DynDinoparcService>,
  pub forum: Arc<DynForumService>,
  pub hammerfest: Arc<DynHammerfestService>,
  pub job: Arc<DynJobService>,
  pub job_runtime: Arc<DynJobRuntime<'static, DynJobRuntimeExtra>>,
  pub mailer: Arc<DynMailerService>,
  pub oauth: Arc<DynOauthService>,
  pub twinoid: Arc<DynTwinoidService>,
  pub user: Arc<DynUserService>,
  /// Opentelemetry backend (processors)
  ///
  /// Direct access to the backend enables proxying of Opentelemetry signals.
  pub opentelemetry_backend: OpentelemetryBackend,
  /// Opentelemetry tracer provider
  ///
  /// It is already passed to service, but it may be used for tracing at
  /// higher levels such as the REST interface.
  pub tracer_provider: ArcDynTracerProvider,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[allow(clippy::enum_variant_names)]
pub enum CreateSystemError {
  #[error("failed to acquire database pool")]
  PgPool(#[from] GetPgPoolError),
  #[error("missing backend.secret")]
  MissingBackendSecret,
  #[error("multiple Opentelemetry exporters try to acquire stdout")]
  StdoutContention,
  #[error("unknown Opentelemetry exporter type {0:?}")]
  UnknownExporterType(String),
  #[error("failed to build smtp client")]
  SmtpBuilder(#[from] InvalidHeaderNameError),
  #[error("failed to create PgDinoparcStore")]
  PgDinoparcStore(#[source] NewPgDinoparcStoreError),
  #[error("failed to create PgHammerfestStore")]
  PgHammerfestStore(#[source] NewPgHammerfestStoreError),
  #[error("failed to create PgTwinoidStore")]
  PgTwinoidStore(#[source] WeakError),
}

impl EternaltwinSystem {
  pub async fn create_dyn<ConfigMeta, TyStdout>(
    config: &Config<ConfigMeta>,
    stdout: TyStdout,
  ) -> Result<Self, CreateSystemError>
  where
    TyStdout: std::io::Write + Send + Sync + 'static,
  {
    // TODO: Seeded RNG (use clock + seed)
    let uuid_generator: Arc<dyn UuidGenerator> = Arc::new(Uuid4Generator);

    let mut opentelemetry_backend = OpentelemetryBackend::new();
    let tracer_provider: ArcDynTracerProvider = if config.opentelemetry.enabled.value {
      let mut provider_builder = opentelemetry_sdk::trace::TracerProvider::builder().with_config(
        opentelemetry_sdk::trace::Config::default()
          .with_id_generator(opentelemetry::IdGenerator::new(Arc::clone(&uuid_generator)))
          .with_resource({
            let process = Resource::new(
              config
                .opentelemetry
                .attributes
                .value
                .iter()
                .map(|(key, value)| KeyValue::new(key.to_string(), value.value.to_string())),
            );
            let telemetry: Resource = TelemetryResourceDetector.detect(Duration::from_secs(0));
            Resource::merge(&telemetry, &process)
          }),
      );

      let mut stdout = Some(stdout);
      for exporter_config in config.opentelemetry.exporter.value.values() {
        let processor: SharedSpanProcessor = match exporter_config.r#type.value.as_str() {
          "Grpc" => {
            let exporter = opentelemetry_otlp::new_exporter()
              .tonic()
              .with_endpoint(exporter_config.endpoint.value.as_str())
              .with_timeout(exporter_config.timeout.value.into_std())
              .with_metadata({
                let mut metadata = MetadataMap::with_capacity(exporter_config.metadata.value.len());
                for (key, value) in exporter_config.metadata.value.iter() {
                  metadata.insert(
                    MetadataKey::from_str(key).expect("invalid metadata key"),
                    value.value.parse().unwrap(),
                  );
                }
                metadata
              });
            opentelemetry_backend.batch_span_exporter(exporter.build_span_exporter().expect("grpc exporter is valid"))
          }
          "HttpJson" => {
            let exporter = opentelemetry_otlp::new_exporter()
              .http()
              .with_endpoint(exporter_config.endpoint.value.as_str())
              .with_timeout(exporter_config.timeout.value.into_std())
              .with_http_client(HyperClient::default())
              .with_headers({
                let mut metadata: HashMap<String, String> = HashMap::new();
                for (key, value) in exporter_config.metadata.value.iter() {
                  metadata.insert(key.clone(), value.value.clone());
                }
                metadata
              });
            opentelemetry_backend.batch_span_exporter(exporter.build_span_exporter().expect("http exporter is valid"))
          }
          "Human" => {
            let stdout = stdout.take().ok_or(CreateSystemError::StdoutContention)?;
            opentelemetry_backend.batch_span_exporter(HumanOtelExporter::new(stdout))
          }
          "Jsonl" => {
            let stdout = stdout.take().ok_or(CreateSystemError::StdoutContention)?;
            opentelemetry_backend.batch_span_exporter(JsonlOtelExporter::new(stdout))
          }
          ty => return Err(CreateSystemError::UnknownExporterType(ty.to_string())),
        };
        provider_builder = provider_builder.with_span_processor(processor);
      }
      let provider = provider_builder.build();
      ArcDynTracerProvider {
        provider: Arc::new(DynTracerProvider::Regular(provider)),
      }
    } else {
      ArcDynTracerProvider {
        provider: Arc::new(DynTracerProvider::Noop(
          ::opentelemetry::trace::noop::NoopTracerProvider::new(),
        )),
      }
    };

    let dev = DevApi::new();
    let (clock, scheduler): (Arc<dyn Clock>, Arc<DynScheduler>) = match config.backend.clock.value {
      ClockConfig::System => {
        let clock = Arc::new(ErasedScheduler::new(SystemClock));
        let scheduler = Arc::clone(&clock);
        (clock, scheduler)
      }
      ClockConfig::Virtual => {
        let clock = Arc::new(ErasedScheduler::new(VirtualClock::new(Instant::ymd_hms(
          2020, 1, 1, 0, 0, 0,
        ))));
        let scheduler = Arc::clone(&clock);
        (clock, scheduler)
      }
    };
    let mut db = LazyPgPool::new(&config.postgres);
    let secret_str = match config.backend.secret.value.as_ref() {
      Some(secret) => SecretString::new(secret.to_string()),
      None => return Err(CreateSystemError::MissingBackendSecret),
    };
    let secret_bytes = secret_str.as_str().as_bytes().to_vec();

    let rng = OsRng;

    let email_formatter: Arc<dyn EmailFormatter> = match config.backend.mailer.value {
      MailerType::Mock => Arc::new(JsonEmailFormatter),
      MailerType::Network => Arc::new(HtmlEmailFormatter),
    };
    let mailer: Arc<dyn Mailer> = match config.backend.mailer.value {
      MailerType::Mock => Arc::new(TraceMailer::new(
        Arc::clone(&clock),
        MemMailer::new(true),
        tracer_provider.tracer("eternaltwin_mailer::client::mock"),
      )),
      MailerType::Network => Arc::new({
        let mut mailer = SmtpMailer::builder(
          config.mailer.host.value.clone(),
          config.mailer.username.value.clone(),
          config.mailer.password.value.clone(),
          config.mailer.sender.value.clone(),
        );
        for header in config.mailer.headers.value.iter() {
          mailer.ascii_header(&header.name, header.value.clone())?;
        }
        TraceMailer::new(
          Arc::clone(&clock),
          mailer.build(),
          tracer_provider.tracer("eternaltwin_mailer::client::smtp"),
        )
      }),
    };
    let password: Arc<dyn PasswordService> = Arc::new(ScryptPasswordService::new(
      rng,
      config.scrypt.max_time.value,
      config.scrypt.max_mem_frac.value,
    ));

    // let auth_store: Arc<dyn AuthStore> = match config.backend.auth_store.value {
    //   StoreConfig::Postgres => Arc::new(PgAuthStore::new(
    //     Arc::clone(&clock),
    //     OwnRef(db.get().await?),
    //     Arc::clone(&uuid_generator),
    //     secret_str.clone(),
    //   )),
    //   StoreConfig::Memory => Arc::new(MemAuthStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator))),
    // };

    let auth_store: Arc<dyn AuthStore> = match config.backend.auth_store.value {
      StoreConfig::Postgres => Arc::new(TraceAuthStore::new(
        Arc::clone(&clock),
        tracer_provider.tracer("eternaltwin_auth_store::pg"),
        PgAuthStore::new(
          Arc::clone(&clock),
          OwnRef(db.get().await?),
          Arc::clone(&uuid_generator),
          secret_str.clone(),
        ),
      )),
      StoreConfig::Memory => Arc::new(TraceAuthStore::new(
        Arc::clone(&clock),
        tracer_provider.tracer("eternaltwin_auth_store::mem"),
        MemAuthStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator)),
      )),
    };

    let dinoparc_store: Arc<dyn DinoparcStore> = match config.backend.dinoparc_store.value {
      StoreConfig::Postgres => Arc::new(
        PgDinoparcStore::new(Arc::clone(&clock), OwnRef(db.get().await?), Arc::clone(&uuid_generator))
          .await
          .map_err(CreateSystemError::PgDinoparcStore)?,
      ),
      StoreConfig::Memory => Arc::new(MemDinoparcStore::new(Arc::clone(&clock))),
    };
    let forum_store: Arc<dyn ForumStore> = match config.backend.forum_store.value {
      StoreConfig::Postgres => Arc::new(PgForumStore::new(
        Arc::clone(&clock),
        OwnRef(db.get().await?),
        Arc::clone(&uuid_generator),
      )),
      StoreConfig::Memory => Arc::new(MemForumStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator))),
    };
    let hammerfest_store: Arc<dyn HammerfestStore> = match config.backend.hammerfest_store.value {
      StoreConfig::Postgres => Arc::new(
        PgHammerfestStore::new(
          Arc::clone(&clock),
          OwnRef(db.get().await?),
          secret_str.clone(),
          Arc::clone(&uuid_generator),
        )
        .await
        .map_err(CreateSystemError::PgHammerfestStore)?,
      ),
      StoreConfig::Memory => Arc::new(MemHammerfestStore::new(Arc::clone(&clock))),
    };
    let job_store: Arc<dyn JobStore<OpaqueTask, OpaqueValue>> = match config.backend.job_store.value {
      StoreConfig::Postgres => Arc::new(PgJobStore::new(
        OwnRef(db.get().await?),
        secret_str.clone(),
        Arc::clone(&uuid_generator),
      )),
      StoreConfig::Memory => Arc::new(MemJobStore::new(Arc::clone(&uuid_generator))),
    };
    let link_store: Arc<dyn LinkStore> = match config.backend.link_store.value {
      StoreConfig::Postgres => Arc::new(PgLinkStore::new(Arc::clone(&clock), OwnRef(db.get().await?))),
      StoreConfig::Memory => Arc::new(MemLinkStore::new(Arc::clone(&clock))),
    };
    let mailer_store: Arc<dyn MailerStore<JsonValue>> = match config.backend.mailer_store.value {
      StoreConfig::Postgres => Arc::new(TraceMailerStore::new(
        Arc::clone(&clock),
        PgMailerStore::new(OwnRef(db.get().await?), secret_str.clone(), Arc::clone(&uuid_generator)),
        tracer_provider.tracer("eternaltwin_mailer::store::pg"),
      )),
      StoreConfig::Memory => Arc::new(TraceMailerStore::new(
        Arc::clone(&clock),
        MemMailerStore::new(Arc::clone(&uuid_generator)).await,
        tracer_provider.tracer("eternaltwin_mailer::store::mem"),
      )),
    };
    let oauth_provider_store: Arc<dyn OauthProviderStore> = match config.backend.oauth_provider_store.value {
      StoreConfig::Postgres => Arc::new(TraceOauthProviderStore::new(
        Arc::clone(&clock),
        PgOauthProviderStore::new(
          Arc::clone(&clock),
          OwnRef(db.get().await?),
          Arc::clone(&password),
          Arc::clone(&uuid_generator),
          secret_str.clone(),
        ),
        tracer_provider.tracer("eternaltwin_oauth_provider_store::pg"),
      )),
      StoreConfig::Memory => Arc::new(TraceOauthProviderStore::new(
        Arc::clone(&clock),
        MemOauthProviderStore::new(Arc::clone(&clock), Arc::clone(&password), Arc::clone(&uuid_generator)),
        tracer_provider.tracer("eternaltwin_oauth_provider_store::mem"),
      )),
    };
    let user_store: Arc<dyn UserStore> = match config.backend.user_store.value {
      StoreConfig::Postgres => Arc::new(TraceUserStore::new(
        Arc::clone(&clock),
        tracer_provider.tracer("eternaltwin_user_store::pg"),
        PgUserStore::new(
          Arc::clone(&clock),
          OwnRef(db.get().await?),
          secret_str.clone(),
          tracer_provider.tracer("eternaltwin_user_store::pg"),
          Arc::clone(&uuid_generator),
        ),
      )),
      StoreConfig::Memory => Arc::new(TraceUserStore::new(
        Arc::clone(&clock),
        tracer_provider.tracer("eternaltwin_user_store::mem"),
        MemUserStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator)),
      )),
    };
    let twinoid_store: Arc<dyn TwinoidStore> = match config.backend.twinoid_store.value {
      StoreConfig::Postgres => Arc::new(
        PgTwinoidStore::new(
          Arc::clone(&clock),
          OwnRef(db.get().await?),
          secret_str.clone(),
          Arc::clone(&uuid_generator),
        )
        .await
        .map_err(CreateSystemError::PgTwinoidStore)?,
      ),
      StoreConfig::Memory => Arc::new(MemTwinoidStore::new(Arc::clone(&clock))),
    };

    let auth = Arc::new(AuthService::new(
      Arc::clone(&auth_store),
      Arc::clone(&clock),
      Arc::clone(&dinoparc_store),
      Arc::clone(&email_formatter),
      Arc::clone(&hammerfest_store),
      Arc::clone(&link_store),
      Arc::clone(&mailer),
      Arc::clone(&oauth_provider_store),
      Arc::clone(&password),
      Arc::clone(&user_store),
      tracer_provider.tracer("eternaltwin_services::auth"),
      Arc::clone(&twinoid_store),
      Arc::clone(&uuid_generator),
      secret_bytes.clone(),
      secret_bytes,
    ));

    let dinoparc = Arc::new(DinoparcService::new(
      Arc::clone(&dinoparc_store),
      Arc::clone(&link_store),
      Arc::clone(&user_store),
    ));

    let forum = Arc::new(ForumService::new(
      Arc::clone(&clock),
      Arc::clone(&forum_store),
      Arc::clone(&user_store),
    ));

    let hammerfest = Arc::new(HammerfestService::new(
      Arc::clone(&hammerfest_store),
      Arc::clone(&link_store),
      Arc::clone(&user_store),
    ));

    let oauth = Arc::new(OauthService::new(
      Arc::clone(&oauth_provider_store),
      Arc::clone(&user_store),
    ));

    let twinoid = Arc::new(TwinoidService::new(
      Arc::clone(&clock),
      Arc::clone(&link_store),
      Arc::clone(&twinoid_store),
      Arc::clone(&user_store),
    ));

    let user = Arc::new(UserService::new(
      Arc::clone(&auth_store),
      Arc::clone(&clock),
      Arc::clone(&dinoparc_store),
      Arc::clone(&hammerfest_store),
      Arc::clone(&link_store),
      password,
      Arc::clone(&twinoid_store),
      Arc::clone(&user_store),
    ));

    let mut job_runtime = JobRuntime::new(
      Arc::clone(&scheduler),
      Arc::clone(&job_store),
      tracer_provider.tracer("eternaltwin_core::job"),
      JobRuntimeExtra {
        hammerfest_store: Arc::clone(&hammerfest_store),
        mailer_client: Arc::clone(&mailer),
        mailer_store: Arc::clone(&mailer_store),
        twinoid_store: Arc::clone(&twinoid_store),
      },
    );
    // job_runtime.register::<ArchiveTwinoidUsers>();
    // job_runtime.register::<ScrapeAllHammerfestProfiles>();
    // job_runtime.register::<ScrapeHammerfestThread>();
    // job_runtime.register::<ScrapeHammerfestThreadList>();
    job_runtime.register::<SendEmailWorker>();
    let job_runtime = Arc::new(job_runtime);

    let mailer = Arc::new(MailerService::new(
      Arc::clone(&scheduler),
      Arc::clone(&job_runtime),
      Arc::clone(&mailer_store),
      Arc::clone(&user_store),
    ));

    let job = Arc::new(JobService::new(
      Arc::clone(&scheduler),
      Arc::clone(&job_runtime),
      Arc::clone(&job_store),
      Arc::clone(&user_store),
    ));

    Ok(EternaltwinSystem {
      dev,
      auth,
      clock: scheduler,
      dinoparc,
      forum,
      hammerfest,
      job,
      job_runtime,
      mailer,
      oauth,
      twinoid,
      user,
      tracer_provider,
      opentelemetry_backend,
    })
  }
}

#[derive(Clone)]
pub struct ExtendedEternaltwinSystem {
  pub base: EternaltwinSystem,
  pub twinoid_oauth_client: Arc<dyn RfcOauthClient>,
}
