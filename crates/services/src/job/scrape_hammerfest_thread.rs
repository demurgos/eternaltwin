use crate::job::reliable_hammerfest_acquire_session::ReliableHammerfestAcquireSession;
use crate::job::{error_sleep, GLOBAL_HAMMEREFEST_RATE_LIMITER};
use async_trait::async_trait;
use eternaltwin_core::hammerfest::{
  HammerfestClient, HammerfestClientCreateSessionError, HammerfestClientGetThreadPageError, HammerfestClientRef,
  HammerfestCredentials, HammerfestForumThreadId, HammerfestForumThreadPageResponse, HammerfestSession,
  HammerfestStore, HammerfestStoreRef, TouchSession,
};
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use eternaltwin_core::rate_limiter::RateLimiterError;
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use std::num::NonZeroU16;
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ScrapeHammerfestThread {
  Start {
    credentials: HammerfestCredentials,
    thread_id: HammerfestForumThreadId,
    last_page: NonZeroU16,
  },
  Fetch {
    timeout: Option<Sleep>,
    credentials: HammerfestCredentials,
    session_task: ReliableHammerfestAcquireSession,
    thread_id: HammerfestForumThreadId,
    page1: NonZeroU16,
    /// Number of times the session was lost consecutively
    session_loss: u32,
    /// Number of times the fetch request failed
    fetch_failure: u32,
  },
  Write {
    timeout: Option<Sleep>,
    credentials: HammerfestCredentials,
    session_task: ReliableHammerfestAcquireSession,
    thread_id: HammerfestForumThreadId,
    response: HammerfestForumThreadPageResponse,
    write_failures: u32,
  },
  Ready(Result<(), ScrapeHammerfestThreadError>),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error, Serialize, Deserialize)]
pub enum ScrapeHammerfestThreadError {
  #[error("wrong credentials")]
  WrongCredentials,
  #[error("theme not found")]
  NotFound,
  #[error("theme access is forbidden")]
  Forbidden,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<HammerfestClientCreateSessionError> for ScrapeHammerfestThreadError {
  fn from(value: HammerfestClientCreateSessionError) -> Self {
    match value {
      HammerfestClientCreateSessionError::WrongCredentials => Self::WrongCredentials,
      HammerfestClientCreateSessionError::Other(e) => Self::Other(e),
    }
  }
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ScrapeHammerfestThread
where
  Cx: TaskCx,
  Cx::Extra: HammerfestClientRef + HammerfestStoreRef,
{
  const NAME: &'static str = "ScrapeHammerfestThread";
  const VERSION: u32 = 1;
  type Output = Result<(), ScrapeHammerfestThreadError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    // Scraping pages in descending order to avoid missing messages due to deletions
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self {
        Self::Start {
          credentials,
          thread_id,
          last_page,
        } => {
          *self = Self::Fetch {
            timeout: None,
            credentials: credentials.clone(),
            session_task: ReliableHammerfestAcquireSession::new(credentials.clone()),
            thread_id: *thread_id,
            page1: *last_page,
            session_loss: 0,
            fetch_failure: 0,
          }
        }
        Self::Fetch {
          timeout,
          credentials,
          session_task,
          thread_id,
          page1,
          session_loss,
          fetch_failure,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          match GLOBAL_HAMMEREFEST_RATE_LIMITER
            .lock()
            .expect("failed to acquire Hammerfest RateLimiter")
            .acquire(cx.now(), 1)
          {
            Ok(()) => {}
            Err(RateLimiterError::Wait(t)) => {
              cx.inc_starvation();
              *timeout = Some(cx.sleep_until(t));
              continue;
            }
            Err(RateLimiterError::OverLimit) => unreachable!("acquiring 1 token is never over the limit"),
          };
          let session: HammerfestSession = match session_task.poll(cx).await {
            TaskPoll::Pending => return TaskPoll::Pending,
            TaskPoll::Ready(Err(e)) => {
              *self = Self::Ready(Err(ScrapeHammerfestThreadError::from(e)));
              continue;
            }
            TaskPoll::Ready(Ok(session)) => session,
          };
          *session_loss = 0;
          match cx
            .extra()
            .hammerfest_client()
            .get_forum_thread_page(Some(&session), credentials.server, *thread_id, *page1)
            .await
          {
            Err(e) => {
              match e {
                HammerfestClientGetThreadPageError::NotFound => {
                  *self = Self::Ready(Err(ScrapeHammerfestThreadError::NotFound));
                }
                HammerfestClientGetThreadPageError::Forbidden => {
                  // May be caused by a session loss, check if the session is still active
                  match cx
                    .extra()
                    .hammerfest_client()
                    .test_session(credentials.server, &session.key)
                    .await
                  {
                    Ok(Some(sess)) if sess.key == session.key && sess.user.id == session.user.id => {
                      *self = Self::Ready(Err(ScrapeHammerfestThreadError::Forbidden));
                    }
                    Ok(_) => {
                      // No session or diferent session
                      eprintln!("[ScrapeHammerfestThread::session_loss_redirect]");
                      if let Err(e) = cx
                        .extra()
                        .hammerfest_store()
                        .touch_session(TouchSession {
                          now: cx.now(),
                          server: session.user.server,
                          key: session.key.clone(),
                          user: None,
                        })
                        .await
                      {
                        eprintln!("[ScrapeHammerfestThread::touch_session_error] {e:#}");
                      }
                      *session_task = ReliableHammerfestAcquireSession::new(credentials.clone());
                      *session_loss += 1;
                      *timeout = Some(cx.sleep(error_sleep(*session_loss)));
                      continue;
                    }
                    Err(e) => {
                      eprintln!("[ScrapeHammerfestThread::forbidden] {e:#}");
                      *fetch_failure += 1;
                      *timeout = Some(cx.sleep(error_sleep(*fetch_failure)));
                      continue;
                    }
                  }
                }
                HammerfestClientGetThreadPageError::Other(e) => {
                  eprintln!("[ScrapeHammerfestThread::fetch] {e:#}");
                  *fetch_failure += 1;
                  *timeout = Some(cx.sleep(error_sleep(*fetch_failure)));
                  continue;
                }
              }
            }
            Ok(response) => {
              if response.session.is_none() {
                *session_loss += 1;
                if let Err(e) = cx
                  .extra()
                  .hammerfest_store()
                  .touch_session(TouchSession {
                    now: cx.now(),
                    server: session.user.server,
                    key: session.key.clone(),
                    user: None,
                  })
                  .await
                {
                  eprintln!("[ScrapeHammerfestThread::touch_session] {e:#}");
                }
                *session_task = ReliableHammerfestAcquireSession::new(credentials.clone());
                *timeout = Some(cx.sleep(error_sleep(*session_loss)));
                continue;
              }
              *self = Self::Write {
                timeout: None,
                credentials: credentials.clone(),
                session_task: session_task.clone(),
                thread_id: *thread_id,
                response,
                write_failures: 0,
              }
            }
          }
        }
        Self::Write {
          timeout,
          credentials,
          session_task,
          thread_id,
          response,
          write_failures,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          match cx.extra().hammerfest_store().touch_thread_page(response).await {
            Ok(()) => {
              let next_page = match NonZeroU16::new(response.page.posts.page1.get().saturating_sub(1)) {
                Some(next_page) => next_page,
                None => {
                  *self = Self::Ready(Ok(()));
                  continue;
                }
              };

              *self = Self::Fetch {
                timeout: None,
                credentials: credentials.clone(),
                session_task: session_task.clone(),
                thread_id: *thread_id,
                page1: next_page,
                session_loss: 0,
                fetch_failure: 0,
              }
            }
            Err(e) => {
              eprintln!(
                "[ScrapeHammerfestThread::write] {thread_id}/{}  {e:#}",
                response.page.posts.page1
              );
              *write_failures += 1;
              *timeout = Some(cx.sleep(error_sleep(*write_failures)));
              continue;
            }
          }
        }
        Self::Ready(result) => {
          return TaskPoll::Ready(result.clone());
        }
      }
    }
  }
}
