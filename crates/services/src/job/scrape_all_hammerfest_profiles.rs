use crate::job::reliable_hammerfest_acquire_session::ReliableHammerfestAcquireSession;
use crate::job::{error_sleep, GLOBAL_HAMMEREFEST_RATE_LIMITER};
use async_trait::async_trait;
use eternaltwin_core::hammerfest::{
  HammerfestClient, HammerfestClientCreateSessionError, HammerfestClientRef, HammerfestCredentials,
  HammerfestGetProfileByIdOptions, HammerfestProfileResponse, HammerfestSession, HammerfestStore, HammerfestStoreRef,
  HammerfestUserId, HammerfestUserIdRef, TouchEvniUser, TouchSession,
};
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use eternaltwin_core::rate_limiter::RateLimiterError;
use serde::{Deserialize, Serialize};

const ERROR_THRESHOLD: u32 = 1000;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ScrapeAllHammerfestProfiles {
  Start {
    credentials: HammerfestCredentials,
  },
  Fetch {
    timeout: Option<Sleep>,
    credentials: HammerfestCredentials,
    session_task: ReliableHammerfestAcquireSession,
    last_success: u32,
    next_user: u32,
    /// Number of times the session was lost consecutively
    session_loss: u32,
    /// Number of times the fetch request failed
    fetch_failure: u32,
    /// Deleted users (EVNI)
    failures: Vec<u32>,
  },
  Write {
    timeout: Option<Sleep>,
    credentials: HammerfestCredentials,
    session_task: ReliableHammerfestAcquireSession,
    user: u32,
    profile: HammerfestProfileResponse,
    /// Deleted users (EVNI)
    failures: Vec<u32>,
    write_failures: u32,
  },
  Ready(Vec<u32>, Result<(), HammerfestClientCreateSessionError>),
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ScrapeAllHammerfestProfiles
where
  Cx: TaskCx,
  Cx::Extra: HammerfestClientRef + HammerfestStoreRef,
{
  const NAME: &'static str = "ScrapeAllHammerfestProfiles";
  const VERSION: u32 = 1;
  type Output = Result<(), HammerfestClientCreateSessionError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self {
        Self::Start { credentials } => {
          *self = Self::Fetch {
            timeout: None,
            credentials: credentials.clone(),
            session_task: ReliableHammerfestAcquireSession::new(credentials.clone()),
            last_success: 0,
            next_user: 1,
            session_loss: 0,
            fetch_failure: 0,
            failures: Vec::new(),
          }
        }
        Self::Fetch {
          timeout,
          credentials,
          session_task,
          last_success,
          next_user,
          session_loss,
          fetch_failure,
          failures,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          match GLOBAL_HAMMEREFEST_RATE_LIMITER
            .lock()
            .expect("failed to acquire Hammerfest RateLimiter")
            .acquire(cx.now(), 1)
          {
            Ok(()) => {}
            Err(RateLimiterError::Wait(t)) => {
              cx.inc_starvation();
              *timeout = Some(cx.sleep_until(t));
              continue;
            }
            Err(RateLimiterError::OverLimit) => unreachable!("acquiring 1 token is never over the limit"),
          };
          let session: HammerfestSession = match session_task.poll(cx).await {
            TaskPoll::Pending => return TaskPoll::Pending,
            TaskPoll::Ready(Err(e)) => {
              *self = Self::Ready(failures.clone(), Err(e));
              continue;
            }
            TaskPoll::Ready(Ok(session)) => session,
          };
          let user_id = match HammerfestUserId::new(*next_user) {
            Ok(user_id) => user_id,
            Err(_) => {
              *self = Self::Ready(failures.clone(), Ok(()));
              continue;
            }
          };
          match cx
            .extra()
            .hammerfest_client()
            .get_profile_by_id(
              Some(&session),
              &HammerfestGetProfileByIdOptions {
                server: credentials.server,
                user_id,
              },
            )
            .await
          {
            Err(e) => {
              eprintln!("[ScrapeAllHammerfestProfiles] {e:#}");
              *fetch_failure += 1;
              *timeout = Some(cx.sleep(error_sleep(*fetch_failure)));
              continue;
            }
            Ok(response) => {
              if response.profile.is_none() {
                // This is an EVNI error, it means that the user was deleted but is not an error
                failures.push(*next_user);
                *next_user += 1;
                if (*next_user - *last_success) > ERROR_THRESHOLD {
                  *self = Self::Ready(failures.clone(), Ok(()));
                };
                continue;
              };
              if response.session.is_none() {
                *session_loss += 1;
                if let Err(e) = cx
                  .extra()
                  .hammerfest_store()
                  .touch_session(TouchSession {
                    now: cx.now(),
                    server: session.user.server,
                    key: session.key.clone(),
                    user: None,
                  })
                  .await
                {
                  eprintln!("[ScrapeAllHammerfestProfiles] {e:#}");
                }
                *session_task = ReliableHammerfestAcquireSession::new(credentials.clone());
                *timeout = Some(cx.sleep(error_sleep(*session_loss)));
                continue;
              }
              *self = Self::Write {
                timeout: None,
                credentials: credentials.clone(),
                session_task: session_task.clone(),
                user: *next_user,
                profile: response,
                failures: failures.clone(),
                write_failures: 0,
              }
            }
          }
        }
        Self::Write {
          timeout,
          credentials,
          session_task,
          user,
          profile,
          failures,
          write_failures,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }
          if let Some(evni_user) = failures.last() {
            match cx
              .extra()
              .hammerfest_store()
              .touch_evni_user(TouchEvniUser {
                now: cx.now(),
                user: HammerfestUserIdRef {
                  server: credentials.server,
                  id: HammerfestUserId::new(*evni_user).expect("id is valid"),
                },
              })
              .await
            {
              Ok(()) => {
                failures.pop();
                continue;
              }
              Err(e) => {
                eprintln!("[ScrapeAllHammerfestProfiles] {e:#}");
                *write_failures += 1;
                *timeout = Some(cx.sleep(error_sleep(*write_failures)));
                continue;
              }
            }
          }
          match cx.extra().hammerfest_store().touch_profile(profile).await {
            Ok(()) => {
              *self = Self::Fetch {
                timeout: None,
                credentials: credentials.clone(),
                session_task: session_task.clone(),
                last_success: *user,
                next_user: *user + 1,
                session_loss: 0,
                fetch_failure: 0,
                failures: failures.clone(),
              }
            }
            Err(e) => {
              eprintln!("[ScrapeAllHammerfestProfiles] {e:#}");
              *write_failures += 1;
              *timeout = Some(cx.sleep(error_sleep(*write_failures)));
              continue;
            }
          }
        }
        Self::Ready(_failures, result) => {
          return TaskPoll::Ready(result.clone());
        }
      }
    }
  }
}
