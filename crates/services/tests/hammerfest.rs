use eternaltwin_core::api::SyncRef;
use eternaltwin_core::core::SecretString;
use eternaltwin_core::hammerfest::{HammerfestStore, HammerfestStoreRef, HammerfestUser};
use eternaltwin_core::link::store::{LinkStore, LinkStoreRef};
use eternaltwin_core::user::{UserStore, UserStoreRef};
use eternaltwin_core::uuid::Uuid4Generator;
use eternaltwin_core::{
  auth::AuthContext,
  clock::VirtualClock,
  hammerfest::{GetHammerfestUserOptions, HammerfestServer},
};
use eternaltwin_db_schema::force_create_latest;
use eternaltwin_hammerfest_store::{mem::MemHammerfestStore, pg::PgHammerfestStore};
use eternaltwin_link_store::{mem::MemLinkStore, pg::PgLinkStore};
use eternaltwin_user_store::{mem::MemUserStore, pg::PgUserStore};
use opentelemetry::trace::noop::NoopTracerProvider;
use opentelemetry::trace::TracerProvider;
use serial_test::serial;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;
use std::marker::PhantomData;
use std::sync::Arc;

use eternaltwin_core::auth::{AuthScope, GuestAuthContext};
use eternaltwin_core::core::Instant;
use eternaltwin_services::hammerfest::{DynHammerfestService, HammerfestService};

async fn make_test_api() -> TestApi<
  Arc<VirtualClock>,
  Arc<dyn HammerfestStore>,
  Arc<DynHammerfestService>,
  Arc<dyn LinkStore>,
  Arc<dyn UserStore>,
> {
  let config = eternaltwin_config::Config::for_test();
  let tracer_provider = NoopTracerProvider::new();
  let tracer = tracer_provider.tracer("eternaltwin_services_test");

  let admin_database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.postgres.host.value)
        .port(config.postgres.port.value)
        .database(&config.postgres.name.value)
        .username(&config.postgres.admin_user.value)
        .password(&config.postgres.admin_password.value),
    )
    .await
    .unwrap();
  force_create_latest(&admin_database, true).await.unwrap();
  admin_database.close().await;

  let database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.postgres.host.value)
        .port(config.postgres.port.value)
        .database(&config.postgres.name.value)
        .username(&config.postgres.user.value)
        .password(&config.postgres.password.value),
    )
    .await
    .unwrap();
  let database = Arc::new(database);

  let uuid = Arc::new(Uuid4Generator);
  let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
  let uuid_generator = Arc::new(Uuid4Generator);
  let database_secret = SecretString::new("dev_secret".to_string());
  let hammerfest_store: Arc<dyn HammerfestStore> = Arc::new(
    PgHammerfestStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      database_secret.clone(),
      uuid_generator,
    )
    .await
    .unwrap(),
  );
  let link_store: Arc<dyn LinkStore> = Arc::new(PgLinkStore::new(Arc::clone(&clock), Arc::clone(&database)));
  let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(
    Arc::clone(&clock),
    Arc::clone(&database),
    SecretString::new("dev_secret".to_string()),
    tracer,
    Arc::clone(&uuid),
  ));
  let hammerfest = Arc::new(HammerfestService::new(
    Arc::clone(&hammerfest_store),
    Arc::clone(&link_store),
    Arc::clone(&user_store),
  ));

  TestApi {
    _clock: clock,
    _hammerfest_store: hammerfest_store,
    hammerfest,
    _link_store: PhantomData,
    _user_store: PhantomData,
  }
}

struct TestApi<TyClock, TyHammerfestStore, TyHammerfest, TyLinkStore, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef + Clone + 'static,
  TyLinkStore: LinkStoreRef,
  TyUserStore: UserStoreRef,
  TyHammerfest: SyncRef<HammerfestService<TyHammerfestStore, TyLinkStore, TyUserStore>>,
{
  pub(crate) _clock: TyClock,
  pub(crate) _hammerfest_store: TyHammerfestStore,
  pub(crate) hammerfest: TyHammerfest,
  pub(crate) _link_store: PhantomData<TyLinkStore>,
  pub(crate) _user_store: PhantomData<TyUserStore>,
}

#[tokio::test]
#[serial]
async fn test_empty() {
  inner_test_empty(make_test_api().await).await;
}

async fn inner_test_empty<TyClock, TyHammerfestStore, TyHammerfest, TyLinkStore, TyUserStore>(
  api: TestApi<TyClock, TyHammerfestStore, TyHammerfest, TyLinkStore, TyUserStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef + Clone + 'static,
  TyLinkStore: LinkStoreRef,
  TyUserStore: UserStoreRef,
  TyHammerfest: SyncRef<HammerfestService<TyHammerfestStore, TyLinkStore, TyUserStore>>,
{
  let options = &GetHammerfestUserOptions {
    server: HammerfestServer::HammerfestFr,
    id: "999999".parse().unwrap(),
    time: None,
  };
  let actual = api
    .hammerfest
    .get_user(
      &AuthContext::Guest(GuestAuthContext {
        scope: AuthScope::Default,
      }),
      options,
    )
    .await
    .unwrap();
  let expected: Option<HammerfestUser> = None;
  assert_eq!(actual, expected);
}

#[tokio::test]
#[serial]
async fn test_reference_types() {
  let uuid = Uuid4Generator;
  let clock = VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0));
  let hammerfest_store = MemHammerfestStore::new(&clock);
  let link_store = MemLinkStore::new(&clock);
  let user_store = MemUserStore::new(&clock, &uuid);
  let hammerfest = HammerfestService::new(&hammerfest_store, &link_store, &user_store);

  let options = &GetHammerfestUserOptions {
    server: HammerfestServer::HammerfestFr,
    id: "999999".parse().unwrap(),
    time: None,
  };
  assert_eq!(
    hammerfest
      .get_user(
        &AuthContext::Guest(GuestAuthContext {
          scope: AuthScope::Default,
        }),
        options
      )
      .await
      .unwrap(),
    None
  );
}
