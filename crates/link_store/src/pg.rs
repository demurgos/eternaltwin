use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Handler, Instant, RawUserDot};
use eternaltwin_core::dinoparc::{DinoparcServer, DinoparcUserId, DinoparcUserIdRef};
use eternaltwin_core::hammerfest::{HammerfestServer, HammerfestUserId, HammerfestUserIdRef};
use eternaltwin_core::link::{
  DeleteLinkError, DeleteLinkOptions, GetLinkOptions, GetLinksFromEtwinOptions, OldRawLink, RawDeleteAllLinks, RawLink,
  TouchLinkError, TouchLinkOptions, VersionedRawLink, VersionedRawLinks,
};
use eternaltwin_core::pg_num::PgU32;
use eternaltwin_core::twinoid::{TwinoidUserId, TwinoidUserIdRef};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{UserId, UserIdRef};
use sqlx::PgPool;

pub struct PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  clock: TyClock,
  database: TyDatabase,
}

impl<TyClock, TyDatabase> PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  pub fn new(clock: TyClock, database: TyDatabase) -> Self {
    Self { clock, database }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<TouchLinkOptions<DinoparcUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    cmd: TouchLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, TouchLinkError<DinoparcUserIdRef>> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      linked_by: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        INSERT INTO dinoparc_user_links(user_id, dinoparc_server, dinoparc_user_id, period, linked_by, unlinked_by)
        VALUES ($1::USER_ID, $2::DINOPARC_SERVER, $3::DINOPARC_USER_ID, PERIOD($4::INSTANT, NULL), $5::USER_ID, NULL)
        RETURNING lower(period) AS linked_at, linked_by;
    ",
    )
    .bind(cmd.etwin.id)
    .bind(cmd.remote.server)
    .bind(cmd.remote.id)
    .bind(now)
    .bind(cmd.linked_by.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(TouchLinkError::other)?;

    match row {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(row) => {
        let link: VersionedRawLink<DinoparcUserIdRef> = VersionedRawLink {
          current: Some(RawLink {
            link: RawUserDot {
              time: row.linked_at,
              user: UserIdRef { id: row.linked_by },
            },
            unlink: (),
            etwin: cmd.etwin,
            remote: cmd.remote,
          }),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<TouchLinkOptions<HammerfestUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    cmd: TouchLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, TouchLinkError<HammerfestUserIdRef>> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      linked_by: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        INSERT INTO hammerfest_user_links(user_id, hammerfest_server, hammerfest_user_id, period, linked_by, unlinked_by)
        VALUES ($1::USER_ID, $2::HAMMERFEST_SERVER, $3::HAMMERFEST_USER_ID, PERIOD($4::INSTANT, NULL), $5::USER_ID, NULL)
        RETURNING lower(period) AS linked_at, linked_by;
    ",
    )
      .bind(cmd.etwin.id)
      .bind(cmd.remote.server)
      .bind(cmd.remote.id)
      .bind(now)
      .bind(cmd.linked_by.id)
      .fetch_optional(&*self.database)
      .await
      .map_err(TouchLinkError::other)?;

    match row {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(row) => {
        let link: VersionedRawLink<HammerfestUserIdRef> = VersionedRawLink {
          current: Some(RawLink {
            link: RawUserDot {
              time: row.linked_at,
              user: UserIdRef { id: row.linked_by },
            },
            unlink: (),
            etwin: cmd.etwin,
            remote: cmd.remote,
          }),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<TouchLinkOptions<TwinoidUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    cmd: TouchLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, TouchLinkError<TwinoidUserIdRef>> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      linked_by: UserId,
    }

    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        INSERT INTO twinoid_user_links(user_id, twinoid_user_id, period, linked_by, unlinked_by)
        VALUES ($1::USER_ID, $2::u32, PERIOD($3::INSTANT, NULL), $4::USER_ID, NULL)
        RETURNING lower(period) AS linked_at, linked_by;
    ",
    )
    .bind(cmd.etwin.id)
    .bind(PgU32::from(cmd.remote.id.get()))
    .bind(now)
    .bind(cmd.linked_by.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(TouchLinkError::other)?;

    match row {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(row) => {
        let link: VersionedRawLink<TwinoidUserIdRef> = VersionedRawLink {
          current: Some(RawLink {
            link: RawUserDot {
              time: row.linked_at,
              user: UserIdRef { id: row.linked_by },
            },
            unlink: (),
            etwin: cmd.etwin,
            remote: cmd.remote,
          }),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<DeleteLinkOptions<DinoparcUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    cmd: DeleteLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, DeleteLinkError<DinoparcUserIdRef>> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      unlinked_at: Instant,
      linked_by: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        UPDATE dinoparc_user_links
        SET period = PERIOD(lower(period), $1::INSTANT), unlinked_by = $2::USER_ID
        WHERE user_id = $3::USER_ID AND dinoparc_server = $4::DINOPARC_SERVER AND dinoparc_user_id = $5::DINOPARC_USER_ID AND upper_inf(period)
        RETURNING lower(period) AS linked_at, upper(period) AS unlinked_at, linked_by;
    ",
    )
      .bind(now)
      .bind(cmd.unlinked_by.id)
      .bind(cmd.etwin.id)
      .bind(cmd.remote.server)
      .bind(cmd.remote.id)
      .fetch_optional(&*self.database)
      .await
      .map_err(DeleteLinkError::other)?;

    let row = row.ok_or(DeleteLinkError::NotFound(cmd.etwin, cmd.remote))?;

    let link: VersionedRawLink<DinoparcUserIdRef> = VersionedRawLink {
      current: None,
      old: vec![OldRawLink {
        link: RawUserDot {
          time: row.linked_at,
          user: UserIdRef { id: row.linked_by },
        },
        unlink: RawUserDot {
          time: row.unlinked_at,
          user: cmd.unlinked_by,
        },
        etwin: cmd.etwin,
        remote: cmd.remote,
      }],
    };
    Ok(link)
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<DeleteLinkOptions<HammerfestUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    cmd: DeleteLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, DeleteLinkError<HammerfestUserIdRef>> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      unlinked_at: Instant,
      linked_by: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        UPDATE hammerfest_user_links
        SET period = PERIOD(lower(period), $1::INSTANT), unlinked_by = $2::USER_ID
        WHERE user_id = $3::USER_ID AND hammerfest_server = $4::HAMMERFEST_SERVER AND hammerfest_user_id = $5::HAMMERFEST_USER_ID AND upper_inf(period)
        RETURNING lower(period) AS linked_at, upper(period) AS unlinked_at, linked_by;
    ",
    )
      .bind(now)
      .bind(cmd.unlinked_by.id)
      .bind(cmd.etwin.id)
      .bind(cmd.remote.server)
      .bind(cmd.remote.id)
      .fetch_optional(&*self.database)
      .await
      .map_err(DeleteLinkError::other)?;

    let row = row.ok_or(DeleteLinkError::NotFound(cmd.etwin, cmd.remote))?;

    let link: VersionedRawLink<HammerfestUserIdRef> = VersionedRawLink {
      current: None,
      old: vec![OldRawLink {
        link: RawUserDot {
          time: row.linked_at,
          user: UserIdRef { id: row.linked_by },
        },
        unlink: RawUserDot {
          time: row.unlinked_at,
          user: cmd.unlinked_by,
        },
        etwin: cmd.etwin,
        remote: cmd.remote,
      }],
    };
    Ok(link)
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<DeleteLinkOptions<TwinoidUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    cmd: DeleteLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, DeleteLinkError<TwinoidUserIdRef>> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      unlinked_at: Instant,
      linked_by: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        UPDATE twinoid_user_links
        SET period = PERIOD(lower(period), $1::INSTANT), unlinked_by = $2::USER_ID
        WHERE user_id = $3::USER_ID AND twinoid_user_id = $4::u32 AND upper_inf(period)
        RETURNING lower(period) AS linked_at, upper(period) AS unlinked_at, linked_by;
    ",
    )
    .bind(now)
    .bind(cmd.unlinked_by.id)
    .bind(cmd.etwin.id)
    .bind(PgU32::from(cmd.remote.id.get()))
    .fetch_optional(&*self.database)
    .await
    .map_err(DeleteLinkError::other)?;

    let row = row.ok_or(DeleteLinkError::NotFound(cmd.etwin, cmd.remote))?;

    let link: VersionedRawLink<TwinoidUserIdRef> = VersionedRawLink {
      current: None,
      old: vec![OldRawLink {
        link: RawUserDot {
          time: row.linked_at,
          user: UserIdRef { id: row.linked_by },
        },
        unlink: RawUserDot {
          time: row.unlinked_at,
          user: cmd.unlinked_by,
        },
        etwin: cmd.etwin,
        remote: cmd.remote,
      }],
    };
    Ok(link)
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<GetLinkOptions<DinoparcUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    query: GetLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, WeakError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      linked_by: UserId,
      user_id: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        SELECT lower(period) AS linked_at, linked_by, user_id
        FROM dinoparc_user_links
        WHERE dinoparc_server = $1::DINOPARC_SERVER
          AND dinoparc_user_id = $2::DINOPARC_USER_ID
          AND upper_inf(period);
    ",
    )
    .bind(query.remote.server)
    .bind(query.remote.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    match row {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(row) => {
        let link: VersionedRawLink<DinoparcUserIdRef> = VersionedRawLink {
          current: Some(RawLink {
            link: RawUserDot {
              time: row.linked_at,
              user: UserIdRef { id: row.linked_by },
            },
            unlink: (),
            etwin: UserIdRef { id: row.user_id },
            remote: query.remote,
          }),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<GetLinkOptions<HammerfestUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    query: GetLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, WeakError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      linked_by: UserId,
      user_id: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        SELECT lower(period) AS linked_at, linked_by, user_id
        FROM hammerfest_user_links
        WHERE hammerfest_server = $1::HAMMERFEST_SERVER
          AND hammerfest_user_id = $2::HAMMERFEST_USER_ID
          AND upper_inf(period);
    ",
    )
    .bind(query.remote.server)
    .bind(query.remote.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    match row {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(row) => {
        let link: VersionedRawLink<HammerfestUserIdRef> = VersionedRawLink {
          current: Some(RawLink {
            link: RawUserDot {
              time: row.linked_at,
              user: UserIdRef { id: row.linked_by },
            },
            unlink: (),
            etwin: UserIdRef { id: row.user_id },
            remote: query.remote,
          }),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<GetLinkOptions<TwinoidUserIdRef>> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(
    &self,
    query: GetLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, WeakError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      linked_at: Instant,
      linked_by: UserId,
      user_id: UserId,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        SELECT lower(period) AS linked_at, linked_by, user_id
        FROM twinoid_user_links
        WHERE twinoid_user_id = $1::u32 AND upper_inf(period);
    ",
    )
    .bind(PgU32::from(query.remote.id.get()))
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    match row {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(row) => {
        let link: VersionedRawLink<TwinoidUserIdRef> = VersionedRawLink {
          current: Some(RawLink {
            link: RawUserDot {
              time: row.linked_at,
              user: UserIdRef { id: row.linked_by },
            },
            unlink: (),
            etwin: UserIdRef { id: row.user_id },
            remote: query.remote,
          }),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<GetLinksFromEtwinOptions> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(&self, query: GetLinksFromEtwinOptions) -> Result<VersionedRawLinks, WeakError> {
    let mut links = VersionedRawLinks::default();

    {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        dinoparc_server: DinoparcServer,
        dinoparc_user_id: DinoparcUserId,
        linked_at: Instant,
        linked_by: UserId,
      }

      let rows = sqlx::query_as::<_, Row>(
        r"
          SELECT dinoparc_server, dinoparc_user_id, lower(period) AS linked_at, linked_by
          FROM dinoparc_user_links
          WHERE dinoparc_user_links.user_id = $1::UUID AND upper_inf(period);
    ",
      )
      .bind(query.etwin.id)
      .fetch_all(&*self.database)
      .await
      .map_err(WeakError::wrap)?;

      for row in rows.into_iter() {
        let link: RawLink<DinoparcUserIdRef> = RawLink {
          link: RawUserDot {
            time: row.linked_at,
            user: UserIdRef { id: row.linked_by },
          },
          unlink: (),
          etwin: query.etwin,
          remote: DinoparcUserIdRef {
            server: row.dinoparc_server,
            id: row.dinoparc_user_id,
          },
        };
        match link.remote.server {
          DinoparcServer::DinoparcCom => links.dinoparc_com.current = Some(link),
          DinoparcServer::EnDinoparcCom => links.en_dinoparc_com.current = Some(link),
          DinoparcServer::SpDinoparcCom => links.sp_dinoparc_com.current = Some(link),
        }
      }
    }
    {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        hammerfest_server: HammerfestServer,
        hammerfest_user_id: HammerfestUserId,
        linked_at: Instant,
        linked_by: UserId,
      }

      let rows = sqlx::query_as::<_, Row>(
        r"
          SELECT hammerfest_server, hammerfest_user_id, lower(period) AS linked_at, linked_by
          FROM hammerfest_user_links
          WHERE hammerfest_user_links.user_id = $1::UUID AND upper_inf(period);
    ",
      )
      .bind(query.etwin.id)
      .fetch_all(&*self.database)
      .await
      .map_err(WeakError::wrap)?;

      for row in rows.into_iter() {
        let link: RawLink<HammerfestUserIdRef> = RawLink {
          link: RawUserDot {
            time: row.linked_at,
            user: UserIdRef { id: row.linked_by },
          },
          unlink: (),
          etwin: query.etwin,
          remote: HammerfestUserIdRef {
            server: row.hammerfest_server,
            id: row.hammerfest_user_id,
          },
        };
        match link.remote.server {
          HammerfestServer::HammerfestEs => links.hammerfest_es.current = Some(link),
          HammerfestServer::HammerfestFr => links.hammerfest_fr.current = Some(link),
          HammerfestServer::HfestNet => links.hfest_net.current = Some(link),
        }
      }
    }
    {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        twinoid_user_id: PgU32,
        linked_at: Instant,
        linked_by: UserId,
      }

      let row = sqlx::query_as::<_, Row>(
        r"
          SELECT twinoid_user_id, lower(period) AS linked_at, linked_by
          FROM twinoid_user_links
          WHERE twinoid_user_links.user_id = $1::UUID AND upper_inf(period);
    ",
      )
      .bind(query.etwin.id)
      .fetch_optional(&*self.database)
      .await
      .map_err(WeakError::wrap)?;

      if let Some(row) = row {
        let link: RawLink<TwinoidUserIdRef> = RawLink {
          link: RawUserDot {
            time: row.linked_at,
            user: UserIdRef { id: row.linked_by },
          },
          unlink: (),
          etwin: query.etwin,
          remote: TwinoidUserIdRef {
            id: TwinoidUserId::new(u32::from(row.twinoid_user_id)).map_err(WeakError::wrap)?,
          },
        };
        links.twinoid.current = Some(link);
      }
    }

    Ok(links)
  }
}

#[async_trait]
impl<TyClock, TyDatabase> Handler<RawDeleteAllLinks> for PgLinkStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn handle(&self, cmd: RawDeleteAllLinks) -> Result<(), WeakError> {
    let now = cmd.now;

    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    // language=PostgreSQL
    sqlx::query(
      r"
        UPDATE twinoid_user_links
        SET period = PERIOD(lower(period), $1::INSTANT), unlinked_by = $2::USER_ID
        WHERE user_id = $3::USER_ID AND upper_inf(period)
        RETURNING lower(period) AS linked_at, upper(period) AS unlinked_at, linked_by;
    ",
    )
    .bind(now)
    .bind(cmd.unlinked_by.id)
    .bind(cmd.etwin.id)
    .execute(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    // language=PostgreSQL
    sqlx::query(
      r"
        UPDATE hammerfest_user_links
        SET period = PERIOD(lower(period), $1::INSTANT), unlinked_by = $2::USER_ID
        WHERE user_id = $3::USER_ID AND upper_inf(period)
        RETURNING lower(period) AS linked_at, upper(period) AS unlinked_at, linked_by;
    ",
    )
    .bind(now)
    .bind(cmd.unlinked_by.id)
    .bind(cmd.etwin.id)
    .execute(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    // language=PostgreSQL
    sqlx::query(
      r"
        UPDATE dinoparc_user_links
        SET period = PERIOD(lower(period), $1::INSTANT), unlinked_by = $2::USER_ID
        WHERE user_id = $3::USER_ID AND upper_inf(period)
        RETURNING lower(period) AS linked_at, upper(period) AS unlinked_at, linked_by;
    ",
    )
    .bind(now)
    .bind(cmd.unlinked_by.id)
    .bind(cmd.etwin.id)
    .execute(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    tx.commit().await.map_err(WeakError::wrap)?;

    Ok(())
  }
}

#[cfg(test)]
mod test {
  use crate::pg::PgLinkStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::dinoparc::DinoparcStore;
  use eternaltwin_core::hammerfest::HammerfestStore;
  use eternaltwin_core::link::store::LinkStore;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use eternaltwin_dinoparc_store::pg::PgDinoparcStore;
  use eternaltwin_hammerfest_store::pg::PgHammerfestStore;
  use eternaltwin_user_store::pg::PgUserStore;
  use opentelemetry::trace::noop::NoopTracerProvider;
  use opentelemetry::trace::TracerProvider;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<
    Arc<VirtualClock>,
    Arc<dyn DinoparcStore>,
    Arc<dyn HammerfestStore>,
    Arc<dyn LinkStore>,
    Arc<dyn UserStore>,
  > {
    let config = eternaltwin_config::Config::for_test();
    let tracer_provider = NoopTracerProvider::new();
    let tracer = tracer_provider.tracer("link_store_test");

    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let dinoparc_store: Arc<dyn DinoparcStore> = Arc::new(
      PgDinoparcStore::new(Arc::clone(&clock), Arc::clone(&database), Arc::clone(&uuid_generator))
        .await
        .unwrap(),
    );
    let database_secret = SecretString::new("dev_secret".to_string());
    let hammerfest_store: Arc<dyn HammerfestStore> = Arc::new(
      PgHammerfestStore::new(
        Arc::clone(&clock),
        Arc::clone(&database),
        database_secret,
        uuid_generator,
      )
      .await
      .unwrap(),
    );
    let link_store: Arc<dyn LinkStore> = Arc::new(PgLinkStore::new(Arc::clone(&clock), Arc::clone(&database)));
    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      SecretString::new("dev_secret".to_string()),
      tracer,
      Uuid4Generator,
    ));

    TestApi {
      clock,
      dinoparc_store,
      hammerfest_store,
      link_store,
      user_store,
    }
  }

  test_link_store!(
    #[serial]
    || make_test_api().await
  );
}
