# Eternaltwin website

## Requirements

- Node 18.12.0 or later
- Rust 1.75 or later
- Yarn 1.22

## Getting started

```
yarn install
yarn start
```

```
yarn postmark email raw --from="support@eternaltwin.org" --to="demurgos@demurgos.net" --subject="Test email" --html="This is a test email"
```

The commands above will install the dependencies, compile the website and start it.
By default, the website starts with an in-memory backend implementation that does not require a database.

See [DB documentation](./docs/db.md) to install and configure a Postgres database, then run `yarn run db:create` or import a database.

## Project tasks

This repository uses `yarn` to run project-related tasks such as building or testing.

The tasks are defined in the `scripts` field of `package.json`, you can run them with `yarn run <taskname>` (or `yarn <taskname>` if there is no ambiguity with existing yarn commands).

The `website` package has more advanced tasks described in its `README.md`, all the other packages have the same structure and tasks:

- `yarn build`: Compile the library
- `yarn test`: Compile the tests and run them
- `yarn lint`: Check for common errors and style issues.
- `yarn format`: Attempt to fix style issues automatically.
- `yarn set version latest'`: Update Yarn itself.
- `yarn up '*' '@!(eternaltwin)/*'`: Update all Typescript dependencies.
- `cargo upgrade --incompatible'`: Update all Rust dependencies (requires `cargo-edit`: `cargo install cargo-edit`).

## Code coverage

To get Rust's code coverage, run the following commands:

```
cargo install grcov
export CARGO_INCREMENTAL=0
export RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off"
cargo +nightly test
grcov ./target/debug/ -s . -t html --llvm --branch --ignore-not-existing -o ./target/debug/coverage/
```

The report will be in `./target/debug/coverage/`.

## Release

Run `cargo xtask release <version>` to set the version of all packages.
Commit and send to GitLab. Once merged into the `main` branch, you can do
the publication step.

Note: Internally, `cargo xtask` calls:
- `cargo set-version <version>`
- `yarn workspaces foreach --all version <version>`

## Publish

## Configuration

The website is configured using a local `eternaltwin.toml` file. Configuration changed
in version 0.16 and documentation needs to be written.

## Wiki

Partially Outdated: See [wiki](https://gitlab.com/eternaltwin/etwin/-/wikis/home) and `README.md` files in package directories for help.
